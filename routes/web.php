<?php


Route::get('/', 'FrontController@index');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/users/all', 'UserController@all')->name('user.all');
Route::get('/roles/all', 'RoleController@all')->name('role.all');
Route::get('/users/{user}/change-status', 'UserController@changeStatus');
Route::get('/currencies/all', 'CurrencyController@all')->name('currency.all');
Route::get('/languages/all', 'LanguageController@all')->name('language.all');
Route::get('/locale/{locale}', 'LanguageController@setLocale');
Route::get('/messenger/users', 'MessengerController@users');
Route::get('/notices/all', 'NoticeController@all')->name('notice.all');
Route::post('/profile/change-password', 'ProfileController@changePass')->name('profile.update-password');
Route::get('/marital-info/all', 'MaritalInfoController@all')->name('marital_info.all');
Route::get('/religions/all', 'ReligionController@all')->name('religion.all');
Route::apiResources([
    'users' => 'UserController',
    'roles' => 'RoleController',
    'user-role' => 'UserRoleController',
    'role-permission' => 'RolePermissionController',
    'user-permission' => 'UserPermissionController',
    'currencies' => 'CurrencyController',
    'languages' => 'LanguageController',
    'phrases' => 'PhraseController',
    'settings' => 'SettingsController',
    'messenger' => 'MessengerController',
    'calender' => 'CalenderController',
    'notices' => 'NoticeController',
    'activities' => 'ActivityController',
    'profile' => 'ProfileController',
]);
Route::resource('languages.phrases', 'LanguagePhraseController', ['only' => ['index','store']]);
Route::get('role-permission', 'RolePermissionController@index')->name('role-permission.index');
Route::get('user-permission', 'UserPermissionController@index')->name('user-permission.index');
