<header class="header-top" header-theme="light">
    <div class="container-fluid">
        <div class="d-flex justify-content-between">
            <div class="top-menu d-flex align-items-center">
                <button type="button" class="btn-icon mobile-nav-toggle d-lg-none"><span></span></button>
                <div class="header-search">
                    <div class="input-group">
                        <span class="input-group-addon search-close"><i class="ik ik-x"></i></span>
                        <input type="text" class="form-control">
                        <span class="input-group-addon search-btn"><i class="ik ik-search"></i></span>
                    </div>
                </div>
                <button type="button" id="navbar-fullscreen" class="nav-link"><i class="ik ik-maximize"></i>
                </button>
            </div>
            <div class="top-menu d-flex align-items-center">
                <div>
                    <select class="form-control select2" id="headerLanguage">
                    </select>
                </div>
{{--                <div class="dropdown">--}}
{{--                    <a class="nav-link dropdown-toggle" href="#" id="notiDropdown" role="button"--}}
{{--                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i--}}
{{--                                class="ik ik-bell"></i><span class="badge bg-danger">3</span></a>--}}
{{--                    <div class="dropdown-menu dropdown-menu-right notification-dropdown"--}}
{{--                         aria-labelledby="notiDropdown">--}}
{{--                        <h4 class="header">{{__('Notifications')}}</h4>--}}
{{--                        <div class="notifications-wrap">--}}
{{--                            <a href="#" class="media">--}}
{{--                                            <span class="d-flex">--}}
{{--                                                <img src="{{asset('resources')}}/img/users/1.jpg" class="rounded-circle"--}}
{{--                                                     alt="">--}}
{{--                                            </span>--}}
{{--                                <span class="media-body">--}}
{{--                                                <span class="heading-font-family media-heading">Steve Smith</span>--}}
{{--                                                <span class="media-content">I slowly updated projects</span>--}}
{{--                                            </span>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                        <div class="footer"><a href="javascript:void(0);">See all activity</a></div>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <button type="button" class="nav-link ml-10 right-sidebar-toggle"><i
                            class="ik ik-message-square"></i></button>
                <div class="dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="menuDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ik ik-plus"></i></a>
                    <div class="dropdown-menu dropdown-menu-right menu-grid" aria-labelledby="menuDropdown">
                        <a class="dropdown-item" href="{{route('home')}}" data-toggle="tooltip" data-placement="top"
                           title="Root"><i class="ik ik-home"></i></a>
                        @foreach($activeModules as $activeModule)
                            <a class="dropdown-item" href="{{url($activeModule->getLowerName())}}" data-toggle="tooltip"
                               data-placement="top" title="{{$activeModule->getName()}}"><i
                                        class="ik ik-users"></i></a>
                        @endforeach
                        <a class="dropdown-item" href="#" data-toggle="tooltip" data-placement="top" title="{{__('More')}}"><i
                                    class="ik ik-more-horizontal"></i></a>
                    </div>
                </div>
                <button type="button" class="nav-link ml-10" id="apps_modal_btn" data-toggle="modal"
                        data-target="#appsModal"><i class="ik ik-grid"></i></button>
                <div class="dropdown">
                    <a class="dropdown-toggle" href="javascript:void(0)" id="userDropdown" role="button" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        @if(Auth::user()->details && Auth::user()->details->photo)
                            <img src="{{asset('storage')}}/user/photo/{{Auth::user()->details->photo}}"
                                 alt="{{Auth::user()->details->photo}}" class="avatar rounded-circle" />
                        @else
                            <img src="{{asset('resources')}}/img/user.jpg" class="avatar rounded-circle" />
                        @endif
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="{{route('profile.index')}}"><i class="ik ik-user dropdown-icon"></i>
                            Profile</a>
                        <a class="dropdown-item" href="{{route('settings.index')}}"><i class="ik ik-settings dropdown-icon"></i> {{__('Settings')}}</a>
{{--                        <a class="dropdown-item" href="#"><span class="float-right"><span--}}
{{--                                        class="badge badge-primary">6</span></span><i--}}
{{--                                    class="ik ik-mail dropdown-icon"></i> Inbox</a>--}}
{{--                        <a class="dropdown-item" href="#"><i class="ik ik-navigation dropdown-icon"></i> Message</a>--}}

                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i
                                    class="ik ik-power dropdown-icon"></i> {{ __('Logout') }}</a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
