<!doctype html>
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') | {{$settings['title'] ?? config('app.name', 'Laravel')}}</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @if($settings['favicon'])
        <link rel="icon" href="{{url('storage',$settings['favicon'])}}" type="image/x-icon"/>
    @else
        <link rel="icon" href="{{asset('resources')}}/favicon.ico" type="image/x-icon"/>
    @endif

    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800" rel="stylesheet">
    <!-- Scripts -->
{{--    <script src="{{ asset('js/app.js') }}" defer></script>--}}

    <!-- Styles -->
{{--    <link href="{{ asset('css/app.css') }}" rel="stylesheet">--}}
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/ionicons/dist/css/ionicons.min.css">
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/icon-kit/dist/css/iconkit.min.css">
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/perfect-scrollbar/css/perfect-scrollbar.css">
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/animate/animate.css">
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/nprogress/nprogress.css">
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/datedropper/datedropper.min.css">
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/timedropper/timedropper.min.css">
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/jquery-toast-plugin/dist/jquery.toast.min.css">
    <link rel="stylesheet" href="{{asset('resources')}}/dist/css/theme.min.css">
    <script src="{{asset('resources')}}/src/js/vendor/modernizr-2.8.3.min.js"></script>
    <link rel="stylesheet" href="{{asset('resources')}}/custom/style.css">
    @yield('extraCSS')
</head>

<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<div class="wrapper" id="app">
    @yield('header')

    <div class="page-wrap">
        @yield('sidebar-left')
        <div class="main-content">
            @yield('content')
        </div>
        @yield('sidebar-right')
        @yield('footer')
    </div>
</div>


<div class="modal fade apps-modal" id="appsModal" tabindex="-1" role="dialog" aria-labelledby="appsModalLabel"
     aria-hidden="true" data-backdrop="false">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ik ik-x-circle"></i></button>
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="quick-search">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 ml-auto mr-auto">
                            <div class="input-wrap">
                                <input type="text" id="quick-search" class="form-control" placeholder="{{__('Search')}}..."/>
                                <i class="ik ik-search"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-body d-flex align-items-center">
                <div class="container">
                    <div class="apps-wrap">
                        <div class="app-item">
                            <a href="{{route('home')}}"><i class="ik ik-home ik-5x"></i><span>{{__('Home')}}</span></a>
                        </div>
                        @foreach($activeModules as $activeModule)
                            <div class="app-item">
                                <a href="{{url($activeModule->getLowerName())}}"><i
                                            class="ik ik-archive ik-5x"></i><span>{{$activeModule->getName()}}</span></a>
                            </div>
                        @endforeach
                        <div class="app-item">
                            <a href="{{url('/')}}"><i class="ik ik-more-horizontal"></i><span>{{__('More')}}</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="contains">
    {{-- <div id="layout-wrap"></div> --}}
    <div class="right-sidebar-toggle"></div>
    <div class="right-sidebar" style="height: 5px;display: none;"></div>
    <div class="messages" style="height: 5px;display: none;"></div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>window.jQuery || document.write('<script src="{{asset('resources')}}/src/js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
<script src="{{asset('resources')}}/plugins/popper.js/dist/umd/popper.min.js"></script>
<script src="{{asset('resources')}}/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="{{asset('resources')}}/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js"></script>
<script src="{{asset('resources')}}/plugins/moment/moment.js"></script>
<script src="{{asset('resources')}}/plugins/screenfull/dist/screenfull.js"></script>
<script src="{{asset('resources')}}/plugins/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="{{asset('resources')}}/plugins/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('resources')}}/plugins/select2/dist/js/select2.min.js"></script>
<script src="{{asset('resources')}}/plugins/nprogress/nprogress.js"></script>
{{--<script src="{{asset('resources')}}/js/datatables.js"></script>--}}
<script src="{{asset('resources')}}/plugins/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
<script src="{{asset('resources')}}/js/alerts.js"></script>
<script src="{{asset('resources')}}/plugins/datedropper/datedropper.min.js"></script>
<script src="{{asset('resources')}}/plugins/timedropper/timedropper.min.js"></script>
<script src="{{asset('resources')}}/dist/js/theme.min.js"></script>
{{--<script src="{{asset('resources')}}/js/form-picker.js"></script>--}}
@yield('extraJS')
<script src="{{asset('resources')}}/custom/script.js"></script>
<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    $(document).ready(function () {
        var myItems = new PerfectScrollbar(".sidebar-content", {
            wheelPropagation: true
        });
        loadLanguage();

        function loadLanguage() {
            $.ajax({
                url: '{{route('language.all')}}',
                type: 'GET',
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    $('#headerLanguage').empty();
                    $.each(response, function (i, language) {
                        if (language.code === `{{session('locale')}}`) {
                            $('#headerLanguage').append(`<option value="${language.id}" data-image="{{asset('resources/img/flag.png')}}" selected>${language.name}</option>`);
                        } else {
                            $('#headerLanguage').append(`<option value="${language.id}">${language.name}</option>`);
                        }
                    });
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            })
        }

        $(document).on('change', '#headerLanguage', function () {
            let code = $(this).val();
            $.ajax({
                url: '{{url('locale')}}/' + code,
                type: 'GET',
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    window.location.reload();
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            })
        });

        (function (b, o, i, l, e, r) {
            b.GoogleAnalyticsObject = l;
            b[l] || (b[l] =
                function () {
                    (b[l].q = b[l].q || []).push(arguments)
                });
            b[l].l = +new Date;
            e = o.createElement(i);
            r = o.getElementsByTagName(i)[0];
            e.src = 'https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e, r)
        }(window, document, 'script', 'ga'));
        ga('create', 'UA-XXXXX-X', 'auto');
        ga('send', 'pageview');
        $(document).ajaxStart(function () {
            NProgress.start();
        });
        $(document).ajaxComplete(function () {
            NProgress.done();
        });

    })
</script>
</body>
</html>
