@extends('master')
@section('title')
    {{__('Profile')}}
@endsection
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-file-text bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{__('Profile')}}</h5>
                            <span>{{__('User profile informations')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{url('/')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{__('Profile')}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-5">
                <div class="card">
                    <div class="card-body">
                        <div class="text-center">
                            @if(Auth::user()->details && Auth::user()->details->photo)
                                <img src="{{asset('storage')}}/user/photo/{{Auth::user()->details->photo}}"
                                     alt="{{Auth::user()->details->photo}}" class="rounded-circle" width="150"/>
                            @else
                                <img src="{{asset('resources')}}/img/user.jpg" class="rounded-circle" width="150"/>
                            @endif
                            <h4 class="card-title mt-10">{{auth()->user()->details->name ?? ''}}</h4>
                            <div class="m-auto">
                                <button class="btn btn-icon btn-facebook"><i class="fab fa-facebook-f"></i></button>
                                <button class="btn btn-icon btn-twitter"><i class="fab fa-twitter"></i></button>
                                <button class="btn btn-icon btn-instagram"><i class="fab fa-instagram"></i></button>
                            </div>
                        </div>
                    </div>
                    <hr class="mb-0">
                    <div class="card-body">
                        <small class="text-muted d-block">{{__('Email')}} </small>
                        <h6>{{(Auth::user()->email) ?? ''}} <small class="text-muted">({{__('Primary')}})</small></h6>
                        <h6>{{(Auth::user()->details->email) ?? ''}}</h6>
                        <small class="text-muted d-block pt-10">{{__('Phone')}}</small>
                        <h6>{{Auth::user()->details->phone ?? ''}}</h6>
                        <small class="text-muted d-block pt-10">{{__('Address')}}</small>
                        <h6>{{Auth::user()->details->address->street ?? ''}}
                            , {{Auth::user()->details->address->city ?? ''}}
                            , {{Auth::user()->details->address->district ?? ''}}
                            , {{Auth::user()->details->address->zip_code ?? ''}}
                            , {{Auth::user()->details->address->country ?? ''}}</h6>
                        <div class="map-box">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d456.54204407280827!2d90.3856132251634!3d23.735382185787646!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755b98b5de435c7%3A0xd386ad8245260a9d!2sPlanet+Hack+Bangladesh+Limited!5e0!3m2!1sen!2sbd!4v1554537349481!5m2!1sen!2sbd"
                                width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-7">
                <div class="card">
                    <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#last-month"
                               role="tab"
                               aria-controls="pills-profile" aria-selected="true">{{__('Profile')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month"
                               role="tab" aria-controls="pills-setting"
                               aria-selected="false">{{__('Update Profile')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-password-tab" data-toggle="pill" href="#changePassword"
                               role="tab" aria-controls="pills-password"
                               aria-selected="false">{{__('Change Password')}}</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="last-month" role="tabpanel"
                             aria-labelledby="pills-profile-tab">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 col-4"><strong>{{__('Name')}}</strong>
                                        <br>
                                        <p class="text-muted">{{Auth::user()->details->name ?? ''}}</p>
                                    </div>
                                    <div class="col-md-4 col-4"><strong>{{__('Mobile')}}</strong>
                                        <br>
                                        <p class="text-muted">{{Auth::user()->details->phone ?? ''}}</p>
                                    </div>
                                    <div class="col-md-4 col-4"><strong>{{__('Email')}}</strong>
                                        <br>
                                        <p class="text-muted">{{Auth::user()->details->email ?? ''}}</p>
                                    </div>
                                </div>
                                <div class="mt-30">
                                    <h6>{{__('Personal Info')}}</h6>
                                    <hr/>
                                    <ul class="offset-md-2 list-group-flush">
                                        <li class="list-group-item border-top-0">{{__('Gender')}}
                                            @if(Auth::user()->details)
                                                <span
                                                    class="float-right">{{(Auth::user()->details->gender) ? "Female":"Male"}}</span>
                                            @endif
                                        </li>
                                        <li class="list-group-item">{{__('Birthday')}} <span
                                                class="float-right">{{Auth::user()->details->birthday ?? ''}}</span>
                                        </li>
                                        <li class="list-group-item border-bottom-0">{{__('Marital Status')}}<span
                                                class="float-right">{{Auth::user()->details->maritalStatus->name ?? ''}}</span>
                                        </li>
                                        <li class="list-group-item border-bottom-0">{{__('Religion')}} <span
                                                class="float-right">{{Auth::user()->details->religion->name ?? ''}}</span>
                                        </li>
                                    </ul>
                                    <h6>{{__('Contact Info')}}</h6>
                                    <hr/>
                                    <ul class="offset-md-2 list-group-flush border-0">
                                        <li class="list-group-item border-top-0">{{__('Street/Holding No.')}} <span
                                                class="float-right">{{Auth::user()->details->address->street ?? ''}}</span>
                                        </li>
                                        <li class="list-group-item">{{__('City')}} <span
                                                class="float-right">{{Auth::user()->details->address->city ?? ''}}</span>
                                        </li>
                                        <li class="list-group-item">{{__('District')}} <span
                                                class="float-right">{{Auth::user()->details->address->district ?? ''}}</span>
                                        </li>
                                        <li class="list-group-item">{{__('Zip code')}} <span
                                                class="float-right">{{Auth::user()->details->address->zip_code ?? ''}}</span>
                                        </li>
                                        <li class="list-group-item border-bottom-0">{{__('Country')}} <span
                                                class="float-right">{{Auth::user()->details->address->country ?? ''}}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="previous-month" role="tabpanel"
                             aria-labelledby="pills-setting-tab">
                            <div class="card-body">
                                <form class="form-horizontal" action="{{route('profile.store')}}" method="POST"
                                      enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="id" value="{{Auth::id()}}"/>
                                    <div class="form-group text-center justify-content-md-center">
                                        @if(Auth::user()->details && Auth::user()->details->photo)
                                            <img src="{{asset('storage')}}/user/photo/{{Auth::user()->details->photo}}"
                                                 alt="{{Auth::user()->details->photo}}" class="rounded-circle"
                                                 width="150"/>
                                        @else
                                            <img src="{{asset('resources')}}/img/user.jpg" class="rounded-circle"
                                                 width="150"/>
                                        @endif
                                        <input type="file" id="photo" name="photo" placeholder="{{__('photo')}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">{{__('Name')}} <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control required" id="name" name="name"
                                               placeholder="{{__('Name')}}"
                                               value="{{Auth()->user()->details->name ?? ''}}"
                                               required>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="email">{{__('Email')}}</label>
                                            <input type="email" class="form-control" id="email"
                                                   placeholder="{{__('Email')}}"
                                                   name="email" value="{{(Auth::user()->details->email) ?? ''}}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="phone">{{__('Phone')}} <span
                                                    class="text-danger">*</span></label>
                                            <input type="text" class="form-control required" id="phone"
                                                   placeholder="{{__('Phone')}}" name="phone"
                                                   value="{{Auth::user()->details->phone ?? ''}}" required/>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="street">{{__('Street/Holding No')}} <span
                                                    class="text-danger">*</span></label>
                                            <input type="text" class="form-control required" id="street" name="street"
                                                   placeholder="1234 Main St"
                                                   value="{{Auth::user()->details->address->street ?? ''}}" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="city">{{__('City')}} <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control required" id="city" name="city"
                                                   placeholder="{{__('Dhaka')}}"
                                                   value="{{Auth::user()->details->address->city ?? ''}}" required>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-5">
                                            <label for="district">{{__('District')}} <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control required" id="district"
                                                   name="district"
                                                   placeholder="{{__('Dhaka')}}"
                                                   value="{{Auth::user()->details->address->district ?? ''}}" required/>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="zip">{{__('Zip')}} <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control required" id="zip" name="zip"
                                                   placeholder="{{__('1234')}}"
                                                   value="{{Auth::user()->details->address->zip_code ?? ''}}" required/>
                                        </div>
                                        <div class="form-group col-md-5">
                                            <label for="country">{{__('Country')}} <span
                                                    class="text-danger">*</span></label>
                                            <input type="text" class="form-control required" id="country" name="country"
                                                   placeholder="Bangladesh"
                                                   value="{{Auth::user()->details->address->country ?? ''}}" required/>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-2">
                                            <label for="gender">{{__('Gender')}}</label>
                                            <select id="gender" class="form-control select2" name="gender">
                                                <option selected value="" disabled>{{__('Select Gender')}}</option>
                                                @if(!Auth::user()->details)
                                                    <option value="0">{{__('Male')}}</option>
                                                    <option value="1">{{__('Female')}}</option>
                                                @else
                                                    <option
                                                        value="1" {{(Auth::user()->details->address->gender==0) ? 'selected':''}}>
                                                        Male
                                                    </option>
                                                    <option
                                                        value="1" {{(Auth::user()->details->address->gender==1) ? 'selected':''}}>
                                                        Female
                                                    </option>
                                                @endif
                                            </select>
                                        </div>
                                        <div class="form-group col-md-5">
                                            <label for="dob">{{__('Date of Birth')}}</label>
                                            <input type="text" class="form-control date" id="dob"
                                                   placeholder="dd-mm-YYYY"
                                                   name="dob" value="{{(Auth::user()->details->birthday) ?? ''}}">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="religion">{{__('Religion')}}</label>
                                            <select id="religion" class="form-control select2" name="religion">
                                                <option>Loading..</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="marital_status">{{__('Marital Status')}}</label>
                                            <select id="marital_status" class="form-control select2"
                                                    name="marital_status">
                                                <option>Loading..</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="text-center m-30">
                                        <button class="btn btn-success d-inline"
                                                type="submit">{{__('Update Profile')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="changePassword" role="tabpanel"
                             aria-labelledby="pills-setting-tab">
                            <div class="card-body">
                                <form class="form-horizontal updatePassForm" method="POST" action="javascript:void(0)">
                                    @csrf
                                    <div class="form-group">
                                        <label for="example-password">{{__('Current Password')}}</label>
                                        <input type="password" placeholder="{{__('current Password')}}"
                                               class="form-control"
                                               name="current_password" id="example-password" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-password">{{__('New Password')}}</label>
                                        <input type="password" placeholder="{{__('type new password here')}} "
                                               class="form-control"
                                               name="password" id="example-password" required
                                               autocomplete="new-password">
                                    </div>
                                    <div class="form-group">
                                        <label for="example-password">{{__('Confirm New Password')}}</label>
                                        <input type="password" class="form-control"
                                               name="password_confirmation" id="example-password" required
                                               autocomplete="new-password">
                                    </div>
                                    <div class="row justify-content-center m-30">
                                        <button class="btn btn-success block"
                                                type="submit">{{__('Update Password')}}</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extraCSS')
@endsection
@section('extraJS')
    <script>
        $(document).ready(function () {
            $('.select2').each(function () {
                $(this).select2();
            });
            $(document).on('change', 'input[type=file]', function (event) {
                var that = $(this);
                var reader = new FileReader();
                reader.onload = function () {
                    var img = that.closest('.form-group').find('img');
                    img.attr('src', reader.result);
                };
                reader.readAsDataURL(event.target.files[0]);
            });
            loadReligion();

            function loadReligion() {
                $.ajax({
                    url: '{{route('religion.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#religion').empty();
                        $('#religion').append('<option selected value="" disabled>{{__("Select a Religion")}}</option>');
                        $.each(response, function (i, religion) {
                            $('#religion').append($('<option>', {
                                value: religion.id,
                                text: religion.name
                            }));
                        });
                        $('#religion').val({{(Auth::user()->details->religion_id) ?? ''}});
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }

            loadMStatus();

            function loadMStatus() {
                $.ajax({
                    url: '{{route('marital_info.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#marital_status').empty();
                        $('#marital_status').append('<option selected value="" disabled>{{__("Select status")}}</option>');
                        $.each(response, function (i, mstatus) {
                            $('#marital_status').append($('<option>', {
                                value: mstatus.id,
                                text: mstatus.name
                            }));
                        });
                        $('#marital_status').val({{(Auth::user()->details->marital_id) ?? ''}});
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }

            $(document).on('submit', '.updatePassForm', function () {
                let data = $(this).serialize();
                $.ajax({
                    url: '{{route('profile.update-password')}}',
                    type: 'POST',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        showSuccessToast("{{__('Updated Successfully')}}");
                        $('.updatePassForm').reset();
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
        });
    </script>
@endsection
