@extends('layouts.auth')

@section('title')
{{__('Register')}}
@endsection

@section('content')
    <div class="container-fluid h-100">
        <div class="row flex-row h-100 bg-white">
            <div class="col-xl-8 col-lg-6 col-md-5 p-0 d-md-block d-lg-block d-sm-none d-none">
                <div class="lavalite-bg"
                     style="background-image: url({{$settings['register'] ? asset('storage/'.$settings['register']):asset('resources/register-bg.jpg')}})">
                    <div class="lavalite-overlay"></div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-7 my-auto p-0">
                <div class="authentication-form mx-auto">
                    <div class="text-center mb-4">
                        <a href="{{url('/')}}"><img src="{{$settings['logo'] ? asset('storage/'.$settings['logo']):asset('resources/logo.png')}}" alt=""></a>
                    </div>
                    <h3>{{__("New to ".config('app.name','Laravel'))}}</h3>
                    <p>{{__('Join us today! It takes only few steps')}}</p>
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                                   value="{{ old('name') }}" placeholder="{{__('Name')}}" required autocomplete="name" autofocus>
                            <i class="ik ik-user"></i>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ __("$message") }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                                   value="{{ old('email') }}" placeholder="{{__('Email')}}" required autocomplete="email">
                            <i class="ik ik-mail"></i>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ __("$message") }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                   name="password" placeholder="{{__('Password')}}" required autocomplete="new-password">
                            <i class="ik ik-lock"></i>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ __("$message") }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password_confirmation"
                                   placeholder="{{__('Confirm Password')}}" required autocomplete="new-password">
                            <i class="ik ik-eye-off"></i>
                        </div>
                        <div class="row">
                            <div class="col-12 text-left">
                                <label class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="item_checkbox"
                                           name="item_checkbox" value="option1">
                                    <span class="custom-control-label">&nbsp;{{__('I Accept Terms and Conditions')}}</a></span>
                                </label>
                            </div>
                        </div>
                        <div class="sign-btn text-center">
                            <button type="submit" class="btn btn-theme">{{__('Create Account')}}</button>
                        </div>
                    </form>
                    <div class="register">
                        <p>{{__('Already have an account')}}? <a href="{{route('login')}}">{{__('Sign In')}}</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
