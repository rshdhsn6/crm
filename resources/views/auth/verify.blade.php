@extends('layouts.auth')

@section('title')
{{__('Verify Email')}}
@endsection

@section('content')
    <div class="container-fluid h-100">
        <div class="row flex-row h-100 bg-white">
            <div class="col-xl-8 col-lg-6 col-md-5 p-0 d-md-block d-lg-block d-sm-none d-none">
                <div class="lavalite-bg" style="background-image: url({{$settings['login'] ? asset('storage/'.$settings['login']):asset('resources/login-bg.jpg')}})">
                    <div class="lavalite-overlay"></div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-6 col-md-7 my-auto p-0">
                <div class="authentication-form mx-auto">
                    <div class="text-center mb-4">
                        <a href="{{route('/')}}"><img src="{{$settings['logo'] ? asset('storage/'.$settings['logo']):asset('resources/logo.png')}}" alt=""></a>
                    </div>
                    <h3 class="text-center">{{__('Verify your email')}}</h3>
                    @if (session('resent'))
                        <p class="text-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </p>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a
                            href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                </div>
            </div>
        </div>
    </div>
@endsection
