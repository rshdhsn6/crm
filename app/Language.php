<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Language extends Model
{
    use LogsActivity;

    const name = "Language";
    protected $fillable = ['name', 'code', 'flag'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return $this::name." {$this->name} has been {$eventName}";
    }
    public function phrases()
    {
        return $this->belongsToMany('App\Phrase', 'language_phrases', 'language_id', 'phrase_id');
    }
}
