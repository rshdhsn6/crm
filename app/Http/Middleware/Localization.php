<?php

namespace App\Http\Middleware;

use App\Language;
use App\Settings;
use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Session;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has('locale')) {
            App::setlocale(Session::get('locale'));
        } else {
            if (Schema::hasTable('settings')) {
                $lang = Settings::where('option', 'language')->first()->value;
                $code = Language::find($lang)->code;
                App::setLocale($code);
                Session::put('locale', $code);
            } else {
                Session::put('locale', 'en');
            }
        }
        return $next($request);
    }
}
