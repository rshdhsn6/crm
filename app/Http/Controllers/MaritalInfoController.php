<?php

namespace App\Http\Controllers;

use App\MaritalInfo;
use Illuminate\Http\Request;

class MaritalInfoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display all of the resource.
     * @return Response
     */
    public function all(Request $request)
    {
        if ($request->ajax()) {
            return MaritalInfo::all();
        }
    }
}
