<?php

namespace App\Http\Controllers;

use App\Settings;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Language;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class LanguageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $languages = Language::all();
            return Datatables::of($languages)
                ->addIndexColumn()
                ->addColumn('action', function ($language) {
                    $btn = '<div class="table-actions">';
                    $btn .= '<a href="' . route('languages.phrases.index', $language->id) . '" class="settings-btn" type="button"><i class="ik ik-settings"></i></a>';
                    $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#languageModal" data-whatever="1" data-id="' . $language->id . '"><i class="ik ik-edit-2"></i></a>';
                    $btn .= '<a class="delete-btn" type="button" data-id="' . $language->id . '"><i class="ik ik-trash-2"></i></a>';
                    $btn .= '</div>';
                    if ($language->code != 'en') return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::language');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $rules = [
            'name' => 'required|unique:languages,name',
            'code' => 'required|unique:languages,code',
            'flag' => 'nullable|image|mimes:jpeg,bmp,png'
        ];
        $messages = [
            'code.required' => 'The code field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $language = new Language();
        $language->name = $request->name;
        $language->code = $request->code;
        if ($request->hasFile('flag')) {
            $value = $request->flag;
            $extension = $value->getClientOriginalExtension();
//            $name = $value->getClientOriginalName();
            $image = microtime(true) . ".$extension";
            Image::make($value)->resize(100, 100)->save(public_path('/resources/img/flag/') . $image);
            $language->flag = $image;
        }
//        Create lanaguage file
        File::put(App::langPath() . "/$request->code.json", '');
        $language->save();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request, Language $language)
    {
        if ($request->ajax()) {
            return $language;
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Language $language)
    {
//        dd($request->all(),$language);
        $rules = [
            'name' => 'required|unique:languages,name,'.$language->id,
            'code' => 'required|unique:languages,code,'.$language->id,
            'flag' => 'nullable|image|mimes:jpeg,bmp,png'
        ];
        $messages = [
            'code.required' => 'The code field is required',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $language->name = $request->name;
        if ($language->code != $request->code) {
//            Rename Language file
            File::move(App::langPath() . "/$language->code.json", App::langPath() . "/$request->code.json");
        }
        $language->code = $request->code;
        if ($request->hasFile('flag')) {
            $value = $request->flag;
            $extension = $value->getClientOriginalExtension();
//            $name = $value->getClientOriginalName();
            $image = microtime(true) . ".$extension";
            Image::make($value)->resize(100, 100)->save(public_path('/resources/img/flag/') . $image);
            File::delete(public_path('/resources/img/flag/') . $language->flag);
            $language->flag = $image;
        }
        $language->save();
        return $language;
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Language $language)
    {
        File::delete(App::langPath() . "/$language->code.json");
        File::delete(public_path('/resources/img/flag/') . $language->flag);
        $language->delete();
        return $language;
    }

    public function all()
    {
        return Language::all();
    }

    public function setLocale($locale)
    {
        $code = Language::find($locale)->code;
        Session::put('locale', $code);
        return $code;
    }
}
