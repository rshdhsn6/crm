<?php

namespace App\Http\Controllers;

use App\Phrase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class PhraseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $phrases = Phrase::all();
            return Datatables::of($phrases)
                ->addIndexColumn()
                ->addColumn('action', function ($phrase) {
                    $btn = '<div class="table-actions">';
                    $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#phraseModal" data-whatever="1" data-id="' . $phrase->id . '"><i class="ik ik-edit-2"></i></a>';
                    $btn .= '<a class="delete-btn" type="button" data-id="' . $phrase->id . '"><i class="ik ik-trash-2"></i></a>';
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('phrase');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:phrases,name',
        ];
        $messages = [
            'name.required' => 'The name field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        return Phrase::create(['name' => $request->name]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,Phrase $phrase)
    {
        if($request->ajax()){
            return $phrase;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Phrase $phrase)
    {
        $rules = [
            'name' => 'required|unique:phrases,name,'.$phrase->id,
        ];
        $messages = [
            'name.required' => 'The name field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $phrase->name = $request->name;
        $phrase->save();
        return $phrase;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,Phrase $phrase)
    {
        if($request->ajax()){
            $phrase->delete();
            return $phrase;
        }
    }
    public function all(){
        return Phrase::all();
    }
}
