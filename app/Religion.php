<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Religion extends Model
{
    protected $connection = 'mysql';
    protected $fillable = ['name'];
}
