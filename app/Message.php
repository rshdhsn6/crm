<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['message', 'created_by', 'created_to', 'is_seen', 'read_at'];
}
