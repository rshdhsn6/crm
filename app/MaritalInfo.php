<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MaritalInfo extends Model
{
    protected $connection = 'mysql';
    protected $fillable = ['name'];
}
