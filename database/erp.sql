-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 21, 2019 at 01:58 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `erp`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `log_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `subject_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `causer_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(1, 'default', 'User Estrella Zboncak has been created', 1, 'App\\User', NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Estrella Zboncak\",\"email\":\"admin@gmail.com\",\"email_verified_at\":\"2019-09-21T11:56:50.000000Z\",\"password\":\"$2y$10$W1hQtHdJZ3bfCdWBabdhV.pBxDlV.7qUSiEgN5IApDh0lzbf.O10y\",\"is_approved\":1,\"details_id\":null,\"details_type\":null,\"remember_token\":\"6z0BBUUPew\",\"created_at\":\"2019-09-21 17:56:50\",\"updated_at\":\"2019-09-21 17:56:50\"}}', '2019-09-21 11:56:50', '2019-09-21 11:56:50'),
(2, 'default', 'Division Production Inspector has been created', 1, 'Modules\\Hrm\\Entities\\Division', NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Production Inspector\",\"dept_id\":1,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(3, 'default', 'Division General Farmworker has been created', 2, 'Modules\\Hrm\\Entities\\Division', NULL, NULL, '{\"attributes\":{\"id\":2,\"name\":\"General Farmworker\",\"dept_id\":2,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(4, 'default', 'Division Respiratory Therapy Technician has been created', 3, 'Modules\\Hrm\\Entities\\Division', NULL, NULL, '{\"attributes\":{\"id\":3,\"name\":\"Respiratory Therapy Technician\",\"dept_id\":1,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(5, 'default', 'Division File Clerk has been created', 4, 'Modules\\Hrm\\Entities\\Division', NULL, NULL, '{\"attributes\":{\"id\":4,\"name\":\"File Clerk\",\"dept_id\":2,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(6, 'default', 'Division Chiropractor has been created', 5, 'Modules\\Hrm\\Entities\\Division', NULL, NULL, '{\"attributes\":{\"id\":5,\"name\":\"Chiropractor\",\"dept_id\":5,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(7, 'default', 'Division New Accounts Clerk has been created', 6, 'Modules\\Hrm\\Entities\\Division', NULL, NULL, '{\"attributes\":{\"id\":6,\"name\":\"New Accounts Clerk\",\"dept_id\":1,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(8, 'default', 'Division Silversmith has been created', 7, 'Modules\\Hrm\\Entities\\Division', NULL, NULL, '{\"attributes\":{\"id\":7,\"name\":\"Silversmith\",\"dept_id\":4,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(9, 'default', 'Division Aerospace Engineer has been created', 8, 'Modules\\Hrm\\Entities\\Division', NULL, NULL, '{\"attributes\":{\"id\":8,\"name\":\"Aerospace Engineer\",\"dept_id\":5,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(10, 'default', 'Division Numerical Control Machine Tool Operator has been created', 9, 'Modules\\Hrm\\Entities\\Division', NULL, NULL, '{\"attributes\":{\"id\":9,\"name\":\"Numerical Control Machine Tool Operator\",\"dept_id\":2,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(11, 'default', 'Division Hand Presser has been created', 10, 'Modules\\Hrm\\Entities\\Division', NULL, NULL, '{\"attributes\":{\"id\":10,\"name\":\"Hand Presser\",\"dept_id\":1,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(12, 'default', 'Department New Winifredborough has been created', 1, 'Modules\\Hrm\\Entities\\Department', NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"New Winifredborough\",\"desc\":\"Sit ipsam nulla at ratione. Sit corporis magni quo repudiandae asperiores vero.\",\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(13, 'default', 'Department East Otiliabury has been created', 2, 'Modules\\Hrm\\Entities\\Department', NULL, NULL, '{\"attributes\":{\"id\":2,\"name\":\"East Otiliabury\",\"desc\":\"Molestias necessitatibus est earum doloribus voluptas dolor. Sed distinctio aut aperiam accusantium. Sunt et eum ut dolorem.\",\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(14, 'default', 'Department East Evans has been created', 3, 'Modules\\Hrm\\Entities\\Department', NULL, NULL, '{\"attributes\":{\"id\":3,\"name\":\"East Evans\",\"desc\":\"Ut est dolorem non quos ut distinctio. Veniam eaque incidunt aspernatur molestias eveniet. Asperiores repellendus et vel. Iure sit sit rerum dicta quo modi aut.\",\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(15, 'default', 'Department North Brendanside has been created', 4, 'Modules\\Hrm\\Entities\\Department', NULL, NULL, '{\"attributes\":{\"id\":4,\"name\":\"North Brendanside\",\"desc\":\"Est similique vero quo est vero voluptate. Eum amet ea saepe minima. Nulla quas officia numquam vel iste.\",\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(16, 'default', 'Department Camrenfort has been created', 5, 'Modules\\Hrm\\Entities\\Department', NULL, NULL, '{\"attributes\":{\"id\":5,\"name\":\"Camrenfort\",\"desc\":\"Aliquid qui aperiam fugiat et doloremque repellat. Et corrupti et illum aliquam ex. Impedit ratione sed excepturi dolores necessitatibus. Blanditiis iure est ut aut eos omnis.\",\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(17, 'default', 'Shift Schedule odit has been created', 1, 'Modules\\Hrm\\Entities\\ShiftSchedule', NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"odit\",\"from\":\"13:59\",\"to\":\"00:14\",\"status\":1,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(18, 'default', 'Shift Schedule excepturi has been created', 2, 'Modules\\Hrm\\Entities\\ShiftSchedule', NULL, NULL, '{\"attributes\":{\"id\":2,\"name\":\"excepturi\",\"from\":\"02:03\",\"to\":\"12:21\",\"status\":1,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(19, 'default', 'Shift Schedule aut has been created', 3, 'Modules\\Hrm\\Entities\\ShiftSchedule', NULL, NULL, '{\"attributes\":{\"id\":3,\"name\":\"aut\",\"from\":\"07:37\",\"to\":\"23:02\",\"status\":1,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(20, 'default', 'Shift Schedule voluptas has been created', 4, 'Modules\\Hrm\\Entities\\ShiftSchedule', NULL, NULL, '{\"attributes\":{\"id\":4,\"name\":\"voluptas\",\"from\":\"16:42\",\"to\":\"18:43\",\"status\":1,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(21, 'default', 'Shift Schedule nobis has been created', 5, 'Modules\\Hrm\\Entities\\ShiftSchedule', NULL, NULL, '{\"attributes\":{\"id\":5,\"name\":\"nobis\",\"from\":\"11:52\",\"to\":\"05:07\",\"status\":1,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(22, 'default', 'Position Postsecondary Teacher has been created', 1, 'Modules\\Hrm\\Entities\\Position', NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Postsecondary Teacher\",\"desc\":\"Est et commodi tenetur voluptas. Distinctio corrupti possimus occaecati. Dolor est iusto et accusamus voluptas. Et ex qui fugit facilis beatae hic eum.\",\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(23, 'default', 'Position Geologist has been created', 2, 'Modules\\Hrm\\Entities\\Position', NULL, NULL, '{\"attributes\":{\"id\":2,\"name\":\"Geologist\",\"desc\":\"Rerum repudiandae minus id ducimus a. Voluptate tenetur aut magni esse dicta at ut. Vel tempore explicabo id quo tenetur nihil quibusdam.\",\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(24, 'default', 'Position Nutritionist has been created', 3, 'Modules\\Hrm\\Entities\\Position', NULL, NULL, '{\"attributes\":{\"id\":3,\"name\":\"Nutritionist\",\"desc\":\"Et incidunt corrupti earum rerum voluptas similique vitae. Placeat pariatur eos nam ut at. Accusamus soluta eum dolorum reiciendis magni consequatur cumque soluta.\",\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(25, 'default', 'Position Social Media Marketing Manager has been created', 4, 'Modules\\Hrm\\Entities\\Position', NULL, NULL, '{\"attributes\":{\"id\":4,\"name\":\"Social Media Marketing Manager\",\"desc\":\"Ut nostrum error voluptatibus voluptatibus eveniet. Molestiae est ut quae quis. Ut provident voluptatibus animi perferendis voluptatem qui. Eveniet officia ut sed delectus quisquam.\",\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(26, 'default', 'Position Precision Mold and Pattern Caster has been created', 5, 'Modules\\Hrm\\Entities\\Position', NULL, NULL, '{\"attributes\":{\"id\":5,\"name\":\"Precision Mold and Pattern Caster\",\"desc\":\"Et enim veniam consequatur nesciunt provident architecto aliquam. Error molestiae vel nostrum quidem. Dolores placeat est hic ullam. Sint non hic consequatur ipsa.\",\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(27, 'default', 'Position Lay-Out Worker has been created', 6, 'Modules\\Hrm\\Entities\\Position', NULL, NULL, '{\"attributes\":{\"id\":6,\"name\":\"Lay-Out Worker\",\"desc\":\"Et commodi sunt consequatur corrupti reiciendis et dolorum sunt. Vel est exercitationem sit libero dolorum harum. Ipsa cupiditate vel eveniet exercitationem vero nisi a voluptate.\",\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(28, 'default', 'Position Personnel Recruiter has been created', 7, 'Modules\\Hrm\\Entities\\Position', NULL, NULL, '{\"attributes\":{\"id\":7,\"name\":\"Personnel Recruiter\",\"desc\":\"Ratione alias iste alias quis et dolor et. Ducimus ut natus deleniti ut autem suscipit odio. Id iure voluptatum sunt quos.\",\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(29, 'default', 'Position Social and Human Service Assistant has been created', 8, 'Modules\\Hrm\\Entities\\Position', NULL, NULL, '{\"attributes\":{\"id\":8,\"name\":\"Social and Human Service Assistant\",\"desc\":\"Eveniet repellat minima non velit autem. Minus numquam officia ut quod molestiae. Nobis dolores alias non assumenda est et nulla. Pariatur iusto vero rerum aut ut a.\",\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(30, 'default', 'Position Law Clerk has been created', 9, 'Modules\\Hrm\\Entities\\Position', NULL, NULL, '{\"attributes\":{\"id\":9,\"name\":\"Law Clerk\",\"desc\":\"Ad quae dolor error tempora. Voluptas provident perferendis cupiditate. Nemo vitae qui est consequatur.\",\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(31, 'default', 'Position Interviewer has been created', 10, 'Modules\\Hrm\\Entities\\Position', NULL, NULL, '{\"attributes\":{\"id\":10,\"name\":\"Interviewer\",\"desc\":\"Nam qui et itaque et porro. Et omnis cupiditate dolorem enim consequatur sit natus. Et non nemo qui suscipit omnis non error. Aut laudantium possimus earum autem. Blanditiis similique earum sint.\",\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(32, 'default', 'Salary Grade voluptas has been created', 1, 'Modules\\Hrm\\Entities\\Grade', NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"voluptas\",\"type\":0,\"rate\":351,\"overtime_rate\":251,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(33, 'default', 'Salary Grade esse has been created', 2, 'Modules\\Hrm\\Entities\\Grade', NULL, NULL, '{\"attributes\":{\"id\":2,\"name\":\"esse\",\"type\":1,\"rate\":482,\"overtime_rate\":780,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(34, 'default', 'Salary Grade quisquam has been created', 3, 'Modules\\Hrm\\Entities\\Grade', NULL, NULL, '{\"attributes\":{\"id\":3,\"name\":\"quisquam\",\"type\":1,\"rate\":420,\"overtime_rate\":76,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(35, 'default', 'Salary Grade velit has been created', 4, 'Modules\\Hrm\\Entities\\Grade', NULL, NULL, '{\"attributes\":{\"id\":4,\"name\":\"velit\",\"type\":1,\"rate\":337,\"overtime_rate\":808,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(36, 'default', 'Salary Grade repudiandae has been created', 5, 'Modules\\Hrm\\Entities\\Grade', NULL, NULL, '{\"attributes\":{\"id\":5,\"name\":\"repudiandae\",\"type\":1,\"rate\":630,\"overtime_rate\":496,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(37, 'default', 'Salary Grade cupiditate has been created', 6, 'Modules\\Hrm\\Entities\\Grade', NULL, NULL, '{\"attributes\":{\"id\":6,\"name\":\"cupiditate\",\"type\":1,\"rate\":604,\"overtime_rate\":173,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(38, 'default', 'Salary Grade sed has been created', 7, 'Modules\\Hrm\\Entities\\Grade', NULL, NULL, '{\"attributes\":{\"id\":7,\"name\":\"sed\",\"type\":0,\"rate\":989,\"overtime_rate\":142,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(39, 'default', 'Salary Grade laboriosam has been created', 8, 'Modules\\Hrm\\Entities\\Grade', NULL, NULL, '{\"attributes\":{\"id\":8,\"name\":\"laboriosam\",\"type\":0,\"rate\":278,\"overtime_rate\":145,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(40, 'default', 'Salary Grade eos has been created', 9, 'Modules\\Hrm\\Entities\\Grade', NULL, NULL, '{\"attributes\":{\"id\":9,\"name\":\"eos\",\"type\":0,\"rate\":901,\"overtime_rate\":0,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(41, 'default', 'Salary Grade consequatur has been created', 10, 'Modules\\Hrm\\Entities\\Grade', NULL, NULL, '{\"attributes\":{\"id\":10,\"name\":\"consequatur\",\"type\":1,\"rate\":845,\"overtime_rate\":905,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(42, 'default', 'Employee Cesar Hansen has been created', 1, 'Modules\\Hrm\\Entities\\Employee', NULL, NULL, '{\"attributes\":{\"id\":1,\"name\":\"Cesar Hansen\",\"email\":\"blynch@ferry.com\",\"phone\":\"(982) 928-0069 x874\",\"photo\":null,\"gender\":1,\"birthday\":\"1997-05-07\",\"marital_id\":4,\"religion_id\":2,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(43, 'default', 'Employee Prof. Joan Trantow has been created', 2, 'Modules\\Hrm\\Entities\\Employee', NULL, NULL, '{\"attributes\":{\"id\":2,\"name\":\"Prof. Joan Trantow\",\"email\":\"franecki.annamarie@feeney.com\",\"phone\":\"354-277-3604\",\"photo\":null,\"gender\":1,\"birthday\":\"2005-10-26\",\"marital_id\":2,\"religion_id\":2,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(44, 'default', 'Employee Mr. Jaylan Kohler III has been created', 3, 'Modules\\Hrm\\Entities\\Employee', NULL, NULL, '{\"attributes\":{\"id\":3,\"name\":\"Mr. Jaylan Kohler III\",\"email\":\"myrtle52@hotmail.com\",\"phone\":\"(649) 733-2137\",\"photo\":null,\"gender\":1,\"birthday\":\"1983-01-31\",\"marital_id\":4,\"religion_id\":2,\"created_at\":\"2019-09-21 17:57:13\",\"updated_at\":\"2019-09-21 17:57:13\"}}', '2019-09-21 11:57:13', '2019-09-21 11:57:13');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `symbol` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `symbol`, `name`, `created_at`, `updated_at`) VALUES
(1, '৳', 'Taka', NULL, NULL),
(2, '$', 'Dollar', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `code`, `flag`, `created_at`, `updated_at`) VALUES
(1, 'English', 'en', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `language_phrases`
--

CREATE TABLE `language_phrases` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `language_id` bigint(20) UNSIGNED NOT NULL,
  `phrase_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `marital_infos`
--

CREATE TABLE `marital_infos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `marital_infos`
--

INSERT INTO `marital_infos` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Single', NULL, NULL),
(2, 'Married', NULL, NULL),
(3, 'Divorced', NULL, NULL),
(4, 'Widowed', NULL, NULL),
(5, 'Other', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `created_to` bigint(20) UNSIGNED NOT NULL,
  `is_seen` tinyint(1) NOT NULL DEFAULT '0',
  `read_at` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(44, '2014_10_12_000000_create_users_table', 1),
(45, '2014_10_12_100000_create_password_resets_table', 1),
(46, '2019_05_21_061120_create_marital_infos_table', 1),
(47, '2019_05_21_070346_create_religions_table', 1),
(55, '2019_06_12_055013_create_languages_table', 1),
(56, '2019_06_12_055323_create_currencies_table', 1),
(59, '2019_06_15_073424_create_settings_table', 1),
(78, '2019_07_27_164337_create_permission_tables', 1),
(79, '2019_07_30_180451_create_phrases_table', 1),
(80, '2019_07_30_184515_create_language_phrases_table', 1),
(81, '2019_08_20_122825_create_messages_table', 1),
(82, '2019_08_25_180707_create_notices_table', 1),
(83, '2019_08_25_182035_create_notice_documents_table', 1),
(84, '2019_08_27_152454_create_activity_log_table', 1),
(85, '2019_09_01_123448_create_user_details_table', 1),
(86, '2019_09_01_163041_create_user_addresses_table', 1),
(87, '2019_05_25_052858_create_departments_table', 2),
(88, '2019_06_10_063451_create_positions_table', 2),
(89, '2019_06_10_085527_create_divisions_table', 2),
(90, '2019_06_12_045317_create_job_types_table', 2),
(91, '2019_06_12_050210_create_shift_schedules_table', 2),
(92, '2019_06_12_050716_create_holidays_table', 2),
(93, '2019_06_12_052744_create_leave_types_table', 2),
(94, '2019_06_13_095705_create_employees_table', 2),
(95, '2019_06_13_102853_create_employee_addresses_table', 2),
(96, '2019_06_16_102611_create_allowances_table', 2),
(97, '2019_06_25_062753_create_employee_documents_table', 2),
(98, '2019_06_27_093310_create_employee_reschedules_table', 2),
(99, '2019_06_29_053924_create_leave_applications_table', 2),
(100, '2019_06_30_054842_create_attendances_table', 2),
(101, '2019_07_02_070404_create_leave_documents_table', 2),
(102, '2019_07_04_105126_create_grades_table', 2),
(103, '2019_07_04_153430_create_grade_allowances_table', 2),
(104, '2019_07_04_172820_create_employee_officials_table', 2),
(105, '2019_07_06_154234_create_loans_table', 2),
(106, '2019_07_07_160248_create_loan_installments_table', 2),
(107, '2019_07_09_121056_create_salaries_table', 2),
(108, '2019_07_09_122650_create_salary_details_table', 2),
(109, '2019_07_09_122810_create_salary_allowances_table', 2),
(110, '2019_07_24_132116_create_training_types_table', 2),
(111, '2019_07_24_141602_create_trainers_table', 2),
(112, '2019_07_24_172131_create_workshops_table', 2),
(113, '2019_07_24_175656_create_employee_workshops_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notices`
--

CREATE TABLE `notices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` longtext COLLATE utf8mb4_unicode_ci,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notice_documents`
--

CREATE TABLE `notice_documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `notice_id` bigint(20) UNSIGNED NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `phrases`
--

CREATE TABLE `phrases` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `religions`
--

CREATE TABLE `religions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `religions`
--

INSERT INTO `religions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Islam', NULL, NULL),
(2, 'Cristian', NULL, NULL),
(3, 'Buddhist', NULL, NULL),
(4, 'Hindu', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'web', '2019-09-21 11:56:50', '2019-09-21 11:56:50');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `option` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `option`, `value`, `created_at`, `updated_at`) VALUES
(1, 'title', 'HRM', NULL, NULL),
(2, 'address', 'New Market', NULL, NULL),
(3, 'email', 'hrm@erp.com', NULL, NULL),
(4, 'phone', '123456789', NULL, NULL),
(5, 'favicon', '', NULL, NULL),
(6, 'logo', '', NULL, NULL),
(7, 'language', '1', NULL, NULL),
(8, 'currency', '1', NULL, NULL),
(9, 'login', '', NULL, NULL),
(10, 'register', '', NULL, NULL),
(11, 'footer', '<span class=\"text-center text-sm-left d-md-inline-block\">Copyright © 2018 M3 v1.0. All Rights Reserved.</span>\r\n        <span class=\"float-none float-sm-right mt-1 mt-sm-0 text-center\">\r\n            Crafted with <i class=\"fa fa-heart text-danger\"></i> by <a href=\"javascript:void(0)\" class=\"text-dark\" target=\"_blank\">M3</a></span>', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_approved` tinyint(1) NOT NULL DEFAULT '0',
  `details_id` int(11) DEFAULT NULL,
  `details_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `is_approved`, `details_id`, `details_type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Estrella Zboncak', 'admin@gmail.com', '2019-09-21 11:56:50', '$2y$10$W1hQtHdJZ3bfCdWBabdhV.pBxDlV.7qUSiEgN5IApDh0lzbf.O10y', 1, NULL, NULL, '2HRyZuQS49eJ6TEHGgYx43iDjPbel7jgEWDm76U1vFva5wdqiusLOCvlIPgJ', '2019-09-21 11:56:50', '2019-09-21 11:56:50');

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE `user_addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_details_id` bigint(20) UNSIGNED NOT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `birthday` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital_id` bigint(20) UNSIGNED NOT NULL,
  `religion_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_log_log_name_index` (`log_name`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language_phrases`
--
ALTER TABLE `language_phrases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `language_phrases_language_id_foreign` (`language_id`),
  ADD KEY `language_phrases_phrase_id_foreign` (`phrase_id`);

--
-- Indexes for table `marital_infos`
--
ALTER TABLE `marital_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `notices`
--
ALTER TABLE `notices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notices_created_by_foreign` (`created_by`);

--
-- Indexes for table `notice_documents`
--
ALTER TABLE `notice_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notice_documents_notice_id_foreign` (`notice_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phrases`
--
ALTER TABLE `phrases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `religions`
--
ALTER TABLE `religions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_addresses_user_details_id_foreign` (`user_details_id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_details_phone_unique` (`phone`),
  ADD KEY `user_details_marital_id_foreign` (`marital_id`),
  ADD KEY `user_details_religion_id_foreign` (`religion_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `language_phrases`
--
ALTER TABLE `language_phrases`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `marital_infos`
--
ALTER TABLE `marital_infos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `notices`
--
ALTER TABLE `notices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notice_documents`
--
ALTER TABLE `notice_documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `phrases`
--
ALTER TABLE `phrases`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `religions`
--
ALTER TABLE `religions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_addresses`
--
ALTER TABLE `user_addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `language_phrases`
--
ALTER TABLE `language_phrases`
  ADD CONSTRAINT `language_phrases_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `language_phrases_phrase_id_foreign` FOREIGN KEY (`phrase_id`) REFERENCES `phrases` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notices`
--
ALTER TABLE `notices`
  ADD CONSTRAINT `notices_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `notice_documents`
--
ALTER TABLE `notice_documents`
  ADD CONSTRAINT `notice_documents_notice_id_foreign` FOREIGN KEY (`notice_id`) REFERENCES `notices` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD CONSTRAINT `user_addresses_user_details_id_foreign` FOREIGN KEY (`user_details_id`) REFERENCES `user_details` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_details`
--
ALTER TABLE `user_details`
  ADD CONSTRAINT `user_details_marital_id_foreign` FOREIGN KEY (`marital_id`) REFERENCES `marital_infos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_details_religion_id_foreign` FOREIGN KEY (`religion_id`) REFERENCES `religions` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
