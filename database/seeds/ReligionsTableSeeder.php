<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReligionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('religions')->insert([
            ['name' => 'Islam'],
            ['name' => 'Cristian'],
            ['name' => 'Buddhist'],
            ['name' => 'Hindu']
        ]);
    }
}
