<?php

use Illuminate\Database\Seeder;
use Modules\Crm\Database\Seeders\ClientGroupsTableSeeder;
use Modules\Crm\Database\Seeders\ClientTableSeeder;
use Modules\Crm\Database\Seeders\PaymentMethodsTableSeeder;
use Modules\Crm\Database\Seeders\ProjectStatusesTableSeeder;
use Modules\Crm\Database\Seeders\TaskStatusesTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//         $this->call(UsersTableSeeder::class);
        $this->call([
            RolesTableSeeder::class,
            UsersTableSeeder::class,
            LanguagesTableSeeder::class,
            CurrenciesTableSeeder::class,
            SettingsTableSeeder::class,
            MaritalInfosTableSeeder::class,
            ReligionsTableSeeder::class,

            /*-----CRM Database Seeders Start-------*/
            ClientGroupsTableSeeder::class,
            ClientTableSeeder::class,
            ProjectStatusesTableSeeder::class,
            TaskStatusesTableSeeder::class,
            PaymentMethodsTableSeeder::class,
            /*-----CRM Database Seeders End---------*/
        ]);
    }
}
