<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MaritalInfosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('marital_infos')->insert([
            ['name' => 'Single'],
            ['name' => 'Married'],
            ['name' => 'Divorced'],
            ['name' => 'Widowed'],
            ['name' => 'Other'],
        ]);
    }
}
