<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call("OthersTableSeeder");

        DB::table('settings')->insert([
            ['option' => 'title', 'value' => 'ERP'],
            ['option' => 'address', 'value' => 'New Market'],
            ['option' => 'email', 'value' => 'erp@erp.com'],
            ['option' => 'phone', 'value' => '123456789'],
            ['option' => 'favicon', 'value' => ''],
            ['option' => 'logo', 'value' => ''],
            ['option' => 'language', 'value' => 1],
            ['option' => 'currency', 'value' => 1],
            ['option' => 'login', 'value' => ''],
            ['option' => 'register', 'value' => ''],
            ['option' => 'footer', 'value' => '<span class="text-center text-sm-left d-md-inline-block">Copyright © 2019 M3 v1.0. All Rights Reserved.</span>
        <span class="float-none float-sm-right mt-1 mt-sm-0 text-center">
            Crafted with <i class="fa fa-heart text-danger"></i> by <a href="javascript:void(0)" class="text-dark" target="_blank">M3</a></span>'],
        ]);
    }
}
