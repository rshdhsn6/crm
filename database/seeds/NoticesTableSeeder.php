<?php

use Illuminate\Database\Seeder;
use App\Notice;
use App\NoticeDocument;

class NoticesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Notice::class,15)->create()->each(function ($notice) {
            $notice->documents()->saveMany(factory(NoticeDocument::class,rand(0,3))->make());
        });
    }
}
