-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 21, 2019 at 01:58 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `erp_hrm`
--

-- --------------------------------------------------------

--
-- Table structure for table `allowances`
--

CREATE TABLE `allowances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `allowances`
--

INSERT INTO `allowances` (`id`, `name`, `type`, `created_at`, `updated_at`) VALUES
(1, 'House Rent', 0, NULL, NULL),
(2, 'Medical', 0, NULL, NULL),
(3, 'Educational', 0, NULL, NULL),
(4, 'Meal', 1, NULL, NULL),
(5, 'Transportation', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE `attendances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `checkin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `checkout` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `desc`, `created_at`, `updated_at`) VALUES
(1, 'New Winifredborough', 'Sit ipsam nulla at ratione. Sit corporis magni quo repudiandae asperiores vero.', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(2, 'East Otiliabury', 'Molestias necessitatibus est earum doloribus voluptas dolor. Sed distinctio aut aperiam accusantium. Sunt et eum ut dolorem.', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(3, 'East Evans', 'Ut est dolorem non quos ut distinctio. Veniam eaque incidunt aspernatur molestias eveniet. Asperiores repellendus et vel. Iure sit sit rerum dicta quo modi aut.', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(4, 'North Brendanside', 'Est similique vero quo est vero voluptate. Eum amet ea saepe minima. Nulla quas officia numquam vel iste.', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(5, 'Camrenfort', 'Aliquid qui aperiam fugiat et doloremque repellat. Et corrupti et illum aliquam ex. Impedit ratione sed excepturi dolores necessitatibus. Blanditiis iure est ut aut eos omnis.', '2019-09-21 11:57:13', '2019-09-21 11:57:13');

-- --------------------------------------------------------

--
-- Table structure for table `divisions`
--

CREATE TABLE `divisions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dept_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `divisions`
--

INSERT INTO `divisions` (`id`, `name`, `dept_id`, `created_at`, `updated_at`) VALUES
(1, 'Production Inspector', 1, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(2, 'General Farmworker', 2, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(3, 'Respiratory Therapy Technician', 1, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(4, 'File Clerk', 2, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(5, 'Chiropractor', 5, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(6, 'New Accounts Clerk', 1, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(7, 'Silversmith', 4, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(8, 'Aerospace Engineer', 5, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(9, 'Numerical Control Machine Tool Operator', 2, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(10, 'Hand Presser', 1, '2019-09-21 11:57:13', '2019-09-21 11:57:13');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `birthday` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital_id` bigint(20) UNSIGNED NOT NULL,
  `religion_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `email`, `phone`, `photo`, `gender`, `birthday`, `marital_id`, `religion_id`, `created_at`, `updated_at`) VALUES
(1, 'Cesar Hansen', 'blynch@ferry.com', '(982) 928-0069 x874', NULL, 1, '1997-05-07', 4, 2, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(2, 'Prof. Joan Trantow', 'franecki.annamarie@feeney.com', '354-277-3604', NULL, 1, '2005-10-26', 2, 2, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(3, 'Mr. Jaylan Kohler III', 'myrtle52@hotmail.com', '(649) 733-2137', NULL, 1, '1983-01-31', 4, 2, '2019-09-21 11:57:13', '2019-09-21 11:57:13');

-- --------------------------------------------------------

--
-- Table structure for table `employee_addresses`
--

CREATE TABLE `employee_addresses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_addresses`
--

INSERT INTO `employee_addresses` (`id`, `employee_id`, `street`, `city`, `district`, `zip_code`, `country`, `created_at`, `updated_at`) VALUES
(1, 1, '562 Schumm Cove Suite 977', 'East Blair', 'South Kaitlyn', '557', 'Guatemala', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(2, 2, '7137 Joanie Park Apt. 863', 'West Maybelleborough', 'O\'Connellhaven', '601', 'Iran', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(3, 3, '631 Schuppe Locks', 'North Marcos', 'New Santiagoview', '38', 'Cameroon', '2019-09-21 11:57:13', '2019-09-21 11:57:13');

-- --------------------------------------------------------

--
-- Table structure for table `employee_documents`
--

CREATE TABLE `employee_documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_documents`
--

INSERT INTO `employee_documents` (`id`, `employee_id`, `title`, `file`, `created_at`, `updated_at`) VALUES
(1, 1, 'quas', '72ce74b0-15f6-3b89-aeb2-571c0f62da3c.pdf', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(2, 1, 'corrupti', '8f2ae426-4ae9-3a0d-8932-88af370994ed.jpg', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(3, 1, 'eos', '4ba45757-9ab5-36e9-8842-e54152bb3993.docx', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(4, 2, 'cumque', 'e374eb1d-e2a9-30f8-873d-280682498eb4.pdf', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(5, 2, 'voluptatem', 'f80f9a4f-ebfa-3aed-9b52-0f78166a94f1.jpg', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(6, 2, 'nulla', 'c81c9383-4474-33d8-9c0f-c44afd3c9c4b.docx', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(7, 3, 'recusandae', 'a74f4fce-4f4a-39d8-87a6-13e0d66c15a9.pdf', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(8, 3, 'et', 'f3be7002-0ebd-3781-a484-2bd3b5aac912.docx', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(9, 3, 'totam', '4c497b76-be3f-3500-9b61-4c34c340355a.jpg', '2019-09-21 11:57:13', '2019-09-21 11:57:13');

-- --------------------------------------------------------

--
-- Table structure for table `employee_officials`
--

CREATE TABLE `employee_officials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `division` bigint(20) UNSIGNED NOT NULL,
  `position` bigint(20) UNSIGNED NOT NULL,
  `job_type` bigint(20) UNSIGNED NOT NULL,
  `from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `supervisor` bigint(20) UNSIGNED DEFAULT NULL,
  `shift` bigint(20) UNSIGNED NOT NULL,
  `grade_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employee_officials`
--

INSERT INTO `employee_officials` (`id`, `employee_id`, `division`, `position`, `job_type`, `from`, `to`, `supervisor`, `shift`, `grade_id`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 1, 1, '2014-03-31', '2014-05-01', 1, 4, 3, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(2, 2, 6, 1, 2, '1995-10-28', NULL, 2, 5, 2, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(3, 3, 6, 1, 3, '1996-08-31', '1996-12-31', 2, 5, 5, '2019-09-21 11:57:13', '2019-09-21 11:57:13');

-- --------------------------------------------------------

--
-- Table structure for table `employee_reschedules`
--

CREATE TABLE `employee_reschedules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `shift` bigint(20) UNSIGNED NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employee_workshops`
--

CREATE TABLE `employee_workshops` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `workshop_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `rate` int(11) NOT NULL DEFAULT '0',
  `overtime_rate` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `name`, `type`, `rate`, `overtime_rate`, `created_at`, `updated_at`) VALUES
(1, 'voluptas', 0, 351, 251, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(2, 'esse', 1, 482, 780, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(3, 'quisquam', 1, 420, 76, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(4, 'velit', 1, 337, 808, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(5, 'repudiandae', 1, 630, 496, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(6, 'cupiditate', 1, 604, 173, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(7, 'sed', 0, 989, 142, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(8, 'laboriosam', 0, 278, 145, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(9, 'eos', 0, 901, 0, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(10, 'consequatur', 1, 845, 905, '2019-09-21 11:57:13', '2019-09-21 11:57:13');

-- --------------------------------------------------------

--
-- Table structure for table `grade_allowances`
--

CREATE TABLE `grade_allowances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `grade_id` bigint(20) UNSIGNED NOT NULL,
  `allowance_id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `rate` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grade_allowances`
--

INSERT INTO `grade_allowances` (`id`, `grade_id`, `allowance_id`, `type`, `rate`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 0, 9, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(2, 1, 5, 1, 0, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(3, 1, 2, 1, 6, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(4, 2, 3, 1, 6, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(5, 2, 3, 0, 4, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(6, 2, 5, 0, 5, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(7, 3, 4, 1, 2, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(8, 3, 1, 0, 9, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(9, 3, 5, 1, 2, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(10, 4, 5, 1, 0, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(11, 4, 5, 0, 3, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(12, 4, 1, 1, 3, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(13, 5, 4, 0, 8, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(14, 5, 2, 0, 8, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(15, 5, 4, 0, 8, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(16, 6, 3, 1, 2, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(17, 6, 2, 1, 1, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(18, 6, 2, 0, 1, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(19, 7, 5, 1, 5, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(20, 7, 4, 1, 0, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(21, 7, 3, 1, 0, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(22, 8, 1, 1, 6, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(23, 8, 5, 0, 9, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(24, 8, 3, 1, 0, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(25, 9, 3, 1, 7, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(26, 9, 4, 0, 2, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(27, 9, 3, 1, 9, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(28, 10, 2, 1, 6, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(29, 10, 1, 0, 9, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(30, 10, 5, 0, 3, '2019-09-21 11:57:13', '2019-09-21 11:57:13');

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE `holidays` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `job_types`
--

CREATE TABLE `job_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `job_types`
--

INSERT INTO `job_types` (`id`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Full time', NULL, NULL),
(2, 'Part Time', NULL, NULL),
(3, 'Contractual', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `leave_applications`
--

CREATE TABLE `leave_applications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `applied_from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `applied_to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved_by` bigint(20) UNSIGNED DEFAULT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `leave_documents`
--

CREATE TABLE `leave_documents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `application_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `leave_types`
--

CREATE TABLE `leave_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `days` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `leave_types`
--

INSERT INTO `leave_types` (`id`, `name`, `days`, `created_at`, `updated_at`) VALUES
(1, 'Maternity', 20, NULL, NULL),
(2, 'Sickness', 10, NULL, NULL),
(3, 'Others', 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `loans`
--

CREATE TABLE `loans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL DEFAULT '0',
  `approved_by` bigint(20) UNSIGNED DEFAULT NULL,
  `approved_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `repayment_from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `repayment_to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `installment` int(11) NOT NULL DEFAULT '1',
  `comment` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `loan_installments`
--

CREATE TABLE `loan_installments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `installment_amount` int(11) NOT NULL DEFAULT '0',
  `paid` int(11) NOT NULL DEFAULT '0',
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `received_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `name`, `desc`, `created_at`, `updated_at`) VALUES
(1, 'Postsecondary Teacher', 'Est et commodi tenetur voluptas. Distinctio corrupti possimus occaecati. Dolor est iusto et accusamus voluptas. Et ex qui fugit facilis beatae hic eum.', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(2, 'Geologist', 'Rerum repudiandae minus id ducimus a. Voluptate tenetur aut magni esse dicta at ut. Vel tempore explicabo id quo tenetur nihil quibusdam.', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(3, 'Nutritionist', 'Et incidunt corrupti earum rerum voluptas similique vitae. Placeat pariatur eos nam ut at. Accusamus soluta eum dolorum reiciendis magni consequatur cumque soluta.', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(4, 'Social Media Marketing Manager', 'Ut nostrum error voluptatibus voluptatibus eveniet. Molestiae est ut quae quis. Ut provident voluptatibus animi perferendis voluptatem qui. Eveniet officia ut sed delectus quisquam.', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(5, 'Precision Mold and Pattern Caster', 'Et enim veniam consequatur nesciunt provident architecto aliquam. Error molestiae vel nostrum quidem. Dolores placeat est hic ullam. Sint non hic consequatur ipsa.', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(6, 'Lay-Out Worker', 'Et commodi sunt consequatur corrupti reiciendis et dolorum sunt. Vel est exercitationem sit libero dolorum harum. Ipsa cupiditate vel eveniet exercitationem vero nisi a voluptate.', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(7, 'Personnel Recruiter', 'Ratione alias iste alias quis et dolor et. Ducimus ut natus deleniti ut autem suscipit odio. Id iure voluptatum sunt quos.', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(8, 'Social and Human Service Assistant', 'Eveniet repellat minima non velit autem. Minus numquam officia ut quod molestiae. Nobis dolores alias non assumenda est et nulla. Pariatur iusto vero rerum aut ut a.', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(9, 'Law Clerk', 'Ad quae dolor error tempora. Voluptas provident perferendis cupiditate. Nemo vitae qui est consequatur.', '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(10, 'Interviewer', 'Nam qui et itaque et porro. Et omnis cupiditate dolorem enim consequatur sit natus. Et non nemo qui suscipit omnis non error. Aut laudantium possimus earum autem. Blanditiis similique earum sint.', '2019-09-21 11:57:13', '2019-09-21 11:57:13');

-- --------------------------------------------------------

--
-- Table structure for table `salaries`
--

CREATE TABLE `salaries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `employee_id` bigint(20) UNSIGNED NOT NULL,
  `from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL DEFAULT '0',
  `generated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `paid_by` bigint(20) UNSIGNED DEFAULT NULL,
  `paid_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paid_status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salary_allowances`
--

CREATE TABLE `salary_allowances` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `salary_id` bigint(20) UNSIGNED NOT NULL,
  `allowance_id` bigint(20) UNSIGNED DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `rate` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salary_details`
--

CREATE TABLE `salary_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `salary_id` bigint(20) UNSIGNED NOT NULL,
  `job_type` bigint(20) UNSIGNED DEFAULT NULL,
  `shift` bigint(20) UNSIGNED DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `basic` double(8,2) NOT NULL DEFAULT '0.00',
  `overtime` double(8,2) NOT NULL DEFAULT '0.00',
  `working_hour` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_working_hour` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `working_day` int(11) NOT NULL,
  `total_working_day` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `overtime_hour` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `overtime_day` int(11) NOT NULL,
  `overtime_rate` int(11) NOT NULL,
  `leave` int(11) NOT NULL,
  `loan` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shift_schedules`
--

CREATE TABLE `shift_schedules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shift_schedules`
--

INSERT INTO `shift_schedules` (`id`, `name`, `from`, `to`, `status`, `created_at`, `updated_at`) VALUES
(1, 'odit', '13:59', '00:14', 1, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(2, 'excepturi', '02:03', '12:21', 1, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(3, 'aut', '07:37', '23:02', 1, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(4, 'voluptas', '16:42', '18:43', 1, '2019-09-21 11:57:13', '2019-09-21 11:57:13'),
(5, 'nobis', '11:52', '05:07', 1, '2019-09-21 11:57:13', '2019-09-21 11:57:13');

-- --------------------------------------------------------

--
-- Table structure for table `trainers`
--

CREATE TABLE `trainers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expertise` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `training_types`
--

CREATE TABLE `training_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `workshops`
--

CREATE TABLE `workshops` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `trainer_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` int(11) NOT NULL DEFAULT '0',
  `from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `allowances`
--
ALTER TABLE `allowances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `divisions`
--
ALTER TABLE `divisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employees_phone_unique` (`phone`),
  ADD KEY `employees_marital_id_foreign` (`marital_id`),
  ADD KEY `employees_religion_id_foreign` (`religion_id`);

--
-- Indexes for table `employee_addresses`
--
ALTER TABLE `employee_addresses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employee_addresses_employee_id_unique` (`employee_id`);

--
-- Indexes for table `employee_documents`
--
ALTER TABLE `employee_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_documents_employee_id_foreign` (`employee_id`);

--
-- Indexes for table `employee_officials`
--
ALTER TABLE `employee_officials`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employee_officials_employee_id_unique` (`employee_id`),
  ADD KEY `employee_officials_division_foreign` (`division`),
  ADD KEY `employee_officials_position_foreign` (`position`),
  ADD KEY `employee_officials_job_type_foreign` (`job_type`),
  ADD KEY `employee_officials_supervisor_foreign` (`supervisor`),
  ADD KEY `employee_officials_shift_foreign` (`shift`),
  ADD KEY `employee_officials_grade_id_foreign` (`grade_id`);

--
-- Indexes for table `employee_reschedules`
--
ALTER TABLE `employee_reschedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_reschedules_employee_id_foreign` (`employee_id`),
  ADD KEY `employee_reschedules_shift_foreign` (`shift`);

--
-- Indexes for table `employee_workshops`
--
ALTER TABLE `employee_workshops`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_workshops_employee_id_foreign` (`employee_id`),
  ADD KEY `employee_workshops_workshop_id_foreign` (`workshop_id`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `grades_name_unique` (`name`);

--
-- Indexes for table `grade_allowances`
--
ALTER TABLE `grade_allowances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grade_allowances_grade_id_foreign` (`grade_id`),
  ADD KEY `grade_allowances_allowance_id_foreign` (`allowance_id`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_types`
--
ALTER TABLE `job_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_applications`
--
ALTER TABLE `leave_applications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leave_applications_employee_id_foreign` (`employee_id`),
  ADD KEY `leave_applications_type_id_foreign` (`type_id`),
  ADD KEY `leave_applications_approved_by_foreign` (`approved_by`);

--
-- Indexes for table `leave_documents`
--
ALTER TABLE `leave_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `leave_documents_application_id_foreign` (`application_id`);

--
-- Indexes for table `leave_types`
--
ALTER TABLE `leave_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loans`
--
ALTER TABLE `loans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `loans_employee_id_foreign` (`employee_id`),
  ADD KEY `loans_approved_by_foreign` (`approved_by`);

--
-- Indexes for table `loan_installments`
--
ALTER TABLE `loan_installments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `loan_installments_employee_id_foreign` (`employee_id`),
  ADD KEY `loan_installments_received_by_foreign` (`received_by`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salaries`
--
ALTER TABLE `salaries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `salaries_employee_id_foreign` (`employee_id`),
  ADD KEY `salaries_generated_by_foreign` (`generated_by`),
  ADD KEY `salaries_paid_by_foreign` (`paid_by`);

--
-- Indexes for table `salary_allowances`
--
ALTER TABLE `salary_allowances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `salary_allowances_salary_id_foreign` (`salary_id`),
  ADD KEY `salary_allowances_allowance_id_foreign` (`allowance_id`);

--
-- Indexes for table `salary_details`
--
ALTER TABLE `salary_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `salary_details_salary_id_foreign` (`salary_id`),
  ADD KEY `salary_details_job_type_foreign` (`job_type`),
  ADD KEY `salary_details_shift_foreign` (`shift`);

--
-- Indexes for table `shift_schedules`
--
ALTER TABLE `shift_schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trainers`
--
ALTER TABLE `trainers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `training_types`
--
ALTER TABLE `training_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `workshops`
--
ALTER TABLE `workshops`
  ADD PRIMARY KEY (`id`),
  ADD KEY `workshops_type_id_foreign` (`type_id`),
  ADD KEY `workshops_trainer_id_foreign` (`trainer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `allowances`
--
ALTER TABLE `allowances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `divisions`
--
ALTER TABLE `divisions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `employee_addresses`
--
ALTER TABLE `employee_addresses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `employee_documents`
--
ALTER TABLE `employee_documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `employee_officials`
--
ALTER TABLE `employee_officials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `employee_reschedules`
--
ALTER TABLE `employee_reschedules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_workshops`
--
ALTER TABLE `employee_workshops`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `grade_allowances`
--
ALTER TABLE `grade_allowances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job_types`
--
ALTER TABLE `job_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `leave_applications`
--
ALTER TABLE `leave_applications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `leave_documents`
--
ALTER TABLE `leave_documents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `leave_types`
--
ALTER TABLE `leave_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `loans`
--
ALTER TABLE `loans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `loan_installments`
--
ALTER TABLE `loan_installments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `salaries`
--
ALTER TABLE `salaries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `salary_allowances`
--
ALTER TABLE `salary_allowances`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `salary_details`
--
ALTER TABLE `salary_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shift_schedules`
--
ALTER TABLE `shift_schedules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `trainers`
--
ALTER TABLE `trainers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `training_types`
--
ALTER TABLE `training_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `workshops`
--
ALTER TABLE `workshops`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `employees_marital_id_foreign` FOREIGN KEY (`marital_id`) REFERENCES `erp`.`marital_infos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `employees_religion_id_foreign` FOREIGN KEY (`religion_id`) REFERENCES `erp`.`religions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `employee_addresses`
--
ALTER TABLE `employee_addresses`
  ADD CONSTRAINT `employee_addresses_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `employee_documents`
--
ALTER TABLE `employee_documents`
  ADD CONSTRAINT `employee_documents_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `employee_officials`
--
ALTER TABLE `employee_officials`
  ADD CONSTRAINT `employee_officials_division_foreign` FOREIGN KEY (`division`) REFERENCES `divisions` (`id`),
  ADD CONSTRAINT `employee_officials_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `employee_officials_grade_id_foreign` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `employee_officials_job_type_foreign` FOREIGN KEY (`job_type`) REFERENCES `job_types` (`id`),
  ADD CONSTRAINT `employee_officials_position_foreign` FOREIGN KEY (`position`) REFERENCES `positions` (`id`),
  ADD CONSTRAINT `employee_officials_shift_foreign` FOREIGN KEY (`shift`) REFERENCES `shift_schedules` (`id`),
  ADD CONSTRAINT `employee_officials_supervisor_foreign` FOREIGN KEY (`supervisor`) REFERENCES `employees` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `employee_reschedules`
--
ALTER TABLE `employee_reschedules`
  ADD CONSTRAINT `employee_reschedules_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `employee_reschedules_shift_foreign` FOREIGN KEY (`shift`) REFERENCES `shift_schedules` (`id`);

--
-- Constraints for table `employee_workshops`
--
ALTER TABLE `employee_workshops`
  ADD CONSTRAINT `employee_workshops_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `employee_workshops_workshop_id_foreign` FOREIGN KEY (`workshop_id`) REFERENCES `workshops` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `grade_allowances`
--
ALTER TABLE `grade_allowances`
  ADD CONSTRAINT `grade_allowances_allowance_id_foreign` FOREIGN KEY (`allowance_id`) REFERENCES `allowances` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `grade_allowances_grade_id_foreign` FOREIGN KEY (`grade_id`) REFERENCES `grades` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `leave_applications`
--
ALTER TABLE `leave_applications`
  ADD CONSTRAINT `leave_applications_approved_by_foreign` FOREIGN KEY (`approved_by`) REFERENCES `employees` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `leave_applications_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `leave_applications_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `leave_types` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `leave_documents`
--
ALTER TABLE `leave_documents`
  ADD CONSTRAINT `leave_documents_application_id_foreign` FOREIGN KEY (`application_id`) REFERENCES `leave_applications` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `loans`
--
ALTER TABLE `loans`
  ADD CONSTRAINT `loans_approved_by_foreign` FOREIGN KEY (`approved_by`) REFERENCES `employees` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `loans_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `loan_installments`
--
ALTER TABLE `loan_installments`
  ADD CONSTRAINT `loan_installments_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `loan_installments_received_by_foreign` FOREIGN KEY (`received_by`) REFERENCES `employees` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `salaries`
--
ALTER TABLE `salaries`
  ADD CONSTRAINT `salaries_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `salaries_generated_by_foreign` FOREIGN KEY (`generated_by`) REFERENCES `employees` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `salaries_paid_by_foreign` FOREIGN KEY (`paid_by`) REFERENCES `employees` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `salary_allowances`
--
ALTER TABLE `salary_allowances`
  ADD CONSTRAINT `salary_allowances_allowance_id_foreign` FOREIGN KEY (`allowance_id`) REFERENCES `allowances` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `salary_allowances_salary_id_foreign` FOREIGN KEY (`salary_id`) REFERENCES `salaries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `salary_details`
--
ALTER TABLE `salary_details`
  ADD CONSTRAINT `salary_details_job_type_foreign` FOREIGN KEY (`job_type`) REFERENCES `job_types` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `salary_details_salary_id_foreign` FOREIGN KEY (`salary_id`) REFERENCES `salaries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `salary_details_shift_foreign` FOREIGN KEY (`shift`) REFERENCES `shift_schedules` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `workshops`
--
ALTER TABLE `workshops`
  ADD CONSTRAINT `workshops_trainer_id_foreign` FOREIGN KEY (`trainer_id`) REFERENCES `trainers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `workshops_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `training_types` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
