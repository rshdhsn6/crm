<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Query\Expression;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $dbName = DB::connection('hrm')->getDatabaseName();
        Schema::create('user_details', function (Blueprint $table) use ($dbName) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone')->unique();
            $table->string('photo')->nullable();
            $table->boolean('gender')->default(0);
            $table->string('birthday')->nullable();
            $table->unsignedBigInteger('marital_id');
            $table->unsignedBigInteger('religion_id');
            $table->timestamps();

            $table->foreign('marital_id')->references('id')->on('marital_infos')->onDelete('cascade');
            $table->foreign('religion_id')->references('id')->on('religions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
