<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\User;
use App\Notice;
use Faker\Generator as Faker;

$factory->define(Notice::class, function (Faker $faker) {
    return [
        'title'=>$faker->sentence,
        'desc'=>$faker->text,
        'created_by'=> User::all()->random()
    ];
});
