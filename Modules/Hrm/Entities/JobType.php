<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;

class JobType extends Model
{
    protected $connection = 'hrm';
    protected $fillable = ['type'];
}
