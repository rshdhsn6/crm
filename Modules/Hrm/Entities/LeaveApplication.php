<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class LeaveApplication extends Model
{
    use LogsActivity;

    const name = "Leave application";
    protected $connection = "hrm";
    protected $fillable = ['employee_id', 'type_id', 'applied_from', 'applied_to', 'from', 'to', 'approved_by', 'reason'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return $this::name." of {$this->employee->name} has been {$eventName}";
    }
    public function employee(){
        return $this->belongsTo(Employee::class,'employee_id','id');
    }
    public function type(){
        return $this->hasOne(LeaveType::class,'id','type_id');
    }
    public function approvedBy(){
        return $this->hasOne(Employee::class,'id','approved_by');
    }
    public function documents(){
        return $this->hasMany(LeaveDocument::class,'id','application_id');
    }
}
