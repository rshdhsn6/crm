<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    protected $connection = 'hrm';
    protected $fillable = ['employee_id', 'date', 'checkin', 'checkout'];

    public function employee(){
        return $this->belongsTo(Employee::class,'employee_id','id');
    }
}
