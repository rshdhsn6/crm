<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ShiftSchedule extends Model
{
    use LogsActivity;

    const name = "Shift Schedule";
    protected $connection = 'hrm';
    protected $fillable = ['name','from','to','status'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return $this::name." {$this->name} has been {$eventName}";
    }
}
