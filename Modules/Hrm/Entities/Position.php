<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Position extends Model
{
    use LogsActivity;

    const name = "Position";
    protected $connection = 'hrm';
    protected $fillable = ['name', 'desc'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return $this::name." {$this->name} has been {$eventName}";
    }

    public function employees()
    {
        return $this->hasMany(EmployeeOfficial::class, 'position', 'id');
    }
}
