<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;

class SalaryDetails extends Model
{
    protected $connection = 'hrm';
    protected $fillable = ['salary_id', 'job_type', 'shift', 'type','basic', 'total_working_hour', 'working_hour', 'total_working_day', 'working_day', 'rate', 'overtime_hour', 'overtime_day', 'overtime_rate', 'leave', 'loan'];

    public function shift(){
        return $this->belongsTo(ShiftSchedule::class,'shift','id');
    }
}
