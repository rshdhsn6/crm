<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Department extends Model
{
    use LogsActivity;

    const name = "Department";
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'hrm';
    protected $fillable = ['name','desc'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return $this::name." {$this->name} has been {$eventName}";
    }

    public function employees(){
        return $this->hasManyThrough( EmployeeOfficial::class,Division::class,'dept_id','division','id','id');
    }
}
