<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class EmployeeAddress extends Model
{
    use LogsActivity;

    const name = "Employee Address";
    protected $connection = 'hrm';
    protected $fillable = ['employee_id', 'street', 'city', 'district', 'zip_code', 'country'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;
    protected static $recordEvents = ['updated'];
    public function getDescriptionForEvent(string $eventName): string
    {
        return "{$this->employee->name}'s ".$this::name." has been {$eventName}";
    }
    public function employee(){
        return $this->belongsTo(Employee::class,'id','employee_id');
    }
}
