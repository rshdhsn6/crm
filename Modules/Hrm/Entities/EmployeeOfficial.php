<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class EmployeeOfficial extends Model
{
    use LogsActivity;

    const name = "Official Info";
    protected $connection = 'hrm';
    protected $fillable = ['employee_id', 'division', 'position', 'job_type', 'from', 'to', 'supervisor', 'shift','grade_id'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;
    protected static $recordEvents = ['updated'];
    public function getDescriptionForEvent(string $eventName): string
    {
        return "{$this->employee->name}'s ".$this::name." has been {$eventName}";
    }

    public function employee(){
        return $this->belongsTo(Employee::class,'id','employee_id');
    }
    public function department(){
        return $this->hasOneThrough(Department::class, Division::class,'dept_id','id');
    }
    public function division(){
        return $this->hasOne(Division::class,'id','division');
    }
    public function position(){
        return $this->hasOne(Position::class,'id','position');
    }
    public function job_type(){
        return $this->hasOne(JobType::class,'id','job_type');
    }
    public function supervisor(){
        return $this->hasOne(Employee::class,'id','supervisor');
    }
    public function shift(){
        return $this->hasOne(ShiftSchedule::class,'id','shift');
    }
    public function grade(){
        return $this->hasOne(Grade::class,'id','grade_id');
    }
    public function allowances(){
        return $this->hasMany(GradeAllowance::class,'id','grade_id');
    }
}
