<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;

class EmployeeDocument extends Model
{    const name = "Employee documents";
    protected $connection = 'hrm';
    protected $fillable = ['employee_id', 'title', 'file'];
}
