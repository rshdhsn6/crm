<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class GradeAllowance extends Model
{
    use LogsActivity;

    const name = "Salary Grade Allowance";
    protected $connection = 'hrm';
    protected $fillable = ['grade_id', 'allowance_id', 'type', 'rate'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;
    protected static $recordEvents = ['updated'];

    public function getDescriptionForEvent(string $eventName): string
    {
        return $this::name." of {$this->grade->name} has been {$eventName}";
    }
    public function grade()
    {
        return $this->hasOne(Grade::class, 'id', 'grade_id');
    }
    public function allowance()
    {
        return $this->hasOne(Allowance::class, 'id', 'allowance_id');
    }
}
