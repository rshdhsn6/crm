<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;

class SalaryAllowance extends Model
{
    protected $connection = 'hrm';
    protected $fillable = ['salary_id', 'allowance_id', 'type', 'rate'];

    public function allowance(){
        return $this->belongsTo(Allowance::class,'allowance_id','id');
    }
}
