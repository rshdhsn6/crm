<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Grade extends Model
{
    use LogsActivity;

    const name = "Salary Grade";
    protected $connection = 'hrm';
    protected $fillable = ['name', 'type', 'rate', 'overtime_rate', 'amount'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return $this::name." {$this->name} has been {$eventName}";
    }
    public function allowances(){
        return $this->hasMany(GradeAllowance::class,'grade_id','id');
    }
}
