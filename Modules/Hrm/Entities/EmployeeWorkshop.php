<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;

class EmployeeWorkshop extends Model
{
    const name = "Employee Workshop";
    protected $connection='hrm';
    protected $fillable = ['employee_id','workshop_id'];

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'id', 'employee_id');
    }
    public function workshop()
    {
        return $this->belongsTo(Workshop::class, 'id', 'workshop_id');
    }
}
