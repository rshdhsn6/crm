<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class LeaveType extends Model
{
    use LogsActivity;

    const name = "Leave Type";
    protected $connection = "hrm";
    protected $fillable = ['name','days'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return $this::name." of {$this->name} has been {$eventName}";
    }
}
