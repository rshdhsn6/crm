<?php

namespace Modules\Hrm\Entities;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Trainer extends Model
{
    use LogsActivity;

    const name = "Trainer";
    protected $connection='hrm';
    protected $fillable = ['name', 'email', 'phone', 'address', 'company', 'expertise','photo'];
    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return $this::name." {$this->name} has been {$eventName}";
    }
}
