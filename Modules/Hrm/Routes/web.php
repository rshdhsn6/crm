<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('hrm')->group(function () {
    Route::get('/', 'HrmController@index')->name('hrm');
    Route::prefix('employee')->group(function () {
        Route::get('/departments/all', 'DepartmentController@all')->name('department.all');
        Route::get('/positions/all', 'PositionController@all')->name('position.all');
        Route::get('/divisions/all', 'DivisionController@all')->name('division.all');
        Route::get('/employees/all', 'EmployeeController@all')->name('employee.all');
        Route::apiResources([
            'departments' => 'DepartmentController',
            'positions' => 'PositionController',
            'divisions' => 'DivisionController',
            'employees' => 'EmployeeController',
            'reschedules' => 'RescheduleController',
        ]);
        Route::name('employees.')->group(function () {
            Route::apiResource('documents', 'EmployeeDocController');
        });
    });
    Route::get('/job-type/all', 'JobTypeController@all')->name('job_type.all');
    Route::get('/shift-schedules/all', 'ShiftScheduleController@all')->name('shift_schedule.all');
    Route::apiResources([
        'allowances' => 'AllowanceController',
        'shift-schedules' => 'ShiftScheduleController',
        'attendances' => 'AttendanceController',
        'loans' => 'LoanController',
    ]);
    Route::prefix('loan')->group(function () {
        Route::get('employees/{employee}/installments/{date?}', 'LoanInstallmentController@installment')->name('loans.employee.installment');
        Route::apiResources([
            'installments' => 'LoanInstallmentController',
        ]);
    });
    Route::prefix('salaries')->name('salaries.')->group(function () {
        Route::apiResource('/generate', 'SalaryGenerationController')->only(['index', 'store']);
        Route::get('/salary/{salary}/change-status', 'SalaryController@changeStatus')->name('salary.change-status');
        Route::apiResource('/salary', 'SalaryController');
    });

    Route::prefix('leave')->name('leave.')->group(function () {
        Route::get('/types/all', 'LeaveTypeController@all')->name('types.all');
        Route::apiResources([
            'holidays' => 'HolidayController',
            'types' => 'LeaveTypeController',
            'applications' => 'LeaveApplicationController',
        ]);
    });
    Route::get('/grade/all', 'GradeController@all')->name('grade.all');
    Route::apiResource('grades', 'GradeController');

    Route::prefix('trainings')->name('trainings.')->group(function () {
        Route::get('/types/all', 'TrainingTypeController@all')->name('types.all');
        Route::get('/trainers/all', 'TrainerController@all')->name('trainers.all');
        Route::apiResources([
            'types' => 'TrainingTypeController',
            'trainers' => 'TrainerController',
            'workshops' => 'WorkshopController',
        ]);
    });
    Route::prefix('report')->name('report.')->group(function () {
        Route::get('loan', 'ReportController@loan')->name('loan');
    });
    Route::name('hrm.')->group(function () {
        Route::apiResources([
            'settings' => 'SettingsController',
        ]);
    });

//    Common Menu items
    Route::get('/notices', 'Common\NoticeController@index')->name('hrm.notices.index');
    Route::get('/messenger', 'Common\MessengerController@index')->name('hrm.messenger.index');
    Route::get('/calender', 'Common\CalenderController@index')->name('hrm.calender.index');
    Route::get('/activities', 'Common\ActivityController@index')->name('hrm.activities.index');
    Route::get('/users', 'Common\UserController@index')->name('hrm.users.index');
    Route::get('/roles', 'Common\RoleController@index')->name('hrm.roles.index');
    Route::get('/user-role', 'Common\UserRoleController@index')->name('hrm.user-role.index');
    Route::get('/role-permission', 'Common\RolePermissionController@index')->name('hrm.role-permission.index');
    Route::get('/user-permission', 'Common\UserPermissionController@index')->name('hrm.user-permission.index');
    Route::get('/languages', 'Common\LanguageController@index')->name('hrm.languages.index');
    Route::get('/languages/{language}/phrases', 'Common\LanguagePhraseController@index')->name('hrm.languages.phrases.index');
    Route::get('/phrases', 'Common\PhraseController@index')->name('hrm.phrases.index');
    Route::get('/currencies', 'Common\CurrencyController@index')->name('hrm.currencies.index');
    Route::get('/profile', 'Common\ProfileController@index')->name('hrm.profile.index');
});

