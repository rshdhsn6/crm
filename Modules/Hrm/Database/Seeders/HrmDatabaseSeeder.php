<?php

namespace Modules\Hrm\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class HrmDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");

        $this->call([
            JobTypesTableSeeder::class,
            AllowancesTableSeeder::class,
            LeaveTypesTableSeeder::class,
            DepartmentsTableSeeder::class,
            DivisionsTableSeeder::class,
            ShiftSchedulesTableSeeder::class,
            PositionsTableSeeder::class,
            GradeTableSeeder::class,
            EmployeesTableSeeder::class,
        ]);
    }
}
