<?php

namespace Modules\Hrm\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Hrm\Entities\Employee;
use Modules\Hrm\Entities\EmployeeAddress;
use Modules\Hrm\Entities\EmployeeDocument;
use Modules\Hrm\Entities\EmployeeOfficial;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");

        factory(Employee::class, 3)->create()->each(function ($employee) {
                $employee->address()->save(factory(EmployeeAddress::class)->make());
                $employee->official()->save(factory(EmployeeOfficial::class)->make());
                $employee->documents()->saveMany(factory(EmployeeDocument::class,3)->make());
            });

    }
}
