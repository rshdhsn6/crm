<?php

namespace Modules\Hrm\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Hrm\Entities\Allowance;

class AllowancesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");

        DB::connection('hrm')->table('allowances')->insert([
            ['name' => 'House Rent', 'type'=>'0'],
            ['name' => 'Medical', 'type'=>'0'],
            ['name' => 'Educational', 'type'=>'0'],
            ['name' => 'Meal', 'type'=>'1'],
            ['name' => 'Transportation', 'type'=>'1'],
        ]);
    }
}
