<?php

namespace Modules\Hrm\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LeaveTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");

        DB::connection('hrm')->table('leave_types')->insert([
            ['name' => 'Maternity','days'=>20],
            ['name' => 'Sickness','days'=>10],
            ['name' => 'Others','days'=>10]
        ]);
    }
}
