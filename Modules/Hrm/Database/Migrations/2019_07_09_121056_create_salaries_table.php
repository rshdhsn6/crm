<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('hrm')->create('salaries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id');
            $table->string('from');
            $table->string('to');
            $table->integer('amount')->default(0);
            $table->unsignedBigInteger('generated_by')->nullable();
            $table->unsignedBigInteger('paid_by')->nullable();
            $table->string('paid_at')->nullable();
            $table->boolean('paid_status')->default(0);
            $table->timestamps();

            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');
            $table->foreign('generated_by')->references('id')->on('employees')->onDelete('set null');
            $table->foreign('paid_by')->references('id')->on('employees')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('hrm')->dropIfExists('salaries');
    }
}
