<?php

use Illuminate\Database\Query\Expression;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $dbName = DB::connection()->getDatabaseName();
        Schema::connection('hrm')->create('employees', function (Blueprint $table) use ($dbName){
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('phone')->unique();
            $table->string('photo')->nullable();
            $table->boolean('gender')->default(0);
            $table->string('birthday')->nullable();
            $table->unsignedBigInteger('marital_id');
            $table->unsignedBigInteger('religion_id');
            $table->timestamps();

            $table->foreign('marital_id')->references('id')->on(new Expression($dbName.'.marital_infos'))->onDelete('cascade');
            $table->foreign('religion_id')->references('id')->on(new Expression($dbName.'.religions'))->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('hrm')->dropIfExists('employees');
    }
}
