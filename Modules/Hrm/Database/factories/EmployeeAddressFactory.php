<?php

use Faker\Generator as Faker;
use Modules\Hrm\Entities\EmployeeAddress;

$factory->define(EmployeeAddress::class, function (Faker $faker) {
    return [
        'street' => $faker->streetAddress,
        'city' => $faker->city,
        'district' => $faker->city,
        'zip_code' => $faker->randomNumber(3),
        'country' => $faker->country,
    ];
});
