<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Hrm\Entities\Division::class, function (Faker $faker) {
    return [
        'name'=>$faker->jobTitle,
        'dept_id'=>rand(1,5)
    ];
});
