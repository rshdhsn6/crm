<?php

use Faker\Generator as Faker;
use App\MaritalInfo;
use App\Religion;
use Modules\Hrm\Entities\Employee;

$factory->define(Employee::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'gender' => $faker->boolean,
        'birthday' => $faker->date('Y-m-d'),
        'marital_id' => MaritalInfo::all()->random()->id,
        'religion_id' => Religion::all()->random()->id,
        'photo' => null,
    ];
});
