<?php

use Faker\Generator as Faker;
use Modules\Hrm\Entities\Allowance;
use Modules\Hrm\Entities\GradeAllowance;

$factory->define(GradeAllowance::class, function (Faker $faker) {
    return [
        'allowance_id' => Allowance::all()->random()->id,
        'type' => $faker->boolean,
        'rate' => $faker->randomNumber(1)
    ];
});
