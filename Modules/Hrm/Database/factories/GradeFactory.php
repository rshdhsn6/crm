<?php

use Faker\Generator as Faker;
use Modules\Hrm\Entities\Grade;

$factory->define(Grade::class, function (Faker $faker) {
    return [
        'name'=> $faker->unique()->word,
        'type' => $faker->boolean,
        'rate' => $faker->randomNumber(3),
        'overtime_rate' => $faker->randomNumber(3),
    ];
});
