<?php

use Faker\Generator as Faker;
use Modules\Hrm\Entities\EmployeeDocument;

$factory->define(EmployeeDocument::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'file' => $faker->file(public_path('demo'), public_path('storage\hrm\employee\files'), false),
    ];
});
