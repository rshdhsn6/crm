<?php

use Faker\Generator as Faker;
use Modules\Hrm\Entities\Division;
use Modules\Hrm\Entities\Employee;
use Modules\Hrm\Entities\EmployeeOfficial;
use Modules\Hrm\Entities\JobType;
use Modules\Hrm\Entities\Position;
use Modules\Hrm\Entities\ShiftSchedule;
use Modules\Hrm\Entities\Grade;
use Carbon\Carbon;

$factory->define(EmployeeOfficial::class, function (Faker $faker) {
    $from = $faker->date('Y-m-d');
    $to = Carbon::parse($from)->addMonths($faker->numberBetween(1, 12))->format('Y-m-d');
    return [
        'division' => Division::all()->random()->id,
        'position' => Position::all()->random()->id,
        'job_type' => JobType::all()->random()->id,
        'from' => $from,
        'to' => $faker->randomElement([null, $to]),
        'supervisor' => Employee::all()->random()->id,
        'shift' => ShiftSchedule::all()->random()->id,
        'grade_id' => Grade::all()->random()->id,
    ];
});
