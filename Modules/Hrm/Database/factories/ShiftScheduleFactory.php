<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Hrm\Entities\ShiftSchedule::class, function (Faker $faker) {
    return [
        'name'=>$faker->word,
        'from'=>$from = $faker->time('H:i'),
        'to'=>$faker->time('H:i',$from)
    ];
});
