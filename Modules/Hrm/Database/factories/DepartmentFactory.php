<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Hrm\Entities\Department::class, function (Faker $faker) {
    return [
        'name'=>$faker->city,
        'desc'=>$faker->text
    ];
});
