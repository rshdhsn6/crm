<?php

use Faker\Generator as Faker;

$factory->define(\Modules\Hrm\Entities\Position::class, function (Faker $faker) {
    return [
        'name' => $faker->jobTitle,
        'desc' => $faker->text($maxNbChars = 200)
    ];
});
