<?php

namespace Modules\Hrm\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Modules\Hrm\Entities\Attendance;
use Modules\Hrm\Entities\Employee;
use Yajra\DataTables\DataTables;

class AttendanceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $today = date('Y-m-d');
            $employees = Employee::with(['official.division', 'attendances' => function ($query) use ($today) {
                $query->whereDate('date', $today);
            }])->get();
            return DataTables::of($employees)
//                ->addIndexColumn()
                ->addColumn('length', function ($employee) {
                    $checkin = $employee->attendances->first()['checkin'];
                    $checkout = $employee->attendances->first()['checkout'];
                    if ($checkin && $checkout) {
                        $start = Carbon::parse($checkin);
                        $end = Carbon::parse($checkout);
                        return $start->diff($end)->format('%H hour %I minutes');
                    }
                })
                ->addColumn('checkin', function ($employee) use ($today) {
                    if ($employee->attendances->first()['checkin']) {
                        return $employee->attendances->first()['checkin'];
                    }
                    if (Auth::user()->can('hrm-attendance-add')) {
                        return "<button class='btn btn-success check-btn' data-id='$employee->id' data-date='$today' data-present='1'>Check In</button>";
                    }
                })
                ->addColumn('checkout', function ($employee) use ($today) {
                    if ($employee->attendances->first()['checkout']) {
                        return $employee->attendances->first()['checkout'];
                    }
                    if (Auth::user()->can('hrm-attendance-add')) {
                        return "<button class='btn btn-success check-btn' data-id='$employee->id' data-date='$today' data-present='0'>Check Out</button>";
                    }
                })
                ->addColumn('date', function ($employee) use ($today) {
                    return $today;
                })
                ->addColumn('action', function ($employee) use ($today) {
                    $btn = '<div class="table-actions">';
                    if (count($employee->attendances)) {
                        if (Auth::user()->can('hrm-attendance-edit')) {
                            if ($employee->attendances->first()['checkin'] && $employee->attendances->first()['checkout']) {
                                $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#attendanceModal" data-whatever="1" data-id="' . $employee->attendances->first()['id'] . '"><i class="ik ik-edit-2"></i></a>';
                            }
                        }
                        if (Auth::user()->can('hrm-attendance-delete')) {
                            $btn .= '<a class="clear-btn" type="button" data-id="' . $employee->attendances->first()['id'] . '" title="Clear"><i class="ik ik-trash-2"></i></a>';
                        }
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['checkin', 'checkout', 'action'])
                ->make(true);
        }
        return view('hrm::attendance');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('hrm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'id' => 'required',
            'date' => 'required|date_format:Y-m-d',
        ];
        $messages = [
            'id.required' => 'The employee is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        if ($request->present) {
            $attendance = Attendance::create([
                'employee_id' => $request->id,
                'date' => $request->date,
                'checkin' => date('H:i:s')
            ]);
        } else {
            $attendance = Attendance::where('employee_id', $request->id)->whereDate('date', $request->date)->first();
            if (!$attendance) {
                return response()->json([
                    'message' => 'Please set checkin time first',
                ], 400);
            }
            $attendance = Attendance::updateOrCreate(
                ['employee_id' => $request->id, 'date' => $request->date],
                ['checkout' => date('H:i:s')]
            );
        }
        return $attendance;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request, Attendance $attendance)
    {
        if ($request->ajax()) {
            return $attendance;
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('hrm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Attendance $attendance)
    {
        if ($request->ajax()) {
            $rules = [
                'checkin' => 'required|date_format:H:i',
                'checkout' => 'required|date_format:H:i',
            ];
            $messages = [
                'checkin.required' => 'The checkin time is required.',
            ];

            Validator::make($request->all(), $rules, $messages)->validate();

            $beginHour = Carbon::parse($request->checkin);
            $endHour = Carbon::parse($request->checkout);
            if ($beginHour->gt($endHour)) {
                return response()->json([
                    'message' => 'Checkout Time should be greater than Checkin time',
                ], 400);
            }

            $attendance->checkin = $request->checkin;
            $attendance->checkout = $request->checkout;
            $attendance->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Request $request, Attendance $attendance)
    {
        if ($request->ajax()) {
            $attendance->delete();
            return $attendance;
        }
    }
}
