<?php

namespace Modules\Hrm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Modules\Hrm\Entities\Employee;
use Modules\Hrm\Entities\EmployeeDocument;
use Modules\Hrm\Entities\Position;

class EmployeeDocController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $employees = Employee::with('documents')->get();
        return view('hrm::employeeDoc', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('hrm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'employee' => 'required|integer',
            'title' => 'string|nullable',
            'doc' => 'file|nullable|mimes:jpeg,bmp,png,pdf,txt,doc,docx',
        ];
        $messages = [
            'employee.required' => 'The Employee is required.',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        $document = new EmployeeDocument();
        $document->employee_id = $request->employee;
        $document->title = $request->title;
        if ($request->hasFile('doc')) {
            $doc = $request->doc;
            $extension = $doc->getClientOriginalExtension();
            $name = microtime(true) . ".$extension";
            $doc->move(storage_path('/app/public/hrm/employee/files/'), $name);
            $document->file = $name;
        }
        $document->save();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request,EmployeeDocument $document)
    {
        if($request->ajax()){
            return $document;
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('hrm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, EmployeeDocument $document)
    {
        $rules = [
            'employee' => 'required|integer',
            'title' => 'string|nullable',
            'doc' => 'file|nullable|mimes:jpeg,bmp,png,pdf,txt,doc,docx',
        ];
        $messages = [
            'employee.required' => 'The Employee is required.',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        $document->employee_id = $request->employee;
        $document->title = $request->title;
        if ($request->hasFile('doc')) {
            $doc = $request->doc;
            $extension = $doc->getClientOriginalExtension();
            $name = microtime(true) . ".$extension";
            $doc->move(storage_path('/app/public/hrm/employee/files/'), $name);
            File::delete(storage_path('/app/public/hrm/employee/files/') . $document->file);
            $document->file = $name;
        }
        $document->save();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Request $request, EmployeeDocument $document)
    {
        if ($request->ajax()) {
            File::delete(storage_path('/app/public/hrm/employee/files/') . $document->file);
            $document->delete();
            return $document;
        }
    }
}
