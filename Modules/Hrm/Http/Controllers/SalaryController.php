<?php

namespace Modules\Hrm\Http\Controllers;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Carbon\CarbonTimeZone;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Hrm\Entities\Department;
use Modules\Hrm\Entities\Division;
use Modules\Hrm\Entities\Holiday;
use Modules\Hrm\Entities\LoanInstallment;
use Modules\Hrm\Entities\Salary;
use Yajra\DataTables\Facades\DataTables;

class SalaryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $salaries = Salary::with('employee.official', 'paidBy', 'details')->get();
            return Datatables::of($salaries)
                ->addIndexColumn()
                ->addColumn('department', function ($salary) {
                    return $salary->employee->official->division()->first()->department->name;
                })
                ->addColumn('division', function ($salary) {
                    return $salary->employee->official->division()->first()->name;
                })
                ->editColumn('from', function ($salary) {
                    return date('d-m-Y', strtotime($salary->from));
                })
                ->editColumn('to', function ($salary) {
                    return date('d-m-Y', strtotime($salary->to));
                })
                ->addColumn('salary', function ($salary) {
                    return $this->calculateSalary($salary);
                })
                ->editColumn('paid_by', function ($salary) {
                    if ($salary->paid_by) {
                        return $salary->paidBy()->first()->name;
                    }
                })
                ->editColumn('paid_at', function ($salary) {
                    if ($salary->paid_at) {
                        return date('d-m-Y', strtotime($salary->paid_at));
                    }
                })
                ->editColumn('status', function ($salary) {
                    if ($salary->paid_status) {
                        return "<span class='text-success'>Paid</span>";
                    } else {
                        if (Auth::user()->can('hrm-salary-pay')) {
                            return "<button class='btn btn-danger paid-btn' data-id='$salary->id'>

</button>";
                        }
                    }
                })
                ->addColumn('action', function ($salary) {
                    $btn = '<div class="table-actions">';
                    if (Auth::user()->can('hrm-salary-view')) {
                        $btn .= '<a class="view-btn" type="button" data-toggle="modal" data-target="#detailsModal" data-id="' . $salary->id . '"><i class="ik ik-eye"></i></a>';
                    }
                    if (Auth::user()->can('hrm-salary-print')) {
                        $btn .= '<a class="print-btn" type="button"  href="' . route('salaries.salary.show', $salary->id) . '"><i class="ik ik-printer"></i></a>';
                    }
                    if (Auth::user()->can('hrm-salary-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $salary->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['status', 'action'])
                ->make(true);
        }
        $departments = Department::all();
        $divisions = Division::all();
        return view('hrm::salary', compact('departments', 'divisions'));
    }

    public function calculateSalary(Salary $salary)
    {
        $salary->load('details', 'allowances.allowance');
        $salaryAmount = $salary->details->basic + $salary->details->overtime - $salary->details->loan;
        foreach ($salary->allowances as $allowance) {
            if ($allowance->type) {
//            calculate percent
                $amount = ($allowance->rate / $salary->details->rate) * 100;
            } else {
//              calculate currency;
                $amount = $allowance->rate;
            }
//            addition or deduction amount
            if ($allowance->allowance->type) {
                $salaryAmount -= $amount;
            } else {
                $salaryAmount += $amount;
            }
        }
        return round($salaryAmount);
    }

    /**
     * Generating Salaries.
     * @return Response
     */
    public function generate()
    {
        return view('hrm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request, Salary $salary)
    {
        $salary->load('employee.official.division.department', 'employee.official.position', 'employee.official.grade', 'details', 'allowances.allowance');
        if ($request->ajax()) {
            return $salary;
        }
        return view('hrm::salary_details', compact('salary'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('hrm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Request $request, Salary $salary)
    {
        if ($request->ajax()) {
            $salary->delete();
            return $salary;
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function changeStatus(Request $request, Salary $salary)
    {
        if ($request->ajax()) {
            $salary->paid_status = 1;
            $salary->paid_at = date('Y-m-d');
            $salary->paid_by = Auth::id();
            $salary->save();
//            add as installment
            $salary->load('details');
            if($salary->details->loan){
                $installment = new LoanInstallment();
                $installment->employee_id = $salary->employee_id;
                $installment->installment_amount = $salary->details->loan;
                $installment->paid = $salary->details->loan;
                $installment->date = date('Y-m-d');
                $installment->comment = "Deducted from salary";
                $installment->received_by = Auth::id();
                $installment->save();
            }
            return $salary;
        }
    }

}
