<?php

namespace Modules\Hrm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Modules\Hrm\Entities\Department;
use DataTables;

class DepartmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $departments = Department::all();
            return Datatables::of($departments)
                ->addIndexColumn()
                ->addColumn('action', function($department){
                    $btn = '<div class="table-actions">';
                    if(Auth::user()->can('hrm-department-edit')){
                        $btn .='<a class="edit-btn" type="button" data-toggle="modal" data-target="#departmentModal" data-whatever="1" data-id="'.$department->id.'"><i class="ik ik-edit-2"></i></a>';
                    }
                    if(Auth::user()->can('hrm-department-edit')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $department->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .='</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::department');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:hrm.departments,name',
            'desc' => 'required',
        ];
        $messages = [
            'desc.required' => 'The description field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $department = new Department();
        $department ->name = $request->name;
        $department->desc = $request->desc;
        $department->save();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request,Department $department)
    {
        if ($request->ajax()) {
            return $department;
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Department $department)
    {
        $rules = [
            'name' => 'required|unique:hrm.departments,name,'.$department->id,
            'desc' => 'required',
        ];
        $messages = [
            'desc.required' => 'The description field is required',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $department->name= $request->name;
        $department->desc = $request->desc;
        $department->save();
        return $department;
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Department $department)
    {
        $department->delete();
        return $department;
    }

    public function all(){
        return Department::all();
    }
}
