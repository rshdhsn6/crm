<?php

namespace Modules\Hrm\Http\Controllers\Common;

use App\Notice;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class NoticeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $notices = Notice::with('createdBy', 'documents')->get();
            return Datatables::of($notices)
                ->addIndexColumn()
                ->editColumn('created_by', function ($notice) {
                    return $notice->createdBy->name;
                })
                ->addColumn('files', function ($notice) {

                    if (count($notice->documents)) {
                        $files = '';
                        foreach ($notice->documents as $doc) {
                            $files .= "<a href='" . asset('storage/notices/' . $doc->file) . "' target='_blank'><i class='ik ik-file ik-3x text-green'></i></a>";
                        }
                        return $files;
                    };
                })
                ->addColumn('action', function ($notice) {
                    $btn = '<div class="table-actions">';
                    if (Auth::user()->can('hrm-notice-view')) {
                        $btn .= '<a class="view-btn" type="button" data-toggle="modal" data-target="#viewModal" data-id="' . $notice->id . '"><i class="ik ik-eye"></i></a>';
                    }
                    if (Auth::user()->can('hrm-notice-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#noticeModal" data-whatever="1" data-id="' . $notice->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if (Auth::user()->can('hrm-notice-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $notice->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['files', 'action'])
                ->make(true);
        }
        return view('hrm::common.notice');
    }

}
