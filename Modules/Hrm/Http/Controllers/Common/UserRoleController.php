<?php

namespace Modules\Hrm\Http\Controllers\Common;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class UserRoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $users = User::with('roles')->get();
            return Datatables::of($users)
                ->addIndexColumn()
                ->editColumn('roles', function ($user) {
                    return $user->roles->pluck('name')->implode(' , ');
                })
                ->addColumn('action', function ($user) {
                    $btn = '<div class="table-actions">';
                    if (Auth::user()->can('hrm-user-role-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#roleModal" data-whatever="1" data-id="' . $user->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if (Auth::user()->can('hrm-user-role-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $user->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::common.user_role');
    }
}
