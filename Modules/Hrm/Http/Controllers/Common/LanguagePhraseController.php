<?php

namespace Modules\Hrm\Http\Controllers\Common;

use App\Language;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

class LanguagePhraseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Language $language)
    {
        $phrases = DB::table('phrases')
            ->leftJoin('language_phrases', function ($join) use ($language) {
                $join->on('language_phrases.phrase_id', '=', 'phrases.id')
                    ->where('language_id', $language->id);
            })
            ->select('phrases.id', 'phrases.name', 'language_phrases.name as translation')
            ->get();
        $language->phrases = $phrases;
        return view('hrm::common.language_phrase', compact('language'));
    }
}
