<?php

namespace Modules\Hrm\Http\Controllers\Common;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $users = User::all();
            return Datatables::of($users)
                ->addIndexColumn()
                ->editColumn('is_approved', function ($user) {
                    if ($user->is_approved) {
                        return "<button class='btn btn-success status-btn' value='$user->id'>Approved</button>";
                    }
                    return "<button class='btn btn-danger status-btn' value='$user->id'>Blocked</button>";
                })
                ->addColumn('action', function ($user) {
                    $btn = '<div class="table-actions">';
                    if (Auth::user()->can('hrm-user-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#userModal" data-whatever="1" data-id="' . $user->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if (Auth::user()->can('hrm-user-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $user->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['is_approved', 'action'])
                ->make(true);
        }
        return view('hrm::common.user');
    }
}
