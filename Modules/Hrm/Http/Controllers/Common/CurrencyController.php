<?php

namespace Modules\Hrm\Http\Controllers\Common;

use App\Currency;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class CurrencyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $currencies = Currency::all();
            return Datatables::of($currencies)
                ->addIndexColumn()
                ->addColumn('action', function ($currency) {
                    $btn = '<div class="table-actions">';
                    if(Auth::user()->can('hrm-currency-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#currencyModal" data-whatever="1" data-id="' . $currency->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if(Auth::user()->can('hrm-currency-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $currency->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::common.currency');
    }
}
