<?php

namespace Modules\Hrm\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Modules\Hrm\Entities\Employee;
use Modules\Hrm\Entities\Loan;
use Modules\Hrm\Entities\LoanInstallment;
use Yajra\DataTables\Facades\DataTables;

class LoanInstallmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $installments = LoanInstallment::with('employee', 'receivedBy')->get();
            return Datatables::of($installments)
                ->addIndexColumn()
                ->editColumn('received_by', function ($installment) {
                    if ($installment->received_by) {
                        return $installment->receivedBy->name;
                    }
                })
                ->editColumn('date', function ($installment) {
                    return date('d-m-Y', strtotime($installment->date));
                })
                ->addColumn('action', function ($loan) {
                    $btn = '<div class="table-actions">';
                    if (Auth::user()->can('hrm-loan-installment-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#installmentModal" data-whatever="1" data-id="' . $loan->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if (Auth::user()->can('hrm-loan-installment-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $loan->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::loan_installment');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'employee' => 'required|integer',
            'installment_amount' => 'required|integer|min:0',
            'paid' => 'required|integer|min:0',
            'date' => 'required|date|date_format:d-M-Y',
            'received_by' => 'required|integer',
            'comment' => 'nullable',
        ];
        $messages = [
            'installment_amount.required' => 'The Installment Amount is required.',
            'received_by.required' => 'The Receivable person is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $installment = new LoanInstallment();
        $installment->employee_id = $request->employee;
        $installment->installment_amount = $request->installment_amount;
        $installment->paid = $request->paid;
        if ($request->date) {
            $installment->date = date('Y-m-d', strtotime($request->date));
        }
        $installment->received_by = $request->received_by;
        $installment->comment = $request->comment;
        $installment->save();
        return $installment;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request, LoanInstallment $installment)
    {
        if ($request->ajax()) {
            return $installment;
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, LoanInstallment $installment)
    {
        $rules = [
            'employee' => 'required|integer',
            'installment_amount' => 'required|integer|min:0',
            'paid' => 'required|integer|min:0',
            'date' => 'required|date|date_format:d-M-Y',
            'received_by' => 'required|integer',
            'comment' => 'nullable',
        ];
        $messages = [
            'installment_amount.required' => 'The Installment Amount is required.',
            'received_by.required' => 'The Receivable person is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();

        $installment->employee_id = $request->employee;
        $installment->installment_amount = $request->installment_amount;
        $installment->paid = $request->paid;
        if ($request->date) {
            $installment->date = date('Y-m-d', strtotime($request->date));
        }
        $installment->received_by = $request->received_by;
        $installment->comment = $request->comment;
        $installment->save();
        return $installment;
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Request $request, LoanInstallment $installment)
    {
        if ($request->ajax()) {
            $installment->delete();
            return $installment;
        }
    }

    public function installment(Employee $employee, $date = null)
    {
        if ($date) {
            $date = Carbon::parse(base64_decode($date));
            $installments['loans'] = $employee->loan->filter(function ($loan) use ($date) {
                $repayment_from = Carbon::parse($loan->repayment_from);
                $repayment_to = Carbon::parse($loan->repayment_to);
                if ($date->between($repayment_from, $repayment_to)) {
                    return true;
                }
                return false;
            });
            $installments['installment_Amount'] = $installments['loans']->sum(function ($loan) {
                return round($loan->amount / $loan->installment, 2);
            });
        } else {
            $installments = $employee->loan();
        }
        return $installments;
    }
}
