<?php

namespace Modules\Hrm\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Hrm\Entities\Loan;
use Yajra\DataTables\Facades\DataTables;

class LoanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $loans = Loan::with('employee', 'approvedBy')->get();
            return Datatables::of($loans)
                ->addIndexColumn()
                ->editColumn('approved_by', function ($loan) {
                    if ($loan->approved_by) {
                        return $loan->approvedBy->name;
                    }
                })
                ->editColumn('repayment_from', function ($loan) {
                    return date('d-m-Y', strtotime($loan->repayment_from));
                })
                ->editColumn('approved_at', function ($loan) {
                    if ($loan->approved_at) {
                        return date('d-m-Y', strtotime($loan->approved_at));
                    }
                })
                ->addColumn('action', function ($loan) {
                    $btn = '<div class="table-actions">';
                    if (Auth::user()->can('hrm-loan-edit')) {
                        $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#loanModal" data-whatever="1" data-id="' . $loan->id . '"><i class="ik ik-edit-2"></i></a>';
                    }
                    if (Auth::user()->can('hrm-loan-delete')) {
                        $btn .= '<a class="delete-btn" type="button" data-id="' . $loan->id . '"><i class="ik ik-trash-2"></i></a>';
                    }
                    $btn .= '</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('hrm::loan');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'employee' => 'required',
            'amount' => 'required',
            'repayment_from' => 'required|date|date_format:d-M-Y',
            'installment' => 'required|integer|min:1',
            'approved_by' => 'nullable|integer',
            'approved_at' => 'required_with:approved_by,true|nullable|date|date_format:d-M-Y',
            'comment' => 'nullable',
        ];
        $messages = [
            'repayment_from.required' => 'The Repayment from field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $loan = new Loan();
        $loan->employee_id = $request->employee;
        $loan->amount = $request->amount;
        $loan->repayment_from = Carbon::parse(date('Y-m-d', strtotime($request->repayment_from)))->startOfMonth();
        $loan->repayment_to = Carbon::parse($loan->repayment_from)->addMonths($request->installment)->endOfMonth();
        $loan->installment = $request->installment;
        $loan->approved_by = $request->approved_by;
        if ($request->approved_at) {
            $loan->approved_at = date('Y-m-d ', strtotime($request->approved_at)) . date('H:i:s');
        }
        $loan->comment = $request->comment;
        $loan->save();
        return $loan;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show(Request $request, Loan $loan)
    {
        if ($request->ajax()) {
            return $loan;
        }
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Loan $loan)
    {
        $rules = [
            'employee' => 'required',
            'amount' => 'required',
            'repayment_from' => 'required|date|date_format:d-M-Y',
            'installment' => 'required|integer|min:1',
            'approved_by' => 'nullable|integer',
            'approved_at' => 'required_with:approved_by,true|nullable|date|date_format:d-M-Y',
            'comment' => 'nullable',
        ];
        $messages = [
            'repayment_from.required' => 'The Repayment from field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $loan->employee_id = $request->employee;
        $loan->amount = $request->amount;
        $loan->repayment_from = Carbon::parse(date('Y-m-d', strtotime($request->repayment_from)))->startOfMonth();
        $loan->repayment_to = Carbon::parse($loan->repayment_from)->addMonths($request->installment)->endOfMonth();
        $loan->installment = $request->installment;
        $loan->approved_by = $request->approved_by;
        if ($request->approved_at) {
            $loan->approved_at = date('Y-m-d ', strtotime($request->approved_at)) . date('H:i:s');
        }
        $loan->comment = $request->comment;
        $loan->save();
        return $loan;
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Request $request, Loan $loan)
    {
        if ($request->ajax()) {
            $loan->delete();
            return $loan;
        }
    }
}
