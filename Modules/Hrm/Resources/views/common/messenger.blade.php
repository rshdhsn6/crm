@extends('hrm::master')
@section('title')
    {{__('Messenger')}}
@endsection
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-inbox bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{__('Messenger')}}</h5>
                            <span>{{__('Do chatting, make happy & Feel happy')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{url('/')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">{{__('Settings')}}</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{__('Messenger')}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <messenger-component :auth="{{ Auth::user() }}"></messenger-component>
    </div>

@endsection
@section('extraCSS')
    <style>

    </style>
@endsection
@section('extraJS')
    <script>
        $(document).ready(function () {

        });
    </script>
@endsection
