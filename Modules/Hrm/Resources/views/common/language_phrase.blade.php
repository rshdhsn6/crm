@extends('hrm::master')
@section('title')
    {{__('Phrases')}}
@endsection,
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-inbox bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{__("Phrases")}}</h5>
                            <span>{{__("Phrases of Languages")}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{url('/')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">{{__("Settings")}}</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{__("Language")}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-block">
                        <h3 class="float-left"> {{"$language->name ($language->code)"}} </h3>
                        <div class="float-right">
                            @can('hrm-language-phrase')
                                <button class="btn btn-success save-btn">{{__("Update")}}</button>
                            @endcan
                            <button class="btn btn-warning clear-all">{{__("Clear all")}}</button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="dt-responsive">
                            <table id="phraseTBL" class="table nowrap">
                                <thead>
                                <tr>
                                    <th>{{__("Phrase")}}</th>
                                    <th>{{__("Value")}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($language->phrases as $phrase)
                                    <tr>
                                        <td>{{$phrase->name}}</td>
                                        <td>
                                            <input type="text" class="form-control" value="{{$phrase->translation}}"
                                                   name="{{$phrase->id}}"/>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extraJS')
    <script>
        $(document).ready(function () {
            var oTable = $('.table').DataTable({
                paging: false,
            });
            $(document).on('click', '.save-btn', function () {
                let data = $('#phraseTBL input').serialize();
                $.ajax({
                    url: "{{route('languages.phrases.store',$language->id)}}",
                    type: 'POST',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        showSuccessToast('{{__("Updated Successfully")}}');
                        window.location.replace("{{route('hrm.languages.index')}}");
                        setTimeout("window.location.replace({{route('languages.index')}})", 10000);
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
        });
    </script>
@endsection

