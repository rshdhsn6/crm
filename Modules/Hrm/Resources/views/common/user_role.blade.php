@extends('hrm::master')
@section('title')
    {{__('Roles')}}
@endsection
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-inbox bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{__('User Roles')}}</h5>
                            <span>{{__('Roles of users')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('hrm')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">{{__('Users')}}</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{__('Role')}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-block">
                        <h3 class="float-left"> {{__('Roles')}} </h3>
                    </div>
                    <div class="card-body">
                        <div class="dt-responsive">
                            <table id="#" class="table nowrap">
                                <thead>
                                <tr>
                                    <th>{{__('ID')}}</th>
                                    <th>{{__('User')}}</th>
                                    <th>{{__('Roles')}}</th>
                                    <th>{{__('Created At')}}</th>
                                    <th class="text-right"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Role Modal -->
    <div class="modal animated zoomIn faster" id="roleModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Role')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="RoleForm" action="javascript:void(0)">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Role')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <select class="select2 form-control" id="role" name="role[]" multiple="multiple"
                                        required>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('extraJS')
    <script>
        $(document).ready(function () {
            var oTable = $('.table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('user-role.index') }}",
                deferRender: true,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'roles', name: 'roles'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            $('.select2').select2();
            $('.modal').on('hidden.bs.modal', function (e) {
                $('form').trigger("reset");
                $('.select2').select2();
            });
            $('.modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var modal = $(this);
                modal.find('.modal-title').text('{{__("Edit User Roles")}}');
                var id = button.data('id');
                $.ajax({
                    url: '{{route('user-role.index')}}/' + id,
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        if (response.roles) {
                            let roles = response.roles.map(function (value, key) {
                                return value.id;
                            });
                            modal.find('#role').val(roles).trigger('change');
                        }
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
                modal.find('button[type=submit]').addClass('update-btn').text('{{__("Update")}}').val(id);
            });
            loadRole();

            function loadRole() {
                $.ajax({
                    url: '{{route('role.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#role').empty();
                        $.each(response, function (i, role) {
                            $('#role').append($('<option>', {
                                value: role.id,
                                text: role.name
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }

            $(document).on('click', '.update-btn', function () {
                let data = $('form').serialize();
                let id = $(this).val();
                $.ajax({
                    url: "{{ route('user-role.index')}}/" + id,
                    type: 'PUT',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#roleModal').modal('toggle');
                        showSuccessToast('{{__("Updated Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });

            $(document).on('click', '.delete-btn', function (e) {
                e.preventDefault(); // does not go through with the link.
                let confirmation = confirm("{{__('Are you sure to clear Role')}} ?");
                if (!confirmation) {
                    return;
                }
                var id = $(this).data('id');
                $.ajax({
                    url: '{{ route('user-role.index') }}/' + id,
                    type: 'DELETE',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        showSuccessToast('{{__("Cleared Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });

        });
    </script>
@endsection

