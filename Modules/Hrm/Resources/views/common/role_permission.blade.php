@extends('hrm::master')
@section('title')
    {{__('Role Permissions')}}
@endsection
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-inbox bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{__('Role Permissions')}}</h5>
                            <span>{{__('Permissions of roles')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('hrm')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">{{__('Users')}}</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{__('Role')}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-block">
                        <h3 class="float-left"> {{__('Role Permissions')}} </h3>
                        <div class="float-right">
                            @can('hrm-permission-edit')
                                <button class="btn btn-success update-btn">{{__('Update')}}</button>
                            @endcan
                            <button class="btn btn-warning clear-all">{{__('Clear all')}}</button>
                        </div>
                    </div>
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>{{__('Super Admin will get all permissions weather it is set or not')}}</strong> .
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="ik ik-x"></i>
                        </button>
                    </div>
                    <div class="card-body">
                        <div class="col-md-4  mx-auto">
                            <select class="form-control select2" id="roledropdown">
                            </select>
                        </div>
                        <div class="row display-item hidden">
                            <div class="col-md-12">
                                <nav>
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        {{--                                        <a class="nav-item nav-link active" id="nav-general-tab" data-toggle="tab"--}}
                                        {{--                                           href="#nav-general"--}}
                                        {{--                                           role="tab" aria-controls="nav-general" aria-selected="false">General</a>--}}
                                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab"
                                           href="#nav-home"
                                           role="tab" aria-controls="nav-home" aria-selected="true">HRM</a>
                                    </div>
                                </nav>
                                <div class="tab-content" id="nav-tabContent">
                                    {{--                                    <div class="tab-pane fade p-5 show active" id="nav-general" role="tabpanel"--}}
                                    {{--                                         aria-labelledby="nav-general-tab">--}}
                                    {{--                                        Others coming soon....--}}
                                    {{--                                    </div>--}}
                                    <div class="tab-pane fade p-5 show active" id="nav-home" role="tabpanel"
                                         aria-labelledby="nav-home-tab">
                                        <div class="dt-responsive">
                                            <table id="#" class="table nowrap">
                                                <thead>
                                                <th>{{__('Section')}}</th>
                                                <th class="text-center">{{__('Permissions')}}</th>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale"> {{__('Position')}} </label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-position-view"
                                                                   value="hrm-position-view">
                                                            <label class="form-check-label" for="hrm-position-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-position-add"
                                                                   value="hrm-position-add">
                                                            <label class="form-check-label" for="hrm-position-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-position-edit"
                                                                   value="hrm-position-edit">
                                                            <label class="form-check-label" for="hrm-position-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-position-delete"
                                                                   value="hrm-position-delete">
                                                            <label class="form-check-label" for="hrm-position-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale"> {{__('Department')}} </label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-department-view"
                                                                   value="hrm-department-view">
                                                            <label class="form-check-label" for="hrm-department-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-department-add"
                                                                   value="hrm-department-add">
                                                            <label class="form-check-label" for="hrm-department-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-department-edit"
                                                                   value="hrm-department-edit">
                                                            <label class="form-check-label" for="hrm-department-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-department-delete"
                                                                   value="hrm-department-delete">
                                                            <label class="form-check-label" for="hrm-department-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale"> {{__('Division')}} </label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-division-view"
                                                                   value="hrm-division-view">
                                                            <label class="form-check-label" for="hrm-division-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-division-add"
                                                                   value="hrm-division-add">
                                                            <label class="form-check-label" for="hrm-division-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-division-edit"
                                                                   value="hrm-division-edit">
                                                            <label class="form-check-label" for="hrm-division-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-division-delete"
                                                                   value="hrm-division-delete">
                                                            <label class="form-check-label" for="hrm-division-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale"> {{__('Employee')}} </label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-employee-view"
                                                                   value="hrm-employee-view">
                                                            <label class="form-check-label" for="hrm-employee-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-employee-add"
                                                                   value="hrm-employee-add">
                                                            <label class="form-check-label" for="hrm-employee-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-employee-edit"
                                                                   value="hrm-employee-edit">
                                                            <label class="form-check-label" for="hrm-employee-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-employee-delete"
                                                                   value="hrm-employee-delete">
                                                            <label class="form-check-label" for="hrm-employee-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale"> {{__('Training Types')}} </label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-training-type-view"
                                                                   value="hrm-training-type-view">
                                                            <label class="form-check-label"
                                                                   for="hrm-training-type-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-training-type-add"
                                                                   value="hrm-training-type-add">
                                                            <label class="form-check-label" for="hrm-training-type-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-training-type-edit"
                                                                   value="hrm-training-type-edit">
                                                            <label class="form-check-label"
                                                                   for="hrm-training-type-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-training-type-delete"
                                                                   value="hrm-training-type-delete">
                                                            <label class="form-check-label"
                                                                   for="hrm-training-type-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale"> {{__('Trainer')}} </label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-trainer-view"
                                                                   value="hrm-trainer-view">
                                                            <label class="form-check-label" for="hrm-trainer-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-trainer-add"
                                                                   value="hrm-trainer-add">
                                                            <label class="form-check-label" for="hrm-trainer-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-trainer-edit"
                                                                   value="hrm-trainer-edit">
                                                            <label class="form-check-label" for="hrm-trainer-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-trainer-delete"
                                                                   value="hrm-trainer-delete">
                                                            <label class="form-check-label" for="hrm-trainer-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale"> {{__('Workshop')}} </label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-workshop-view"
                                                                   value="hrm-workshop-view">
                                                            <label class="form-check-label" for="hrm-workshop-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-workshop-add"
                                                                   value="hrm-workshop-add">
                                                            <label class="form-check-label" for="hrm-workshop-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-workshop-edit"
                                                                   value="hrm-workshop-edit">
                                                            <label class="form-check-label" for="hrm-workshop-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-workshop-delete"
                                                                   value="hrm-workshop-delete">
                                                            <label class="form-check-label" for="hrm-workshop-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale"> {{__('Salary Grade')}}</label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-salary-grade-view"
                                                                   value="hrm-salary-grade-view">
                                                            <label class="form-check-label" for="hrm-salary-grade-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-salary-grade-add"
                                                                   value="hrm-salary-grade-add">
                                                            <label class="form-check-label" for="hrm-salary-grade-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-salary-grade-edit"
                                                                   value="hrm-salary-grade-edit">
                                                            <label class="form-check-label" for="hrm-salary-grade-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-salary-grade-delete"
                                                                   value="hrm-salary-grade-delete">
                                                            <label class="form-check-label"
                                                                   for="hrm-salary-grade-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale"> {{__('Salary')}} </label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-salary-view"
                                                                   value="hrm-salary-view">
                                                            <label class="form-check-label" for="hrm-salary-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-salary-add"
                                                                   value="hrm-salary-add">
                                                            <label class="form-check-label" for="hrm-salary-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-salary-print"
                                                                   value="hrm-salary-print">
                                                            <label class="form-check-label" for="hrm-salary-print">
                                                                {{__('Print')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-salary-delete"
                                                                   value="hrm-salary-delete">
                                                            <label class="form-check-label" for="hrm-salary-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-salary-pay"
                                                                   value="hrm-salary-pay">
                                                            <label class="form-check-label" for="hrm-salary-pay">
                                                                {{__('Pay')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale"> {{__('Attendance')}} </label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-attendance-view"
                                                                   value="hrm-attendance-view">
                                                            <label class="form-check-label" for="hrm-attendance-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-attendance-add"
                                                                   value="hrm-attendance-add">
                                                            <label class="form-check-label" for="hrm-attendance-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-attendance-edit"
                                                                   value="hrm-attendance-edit">
                                                            <label class="form-check-label" for="hrm-attendance-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-attendance-delete"
                                                                   value="hrm-attendance-delete">
                                                            <label class="form-check-label" for="hrm-attendance-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale"> {{__('Shift Schedule')}}</label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-shift-view"
                                                                   value="hrm-shift-view">
                                                            <label class="form-check-label" for="hrm-shift-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-shift-add"
                                                                   value="hrm-shift-add">
                                                            <label class="form-check-label" for="hrm-shift-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-shift-edit"
                                                                   value="hrm-shift-edit">
                                                            <label class="form-check-label" for="hrm-shift-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-shift-delete"
                                                                   value="hrm-shift-delete">
                                                            <label class="form-check-label" for="hrm-shift-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale"> {{__('Shift Reschedule')}}</label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-reshift-view"
                                                                   value="hrm-reshift-view">
                                                            <label class="form-check-label" for="hrm-reshift-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-reshift-add"
                                                                   value="hrm-reshift-add">
                                                            <label class="form-check-label" for="hrm-reshift-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-reshift-edit"
                                                                   value="hrm-reshift-edit">
                                                            <label class="form-check-label" for="hrm-reshift-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-reshift-delete"
                                                                   value="hrm-reshift-delete">
                                                            <label class="form-check-label" for="hrm-reshift-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale">{{__('Holidays')}}</label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-holiday-view"
                                                                   value="hrm-holiday-view">
                                                            <label class="form-check-label" for="hrm-holiday-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-holiday-add"
                                                                   value="hrm-holiday-add">
                                                            <label class="form-check-label" for="hrm-holiday-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-holiday-edit"
                                                                   value="hrm-holiday-edit">
                                                            <label class="form-check-label" for="hrm-holiday-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-holiday-delete"
                                                                   value="hrm-holiday-delete">
                                                            <label class="form-check-label" for="hrm-holiday-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale">{{__('Leave Type')}}</label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-leave-type-view"
                                                                   value="hrm-leave-type-view">
                                                            <label class="form-check-label" for="hrm-leave-type-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-leave-type-add"
                                                                   value="hrm-leave-type-add">
                                                            <label class="form-check-label" for="hrm-leave-type-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-leave-type-edit"
                                                                   value="hrm-leave-type-edit">
                                                            <label class="form-check-label" for="hrm-leave-type-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-leave-type-delete"
                                                                   value="hrm-leave-type-delete">
                                                            <label class="form-check-label" for="hrm-leave-type-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale">{{__('Leave Application')}}</label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-leave-application-view"
                                                                   value="hrm-leave-application-view">
                                                            <label class="form-check-label"
                                                                   for="hrm-leave-application-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-leave-application-add"
                                                                   value="hrm-leave-application-add">
                                                            <label class="form-check-label"
                                                                   for="hrm-leave-application-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-leave-application-edit"
                                                                   value="hrm-leave-application-edit">
                                                            <label class="form-check-label"
                                                                   for="hrm-leave-application-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-leave-application-delete"
                                                                   value="hrm-leave-application-delete">
                                                            <label class="form-check-label"
                                                                   for="hrm-leave-application-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale">{{__('Allowances')}}</label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-allowance-view"
                                                                   value="hrm-allowance-view">
                                                            <label class="form-check-label" for="hrm-allowance-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-allowance-add"
                                                                   value="hrm-allowance-add">
                                                            <label class="form-check-label" for="hrm-allowance-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-allowance-edit"
                                                                   value="hrm-allowance-edit">
                                                            <label class="form-check-label" for="hrm-allowance-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-allowance-delete"
                                                                   value="hrm-allowance-delete">
                                                            <label class="form-check-label" for="hrm-allowance-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale">{{__('Loan')}}</label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-loan-view"
                                                                   value="hrm-loan-view">
                                                            <label class="form-check-label" for="hrm-loan-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-loan-add"
                                                                   value="hrm-loan-add">
                                                            <label class="form-check-label" for="hrm-loan-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-loan-edit"
                                                                   value="hrm-loan-edit">
                                                            <label class="form-check-label" for="hrm-loan-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-loan-delete"
                                                                   value="hrm-loan-delete">
                                                            <label class="form-check-label" for="hrm-loan-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale">{{__('Loan Installment')}}</label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-loan-installment-view"
                                                                   value="hrm-loan-installment-view">
                                                            <label class="form-check-label"
                                                                   for="hrm-loan-installment-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-loan-installment-add"
                                                                   value="hrm-loan-installment-add">
                                                            <label class="form-check-label"
                                                                   for="hrm-loan-installment-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-loan-installment-edit"
                                                                   value="hrm-loan-installment-edit">
                                                            <label class="form-check-label"
                                                                   for="hrm-loan-installment-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-loan-installment-delete"
                                                                   value="hrm-loan-installment-delete">
                                                            <label class="form-check-label"
                                                                   for="hrm-loan-installment-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale">{{__('Report')}}</label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-report-loan"
                                                                   value="hrm-report-loan">
                                                            <label class="form-check-label" for="hrm-report-loan">
                                                                {{__('Loan')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale">{{__('Notice')}}</label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-notice-view"
                                                                   value="hrm-notice-view">
                                                            <label class="form-check-label" for="hrm-notice-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-notice-add"
                                                                   value="hrm-notice-add">
                                                            <label class="form-check-label" for="hrm-notice-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-notice-edit"
                                                                   value="hrm-notice-edit">
                                                            <label class="form-check-label" for="hrm-notice-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-notice-delete"
                                                                   value="hrm-notice-delete">
                                                            <label class="form-check-label" for="hrm-notice-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale">{{__('Log')}}</label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-log-view"
                                                                   value="hrm-log-view">
                                                            <label class="form-check-label" for="hrm-log-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale">{{__('User')}}</label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-user-view"
                                                                   value="hrm-user-view">
                                                            <label class="form-check-label" for="hrm-user-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-user-add"
                                                                   value="hrm-user-add">
                                                            <label class="form-check-label" for="hrm-user-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-user-edit"
                                                                   value="hrm-user-edit">
                                                            <label class="form-check-label" for="hrm-user-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-user-delete"
                                                                   value="hrm-user-delete">
                                                            <label class="form-check-label" for="hrm-user-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-user-status"
                                                                   value="hrm-user-status">
                                                            <label class="form-check-label" for="hrm-user-status">
                                                                {{__('Change Status')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale">{{__('Role')}}</label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-role-view"
                                                                   value="hrm-role-view">
                                                            <label class="form-check-label" for="hrm-role-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-role-add"
                                                                   value="hrm-role-add">
                                                            <label class="form-check-label" for="hrm-role-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-role-edit"
                                                                   value="hrm-role-edit">
                                                            <label class="form-check-label" for="hrm-role-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-role-delete"
                                                                   value="hrm-role-delete">
                                                            <label class="form-check-label" for="hrm-role-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale">{{__('User Role')}}</label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-user-role-view"
                                                                   value="hrm-user-role-view">
                                                            <label class="form-check-label" for="hrm-user-role-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-user-role-edit"
                                                                   value="hrm-user-role-edit">
                                                            <label class="form-check-label" for="hrm-user-role-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-user-role-delete"
                                                                   value="hrm-user-role-delete">
                                                            <label class="form-check-label" for="hrm-user-role-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale">{{__('Permission')}}</label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-permission-view"
                                                                   value="hrm-permission-view">
                                                            <label class="form-check-label" for="hrm-permission-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-permission-edit"
                                                                   value="hrm-permission-edit">
                                                            <label class="form-check-label" for="hrm-permission-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale">{{__('Settings')}}</label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-settings-view"
                                                                   value="hrm-settings-view">
                                                            <label class="form-check-label" for="hrm-settings-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-settings-edit"
                                                                   value="hrm-settings-edit">
                                                            <label class="form-check-label" for="hrm-settings-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale">{{__('Language')}}</label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-language-view"
                                                                   value="hrm-language-view">
                                                            <label class="form-check-label" for="hrm-language-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-language-add"
                                                                   value="hrm-language-add">
                                                            <label class="form-check-label" for="hrm-language-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-language-edit"
                                                                   value="hrm-language-edit">
                                                            <label class="form-check-label" for="hrm-language-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-language-delete"
                                                                   value="hrm-language-delete">
                                                            <label class="form-check-label" for="hrm-language-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-language-phrase"
                                                                   value="hrm-language-phrase">
                                                            <label class="form-check-label" for="hrm-language-phrase">
                                                                {{__('Phrase')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale">{{__('Phrase')}}</label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-phrase-view"
                                                                   value="hrm-phrase-view">
                                                            <label class="form-check-label" for="hrm-phrase-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-phrase-add"
                                                                   value="hrm-phrase-add">
                                                            <label class="form-check-label" for="hrm-phrase-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-phrase-edit"
                                                                   value="hrm-phrase-edit">
                                                            <label class="form-check-label" for="hrm-phrase-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-phrase-delete"
                                                                   value="hrm-phrase-delete">
                                                            <label class="form-check-label" for="hrm-phrase-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input class="form-check-input select-row" id="sale"
                                                               type="checkbox">
                                                        <label class="form-check-label" for="sale">{{__('Currency')}}</label>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-currency-view"
                                                                   value="hrm-currency-view">
                                                            <label class="form-check-label" for="hrm-currency-view">
                                                                {{__('View')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-currency-add"
                                                                   value="hrm-currency-add">
                                                            <label class="form-check-label" for="hrm-currency-add">
                                                                {{__('Add')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-currency-edit"
                                                                   value="hrm-currency-edit">
                                                            <label class="form-check-label" for="hrm-currency-edit">
                                                                {{__('Edit')}} </label>
                                                        </div>
                                                        <div class="form-check form-check-inline col-md-2">
                                                            <input class="form-check-input" type="checkbox"
                                                                   id="hrm-currency-delete"
                                                                   value="hrm-currency-delete">
                                                            <label class="form-check-label" for="hrm-currency-delete">
                                                                {{__('Delete')}} </label>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extraCSS')

@endsection
@section('extraJS')
    <script>
        $(document).ready(function () {
            $('.select2').select2();
            loadRole();

            function loadRole() {
                $.ajax({
                    url: '{{route('role.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#roledropdown').empty();
                        $('#roledropdown').append('<option selected value="" disabled>{{__("Select a role")}}</option>');
                        $.each(response, function (i, role) {
                            $('#roledropdown').append($('<option>', {
                                value: role.id,
                                text: role.name
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }

            $(document).on('change', '#roledropdown', function () {
                let id = $(this).val();
                if (id) {
                    $('.display-item').removeClass('hidden');
                    $('input:checkbox').prop('checked', false);
                    $.ajax({
                        url: '{{route('role-permission.index')}}/' + id,
                        type: 'GET',
                        beforeSend: function (request) {
                            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                        },
                        success: function (response) {
                            $.each(response.permissions, function (i, permission) {
                                $(`input[value='${permission.name}']`).prop('checked', true);
                            });
                        },
                        error: function (data) {
                            let msg = '';
                            if (data.responseJSON.errors) {
                                $.each(data.responseJSON.errors, function (i, error) {
                                    msg += '<p">' + error[0] + '</p>';
                                });
                            } else {
                                msg = data.responseJSON.message;
                            }
                            showDangerToast(msg);
                        }
                    });
                }
            });

            $('table td:first-child,table th').addClass('bg-info text-white pl-5 h6');
            $('table td:first-child input:checkbox').addClass('select-row');
            $(document).on('change', '.select-row', function (event) {
                let block = $(this).closest('tr');
                if ($(this).prop('checked')) {
                    block.find('input:checkbox').prop('checked', true);
                } else {
                    block.find('input:checkbox').prop('checked', false);
                }
            });
            $(".clear-all").click(function () {
                $('input:checkbox').prop('checked', false);
                return false;
            });
            $(document).on('click', '.update-btn', function () {
                let id = $('#roledropdown').val();
                if (!id) {
                    alert("{{__('Please Select a role')}}");
                    return;
                }
                let data = $('input:checkbox:checked').map(function () {
                    if (this.value != 'on') return this.value;
                }).get().join(',');
                $.ajax({
                    url: '{{route('role-permission.index')}}',
                    type: 'POST',
                    data: {'role': id, 'permissions': data},
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        showSuccessToast('{{__("Updated Successfully")}}');
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
        });
    </script>
@endsection

