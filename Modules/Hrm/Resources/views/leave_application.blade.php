@extends('hrm::master')
@section('title')
{{__('Leave Application')}}
@endsection
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-inbox bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{__('Leave Applications')}}</h5>
                            <span>{{__('All leave application with details')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('hrm')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">{{__('Employee')}}</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{__('Leave Application')}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header row">
                        <div class="col-sm-6 m-auto">
                            <div class="card-search with-adv-search dropdown">
                                <form action="javascript:void(0)">
                                    <input type="text" class="form-control global_filter" id="global_filter"
                                           placeholder="{{__('Search')}}.." required>
                                    <button type="submit" class="btn btn-icon"><i class="ik ik-search"></i></button>
                                    <button type="button" id="adv_wrap_toggler"
                                            class="adv-btn ik ik-chevron-down dropdown-toggle" data-toggle="dropdown"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="adv-search-wrap dropdown-menu dropdown-menu-right"
                                         aria-labelledby="adv_wrap_toggler">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control column_filter"
                                                           id="col1_filter" placeholder="Name" data-column="1">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="select2 column_filter typeList"
                                                            id="col2_filter" data-column="2">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="select2 column_filter approverList"
                                                            id="col9_filter" data-column="9">
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        @if(Auth::user()->can('hrm-leave-application-add'))
                            <button type="button" class="btn btn-success float-right" data-toggle="modal"
                                    data-target="#applicationModal" data-whatever="0"> + {{__('Add Leave Application')}}
                            </button>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="dt-responsive">
                            <table id="#" class="table nowrap">
                                <thead>
                                <tr>
                                    <th>{{__('ID')}}</th>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Type')}}</th>
                                    <th>{{__('Applied from')}}</th>
                                    <th>{{__('Applied to')}}</th>
                                    <th>{{__('Approved from')}}</th>
                                    <th>{{__('Approved to')}}</th>
                                    <th>{{__('Applied Days')}}</th>
                                    <th>{{__('Approved Days')}}</th>
                                    <th>{{__('Approved By')}}</th>
                                    <th>{{__('Created At')}}</th>
                                    <th class="text-right"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Leave Application Modal -->
    <div class="modal animated zoomIn faster" id="applicationModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Leave Application')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="leaveForm" action="javascript:void(0)" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Employee Name')}} <span
                                    class="text-danger">*</span></label>
                            <div class="col-sm-8 col-lg-8">
                                <select class="form-control select2 employee required" id="" name="employee" required>
                                    <option>Loading...</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Type')}} <span
                                    class="text-danger">*</span></label>
                            <div class="col-sm-8 col-lg-8">
                                <select class="form-control select2 required" id="leaveType" name="type" required>
                                    <option>Loading...</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Applied From')}} <span
                                    class="text-danger">*</span></label>
                            <div class="col-sm-8 col-lg-8">
                                <input class="form-control date required" type="text" id="appliedFrom"
                                       name="applied_from"
                                       placeholder="{{__('From date')}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Applied To')}} <span
                                    class="text-danger">*</span></label>
                            <div class="col-sm-8 col-lg-8">
                                <input class="form-control date required" type="text" id="appliedTo" name="applied_to"
                                       placeholder="{{__('To date')}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Days')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input class="form-control font-weight-bold form-txt-danger" id="appliedDay" type="text"
                                       placeholder="{{__('applied days')}}" value="0" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Approved From')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input class="form-control date" type="text" id="from" name="from"
                                       placeholder="{{__('From date')}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Approved to')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input class="form-control date" type="text" id="to" name="to" placeholder="{{__('To date')}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Days')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input class="form-control font-weight-bold form-txt-danger" type="text"
                                       id="approvedDay" placeholder="{{__('approved days')}}" value="0" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Approved By')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <select class="form-control select2 employee" id="" name="approved_by">
                                    <option>Loading...</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Reason')}}</label>
                            <div class="col-sm-8 col-lg-8">
                            <textarea class="form-control" id="exampleTextarea1" rows="4"
                                      placeholder="{{__('reason here')}}.." name="reason"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label"
                                   for="inlineFormInputGroup1">{{__('Documents')}}</label>
                            <div class="repeater col-sm-8 col-lg-8  mx-auto">
                                <div data-repeater-list="doc">
                                    <div data-repeater-item class="d-flex mb-2">
                                        <div class="input-group row no-gutters">
                                            <div class="input-group-prepend col-md-6 mr-1 col-xs-12">
                                                <input type="text" class="form-control"
                                                       placeholder="{{__('Subject of file')}}" name="title"/>
                                            </div>
                                            <div class="custom-file">
                                                <input type="file" class="form-control custom-file-input"
                                                       id="doc"
                                                       aria-describedby="docAddon" name="doc">
                                                <label class="custom-file-label" for="doc">{{__('Choose file')}}</label>
                                            </div>
                                        </div>
                                        <button data-repeater-delete type="button" class="btn btn-danger btn-icon ml-2">
                                            <i class="ik ik-trash-2"></i></button>
                                    </div>
                                </div>
                                <button data-repeater-create type="button" class="btn btn-success btn-icon ml-2 mb-2"><i
                                        class="ik ik-plus"></i></button>
                                {{__('Add New Document')}}
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('extraJS')
    <script src="{{asset('resources')}}/plugins/jquery.repeater/jquery.repeater.min.js"></script>
    <script>
        $(document).ready(function () {
            $('.select2').select2();
            // repeater
            $('.repeater').repeater({
                initEmpty: false,
                defaultValues: {
                    'title': 'Document name'
                },
                show: function () {
                    $(this).slideDown();
                },
                hide: function (deleteElement) {
                    if (confirm('{{__("Are you sure you want to delete this element")}}?')) {
                        $(this).slideUp(deleteElement);
                    }
                },
                ready: function (setIndexes) {

                },
                isFirstItemUndeletable: true
            });
            var oTable = $('.table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('leave.applications.index') }}",
                deferRender: true,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'employee.name', name: 'employee.name'},
                    {data: 'type.name', name: 'type.name'},
                    {data: 'applied_from', name: 'applied_from'},
                    {data: 'applied_to', name: 'applied_to'},
                    {data: 'from', name: 'from'},
                    {data: 'to', name: 'to'},
                    {data: 'applied_days', name: 'applied_days'},
                    {data: 'approved_days', name: 'approved_days'},
                    {data: 'approved_by', name: 'approved_by'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            loadEmployee();

            function loadEmployee() {
                $.ajax({
                    url: '{{route('employee.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('.employee,.approverList').empty();
                        $('.employee').append('<option selected value="" disabled>{{__("Select an employee")}}</option>');
                        $('.approverList').append('<option selected value="">{{__("Select an employee")}}</option>');
                        $.each(response, function (i, employee) {
                            $('.employee').append($('<option>', {
                                value: employee.id,
                                text: employee.name
                            }));
                            $('.approverList').append($('<option>', {
                                value: employee.name,
                                text: employee.name
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }

            leaveTypes();

            function leaveTypes() {
                $.ajax({
                    url: '{{route('leave.types.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#leaveType,.typeList').empty();
                        $('#leaveType').append('<option selected value="" disabled>{{__("Select Leave Type")}}</option>');
                        $('.typeList').append('<option selected value="">{{__("Select Leave Type")}}</option>');
                        $.each(response, function (i, type) {
                            $('#leaveType').append($('<option>', {
                                value: type.id,
                                text: type.name
                            }));
                            $('.typeList').append($('<option>', {
                                value: type.name,
                                text: type.name
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
                $(document).on('change', '.custom-file-input', function (event) {
                    var fileName = event.target.files[0].name;
                    let block = $(this).closest('.input-group');
                    block.find('.custom-file-label').addClass("selected").html(fileName);
                });
            }

            $(document).on('change', '#appliedFrom,#appliedTo', function () {
                let from = $('#appliedFrom').val();
                let to = $('#appliedTo').val();
                if (from && to) {
                    let differnce = moment(to).diff(moment(from), 'days') + 1;
                    $('#appliedDay').val(differnce);
                } else {
                    $('#appliedDay').val(0);
                }
            });
            $(document).on('change', '#from,#to', function () {
                let from = $('#from').val();
                let to = $('#to').val();
                if (from && to) {
                    let differnce = moment(to).diff(moment(from), 'days') + 1;
                    $('#approvedDay').val(differnce);
                } else {
                    $('#approvedDay').val(0);
                }
            });
            $('.modal').on('hidden.bs.modal', function (e) {
                $('form').trigger("reset");
                $('.select2').select2();
            });
            $('.modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var purpose = button.data('whatever'); // Extract info from data-* attributes
                var modal = $(this);
                if (purpose) {
                    modal.find('.modal-title').text('{{__("Edit Leave Application")}}');
                    var id = button.data('id');
                    $.ajax({
                        url: '{{route('leave.applications.index')}}/' + id,
                        type: 'GET',
                        beforeSend: function (request) {
                            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                        },
                        success: function (response) {
                            modal.find('[name=employee]').val(response.employee_id).trigger('change');
                            modal.find('[name=type]').val(response.type_id).trigger('change');

                            let appliedFrom = moment(response.applied_from);
                            modal.find('input[name=applied_from]').val(appliedFrom.format("DD-MMM-YYYY"));

                            let appliedTo = moment(response.applied_to);
                            modal.find('input[name=applied_to]').val(appliedTo.format("DD-MMM-YYYY"));

                            let appliedDay = appliedTo.diff(appliedFrom, 'days') + 1;
                            $('#appliedDay').val(appliedDay);
                            if (response.from) {
                                var approvedFrom = moment(response.from);
                                modal.find('input[name=from]').val(approvedFrom.format("DD-MMM-YYYY"));
                            }
                            if (response.to) {
                                var approvedTo = moment(response.to);
                                modal.find('input[name=to]').val(approvedTo.format("DD-MMM-YYYY"));
                            }
                            if (response.from && response.to) {
                                let approvedDay = approvedTo.diff(approvedFrom, 'days') + 1;
                                $('#approvedDay').val(approvedDay);
                            }
                            modal.find('[name=approved_by]').val(response.approved_by).trigger('change');
                            modal.find('textarea[name=reason]').val(response.reason);
                        },
                        error: function (data) {
                            let msg = '';
                            if (data.responseJSON.errors) {
                                $.each(data.responseJSON.errors, function (i, error) {
                                    msg += '<p">' + error[0] + '</p>';
                                });
                            } else {
                                msg = data.responseJSON.message;
                            }
                            showDangerToast(msg);
                        }
                    });
                    modal.find('button[type=submit]').removeClass('save-btn').addClass('update-btn').text('{{__("Update")}}').val(id);
                } else {
                    modal.find('.modal-title').text('{{__("Add Leave Application")}}');
                    modal.find('button[type=submit]').removeClass('update-btn').addClass('save-btn').text('{{__("Save")}}');
                }
            });

            $(document).on('click', '.save-btn', function (e) {
                e.preventDefault();
                var form = $('#leaveForm')[0];
                var data = new FormData(form);
                $.ajax({
                    url: '{{route('leave.applications.store')}}',
                    type: 'POST',
                    data: data,
                    contentType: false,
                    processData: false,
                    cache: false,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#applicationModal').modal('toggle');
                        showSuccessToast('{{__("Added Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
            $(document).on('click', '.update-btn', function () {
                e.preventDefault();
                var form = $('#leaveForm')[0];
                var data = new FormData(form);
                let id = $(this).val();
                $.ajax({
                    url: "{{ route('leave.applications.index')}}/" + id,
                    type: 'PUT',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#applicationModal').modal('toggle');
                        showSuccessToast('{{__("Updated Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
            $(document).on('click', '.delete-btn', function (e) {
                e.preventDefault(); // does not go through with the link.
                let confirmation = confirm("{{__('Are you sure to delete')}} ?");
                if (!confirmation) {
                    return;
                }
                var id = $(this).data('id');
                $.ajax({
                    url: '{{ route('leave.applications.index') }}/' + id,
                    type: 'DELETE',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        showSuccessToast('{{__("Deleted Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });


        });
    </script>
@endsection
