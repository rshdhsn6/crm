@extends('hrm::master')
@section('title')
{{__('Home')}}
@endsection
@section('content')
    <div class="widget timeline text-white">
        <div class="widget-body color-overlay">
            <div class="d-flex justify-content-between align-items-center">
                <div class="state">
                    <div class="day-number mr-3 d-inline-block display-4">{{date('d')}}</div>
                    <div class="d-inline-block">
                        <div class="h4">{{date('l')}}</div>
                        <div class="">{{date('F Y')}}</div>
                    </div>
                </div>
                <div id="clock" class="col-6 text-right h1">
                    <span class="hours"></span> <span class="delemeter invisible">:</span>
                    <span class="minutes"></span> <span class="delemeter invisible">:</span>
                    <span class="seconds"></span>
                    {{--                        <span id="ampm"></span>--}}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <div class="widget">
                        <div class="widget-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="state">
                                    <h6>{{__('Users')}}</h6>
                                    <h2 id="c-users">0</h2>
                                </div>
                                <div class="icon">
                                    <i class="ik ik-users"></i>
                                </div>
                            </div>
                            <small class="text-small mt-10 d-block">{{__('All Users')}}</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="widget">
                        <div class="widget-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="state">
                                    <h6>{{__('Salary Paid')}}</h6>
                                    <h2 id="c-salary-paid">0</h2>
                                </div>
                                <div class="icon">
                                    <i class="ik ik-dollar-sign"></i>
                                </div>
                            </div>
                            <small class="text-small mt-10 d-block">{{date('1 F - t F (Y)')}}</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="widget">
                        <div class="widget-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="state">
                                    <h6>{{__('Application')}}</h6>
                                    <h2 id="c-leave-application">0</h2>
                                </div>
                                <div class="icon">
                                    <i class="ik ik-file-text"></i>
                                </div>
                            </div>
                            <small class="text-small mt-10 d-block">{{date('1 F - t F (Y)')}}</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="widget">
                        <div class="widget-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="state">
                                    <h6>{{__('Loan')}}</h6>
                                    <h2 id="c-loan">0</h2>
                                </div>
                                <div class="icon">
                                    <i class="ik ik-activity"></i>
                                </div>
                            </div>
                            <small class="text-small mt-10 d-block">{{date('1 F - t F (Y)')}}</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <!-- Project statustic start -->
            <div class="col-xl-12">
                <div class="card proj-progress-card">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-md-6">
                                <h6>{{__('Employee Present Today')}}</h6>
                                <h5 class="mb-30 fw-700"><span id="a-present">0</span><span
                                        class="text-green ml-10 float-right" id="a-present-percent">0%</span></h5>
                                <div class="progress">
                                    <div id="a-present-bar" class="progress-bar bg-red" style="width: 0"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h6>{{__('Employee Absent Today')}}</h6>
                                <h5 class="mb-30 fw-700"><span id="a-absent">0</span><span
                                        class="text-red ml-10 float-right" id="a-absent-percent">0%</span></h5>
                                <div class="progress">
                                    <div id="a-absent-bar" class="progress-bar bg-blue" style="width: 0"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header"><h3>{{__('Employee by Designation')}}</h3></div>
                        <div class="card-body">
                            <canvas id="pie-chart" width="1" height="1"></canvas>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header"><h3>{{__('Employee by Department')}}</h3></div>
                        <div class="card-body">
                            <canvas id="doughnut-chart" width="1" height="1"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h3>{{__('Calender')}}</h3></div>
                    <div class="card-body">
                        <div id="calendar"></div>
                    </div>
                </div>

            </div>
        </div>

        <div class="col-md-4">
            <div class="card feed-card">
                <div class="card-header">
                    <h3>{{__('Notice Board')}}</h3>
                    <div class="card-header-right">
                        <ul class="list-unstyled card-option">
                            <li><i class="ik ik-chevron-left action-toggle"></i></li>
                            <li><i class="ik ik-minus minimize-card"></i></li>
                            <li><i class="ik ik-x close-card"></i></li>
                        </ul>
                    </div>
                </div>
                <div class="card-block">
                    @if($notices->isempty())
                        <p class="text-center">{{__('Wow... No New Notice')}}</p>
                    @else
                        @foreach($notices as $notice)
                            <div class="row mb-30 align-items-center">
                                <div class="col-auto pr-0">
                                    <i class="ik ik-file-text bg-green feed-icon"></i>
                                </div>
                                <div class="col">
                                    {{--                                    <a class="view-btn" type="button" data-toggle="modal" data-target="#viewModal" data-id="' . $notice->id . '"><i class="ik ik-eye"></i></a>--}}
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#noticeModal"
                                       data-id="{{$notice->id}}"
                                       title="{{$notice->title}}">
                                        <h6 class="mb-5">{{Str::limit($notice->title,35)}}<span
                                                class="text-muted float-right f-14">{{\Carbon\Carbon::parse($notice->created_at)->calendar()}}</span>
                                        </h6>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    @endif
                    <div class="text-right">
                        <a href="{{route('hrm.notices.index')}}" class="b-b-primary text-primary">{{__('View all Feeds')}}</a>
                    </div>
                </div>
            </div>
            <div class="card feed-card">
                <div class="card-header">
                    <h3>{{__('Latest Activity')}}</h3>
                    <div class="card-header-right">
                        <ul class="list-unstyled card-option">
                            <li><i class="ik ik-chevron-left action-toggle"></i></li>
                            <li><i class="ik ik-minus minimize-card"></i></li>
                            <li><i class="ik ik-x close-card"></i></li>
                        </ul>
                    </div>
                </div>
                <div class="card-block">
                    @if($activities->isEmpty())
                        <p class="text-center">{{__('Ops...No activities')}}</p>
                    @else
                        @foreach($activities as $activity)
                            <div class="row mb-30 align-items-center">
                                <div class="col-auto pr-0">
                                    {{--                            <i class="ik ik-bell bg-blue feed-icon"></i>--}}
                                    <img src="{{asset('resources')}}/img/user.jpg"
                                         class="rounded-circle img-fluid img-30"
                                         alt="">
                                </div>
                                <div class="col">
                                    <a href="javascript:void(0)">
                                        <h6 class="mb-5">{{$activity->description. " By ".$activity->causer->name}}<span
                                                class="text-muted float-right f-14">{{\Carbon\Carbon::parse($activity->created_at)->calendar()}}</span>
                                        </h6>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    @endif
                    <div class="text-right">
                        <a href="{{route('activities.index')}}" class="b-b-primary text-primary">{{__('View all Activities')}}</a>
                    </div>
                </div>
            </div>

            <div class="card weather-card">
                <div class="card-body">
                    <div class="d-flex">
                        <h4 class="card-title">{{__('Weather Report')}}</h4>
                        {{--                        <select class="form-control w-25 ml-auto">--}}
                        {{--                            <option selected="">Today</option>--}}
                        {{--                            <option value="1">Weekly</option>--}}
                        {{--                        </select>--}}
                    </div>
                    <div class="d-flex align-items-center flex-row mt-10">
                        <div class="p-2 f-50 text-info">
                            <span id="img"></span>
                            <span id="temp"></span>
                        </div>
                        <div class="p-2">
                            <h3 class="mb-0">{{date('l')}}</h3>
                            <small id="location"></small>
                        </div>
                    </div>
                    <table class="table table-borderless">
                        <tbody>
                        <tr>
                            <td>{{__('Wind')}}</td>
                            <td class="font-medium" id="wind"></td>
                        </tr>
                        <tr>
                            <td>{{__('Humidity')}}</td>
                            <td class="font-medium" id="humidity"></td>
                        </tr>
                        <tr>
                            <td>{{__('Pressure')}}</td>
                            <td class="font-medium" id="pressure"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Details Modal -->
    <div class="modal animated zoomIn faster" id="noticeModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <i class='ik ik-bell ik-3x text-red'></i>
                    <h5 class="modal-title ml-3" id="exampleModalCenterTitle">{{__('Notice')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-5">
                    Notice Body
                </div>
                <div class="modal-footer">
                    {{__('Included Files')}} :
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="editEvent" tabindex="-1" role="dialog" aria-labelledby="editEventLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="editEventForm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editEventLabel">{{__('Edit Event')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="editEname">{{__('Event Title')}}</label>
                            <input type="text" class="form-control" id="editEname" name="editEname"
                                   placeholder="{{__('Please enter event title')}}">
                        </div>
                        <div class="form-group">
                            <label for="editStarts">{{__('Start')}}</label>
                            <input type="text" class="form-control datetimepicker-input" id="editStarts"
                                   name="editStarts" data-toggle="datetimepicker" data-target="#editStarts">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button class="btn btn-danger delete-event" type="submit">{{__('Delete')}}</button>
                        <button class="btn btn-success save-event" type="submit">{{__('Save')}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('extraCSS')
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/weather-icons/css/weather-icons.min.css">
    <style>
        .timeline {
            background: #ffffff url('{{asset('resources')}}/img/placeholder/placeimg_400_200_nature.jpg') no-repeat left top;
            background-size: cover;
        }

        .color-overlay {
            padding: 2em;
            box-sizing: border-box;
            background: rgba(123, 94, 155, 0.5);
            line-height: normal;
        }

        .day-number {
            font-weight: 700;
            line-height: 1;
        }
    </style>
@endsection
@section('extraJS')
    <script src="{{asset('resources')}}/js/chart.min.js"></script>
    <script src="{{asset('resources')}}/plugins/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="{{asset('resources')}}/js/calendar.js"></script>
    <script>
        var designationChart = new Chart($("#pie-chart"), {
            type: 'pie',
            labelLength: 10,
            data: {
                labels: ['Red', 'Blue', 'green'],
                datasets: [{
                    label: "{{__('Employee')}}",
                    backgroundColor: ['Red', 'Blue', 'green'],
                    data: [10, 20, 30]
                }]
            },
            options: {
                responsive: true,
                title: {
                    display: false,
                    text: '{{__("Top 5 Designation based on Total Employee")}}'
                },
                legend: {
                    display: true,
                    position: 'bottom',
                    labels: {
                        boxWidth: 10,
                        // fontColor: 'rgb(255, 99, 132)'
                    }
                }
            }
        });
        var deptChart = new Chart($("#doughnut-chart"), {
            type: 'doughnut',
            responsive: true,
            data: {
                labels: ['Red', 'Blue', 'green'],
                datasets: [
                    {
                        label: "{{__('Employee')}}",
                        backgroundColor: ['Red', 'Blue', 'green'],
                        data: [10, 20, 30]
                    }
                ]
            },
            options: {
                responsive: true,
                title: {
                    display: false,
                    text: '{{__("Top 5 Department based on Total Employee")}}'
                },
                legend: {
                    display: true,
                    position: 'bottom',
                    labels: {
                        boxWidth: 10,
                        // fontColor: 'rgb(255, 99, 132)'
                    }
                }
            }
        });

        function setChartData(chart, label, data) {
            // chart.data.labels.push(label);
            if (label.length && data.length) {
                chart.data.labels = label;
                chart.data.datasets.forEach((dataset) => {
					dataset.data = [];
                    $.each(data, function (i, value) {
                        // checking null or NaN value
                        if (value) {
                            dataset.data.push(Math.round(value));
                        } else {
                            dataset.data.push(0);
                        }
                        // set color
                        dataset.backgroundColor.push('#' + Math.random().toString(16).substr(-6));
                    });
                    // dataset.data = data;
                });
                chart.update();
            }
        }

        $(document).ready(function () {
            // Timebar
            var $hOut = $('.hours'),
                $mOut = $('.minutes'),
                $sOut = $('.seconds');

            // $ampmOut = $('#ampm');

            function update() {
                var date = new Date();

                // var ampm = date.getHours() < 12
                //     ? 'AM'
                //     : 'PM';

                // var hours = date.getHours() == 0
                //     ? 12
                //     : date.getHours() > 12
                //         ? date.getHours() - 12
                //         : date.getHours();

                var hours = date.getHours() < 10
                    ? '0' + date.getHours()
                    : date.getHours();

                var minutes = date.getMinutes() < 10
                    ? '0' + date.getMinutes()
                    : date.getMinutes();

                var seconds = date.getSeconds() < 10
                    ? '0' + date.getSeconds()
                    : date.getSeconds();

                $hOut.text(hours);
                $mOut.text(minutes);
                $sOut.text(seconds);
                // $ampmOut.text(ampm);
                $('.delemeter').toggleClass('invisible');
            }

            update();
            window.setInterval(update, 1000);

            //Fetch Data
            $.ajax({
                url: '{{route('hrm')}}',
                type: 'GET',
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    $('#c-users').text(response.total.users);
                    $('#c-salary-paid').text(response.total.salary_paid);
                    $('#c-leave-application').text(response.total.application);
                    $('#c-loan').text(response.total.loan);
                    $('#a-present-percent').text(response.attendance.present_percent + "%");
                    $('#a-present-bar').width(response.attendance.present_percent + "%");
                    $('#a-present').text(response.attendance.present);
                    $('#a-absent').text(response.attendance.absent);
                    $('#a-absent-percent').text(response.attendance.absent_percent + "%");
                    $('#a-absent-bar').width(response.attendance.absent_percent + "%");

                    setChartData(designationChart, response.designation.name, response.designation.total);
                    // setChartData(deptChart, response.department.name, response.department.total);
                    // setChartData(deptChart,lebel,data);
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
            $('#noticeModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var modal = $(this);
                var id = button.data('id');
                $.ajax({
                    url: '{{route('notices.index')}}/' + id,
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        modal.find('.modal-title').text(response.title);
                        modal.find('.modal-body').html(response.desc);
                        modal.find('.modal-footer').empty();
                        let files = '{{__("Included Files")}}: ';
                        $.each(response.documents, function (i, value) {
                            files += `<a href="{{asset('storage')}}/notices/${value.file}" target="_blank"><i class='ik ik-file ik-3x'></i></a>`;
                        });
                        modal.find('.modal-footer').html(files);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });

            navigator.geolocation.getCurrentPosition(showPosition);

            function showPosition(position) {
                let url ="http://api.openweathermap.org/data/2.5/weather?q=dhaka,BD&APPID=3b7b27de37a37fea87f56a1cd797cb20";
                if (position.coords.latitude && position.coords.longitude) {
                    url = `http://api.openweathermap.org/data/2.5/weather?lat=${position.coords.latitude}&lon=${position.coords.longitude}&APPID=3b7b27de37a37fea87f56a1cd797cb20`;
                }
                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function (response) {
                        let temp = parseInt(response.main.temp) - 273;
                        $('.weather-card #temp').html(temp+"<sup>°</sup>");
                        $('.weather-card #location').text(response.name + ", " + response.sys.country);
                        let link = `<img src="http://openweathermap.org/img/w/${response.weather[0].icon}.png" alt="${response.weather[0].main}" title="${response.weather[0].description}"/>`;
                        $('.weather-card #img').html(link);
                        $('.weather-card #wind').text(response.wind.speed);
                        $('.weather-card #humidity').text(response.main.humidity);
                        $('.weather-card #pressure').text(response.main.pressure);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }
        });
    </script>
@endsection

