@extends('hrm::master')
@section('title')
{{__('Reschedule')}}
@endsection
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-inbox bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{__('Reschedules')}}</h5>
                            <span>{{__('All Reschedule information')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('hrm')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">{{__('Employee')}}</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{__('Reschedule')}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-block">
                        <h3 class="float-left"> {{__('Reschedules')}} </h3>
                        @if(Auth::user()->can('hrm-reshift-add'))
                            <button type="button" class="btn btn-success float-right" data-toggle="modal"
                                    data-target="#rescheduleModal" data-whatever="0"> + {{__('Add Reschedule')}}
                            </button>
                        @endcan
                    </div>
                    <div class="card-body">
                        <div class="dt-responsive">
                            <table id="#" class="table nowrap" style="width: 100%">
                                <thead>
                                <tr>
                                    <th>{{__('ID')}}</th>
                                    <th>{{__('Date')}}</th>
                                    <th>{{__('Employee')}}</th>
                                    <th>{{__('Reschedule')}}</th>
                                    <th>{{__('Created At')}}</th>
                                    <th class="text-right"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Reschedule Modal -->
    <div class="modal animated zoomIn faster" id="rescheduleModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Reschedule')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="RescheduleForm" action="javascript:void(0)">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Employee')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <select class="form-control select2" id="employee" name="employee">
                                    <option>Loading...</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Schedule')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <select class="form-control select2" id="shift" name="shift">
                                    <option>Loading...</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('From')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input class="form-control date" type="text" name="from" placeholder="{{__('From date')}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('To')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input class="form-control date" type="text" name="to" placeholder="{{__('To date')}}">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('extraCSS')

@endsection
@section('extraJS')
    <script>
        $(document).ready(function () {
            var oTable = $('.table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('reschedules.index') }}",
                deferRender: true,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'date', name: 'schedule'},
                    {data: 'employee.name', name: 'name'},
                    {data: 'shift', name: 'schedule'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            $(".select2").select2();
            loadEmployee();

            function loadEmployee() {
                $.ajax({
                    url: '{{route('employee.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#employee').empty();
                        $('#employee').append('<option selected value="" disabled>{{__("Select an employee")}}</option>');
                        $.each(response, function (i, employee) {
                            $('#employee').append($('<option>', {
                                value: employee.id,
                                text: employee.name
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }

            loadShift();

            function loadShift() {
                $.ajax({
                    url: '{{route('shift_schedule.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#shift').empty();
                        $('#shift').append('<option selected value="" disabled>{{__("Select a shift")}}</option>');
                        $.each(response, function (i, shift) {
                            $('#shift').append($('<option>', {
                                value: shift.id,
                                text: shift.name + ` (${shift.from} - ${shift.to})`
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }

            $('.modal').on('hidden.bs.modal', function (e) {
                $('form').trigger("reset");
                $(".select2").select2();
            });
            $('.modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var purpose = button.data('whatever'); // Extract info from data-* attributes
                var modal = $(this);
                if (purpose) {
                    modal.find('.modal-title').text('{{__("Edit Reschedule")}}');
                    modal.find('[name=to]').closest('.form-group').hide();
                    modal.find('[name=from]').closest('.form-group').find('label').text('{{__("Date")}}');
                    var id = button.data('id');
                    $.ajax({
                        url: '{{route('reschedules.index')}}/' + id,
                        type: 'GET',
                        beforeSend: function (request) {
                            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                        },
                        success: function (response) {
                            modal.find('[name=employee]').val(response.employee_id).trigger('change');
                            modal.find('[name=shift]').val(response.shift).trigger('change');
                            modal.find('[name=from]').val(moment(response.date).format("DD-MMM-YYYY"));
                        },
                        error: function (data) {
                            let msg = '';
                            if (data.responseJSON.errors) {
                                $.each(data.responseJSON.errors, function (i, error) {
                                    msg += '<p">' + error[0] + '</p>';
                                });
                            } else {
                                msg = data.responseJSON.message;
                            }
                            showDangerToast(msg);
                        }
                    });
                    modal.find('button[type=submit]').removeClass('save-btn').addClass('update-btn').text('{{__("Update")}}').val(id);
                } else {
                    modal.find('.modal-title').text('{{__("Add Reschedule")}}');
                    modal.find('[name=from]').closest('.form-group').find('label').text('{{__("From")}}');
                    modal.find('[name=to]').closest('.form-group').show();
                    modal.find('button[type=submit]').removeClass('update-btn').addClass('save-btn').text('{{__("Save")}}');
                }
            });
            $(document).on('click', '.save-btn', function () {
                let data = $('form').serialize();
                $.ajax({
                    url: '{{route('reschedules.store')}}',
                    type: 'POST',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#rescheduleModal').modal('toggle');
                        $('form').trigger("reset");
                        showSuccessToast('{{__("Added Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
            $(document).on('click', '.update-btn', function () {
                let data = $('form').serialize();
                let id = $(this).val();
                $.ajax({
                    url: "{{ route('reschedules.index')}}/" + id,
                    type: 'PUT',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#rescheduleModal').modal('toggle');
                        showSuccessToast('{{__("Updated Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });

            $(document).on('click', '.delete-btn', function (e) {
                e.preventDefault(); // does not go through with the link.
                let confirmation = confirm("{{__("Are you sure to delete")}} ?");
                if (!confirmation) {
                    return;
                }
                var id = $(this).data('id');
                $.ajax({
                    url: '{{ route('reschedules.index') }}/' + id,
                    type: 'DELETE',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        showSuccessToast('{{__("Deleted Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });

        });
    </script>
@endsection

