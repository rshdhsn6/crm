@extends('hrm::master')
@section('title')
{{__('Loan')}}
@endsection
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-inbox bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{__('Loans')}}</h5>
                            <span>{{__('All loan informations')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('hrm')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{__('Loan')}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-block">
                        <h3 class="float-left"> {{__('Loans')}} </h3>
                        @if(Auth::user()->can('hrm-loan-add'))
                            <button type="button" class="btn btn-success float-right" data-toggle="modal"
                                    data-target="#loanModal" data-whatever="0"> + {{__('Add Loan')}}
                            </button>
                        @endif
                    </div>
                    <div class="card-body">
                        <div class="dt-responsive">
                            <table id="#" class="table nowrap" style="width: 100%">
                                <thead>
                                <tr>
                                    <th>{{__('ID')}}</th>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Amount')}}</th>
                                    <th>{{__('Repayment from')}}</th>
                                    <th>{{__('Installment')}}</th>
                                    <th>{{__('Approved at')}}</th>
                                    <th>{{__('Approved by')}}</th>
                                    <th class="text-right"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Loan Modal -->
    <div class="modal animated zoomIn faster" id="loanModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Loan')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="LoanForm" action="javascript:void(0)">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Employee')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <select class="form-control select2" id="employee" name="employee">
                                    <option>Loading...</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Amount')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="number" id="amount" class="form-control" value="0" min="0" name="amount"
                                       required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Repayment From')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control date"
                                       value="{{\Carbon\Carbon::now()->addMonth(1)->format('01-M-Y')}}" placeholder=""
                                       name="repayment_from" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Installment Period')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="number" id="installment" class="form-control" value="1" min="1"
                                       name="installment" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Installment')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="number" id="installmentAmount" class="form-control" value="0" min="0"
                                       disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Approved By')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <select class="form-control select2" id="approvedBy" name="approved_by">
                                    <option value="-1">Loading...</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Approved At')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control date" placeholder="{{__('d-m-Y')}}" value=""
                                       name="approved_at" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Comment')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <textarea class="form-control" id="exampleTextarea1" rows="4"
                                          placeholder="{{__('comments here')}}.." name="comment"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('extraCSS')

@endsection
@section('extraJS')
    <script>
        $(document).ready(function () {
            var oTable = $('.table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('loans.index') }}",
                deferRender: true,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'employee.name', name: 'name'},
                    {data: 'amount', name: 'amount'},
                    {data: 'repayment_from', name: 'repayment_from'},
                    {data: 'installment', name: 'installment'},
                    {data: 'approved_at', name: 'approved_at'},
                    {data: 'approved_by', name: 'approved_by'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            $('.select2').select2();

            loadEmployee();

            function loadEmployee() {
                $.ajax({
                    url: '{{route('employee.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#employee,#approvedBy').empty();
                        $('#employee').append('<option selected value="" disabled>{{__("Select an Employee")}}</option>');
                        $('#approvedBy').append('<option selected value="" disabled>{{__("Select an authorized person")}}</option>');
                        $.each(response, function (i, employee) {
                            $('#employee,#approvedBy').append($('<option>', {
                                value: employee.id,
                                text: employee.name
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }

            $(document).on('change input', '#installment,#amount', function () {
                let installment = $('#installment').val();
                let amount = $('#amount').val();
                let installmentAmount = Math.round(amount * 100 / installment) / 100;
                let result = (installmentAmount) ? installmentAmount : 0;
                $('#installmentAmount').val(result);
            });
            $('.modal').on('hidden.bs.modal', function (e) {
                $('form').trigger("reset");
                $(".select2").select2();
            });
            $('.modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var purpose = button.data('whatever'); // Extract info from data-* attributes
                var modal = $(this);
                if (purpose) {
                    modal.find('.modal-title').text('Edit Loan');
                    var id = button.data('id');
                    $.ajax({
                        url: '{{route('loans.index')}}/' + id,
                        type: 'GET',
                        beforeSend: function (request) {
                            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                        },
                        success: function (response) {
                            modal.find('[name=employee]').val(response.employee_id).trigger('change');
                            modal.find('input[name=amount]').val(response.amount);
                            modal.find('input[name=repayment_from]').val(moment(response.repayment_from).format("DD-MMM-YYYY"));
                            let installmentAmount = Math.round((response.amount * 100 / response.installment)) / 100;
                            modal.find('#installmentAmount').val(installmentAmount);
                            modal.find('input[name=installment]').val(response.installment);
                            if (response.approved_by) {
                                modal.find('[name=approved_by]').val(response.approved_by).trigger('change');
                            }
                            if (response.approved_at) {
                                modal.find('[name=approved_at]').val(moment(response.approved_at).format("DD-MMM-YYYY"));
                            }
                            modal.find('[name=comment]').val(response.comment);
                        },
                        error: function (data) {
                            let msg = '';
                            if (data.responseJSON.errors) {
                                $.each(data.responseJSON.errors, function (i, error) {
                                    msg += '<p">' + error[0] + '</p>';
                                });
                            } else {
                                msg = data.responseJSON.message;
                            }
                            showDangerToast(msg);
                        }
                    });
                    modal.find('button[type=submit]').removeClass('save-btn').addClass('update-btn').text('{{__("Update")}}').val(id);
                } else {
                    modal.find('.modal-title').text('{{__("Add Loan")}}');
                    modal.find('button[type=submit]').removeClass('update-btn').addClass('save-btn').text('{{__("Save")}}');
                }
            });
            $(document).on('click', '.save-btn', function () {
                let data = $('form').serialize();
                $.ajax({
                    url: '{{route('loans.store')}}',
                    type: 'POST',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#loanModal').modal('toggle');
                        $('form').trigger("reset");
                        showSuccessToast('{{__("Added Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
            $(document).on('click', '.update-btn', function () {
                let data = $('form').serialize();
                let id = $(this).val();
                $.ajax({
                    url: "{{ route('loans.index')}}/" + id,
                    type: 'PUT',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#loanModal').modal('toggle');
                        showSuccessToast('{{__("Updated Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });

            $(document).on('click', '.delete-btn', function (e) {
                e.preventDefault(); // does not go through with the link.
                let confirmation = confirm("{{__('Are you sure to delete')}} ?");
                if (!confirmation) {
                    return;
                }
                var id = $(this).data('id');
                $.ajax({
                    url: '{{ route('loans.index') }}/' + id,
                    type: 'DELETE',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        showSuccessToast('{{__("Deleted Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });

        });
    </script>
@endsection

