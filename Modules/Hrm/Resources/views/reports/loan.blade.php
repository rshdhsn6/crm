@extends('hrm::master')
@section('title')
    {{__('Report')}}
@endsection
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-inbox bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{__('Loan')}}</h5>
                            <span>{{__('Loan status of Employees')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('hrm')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">{{__('Report')}}</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{__('Loan')}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-block">
                        <h3 class="float-left"> {{__('Loans')}} </h3>
                    </div>
                    <div class="card-body">
                        <div class="dt-responsive">
                            <table id="#" class="table nowrap">
                                <thead>
                                <tr>
                                    <th>{{__('ID')}}</th>
                                    <th>{{__('Employee')}}</th>
                                    <th>{{__('Division')}}</th>
                                    <th>{{__('Department')}}</th>
                                    <th>{{__('Loan')}}</th>
                                    <th>{{__('Paid')}}</th>
                                    <th>{{__('Due')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extraJS')
    <script>
        $(document).ready(function () {
            var oTable = $('.table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('report.loan') }}",
                deferRender: true,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'division', name: 'division'},
                    {data: 'department', name: 'department'},
                    {data: 'loan', name: 'loan'},
                    {data: 'installment', name: 'installment'},
                    {
                        data: null, name: 'due', render: function (data, type, row) {
                            let due = Math.round(data.loan - data.installment);
                            if(due>0){
                                return "<span class='text-warning'>"+due+"</span>";
                            }
                            return due;
                        }
                    },
                ]
            });
        });
    </script>
@endsection

