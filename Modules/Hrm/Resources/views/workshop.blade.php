@extends('hrm::master')
@section('title')
{{__('Workshop')}}
@endsection
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-inbox bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{__('Workshops')}}</h5>
                            <span>{{__('All workshop informations')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('hrm')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">{{__('Employee')}}</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{__('Workshop')}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header row">
                        <div class="col-sm-6 m-auto">
                            <div class="card-search with-adv-search dropdown">
                                <form action="javascript:void(0)">
                                    <input type="text" class="form-control global_filter" id="global_filter"
                                           placeholder="{{__('Search')}}..">
                                    <button type="submit" class="btn btn-icon"><i class="ik ik-search"></i></button>
                                    <button type="button" id="adv_wrap_toggler"
                                            class="adv-btn ik ik-chevron-down dropdown-toggle" data-toggle="dropdown"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="adv-search-wrap dropdown-menu dropdown-menu-right"
                                         aria-labelledby="adv_wrap_toggler">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control column_filter"
                                                           id="col1_filter" placeholder="{{__('Name')}}" data-column="1">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="select2 column_filter" id="col2_filter"
                                                            data-column="2">
                                                        <option selected value="">{{__('Select Workshop type')}}</option>
                                                        @foreach($types as $type)
                                                            <option value="{{$type->name}}">{{$type->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="select2 column_filter" id="col3_filter"
                                                            data-column="3">
                                                        <option selected value="">{{__('Select a Trainer')}}</option>
                                                        @foreach($trainers as $trainer)
                                                            <option
                                                                value="{{$trainer->name}}">{{$trainer->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        @can('hrm-workshop-add')
                            <button type="button" class="btn btn-success float-right" data-toggle="modal"
                                    data-target="#workshopModal" data-whatever="0"> + {{__('Add Workshop')}}
                            </button>
                        @endcan
                    </div>
                    <div class="card-body">
                        <div class="dt-responsive">
                            <table id="#" class="table nowrap">
                                <thead>
                                <tr>
                                    <th>{{__('ID')}}</th>
                                    <th>{{__('Workshop')}}</th>
                                    <th>{{__('Type')}}</th>
                                    <th>{{__('Trainer')}}</th>
                                    <th>{{__('From')}}</th>
                                    <th>{{__('To')}}</th>
                                    <th>{{__('Amount')}}</th>
                                    <th>{{__('Created At')}}</th>
                                    <th class="text-right"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Workshop Modal -->
    <div class="modal animated zoomIn faster" id="viewModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Workshop')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4 text-center">
                            <img src="{{asset('resources/img/user.jpg')}}" class="img-thumbnail rounded-circle"
                                 height='150' width='150'/>
                        </div>
                        <div class="col-md-8">
                            <dl class="row">
                                <dt class="col-sm-4">{{__('Type')}}:</dt>
                                <dd class="col-sm-8 type"></dd>
                                <dt class="col-sm-4">{{__('Trainer')}}:</dt>
                                <dd class="col-sm-8 trainer"></dd>
                                <dt class="col-sm-4">{{__('From')}}:</dt>
                                <dd class="col-sm-8 from"></dd>
                                <dt class="col-sm-4">{{__('To')}}:</dt>
                                <dd class="col-sm-8 to"></dd>
                                <dt class="col-sm-4">{{__('Cost')}}:</dt>
                                <dd class="col-sm-8 cost"></dd>
                                <dt class="col-sm-4">{{__('Description')}}:</dt>
                                <dd class="col-sm-8 desc"></dd>
                            </dl>
                        </div>
                    </div>
                    <p class="mb-10">{{__('Participants')}}</p>
                    <hr/>
                    <ul class="list-unstyled mb-20" id="participants">

                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal animated zoomIn faster" id="workshopModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Workshop')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="WorkshopForm" action="javascript:void(0)">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Title')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control" placeholder="{{__('title here')}}" name="name" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Type')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <select class="form-control select2" name="type" id="type" required>
                                    <option selected value="" disabled>{{__('Select Workshop type')}}</option>
                                    @foreach($types as $type)
                                        <option value="{{$type->id}}">{{$type->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Trainer')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <select class="form-control select2" name="trainer" id="trainer" required>
                                    <option selected value="" disabled>{{__('Select a Trainer')}}</option>
                                    @foreach($trainers as $trainer)
                                        <option value="{{$trainer->id}}">{{$trainer->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Cost')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="number" class="form-control" min="0" value="0" name="cost" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('From')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control date" placeholder="{{__('From date')}}" name="from"
                                       required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('To')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control date" placeholder="{{__('To date')}}" name="to" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Employees')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <select class="form-control select2" multiple="multiple" id="employee"
                                        name="employee[]">
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Description')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <textarea class="form-control" id="exampleTextarea1" rows="4"
                                          placeholder="{{__('description here')}}.." name="desc"></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('extraJS')
    <script>
        $(document).ready(function () {
            var oTable = $('.table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('trainings.workshops.index') }}",
                deferRender: true,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'type_id', name: 'type_id'},
                    {data: 'trainer_id', name: 'trainer_id'},
                    {data: 'from', name: 'from'},
                    {data: 'to', name: 'to'},
                    {data: 'amount', name: 'amount'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            // Search API start
            function filterColumn(i) {
                oTable.column(i).search(
                    $('#col' + i + '_filter').val(),
                    false,
                    true
                ).draw();
            }

            function filterGlobal() {
                oTable.search(
                    $('#global_filter').val(),
                    false,
                    true
                ).draw();
            }

            $('input.global_filter').on('keyup', function () {
                filterGlobal();
            });
            $('.column_filter').on('keyup change', function () {
                filterColumn($(this).attr('data-column'));
            });
            // Search API end

            $('.select2').select2();
            loadEmployee();

            function loadEmployee() {
                $.ajax({
                    url: '{{route('employee.all')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#employee').empty();
                        $.each(response, function (i, employee) {
                            $('#employee').append($('<option>', {
                                value: employee.id,
                                text: employee.name
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }

            $('.modal').on('hidden.bs.modal', function (e) {
                $('form').trigger("reset");
                $('.select2').select2();
                $("#participants").html('');
            });
            $('.modal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                var purpose = button.data('whatever'); // Extract info from data-* attributes

                var modal = $(this);
                if (purpose) {
                    modal.find('.modal-title').text('{{__("Edit Workshop Details")}}');
                    var id = button.data('id');
                    $.ajax({
                        url: '{{route('trainings.workshops.index')}}/' + id,
                        type: 'GET',
                        beforeSend: function (request) {
                            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                        },
                        success: function (response) {
                            modal.find('input[name=name]').val(response.name);
                            modal.find('input[name=cost]').val(response.amount);
                            modal.find('input[name=from]').val(moment(response.from).format("DD-MMM-YYYY"));
                            modal.find('input[name=to]').val(moment(response.to).format("DD-MMM-YYYY"));
                            modal.find('#type').val(response.type_id).trigger('change');
                            modal.find('#trainer').val(response.trainer_id).trigger('change');
                            modal.find('textarea[name=desc]').val(response.desc);
                            if (response.employees) {
                                let employees = response.employees.map(function (value, key) {
                                    return value.id;
                                });
                                modal.find('#employee').val(employees).trigger('change');
                            }
                        },
                        error: function (data) {
                            let msg = '';
                            if (data.responseJSON.errors) {
                                $.each(data.responseJSON.errors, function (i, error) {
                                    msg += '<p">' + error[0] + '</p>';
                                });
                            } else {
                                msg = data.responseJSON.message;
                            }
                            showDangerToast(msg);
                        }
                    });
                    modal.find('button[type=submit]').removeClass('save-btn').addClass('update-btn').text('{{__("Update")}}').val(id);
                } else {
                    modal.find('.modal-title').text('{{__("Add Workshop Details")}}');
                    modal.find('button[type=submit]').removeClass('update-btn').addClass('save-btn').text('{{__("Save")}}');
                }
            });
            $(document).on('click', '.view-btn', function () {
                let id = $(this).data('id');
                $.ajax({
                    url: "{{ route('trainings.workshops.index')}}/" + id,
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        console.log(response);
                        $('#viewModal .modal-title').text(response.name);
                        $('#viewModal .type').text(response.type.name);
                        $('#viewModal .trainer').text(response.trainer.name);
                        $('#viewModal .from').text(moment(response.from).format("DD-MMM-YYYY"));
                        $('#viewModal .to').text(moment(response.to).format("DD-MMM-YYYY"));
                        $('#viewModal .cost').text(response.amount);
                        $('#viewModal .desc').text(response.desc);
                        if (response.employees.length) {
                            $.each(response.employees, function (i, employee) {

                                let image = `<img src="{{asset('resources/img/user.jpg')}}" title="${employee.name}" class="img-thumbnail rounded-circle" height='100' width='100'/>`;
                                if (employee.photo) {
                                    image = `<img src="{{asset('storage/hrm/employee/photo')}}/${employee.photo}" title="${employee.name}" class="img-thumbnail rounded-circle" height='100' width='100'/>`;
                                }
                                $("#participants").append("<li class='d-inline-block mx-2'>" + image + "</li>");
                            });
                        }
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
            $(document).on('click', '.save-btn', function () {
                let data = $('form').serialize();
                $.ajax({
                    url: '{{route('trainings.workshops.store')}}',
                    type: 'POST',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#workshopModal').modal('toggle');
                        $('form').trigger("reset");
                        showSuccessToast('{{__("Added Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
            $(document).on('click', '.update-btn', function () {
                let data = $('form').serialize();
                let id = $(this).val();
                $.ajax({
                    url: "{{ route('trainings.workshops.index')}}/" + id,
                    type: 'PUT',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#workshopModal').modal('toggle');
                        showSuccessToast('{{__("Updated Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
            $(document).on('click', '.delete-btn', function (e) {
                e.preventDefault(); // does not go through with the link.
                let confirmation = confirm("{{__('Are you sure to delete')}} ?");
                if (!confirmation) {
                    return;
                }
                var id = $(this).data('id');
                $.ajax({
                    url: '{{ route('trainings.workshops.index') }}/' + id,
                    type: 'DELETE',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        showSuccessToast('{{__("Deleted Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
        });
    </script>
@endsection

