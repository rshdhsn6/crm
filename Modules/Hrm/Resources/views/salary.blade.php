@extends('hrm::master')
@section('title')
{{__('Salary')}}
@endsection
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-inbox bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{__('Salaries')}}</h5>
                            <span>{{__('All Salary information')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('hrm')}}"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="javascript:void(0)">{{__('Salary')}}</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">{{__('Generator')}}</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-block">
                        <div class="col-sm-6 m-auto">
                            <div class="card-search with-adv-search dropdown">
                                <form action="javascript:void(0)">
                                    <input type="text" class="form-control global_filter" id="global_filter"
                                           placeholder="{{__('Search')}}.." required>
                                    <button type="submit" class="btn btn-icon"><i class="ik ik-search"></i></button>
                                    <button type="button" id="adv_wrap_toggler"
                                            class="adv-btn ik ik-chevron-down dropdown-toggle" data-toggle="dropdown"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                    <div class="adv-search-wrap dropdown-menu dropdown-menu-right"
                                         aria-labelledby="adv_wrap_toggler">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control column_filter"
                                                           id="col1_filter" placeholder="{{__('Name')}}" data-column="1">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="select2 column_filter" id="col2_filter" data-column="2">
                                                        <option selected value="">{{__('Select a Department')}}</option>
                                                        @foreach($departments as $department)
                                                        <option value="{{$department->name}}">{{$department->name}}</option>
                                                            @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="select2 column_filter" id="col3_filter" data-column="3">
                                                        <option selected value="">{{__('Select a Division')}}</option>
                                                        @foreach($divisions as $division)
                                                            <option value="{{$division->name}}">{{$division->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select class="select2 column_filter" id="col11_filter" data-column="11">
                                                        <option selected value="">{{__('Select a status')}}</option>
                                                        <option value="Paid">{{__('Paid')}}</option>
                                                        <option value="Make paid">{{__('Unpaid')}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="dt-responsive">
                            <table id="salaryTBL" class="table nowrap" style="width: 100%">
                                <thead>
                                <tr>
                                    <th>{{__('ID')}}</th>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Department')}}</th>
                                    <th>{{__('Division')}}</th>
                                    <th>{{__('From')}}</th>
                                    <th>{{__('To')}}</th>
                                    <th>{{__('Working Hour')}}</th>
                                    <th>{{__('Working Days')}}</th>
                                    <th>{{__('Salary')}}</th>
                                    <th>{{__('Paid By')}}</th>
                                    <th>{{__('Paid At')}}</th>
                                    <th>{{__('Status')}}</th>
                                    <th>{{__('Generated at')}}</th>
                                    <th class="text-right"></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Salary Details Modal -->
    <div class="modal animated zoomIn faster" id="detailsModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                {{--                <div class="modal-header">--}}
                {{--                    <h5 class="modal-title" id="exampleModalCenterTitle">Salary Details</h5>--}}
                {{--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                {{--                        <span aria-hidden="true">&times;</span>--}}
                {{--                    </button>--}}
                {{--                </div>--}}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                                <img src="{{asset('resources')}}/img/user.jpg"
                                     class="rounded-circle m-auto d-block"
                                     height="150" width="150"/>
                        </div>
                        <div class="col-md-8 col-sm-12">
                            <h4 class="mt-0 name"></h4>
                            <hr/>
                            <dl class="row">
                                <dl class="row">
                                    <dt class="col-sm-3">{{__('Employee ID')}}</dt>
                                    <dd class="col-sm-9 employeeId"></dd>
                                    <dt class="col-sm-3">{{__('Position')}}</dt>
                                    <dd class="col-sm-9 position"></dd>
                                    <dt class="col-sm-3">{{__('Department')}}</dt>
                                    <dd class="col-sm-9 department"></dd>
                                    <dt class="col-sm-3">{{__('Division')}}</dt>
                                    <dd class="col-sm-9 division"></dd>
                                    <dt class="col-sm-3">{{__('Joining Date')}}</dt>
                                    <dd class="col-sm-9 joiningDate"></dd>
                                </dl>
                            </dl>
                        </div>
                    </div>

                    <p class="h5">{{__('Salary Details')}}</p>
                    <hr/>
                    <div class="row">
                        <div class="col-sm-6">
                            <dl class="row">
                                <dt class="col-sm-5">{{__('Salary Grade')}}</dt>
                                <dd class="col-sm-7 grade"></dd>
                                <dt class="col-sm-5">{{__('Type')}}</dt>
                                <dd class="col-sm-7 salaryType"></dd>
                            </dl>

                        </div>
                        <div class="col-sm-6">
                            <dl class="row">
                                <dt class="col-sm-5">{{__('Basic')}}</dt>
                                <dd class="col-sm-7 basicAmount"></dd>
                                <dt class="col-sm-5">{{__('Overtime (Hourly)')}}</dt>
                                <dd class="col-sm-7 overtimeAmount"></dd>
                            </dl>
                        </div>
                        <div class="col-md-12">
                            <div class="widget">
                                <div class="widget-header">
                                    <h5 class="widget-title">{{__('Basics')}}</h5>
                                    <div class="widget-tools pull-right">
                                        <button type="button"
                                                class="btn btn-widget-tool minimize-widget ik ik-minus"></button>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="dt-responsive">
                                        <table id="basicTBL" class="table table-borderless table-sm">
                                            <thead>
                                            <tr>
                                                <th>{{__('Total Days')}}</th>
                                                <th>{{__('Working Days')}}</th>
                                                <th>{{__('Total period')}}</th>
                                                <th>{{__('Period')}}</th>
                                                <th>{{__('Rate')}}</th>
                                                <th>{{__('Amount')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="widget">
                                <div class="widget-header">
                                    <h5 class="widget-title">{{__('Allowances')}}</h5>
                                    <div class="widget-tools pull-right">
                                        <button type="button"
                                                class="btn btn-widget-tool minimize-widget ik ik-minus"></button>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="dt-responsive">
                                        <table id="allowanceTBL" class="table table-borderless table-sm">
                                            <thead>
                                            <tr>
                                                <th>{{__('Name')}}</th>
                                                <th>{{__('Rate')}}</th>
                                                <th>{{__('Amount')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="widget">
                                <div class="widget-header">
                                    <h5 class="widget-title">{{__('Deductions')}}</h5>
                                    <div class="widget-tools pull-right">
                                        <button type="button"
                                                class="btn btn-widget-tool minimize-widget ik ik-minus"></button>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="dt-responsive">
                                        <table id="deductionTBL" class="table table-borderless table-sm">
                                            <thead>
                                            <tr>
                                                <th>{{__('Name')}}</th>
                                                <th>{{__('Rate')}}</th>
                                                <th>{{__('Amount')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="widget">
                                <div class="widget-header">
                                    <h5 class="widget-title">{{__('Leave')}}</h5>
                                    <div class="widget-tools pull-right">
                                        <button type="button"
                                                class="btn btn-widget-tool minimize-widget ik ik-minus"></button>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="dt-responsive">
                                        <table id="leaveTBL" class="table table-borderless table-sm">
                                            <thead>
                                            <tr>
                                                <th>{{__('Day')}}</th>
                                                <th>{{__('Amount')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="widget">
                                <div class="widget-header">
                                    <h5 class="widget-title">{{__('Loans')}}</h5>
                                    <div class="widget-tools pull-right">
                                        <button type="button"
                                                class="btn btn-widget-tool minimize-widget ik ik-minus"></button>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="col-sm-12">
                                        <dl class="row">
                                            <dt class="col-sm-5">{{__('Amount')}}</dt>
                                            <dd class="col-sm-7 loanAmount"></dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="widget">
                                <div class="widget-header">
                                    <h5 class="widget-title">{{__('Overtime')}}</h5>
                                    <div class="widget-tools pull-right">
                                        <button type="button"
                                                class="btn btn-widget-tool minimize-widget ik ik-minus"></button>
                                    </div>
                                </div>
                                <div class="widget-body">
                                    <div class="dt-responsive">
                                        <table id="overtimeTBL" class="table table-borderless table-sm">
                                            <thead>
                                            <tr>
                                                <th>{{__('Day')}}</th>
                                                <th>{{__('Period')}}</th>
                                                <th>{{__('Rate')}}</th>
                                                <th>{{__('Amount')}}</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extraCSS')

@endsection
@section('extraJS')
    <script>
        $(document).ready(function () {
            var oTable = $('#salaryTBL').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('salaries.salary.index') }}",
                deferRender: true,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'employee.name', name: 'employee.name'},
                    {data: 'department', name: 'department'},
                    {data: 'division', name: 'division'},
                    {data: 'from', name: 'from'},
                    {data: 'to', name: 'to'},
                    {data: 'details.working_hour', name: 'details.working_hour'},
                    {data: 'details.working_day', name: 'details.working_day'},
                    {data: 'salary', name: 'salary'},
                    {data: 'paid_by', name: 'paid_by'},
                    {data: 'paid_at', name: 'paid_at'},
                    {data: 'status', name: 'status'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            // Search API start
            function filterColumn(i) {
                oTable.column(i).search(
                    $('#col' + i + '_filter').val(),
                    false,
                    true
                ).draw();
            }

            function filterGlobal() {
                oTable.search(
                    $('#global_filter').val(),
                    false,
                    true
                ).draw();
            }

            $('input.global_filter').on('keyup', function () {
                filterGlobal();
            });
            $('.column_filter').on('keyup change', function () {
                filterColumn($(this).attr('data-column'));
            });
            // Search API end

            var basicTBL = $('#basicTBL').DataTable({
                paging: false,
                lengthChange: false,
                searching: false,
                ordering: false,
                info: false,
            });
            var allowanceTBL = $('#allowanceTBL').DataTable({
                paging: false,
                lengthChange: false,
                searching: false,
                ordering: false,
                info: false,
            });
            var deductionTBL = $('#deductionTBL').DataTable({
                paging: false,
                lengthChange: false,
                searching: false,
                ordering: false,
                info: false,
            });
            var loanTBL = $('#loanTBL').DataTable({
                paging: false,
                lengthChange: false,
                searching: false,
                ordering: false,
                info: false,
            });
            var overtimeTBL = $('#overtimeTBL').DataTable({
                paging: false,
                lengthChange: false,
                searching: false,
                ordering: false,
                info: false,
            });
            var leaveTBL = $('#leaveTBL').DataTable({
                paging: false,
                lengthChange: false,
                searching: false,
                ordering: false,
                info: false,
            });
            $(".select2").select2();
            $(document).on('click', '.view-btn', function () {
                var id = $(this).data('id');
                $.ajax({
                    url: '{{route('salaries.salary.index')}}/' + id,
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        setSalary(response);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });

            function setSalary(response) {
                $('#detailsModal .name').text(response.employee.name);
                if (response.employee.photo) {
                    $('#detailsModal img').attr('src', response.employee.photo);
                }
                $('#detailsModal .employeeId').text(response.employee.id);
                $('#detailsModal .position').text(response.employee.official.position.name);
                $('#detailsModal .department').text(response.employee.official.division.department.name);
                $('#detailsModal .division').text(response.employee.official.division.name);
                $('#detailsModal .joiningDate').text(moment(response.employee.official.from).format('D-M-Y'));
                $('#detailsModal .grade').text(response.employee.official.grade.name);
                if (response.details.type) {
                    $('#detailsModal .salaryType').text('{{__("Hourly")}}');
                } else {
                    $('#detailsModal .salaryType').text('{{__("Monthly")}}');
                }
                $('#detailsModal .basicAmount').text(response.employee.official.grade.rate);
                $('#detailsModal .overtimeAmount').text(response.employee.official.grade.overtime_rate);
                //basic
                basicTBL.row.add([
                    response.details.total_working_day,
                    response.details.working_day,
                    response.details.total_working_hour,
                    response.details.working_hour,
                    response.details.rate,
                    response.details.basic
                ]).draw(true);
                //allowances
                $.each(response.allowances, function (i, allowance) {
                    if (allowance.type) {
                        //percentage
                        rate = allowance.rate + "%";
                        amount = (allowance.rate / 100) * response.details.basic;
                    } else {
                        //currency
                        rate = allowance.rate;
                        amount = allowance.rate;
                    }
                    if (allowance.allowance.type) {
                        //Deduction Allowances
                        deductionTBL.row.add([
                            allowance.allowance.name,
                            rate,
                            amount
                        ]).draw(true);
                    } else {
                        //Additional Allowances
                        allowanceTBL.row.add([
                            allowance.allowance.name,
                            rate,
                            amount
                        ]).draw(true);
                    }
                });
                $('#detailsModal .loanAmount').text(response.details.loan);
                //overtime
                overtimeTBL.row.add([
                    response.details.overtime_day,
                    response.details.overtime_hour,
                    response.details.overtime_rate,
                    response.details.overtime
                ]).draw(true);
                //leave
                leaveTBL.row.add([
                    response.details.leave,
                    0
                ]).draw(true);
            }

            $('#detailsModal').on('hidden.bs.modal', function (e) {
                basicTBL.clear().draw(true);
                allowanceTBL.clear().draw(true);
                deductionTBL.clear().draw(true);
                overtimeTBL.clear().draw(true);
                leaveTBL.clear().draw(true);
            });
            $(document).on('click', '.paid-btn', function () {
                let confirmation = confirm("{{__('Are you sure to change the status')}}?");
                if (!confirmation) {
                    return;
                }
                var id = $(this).data('id');
                $.ajax({
                    url: '{{route('salaries.salary.index')}}/' + id + '/change-status',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        showSuccessToast('{{__("Updated Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });

            $(document).on('click', '.delete-btn', function (e) {
                e.preventDefault(); // does not go through with the link.
                let confirmation = confirm("{{__('Are you sure to delete')}} ?");
                if (!confirmation) {
                    return;
                }
                var id = $(this).data('id');
                $.ajax({
                    url: '{{ route('salaries.salary.index') }}/' + id,
                    type: 'DELETE',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        showSuccessToast('{{__("Deleted Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });

        });
    </script>
@endsection

