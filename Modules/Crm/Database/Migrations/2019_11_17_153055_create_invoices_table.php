<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('crm')->create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client_id')->unsigned();
            $table->bigInteger('project_id')->unsigned();
            $table->string('billdate')->nullable();
            $table->string('duedate')->nullable();
            $table->bigInteger('invoice_value')->default(0)->nullable();
            $table->bigInteger('payment_received')->default(0)->nullable();
            $table->bigInteger('discount')->default(0)->nullable();
            $table->text('note')->nullable();
            $table->integer('status')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
