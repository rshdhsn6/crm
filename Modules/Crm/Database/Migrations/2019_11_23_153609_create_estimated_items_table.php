<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstimatedItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('crm')->create('estimated_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('estimate_id')->unsigned();
            $table->bigInteger('item_id')->unsigned();
            $table->integer('quantity')->default(1)->nullable();
            $table->integer('rate')->default(0)->nullable();
            $table->bigInteger('discount')->default(0)->nullable();
            $table->integer('discount_type')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estimated_items');
    }
}
