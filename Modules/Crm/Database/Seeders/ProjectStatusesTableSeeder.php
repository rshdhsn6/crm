<?php

namespace Modules\Crm\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProjectStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::connection('crm')->table('project_statuses')->insert([
            ['title' => 'Open'],
            ['title' => 'Completed'],
            ['title' => 'Hold'],
            ['title' => 'Canceled'],
        ]);
    }
}
