<?php

namespace Modules\Crm\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Crm\Entities\Client;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $connection = 'crm';

        $clients = factory(Client::class, 100)->make();

        $clients->each(function($c) use($connection) {
            $c->setConnection($connection);
            $c->save();
        });
    }
}
