<?php

namespace Modules\Crm\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::connection('crm')->table('client_groups')->insert([
            ['name' => 'General', 'desc'=>''],
            ['name' => 'VIP', 'desc'=>''],
            ['name' => 'Low Budget', 'desc'=>''],
            ['name' => 'High Budget', 'desc'=>''],
        ]);
    }
}
