<?php

namespace Modules\Crm\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TaskStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::connection('crm')->table('task_statuses')->insert([
            ['title' => 'To do', 'colorcode' => '#F9A52D'],
            ['title' => 'In progress', 'colorcode' => '#1672B9'],
            ['title' => 'Testing', 'colorcode' => '#ad159e'],
            ['title' => 'Done', 'colorcode' => '#00B393'],
        ]);
    }
}
