<?php

namespace Modules\Crm\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentMethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::connection('crm')->table('payment_methods')->insert([
            ['title' => 'Cash', 'status' => 1],
            ['title' => 'Card', 'status' => 1],
            ['title' => 'Cheque', 'status' => 1],
            ['title' => 'bKash', 'status' => 1],
            ['title' => 'Rocket', 'status' => 1],
        ]);
    }
}
