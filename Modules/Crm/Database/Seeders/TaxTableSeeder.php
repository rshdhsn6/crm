<?php

namespace Modules\Crm\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TaxTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::connection('crm')->table('taxes')->insert([
            ['title' => 'Tax(10%)', 'prercentage' => 10],
            ['title' => 'Tax(5%)', 'prercentage' => 5],
        ]);
    }
}
