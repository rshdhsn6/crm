<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Modules\Crm\Entities\Client;

$factory->define(Client::class, function (Faker $faker) {
    return [
        'client_group_id' => $faker->numberBetween($min = 1, $max = 4),
        'name' => $faker->company,
        'phone' => '+880'.$faker->numberBetween($min = 1000000000, $max = 1999999999),
        'email' => $faker->email,
        'address' => $faker->streetName,
        'city' => $faker->city,
        'state' => $faker->state,
        'zip' => $faker->postcode,
        'country' => $faker->country,
        'website' => 'www.'.$faker->domainName,
        'vat_number' => $faker->numberBetween($min = 10000, $max = 90000),
        'status' => 1,
    ];
});
