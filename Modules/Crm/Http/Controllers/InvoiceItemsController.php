<?php

namespace Modules\Crm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Modules\Crm\Entities\InvoiceItem;

class InvoiceItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('crm::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('crm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'item' => 'required',

        ];
        $messages = [
            'item.required' => 'The item field is required.',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();

        if (InvoiceItem::where('invoice_id', '=', $request->invoiceid)->where('item_id', '=', $request->item)->exists()) {
            return response()->json([
                'message' => 'Already exist in your cart.'
            ]);
        }
        DB::beginTransaction();
        try {
            $invoiceItem = new InvoiceItem();
            $invoiceItem->invoice_id   = $request->invoiceid;
            $invoiceItem->item_id       = $request->item;
            $invoiceItem->quantity      = $request->quantity;
            $invoiceItem->rate          = $request->rate;
            $invoiceItem->discount      = $request->discount;
            $invoiceItem->discount_type = $request->itemdisctype;
            $invoiceItem->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('crm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('crm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
