<?php

namespace Modules\Crm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Modules\Crm\Entities\ClientProject;
use Modules\Crm\Entities\Estimate;
use Modules\Crm\Entities\EstimatedItem;
use Modules\Crm\Entities\Item;
use Yajra\DataTables\Facades\DataTables;

class EstimatesController extends Controller
{
    /*Note:: Status
    *1 for draft
    *2 for processing
    *3 for estimated
    *4 for invoiced
    *5 for cancel
    *6 for re-open
    */

    public function estimateRequests(Request $request)
    {
        if ($request->ajax()) {
            $estimates = Estimate::with('client')->whereIn('status', [1,2])->orderBy('updated_at', 'desc')->get();
            return Datatables::of($estimates)
                ->addIndexColumn()
                ->addColumn('estimateid', function ($estimate) {
                    $estimateId = '<a href="'.route('estimate-request-show',['id' => $estimate->id]).'" target="_blank" class="clickable">Request - '.$estimate->id.'</a>';
                    return $estimateId;
                })
                ->addColumn('created_at', function ($estimate) {
                    $createdAt = date('d M Y h:i a', strtotime($estimate->created_at));
                    return $createdAt;
                })
                ->addColumn('updated_at', function ($estimate) {
                    $updatedAt = date('d M Y h:i a', strtotime($estimate->updated_at));
                    return $updatedAt;
                })
                ->addColumn('status', function ($estimate) {
                    if ($estimate->status == 1) {
                        $status = '<span class="badge badge-pill badge-secondary mb-1">Draft</span>';
                    }elseif ($estimate->status == 2) {
                        $status = '<span class="badge badge-pill badge-primary mb-1">Processing</span>';
                    }
                    
                    return $status;
                })
                ->addColumn('action', function ($estimate) {
                    $btn = '<div class="table-actions">';
                    $btn .='<a class="edit-btn" type="button" data-toggle="modal" data-target="#estimateModal" data-whatever="1" data-id="'.$estimate->id.'"><i class="ik ik-edit-2"></i></a>';
                    $btn .='</div>';
                    return $btn;
                })
                ->rawColumns(['estimateid','created_at','updated_at', 'status','action'])
                ->make(true);
        }
        return view('crm::estimates.estimate-requests');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */

    public function estimateRequestShow($id)
    {
        $estimateRequest = Estimate::with('client', 'estimateditems')->where('id', $id)->first();
        return view('crm::estimates.estimate-request-details', compact('estimateRequest'));
    }

    public function estimateRequestStatusUpdate(Request $request)
    {
        $id = $request->estimateid;
        $status = $request->status;
        $estimateRequest = Estimate::find($id);
        $estimateRequest->status = $status;
        $estimateRequest->save();
    }
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $estimates = Estimate::with('client')->whereNotIn('status', [1,2])->get();
            return Datatables::of($estimates)
                ->addIndexColumn()
                ->addColumn('estimateid', function ($estimate) {
                    $estimateId = '<a href="'.route('estimate-details',['id' => $estimate->id]).'" target="_blank" class="clickable">Estimate - '.$estimate->id.'</a>';
                    return $estimateId;
                })
                ->addColumn('created_at', function ($estimate) {
                    $createdAt = date('d M Y h:i a', strtotime($estimate->created_at));
                    return $createdAt;
                })
                ->addColumn('updated_at', function ($estimate) {
                    $updatedAt = date('d M Y h:i a', strtotime($estimate->updated_at));
                    return $updatedAt;
                })
                ->addColumn('status', function ($estimate) {
                    if ($estimate->status == 3) {
                        $status = '<span class="badge badge-pill badge-success mb-1">Estimated</span>';
                    }elseif ($estimate->status == 4) {
                        $status = '<span class="badge badge-pill badge-warning mb-1">Invoiced</span>';
                    }
                    
                    return $status;
                })
                ->addColumn('action', function ($estimate) {
                    $btn = '<div class="table-actions">';
                    $btn .='<a class="edit-btn" type="button" data-toggle="modal" data-target="#estimateModal" data-whatever="1" data-id="'.$estimate->id.'"><i class="ik ik-edit-2"></i></a>';
                    $btn .='</div>';
                    return $btn;
                })
                ->rawColumns(['estimateid','created_at','updated_at', 'status','action'])
                ->make(true);
        }
        return view('crm::estimates.list');
    }

    public function estimateDetails($id)
    {
        $estimate = Estimate::with('client')->find($id);

        // if ($estimate->discount_type == 1) {
        //     $estiamteTotal = $rowtotal =(($estItem->quantity * $estItem->item->rate) - (($estItem->item->discount * $estItem->item->rate)/100));
        // }

        return view('crm::estimates.estimate-details', compact('estimate'));
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('crm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'item' => 'required',
            'client' => 'required',

        ];
        $messages = [
            'item.required' => 'The item field is required.',
            'client.required' => 'The client field is required.',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();

        $item = Item::where('id', $request->item)->first();

        DB::beginTransaction();
        try {
            $estimate = new Estimate();
            $estimate->client_id   = $request->client;
            $estimate->description = $request->description;
            $estimate->budget      = $request->budget;
            $estimate->example_link = $request->examplelink;
            $estimate->status = 1;
            $estimate->save();

            $estimatedItem = new EstimatedItem();
            $estimatedItem->item_id = $item->id;
            $estimatedItem->rate = $item->rate;
            $estimatedItem->quantity = 1;
            $estimatedItem->discount = $item->discount;
            $estimatedItem->discount_type = $item->discount_type;
            $estimate->estimateditems()->save($estimatedItem);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {   
        return Estimate::with('estimateditems')->find($id);

        //return view('crm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return Estimate::with('client')->find($id);
        //return view('crm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'item' => 'required',
            'client' => 'required',

        ];
        $messages = [
            'item.required' => 'The item field is required.',
            'client.required' => 'The client field is required.',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();

        DB::beginTransaction();
        try {
            $estimate = Estimate::find($id);
            $estimate->client_id   = $request->client;
            $estimate->description = $request->description;
            $estimate->budget      = $request->budget;
            $estimate->example_link = $request->examplelink;
            $estimate->status = $request->status;
            $estimate->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
    }

    public function updateEstimate(Request $request)
    {
        $id = $request->esitmateid;
        $estimate = Estimate::find($id);
        if ($request->client) {
            $estimate->client_id = $request->client;
        }
        $estimate->description  = $request->description;
        $estimate->budget       = $request->budget;
        $estimate->example_link = $request->examplelink;
        $estimate->save();
    }
    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function markAsProject(Request $request)
    {
        $id = $request->estimateid;

        DB::beginTransaction();
        try {
            $estimate = Estimate::find($id);
            $estimate->status = 4;
            $estimate->save();

            $project = new ClientProject();
            $project->title = $request->title;
            $project->client_id = $estimate->client_id;
            $project->start_date = $request->startdate;
            $project->end_date = $request->enddate;
            $project->price = $request->price;
            $project->project_status_id = 1;
            $estimate->project()->save($project);
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
    }
}
