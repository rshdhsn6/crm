<?php

namespace Modules\Crm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Crm\Entities\Item;
use Yajra\DataTables\Facades\DataTables;

class ItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $items = Item::with('unittype')->orderBy('name', 'asc')->get();
            return Datatables::of($items)
                ->addIndexColumn()
                ->addColumn('description', function ($item) {
                    $description = '<div class="full-text">'.ucfirst($item->description).'</div>';
                    return $description;
                })
                ->addColumn('discount_type', function ($item) {
                    $dType = '<div class="text-center">';
                    if ($item->discount_type == 1) {
                        $dType .= '<p>Percentage(%)</p>';

                    }else{
                        $dType .= '<p>Fixed Amount</p>';
                    }
                    $dType .= '</div>';
                    return $dType;
                })
                ->addColumn('updated_at', function ($item) {
                    $updatedAt = date('d M Y h:i a', strtotime($item->updated_at));
                    return $updatedAt;
                })
                ->addColumn('action', function ($item) {
                    $btn = '<div class="table-actions">';
                    $btn .='<a class="edit-btn" type="button" data-toggle="modal" data-target="#itemModal" data-whatever="1" data-id="'.$item->id.'"><i class="ik ik-edit-2"></i></a>';
                    $btn .='</div>';
                    return $btn;
                })
                ->rawColumns(['description','discount_type', 'updated_at','action'])
                ->make(true);
        }

        return view('crm::items.list');
    }

    public function getItemsIdName()
    {
        return Item::orderBy('name', 'asc')->get(['id', 'name']);
    }



    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('crm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'rate' => 'required',

        ];
        $messages = [
            'name.required' => 'The name field is required.',
            'rate.required' => 'The rate field is required.',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();

        $item = new Item();
        $item->name         = $request->name;
        $item->description  = $request->description;
        $item->unit_type_id = $request->unittype;
        $item->rate         = $request->rate;
        $item->discount     = $request->discount;
        $item->discount_type = $request->discount_type;
        $item->save();
        return $item;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return $item = Item::with('unittype')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('crm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required',
            'rate' => 'required',

        ];
        $messages = [
            'name.required' => 'The name field is required.',
            'rate.required' => 'The rate field is required.',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();

        $item = Item::find($id);
        $item->name         = $request->name;
        $item->description  = $request->description;
        $item->unit_type_id = $request->unittype;
        $item->rate         = $request->rate;
        $item->discount     = $request->discount;
        $item->discount_type = $request->discount_type;
        $item->save();
        return $item;
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
