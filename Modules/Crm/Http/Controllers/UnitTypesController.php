<?php

namespace Modules\Crm\Http\Controllers;

use DataTables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Crm\Entities\UnitType;

class UnitTypesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $unitType = UnitType::all();
            return Datatables::of($unitType)
                ->addIndexColumn()
                ->addColumn('action', function($unitType){
                    $btn = '<div class="table-actions">';
                    $btn .='<a class="edit-btn" type="button" data-toggle="modal" data-target="#unitTypeModal" data-whatever="1" data-id="'.$unitType->id.'"><i class="ik ik-edit-2"></i></a>';
                    $btn .='</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('crm::items.unit_types');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('crm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:crm.unit_types,name',
        ];
        $messages = [
            'name.required' => 'The name field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $unitType = new UnitType();
        $unitType->name        = $request->name;
        $unitType->description = $request->description;
        $unitType->save();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return UnitType::find($id);
        //return view('crm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {

        return view('crm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|unique:crm.unit_types,name,'. $id .'',

        ];
        $messages = [
            'name.required' => 'The name field is required.',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();
        
        $client = UnitType::find($id);
        $client->name        = $request->name;
        $client->description = $request->description;
        $client->save();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
