<?php

namespace Modules\Crm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Crm\Entities\PaymentMethod;
use Yajra\DataTables\Facades\DataTables;

class PaymentMethodsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $methods = PaymentMethod::all();
            return Datatables::of($methods)
                ->addIndexColumn()
                ->addColumn('action', function ($method) {
                    $btn = '<div class="table-actions">';
                    $btn .='<button class="edit-btn edit-method" type="button" value="'.$method->id.'"><i class="ik ik-edit-2"></i></button>';
                    $btn .='</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('crm::settings.payment-methods');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('crm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'     => 'required|unique:crm.payment_methods,title',
        ];
        $messages = [
            'title.required'     => 'The title field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();

        $paymentMethod = new PaymentMethod();
        $paymentMethod->title = $request->title;
        $paymentMethod->status = 1;
        $paymentMethod->save();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return PaymentMethod::find($id);
        //return view('crm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('crm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'title' => 'required|unique:crm.payment_methods,title,'. $id .'',

        ];
        $messages = [
            'title.required' => 'The title field is required.',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();

        $method = PaymentMethod::find($id);
        $method->title = $request->title;
        $method->save();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
