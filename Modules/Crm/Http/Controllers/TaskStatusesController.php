<?php

namespace Modules\Crm\Http\Controllers;

use DataTables;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Crm\Entities\TaskStatus;

class TaskStatusesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $taskstatuses = TaskStatus::all();
            return Datatables::of($taskstatuses)
                ->addIndexColumn()
                ->addColumn('colorcode', function ($taskstatus) {
                    $colorCode = '<span style="background-color:'.$taskstatus->colorcode.'" class="color-tag"></span>'.$taskstatus->colorcode;
                    return $colorCode;
                })
                ->addColumn('action', function($taskstatus){
                    $btn = '<div class="table-actions">';
                    $btn .='<a class="edit-btn" type="button" data-toggle="modal" data-target="#taskStatusModal" data-whatever="1" data-id="'.$taskstatus->id.'"><i class="ik ik-edit-2"></i></a>';
                    $btn .='</div>';
                    return $btn;
                })
                ->rawColumns(['colorcode','created_at','action'])
                ->make(true);
        }
        //return view('crm::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('crm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'     => 'required|unique:crm.task_statuses,title',
            'colorcode' => 'required',
        ];
        $messages = [
            'title.required'     => 'The title field is required.',
            'colorcode.required' => 'The color code field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $taskStatus = new TaskStatus();
        $taskStatus->title     = $request->title;
        $taskStatus->colorcode = $request->colorcode;
        $taskStatus->save();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return TaskStatus::find($id);
        //return view('crm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('crm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'title'     => 'required|unique:crm.task_statuses,title,'. $id .'',
            'colorcode' => 'required',
        ];
        $messages = [
            'title.required'     => 'The title field is required.',
            'colorcode.required' => 'The color code field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $taskStatus = TaskStatus::find($id);
        $taskStatus->title     = $request->title;
        $taskStatus->colorcode = $request->colorcode;
        $taskStatus->save();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
