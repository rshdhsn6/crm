<?php

namespace Modules\Crm\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Crm\Entities\Invoice;
use Modules\Crm\Entities\Payment;

class PaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('crm::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('crm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //return $request->all();
        $invoiceid = $request->invoiceid;

        $paymentmethods = $request->paymentmethod;
        $amounts = $request->amount;
        $paymentdate = $request->paymentdate;

        $data = [];

        $totalamount = 0;
        $i = 0;
        foreach ($paymentmethods as $paymentmethod) {
            $data[] = [
                'invoice_id' => $invoiceid,
                'payment_method_id' => $paymentmethod,
                'amount' => $amounts[$i],
                'paymentdate' => $paymentdate,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];

            $totalamount += $amounts[$i];
            $i++;
        }

        if (Payment::insert($data)) {
            $invoice = Invoice::find($invoiceid);

            $previousamount = $invoice->payment_received;
            $newamount = $previousamount + $totalamount;
            $invoice->payment_received = $newamount;
            $invoice->save();
        }

        return;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('crm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('crm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
