<?php

namespace Modules\Crm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Crm\Entities\ProjectMember;
use Modules\Crm\Entities\ProjectTask;
use Yajra\DataTables\Facades\DataTables;

class ProjectTasksController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $tasks = ProjectTask::with('project', 'employee', 'status')->get();
            return Datatables::of($tasks)
                ->addIndexColumn()
                ->addColumn('status', function ($task) {
                    $selectStatus = '<div class="show-client">';
                    $selectStatus .='<div class="dropdown-toggle d-hover" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$task->status->title.'</div>';
                    $selectStatus .='<div class="dropdown-menu keep-open">
                                        <div class="dropdown-item">
                                            <select name="taskstatusupdate" id="taskstatusupdate-'.$task->id.'" class="form-control select2 taskstatusupdate" onchange="changeTaskStatus('.$task->id.')">
                                            </select>
                                        </div>
                                    </div>';
                    $selectStatus .='</div>';
                    return $selectStatus;
                })
                ->addColumn('action', function ($task) {
                    $btn = '<div class="table-actions">';
                    $btn .='<a class="edit-btn" type="button" data-toggle="modal" data-target="#projectTaskModal" data-whatever="1" data-id="'.$task->id.'"><i class="ik ik-edit-2"></i></a>';
                    $btn .='</div>';
                    return $btn;
                })
                ->rawColumns(['status', 'action'])
                ->make(true);
        }
        return view('crm::projects.tasks');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('crm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'      => 'required',
            'project'    => 'required',
            'assignto'   => 'required',
            'taskstatus' => 'required',
            'startdate'  => 'required',
            'deadline'   => 'required',

        ];
        $messages = [
            'title.required'      => 'The title field is required.',
            'project.required'    => 'The project field is required.',
            'assignto.required'   => 'The assign to field is required.',
            'taskstatus.required' => 'The status field is required.',
            'startdate.required'  => 'The start date field is required.',
            'deadline.required'   => 'The deadline field is required.',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();

        if (!ProjectMember::where('project_id', $request->project)->where('employee_id', $request->assignto)->exists()) {
            $projectMember = new ProjectMember();
            $projectMember->project_id = $request->project;
            $projectMember->employee_id = $request->assignto;
            $projectMember->save();
        }

        $task = new ProjectTask();
        $task->title          = $request->title;
        $task->description    = $request->description;
        $task->project_id     = $request->project;
        $task->employee_id    = $request->assignto;
        $task->task_status_id = $request->taskstatus;
        $task->startdate      = $request->startdate;
        $task->deadline       = $request->deadline;
        $task->save();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return ProjectTask::find($id);
        //return view('crm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('crm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'title'      => 'required',
            'project'    => 'required',
            'assignto'   => 'required',
            'taskstatus' => 'required',
            'startdate'  => 'required',
            'deadline'   => 'required',

        ];
        $messages = [
            'title.required'      => 'The title field is required.',
            'project.required'    => 'The project field is required.',
            'assignto.required'   => 'The assign to field is required.',
            'taskstatus.required' => 'The status field is required.',
            'startdate.required'  => 'The start date field is required.',
            'deadline.required'   => 'The deadline field is required.',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();

        $task = ProjectTask::find($id);
        $task->title          = $request->title;
        $task->description    = $request->description;
        $task->project_id     = $request->project;
        $task->employee_id    = $request->assignto;
        $task->task_status_id = $request->taskstatus;
        $task->startdate      = $request->startdate;
        $task->deadline       = $request->deadline;
        $task->save();
    }

    public function updateTaskStatus(Request $request)
    {
        $id = $request->id;
        $status = $request->status;

        $task = ProjectTask::find($id);
        $task->task_status_id = $status;
        $task->save();
    }
    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
