<?php

namespace Modules\Crm\Http\Controllers;

use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Modules\Crm\Entities\Estimate;
use Modules\Crm\Entities\EstimatedItem;
use Yajra\DataTables\Facades\DataTables;

class EstimatedItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $estimateid = $request->estimateid;
            $estItems = EstimatedItem::with('item', 'item.unittype')->where('estimate_id', $estimateid)->get();
            return Datatables::of($estItems)
                ->addIndexColumn()
                ->addColumn('rate', function ($estItem) {
                    $rate = '<div>';
                    $rate .='<span>'.$estItem->rate.'</span> <span> ('.$estItem->item->unittype->name.')</span>';
                    $rate .='</div>';
                    return $rate;
                })
                ->addColumn('discount', function ($estItem) {
                    $discount = '<div>';
                    $discount .='<span>'.$estItem->discount.'</span>';
                    if ($estItem->discount_type == 1) {
                        $discount .='<span> %</span>';
                    }
                    $discount .='</div>';
                    return $discount;
                })
                ->addColumn('rowtotal', function ($estItem) {
                    if ($estItem->discount_type == 1) {
                        $rowtotal =(($estItem->quantity * $estItem->rate) - (($estItem->discount * $estItem->rate)/100));
                    }else{
                        $rowtotal =(($estItem->quantity * $estItem->rate) - ($estItem->quantity * $estItem->discount));
                    }
                    return $rowtotal;
                })
                ->addColumn('action', function ($estItem) {
                    $btn = '<div class="table-actions">';
                    $btn .='<a class="delete-btn" type="button" data-id="'.$estItem->id.'"><i class="ik ik-x"></i></a>';
                    $btn .='</div>';
                    return $btn;
                })
                ->rawColumns(['rate', 'discount', 'rowtotal', 'action'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('crm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'item' => 'required',

        ];
        $messages = [
            'item.required' => 'The item field is required.',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();

        if (EstimatedItem::where('estimate_id', '=', $request->estimateid)->where('item_id', '=', $request->item)->exists()) {
            return response()->json([
                'message' => 'Already exist in your cart.'
            ]);
        }
        $estimateItem = new EstimatedItem();
        $estimateItem->estimate_id   = $request->estimateid;
        $estimateItem->item_id       = $request->item;
        $estimateItem->quantity      = $request->quantity;
        $estimateItem->rate          = $request->rate;
        $estimateItem->discount      = $request->discount;
        $estimateItem->discount_type = $request->itemdisctype;
        $estimateItem->save();
        return $estimateItem;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('crm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('crm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $estimatedItem = EstimatedItem::find($id);
        $estimatedItem->delete();

    }

    public function estimateDiscount(Request $request)
    {
        $id = $request->id;
        return Estimate::find($id);
    }

    public function estimateDiscountUpdate(Request $request, $id)
    {
        $estimate = Estimate::find($id);
        $estimate->discount = $request->discount;
        $estimate->discount_type = $request->discount_type;
        $estimate->save();
    }


    public function estimatePdfGenerate($id)
    {
        $data = Estimate::with('client', 'estimateditems')->find($id);

        $pdf = PDF::loadView('crm::pdf.estimate', ['data' => $data]);

        return $pdf->download('estimate-'.$data->id.'.pdf');
    }
}
