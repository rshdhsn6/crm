<?php

namespace Modules\Crm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Modules\Crm\Entities\ClientProject;
use Modules\Crm\Entities\ProjectMember;
use Modules\Crm\Entities\ProjectTask;
use Yajra\DataTables\Facades\DataTables;

class ClientProjectsController extends Controller
{
    /**
     * constructor
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $projects = ClientProject::with('client', 'status')->orderBy('end_date', 'desc')->get();
            return Datatables::of($projects)
                ->addIndexColumn()
                ->addColumn('title', function ($project) {
                    $title = '<div class="show-project">';
                    $title .='<a href="'.route('project-details',['id' => $project->id]).'" class="clickable" target="_blank">'.$project->title.'</a>';
                    $title .='</div>';
                    return $title;
                })
                ->addColumn('client', function ($project) {
                    $anchortag = '<div class="show-client">';
                    $anchortag .='<a href="'.route('clients.index').'/'.$project->client->id.'" class="clickable" target="_blank">'.$project->client->name.'</a>';
                    $anchortag .='</div>';
                    return $anchortag;
                })
                ->addColumn('deadline', function ($project) {
                    if ($project->project_status_id == 1) {
                        $deadline ='<span class="text-danger">'.$project->end_date.'</span>';
                    }else{
                        $deadline ='<span>'.$project->end_date.'</span>';
                    }
                    
                    return $deadline;
                })
                ->addColumn('action', function ($project) {
                    $btn = '<div class="table-actions">';
                    $btn .='<a class="edit-btn" type="button" data-toggle="modal" data-target="#projectModal" data-whatever="1" data-id="'.$project->id.'"><i class="ik ik-edit-2"></i></a>';
                    $btn .='</div>';
                    return $btn;
                })
                ->rawColumns(['title', 'client', 'deadline', 'action'])
                ->make(true);
        }        
        return view('crm::projects.list');
    }

    public function projectDetails($id)
    {
        $project = ClientProject::with('client')->find($id);
        $projectMembers = ProjectMember::with('employee')->where('project_id', $id)->get();
        $tasks = ProjectTask::with('employee', 'status')->where('project_id', $id)->orderBy('id', 'desc')->take(10)->get();
        return view('crm::projects.details', compact('project', 'projectMembers', 'tasks'));
    }

    public function projectTasks($projectid)
    {
        $tasks = ProjectTask::with('project', 'employee', 'status')->where('project_id', $projectid)->get();
        return Datatables::of($tasks)
            ->addIndexColumn()
            ->addColumn('status', function ($task) {
                $selectStatus = '<div class="show-client">';
                $selectStatus .='<div class="dropdown-toggle d-hover" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'.$task->status->title.'</div>';
                $selectStatus .='<div class="dropdown-menu keep-open">
                                    <div class="dropdown-item">
                                        <select name="taskstatusupdate" id="taskstatusupdate-'.$task->id.'" class="form-control select2 taskstatusupdate" onchange="changeTaskStatus('.$task->id.')">
                                        </select>
                                    </div>
                                </div>';
                $selectStatus .='</div>';
                return $selectStatus;
            })
            ->addColumn('action', function ($task) {
                $btn = '<div class="table-actions">';
                $btn .='<a class="edit-btn" type="button" data-toggle="modal" data-target="#projectTaskModal" data-whatever="1" data-id="'.$task->id.'"><i class="ik ik-edit-2"></i></a>';
                $btn .='</div>';
                return $btn;
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    public function getProjectNameId()
    {
        return ClientProject::get(['id', 'title']);
    }
    public function getSpecificClientProjectNameId(Request $request)
    {
        $clientid = $request->clientid;

        return ClientProject::where('client_id', $clientid)->get(['id', 'title']);
    }

    public function getClientProjectIdName(Request $request)
    {
        $clientid = $request->clientid;
        return ClientProject::where('client_id', $clientid)->get(['id', 'title']);
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('crm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'client' => 'required',

        ];
        $messages = [
            'title.required' => 'The title field is required.',
            'client.required' => 'The client field is required.',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();

        $cProject = new ClientProject();
        $cProject->title       = $request->title;
        $cProject->client_id   = $request->client;
        $cProject->description = $request->description;
        $cProject->start_date  = $request->startdate;
        $cProject->end_date    = $request->enddate;
        $cProject->price       = $request->price;
        $cProject->project_status_id = 1;
        $cProject->save();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return ClientProject::with('client', 'status')->find($id);
        //return view('crm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('crm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'title' => 'required',
            'client' => 'required',

        ];
        $messages = [
            'title.required' => 'The title field is required.',
            'client.required' => 'The client field is required.',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();

        $cProject = ClientProject::find($id);
        $cProject->title       = $request->title;
        $cProject->client_id   = $request->client;
        $cProject->description = $request->description;
        $cProject->start_date  = $request->startdate;
        $cProject->end_date    = $request->enddate;
        $cProject->price       = $request->price;
        $cProject->project_status_id = $request->status;
        $cProject->save();
        return $cProject;
    }

    public function specificClientProjects(Request $request)
    {
        if ($request->ajax()) {
            $clientid = $request->clientid;
            $projects = ClientProject::with('status')->orderBy('end_date', 'asc')->where('client_id', $clientid)->get();
            return Datatables::of($projects)
                ->addIndexColumn()
                ->addColumn('title', function ($project) {
                    $title = '<div class="show-project">';
                    $title .='<a href="'.route('project-details',['id' => $project->id]).'" class="clickable" target="_blank">'.$project->title.'</a>';
                    $title .='</div>';
                    return $title;
                })
                ->addColumn('deadline', function ($project) {
                    if ($project->project_status_id == 1) {
                        $deadline ='<span class="text-danger">'.$project->end_date.'</span>';
                    }else{
                        $deadline ='<span>'.$project->end_date.'</span>';
                    }
                    
                    return $deadline;
                })
                ->addColumn('action', function ($project) {
                    $btn = '<div class="table-actions">';
                    $btn .='<a class="edit-btn" type="button" data-toggle="modal" data-target="#projectModal" data-whatever="1" data-id="'.$project->id.'"><i class="ik ik-edit-2"></i></a>';
                    $btn .='</div>';
                    return $btn;
                })
                ->rawColumns(['title', 'deadline', 'action'])
                ->make(true);
        }
    }
    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        
    }
}
