<?php

namespace Modules\Crm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Modules\Crm\Entities\ClientContact;
use Yajra\DataTables\Facades\DataTables;

class ClientContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $clientid = $request->clientid;
            $clientContacts = ClientContact::where('client_id', $clientid)->get();
            return Datatables::of($clientContacts)
                ->addIndexColumn()
                ->addColumn('fullname', function ($clientContact) {
                    if ($clientContact->photo == null && $clientContact->gender == 1) {
                        $fullname = '<a href="'.route('contacts.show', $clientContact->id).'" class="clickable"><img class="img-responsive contact-person-photo-small" src="'.asset('storage/crm/male-avatar.png').'"/> '.ucwords($clientContact->first_name.' '.$clientContact->last_name).'</a>';
                    }
                    if ($clientContact->photo == null && $clientContact->gender == 2) {
                        $fullname = '<a href="'.route('contacts.show', $clientContact->id).'" class="clickable"><img class="img-responsive contact-person-photo-small" src="'.asset('storage/crm/female-avatar.png').'"/> '.ucwords($clientContact->first_name.' '.$clientContact->last_name).'</a>';
                    }
                    if ($clientContact->photo == null && $clientContact->gender == 3) {
                        $fullname = '<a href="'.route('contacts.show', $clientContact->id).'" class="clickable"><img class="img-responsive contact-person-photo-small" src="'.asset('storage/crm/other-avatar.png').'"/> '.ucwords($clientContact->first_name.' '.$clientContact->last_name).'</a>';
                    }
                    if ($clientContact->photo) {
                        $fullname = '<a href="'.route('contacts.show', $clientContact->id).'" class="clickable"><img class="img-responsive contact-person-photo-small" src="'.asset('storage/crm/contact/'.$clientContact->photo.'').'"/> '.ucwords($clientContact->first_name.' '.$clientContact->last_name).'</a>';
                    }
                    if ($clientContact->is_primary_contact == 1) {
                        $isPrimay = ' <span class="font-weight-bold text-success primarycontact">Primary Contact</span>';
                    }else{
                        $isPrimay = '';
                    }
                    return $fullname.$isPrimay;
                })
                ->addColumn('created_at', function ($clientContact) {
                    $createdAt = '<div class="text-center">'.date('d M Y h:i a', strtotime($clientContact->created_at)).'</div>';
                    
                    return $createdAt;
                })
                ->addColumn('action', function ($clientContact) {
                    $btn = '<div class="table-actions">';
                    $btn .= '<a class="edit-btn" type="button" data-toggle="modal" data-target="#contactModal" data-whatever="1" data-id="' . $clientContact->id . '"><i class="ik ik-edit-2"></i></a>';
                    $btn .= '<a class="delete-btn" type="button" data-id="' . $clientContact->id . '"><i class="ik ik-trash-2"></i></a>';
                    $btn .= '</div>';
                    
                    return $btn;
                })
                ->rawColumns(['fullname','created_at','action'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('crm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'firstname' => 'required',
            'lastname' => 'required',
            'jobtitle' => 'required',
            'contact_person_phone' => 'required',
            'contact_person_email' => 'required',
        ];
        $messages = [
            'firstname.required' => 'The First Name field is required.',
            'lastname.required'  => 'The Last Name field is required.',
            'jobtitle.required'  => 'The Job Title field is required.',
            'contact_person_phone.required' => 'The Phone field is required.',
            'contact_person_email.required' => 'The Email field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $clientid = $request->client_id;
        if ($request->is_primary_contact) {
            $primarycontactrequest = $request->is_primary_contact;
            if (ClientContact::where('client_id', $clientid)->where('is_primary_contact', '=', 1)->exists()) {
                $warning = ['exists' => true] ;
                return $warning;
            }
        }else{
            $primarycontactrequest = 0;
        }

        $contact = new ClientContact();
        $contact->client_id          = $clientid;
        $contact->first_name         = $request->firstname;
        $contact->last_name          = $request->lastname;
        $contact->gender             = $request->gender;
        $contact->job_title          = $request->jobtitle;
        $contact->phone              = $request->contact_person_phone;
        $contact->email              = $request->contact_person_email;
        $contact->skype              = $request->contact_person_skype;
        $contact->whatsapp           = $request->contact_person_whats_app;
        if ($request->contact_person_password) {
            $password = Hash::make($request->contact_person_password);
        }else{
            $password = Hash::make('12345678');
        }
        $contact->password = $password;
        $contact->is_primary_contact = $primarycontactrequest;
        $contact->is_login_access    = $request->is_login_access;
        $contact->save();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $contactperson = ClientContact::with('company')->find($id);
        return view('crm::clients.contact-profile', compact('contactperson'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Request $request)
    {
        $contactid = $request->id;
        return ClientContact::find($contactid);

        //return view('crm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'firstname' => 'required',
            'lastname' => 'required',
            'jobtitle' => 'required',
            'contact_person_phone' => 'required',
            'contact_person_email' => 'required',
        ];
        $messages = [
            'firstname.required' => 'The First Name field is required.',
            'lastname.required'  => 'The Last Name field is required.',
            'jobtitle.required'  => 'The Job Title field is required.',
            'contact_person_phone.required' => 'The Phone field is required.',
            'contact_person_email.required' => 'The Email field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        /***********checking for primary contact*****************/
        $clientid = $request->client_id;
        $primarycontactrequest = $request->is_primary_contact;


        $contact = ClientContact::find($id);
        if ($primarycontactrequest && ($contact->is_primary_contact == 0)) {
            if (ClientContact::where('client_id', $clientid)->where('is_primary_contact', '=', 1)->exists()) {
                $warning = ['exists' => true] ;
                return $warning;
            }
        }
        /***********end of checking for primary contact***********/
        
        $contact->first_name         = $request->firstname;
        $contact->last_name          = $request->lastname;
        $contact->gender             = $request->gender;
        $contact->job_title          = $request->jobtitle;
        $contact->phone              = $request->contact_person_phone;
        $contact->email              = $request->contact_person_email;
        $contact->skype              = $request->contact_person_skype;
        $contact->whatsapp           = $request->contact_person_whats_app;
        if ($request->contact_person_password) {
            $password = Hash::make($request->contact_person_password);
        }else{
            $password = Hash::make('12345678');
        }
        $contact->password = $password;
        $contact->is_primary_contact = $primarycontactrequest;
        $contact->is_login_access    = $request->is_login_access;
        $contact->save();
    }

    public function updateProfilePhoto(Request $request)
    {
        $rules = [
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ];
        Validator::make($request->all(), $rules)->validate();

        $contact = ClientContact::find($request->id);
        
        if ($request->hasFile('photo')) {
            if ($contact->photo != '') {
                File::delete(storage_path('/app/public/crm/contact/') . $contact->photo);
            }
            $file = $request->photo;
            $image = md5($file->getClientOriginalName() . time()) . "." . $file->getClientOriginalExtension();
            Image::make($file)->resize(400, 400)->save(storage_path('/app/public/crm/contact/') . $image);
            $contact->photo = $image;
            $contact->save();
        }
    }


    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $contact = ClientContact::find($id);
        if ($contact->is_primary_contact == 1) {
            $warning = ['exists' => true] ;
            return $warning;
        }else{
            return "Delete";
        }
    }
}
