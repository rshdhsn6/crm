<?php

namespace Modules\Crm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Modules\Crm\Entities\ClientProject;
use Modules\Crm\Entities\Invoice;
use Yajra\DataTables\Facades\DataTables;

class InvoicesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $invoices = Invoice::with('client')->orderBy('id', 'desc')->get();
            return Datatables::of($invoices)
                ->addIndexColumn()
                ->addColumn('invoiceid', function ($invoice) {
                    $invoiceid = '<div>';
                    $invoiceid .='<a href="'.route('invoices.index').'/'.$invoice->id.'" class="clickable" target="_blank">Inv-'.$invoice->id.'</a>';
                    $invoiceid .='</div>';
                    return $invoiceid;
                })
                ->addColumn('client', function ($invoice) {
                    $client = '<div class="show-client">';
                    $client .='<a href="'.route('clients.index').'/'.$invoice->client->id.'" class="clickable" target="_blank">'.$invoice->client->name.'</a>';
                    $client .='</div>';
                    return $client;
                })
                ->addColumn('project', function ($invoice) {
                    $client = '<div class="show-client">';
                    $client .='<a href="'.route('project-details',['id' => $invoice->project->id]).'" class="clickable" target="_blank">'.$invoice->project->title.'</a>';
                    $client .='</div>';
                    return $client;
                })
                ->addColumn('due', function($invoice){
                    $damount = $invoice->invoice_value - $invoice->payment_received;
                    if ($damount > 0) {
                        $due = '<span class="text-danger">'.$damount.'</span>';
                    }else{
                        $due = '<span class="text-success">'.$damount.'</span>';
                    }
                    return $due;
                })
                ->addColumn('action', function ($invoice) {
                    $action = '<div class="action-box text-center">';
                    $action .='<div class="dropdown-toggle d-hover" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ik ik-more-horizontal-"></i></div>';
                    $action .='<div class="dropdown-menu keep-open">
                                        <div class="dropdown-item">
                                            <ul class="list-group">
                                              <li class="list-group-item"><a data-toggle="modal" data-target="#paymentModal" data-id="'.$invoice->id.'"><i class="ik ik-file-plus"></i> Add Payment</a></li>
                                              <li class="list-group-item"><a data-toggle="modal" data-target="#invoiceModal" data-id="'.$invoice->id.'" data-whatever="1"><i class="ik ik-edit"></i> Edit</a></li>
                                              <li class="list-group-item"><a href="#"><i class="ik ik-trash-2"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>';
                    $action .='</div>';
                    return $action;
                })
                ->rawColumns(['invoiceid', 'client', 'project', 'due', 'action'])
                ->make(true);
        } 
        return view('crm::invoices.list');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('crm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'billdate' => 'required',
            'duedate'  => 'required',
            'client'   => 'required',
            'project'  => 'required',

        ];
        $messages = [
            'billdate.required' => 'The bill date field is required.',
            'duedate.required'  => 'The due date field is required.',
            'client.required'  => 'The client field is required.',
            'project.required'  => 'The project field is required.',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();

        $project = ClientProject::find($request->project)->first('price');

        try {
          DB::beginTransaction();

          $invoice = new Invoice();
          $invoice->client_id  = $request->client;
          $invoice->project_id = $request->project;
          $invoice->billdate   = $request->billdate;
          $invoice->duedate    = $request->duedate;
          $invoice->invoice_value = $project->price;
          $invoice->note       = $request->note;
          $invoice->status     = 1;
          $invoice->save();

          DB::commit();

        } catch (Exception $e) {
          DB::rollback();
        }
        
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $invoice = Invoice::with('project', 'client', 'payments', 'payments.paymentmethod')->find($id);
        return view('crm::invoices.details', compact('invoice'));
    }

    public function getInvoiceToPay(Request $request)
    {
        $id = $request->invoiceid;
        return $invoice = Invoice::find($id);
    }
    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return Invoice::with('project')->find($id);
        //return view('crm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'billdate' => 'required',
            'duedate'  => 'required',
            'client'   => 'required',
            'project'  => 'required',

        ];
        $messages = [
            'billdate.required' => 'The bill date field is required.',
            'duedate.required'  => 'The due date field is required.',
            'client.required'  => 'The client field is required.',
            'project.required'  => 'The project field is required.',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();

        $project = ClientProject::find($request->project)->first('price');

        try {
          DB::beginTransaction();

          $invoice = Invoice::find($id);
          $invoice->client_id  = $request->client;
          $invoice->project_id = $request->project;
          $invoice->billdate   = $request->billdate;
          $invoice->duedate    = $request->duedate;
          $invoice->invoice_value = $project->price;
          $invoice->note       = $request->note;
          $invoice->status     = 1;
          $invoice->save();

          DB::commit();

        } catch (Exception $e) {
          DB::rollback();
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /*
     *specific client invoices
     */
    public function specificClientInvoices(Request $request)
    {
      $clientid = $request->clientid;
      $invoices = Invoice::with('project')->orderBy('duedate', 'desc')->where('client_id', $clientid)->get();
      return Datatables::of($invoices)
          ->addIndexColumn()
          ->addColumn('project', function ($invoice) {
              $client = '<div class="show-client">';
              $client .='<a href="'.route('project-details',['id' => $invoice->project->id]).'" class="clickable" target="_blank">'.$invoice->project->title.'PPPP</a>';
              $client .='</div>';
              return $client;
          })
          ->rawColumns(['project'])
          ->make(true);
    }
}
