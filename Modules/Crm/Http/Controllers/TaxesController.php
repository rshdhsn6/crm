<?php

namespace Modules\Crm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Modules\Crm\Entities\Tax;
use Yajra\DataTables\Facades\DataTables;

class TaxesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $taxes = Tax::all();
            return Datatables::of($taxes)
                ->addIndexColumn()
                ->addColumn('action', function ($tax) {
                    $btn = '<div class="table-actions">';
                    $btn .='<button class="edit-btn edit-method" type="button" type="button" data-toggle="modal" data-target="#taxModal" data-whatever="1" data-id="'.$tax->id.'"><i class="ik ik-edit-2"></i></button>';
                    $btn .='</div>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        //return view('crm::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('crm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'taxtitle'     => 'required|unique:crm.taxes,title',
            'taxpercentage' => 'required',
        ];
        $messages = [
            'taxtitle.required'     => 'The title field is required.',
            'taxpercentage.required' => 'The color code field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $tax = new Tax();
        $tax->title     = $request->taxtitle;
        $tax->percentage = $request->taxpercentage;
        $tax->save();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return Tax::find($id);
        //return view('crm::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('crm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'taxtitle'     => 'required|unique:crm.taxes,title,'. $id .'',
            'taxpercentage' => 'required',
        ];
        $messages = [
            'taxtitle.required'     => 'The title field is required.',
            'taxpercentage.required' => 'The color code field is required.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();
        $taskStatus = Tax::find($id);
        $taskStatus->title     = $request->taxtitle;
        $taskStatus->percentage = $request->taxpercentage;
        $taskStatus->save();
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
