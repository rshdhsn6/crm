<?php

namespace Modules\Crm\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Modules\Crm\Entities\Client;
use Modules\Crm\Entities\ClientContact;
use Modules\Crm\Entities\ClientGroup;
use Modules\Crm\Entities\ClientProject;
use Modules\Crm\Entities\Invoice;
use Yajra\DataTables\Facades\DataTables;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $clients = Client::with(['primarycontact' => function($query){
                $query->where('is_primary_contact', '=', 1);
            }])->orderBy('created_at', 'desc')->get();
            //$clients = Client::with('primarycontact')->get();
            return Datatables::of($clients)
                ->addIndexColumn()
                ->addColumn('name', function ($client) {
                    $company = '<a href="'.route('clients.show', $client->id).'" class="clickable">'.ucwords($client->name).'</a>';
                    return $company;
                })
                ->addColumn('primarycontact', function ($client) {
                    if ($client->primarycontact) {
                        if ($client->primarycontact->photo == '' && $client->primarycontact->gender == 1) {
                            $fullname = '<a href="'.route('contacts.show', $client->primarycontact->id).'" class="clickable"><img class="img-responsive contact-person-photo-small" src="'.asset('storage/crm/male-avatar.png').'"/> '.ucwords($client->primarycontact->first_name.' '.$client->primarycontact->last_name).'</a>';
                        }
                        if ($client->primarycontact->photo == '' && $client->primarycontact->gender == 2) {
                            $fullname = '<a href="'.route('contacts.show', $client->primarycontact->id).'" class="clickable"><img class="img-responsive contact-person-photo-small" src="'.asset('storage/crm/female-avatar.png').'"/> '.ucwords($client->primarycontact->first_name.' '.$client->primarycontact->last_name).'</a>';
                        }
                        if ($client->primarycontact->photo == '' && $client->primarycontact->gender == 3) {
                            $fullname = '<a href="'.route('contacts.show', $client->primarycontact->id).'" class="clickable"><img class="img-responsive contact-person-photo-small" src="'.asset('storage/crm/other-avatar.png').'"/> '.ucwords($client->primarycontact->first_name.' '.$client->primarycontact->last_name).'</a>';
                        }
                        if ($client->primarycontact->photo) {
                            $fullname = '<a href="'.route('contacts.show', $client->primarycontact->id).'" class="clickable"><img class="img-responsive contact-person-photo-small" src="'.asset('storage/crm/contact/'.$client->primarycontact->photo.'').'"/> '.ucwords($client->primarycontact->first_name.' '.$client->primarycontact->last_name).'</a>';
                        }
                    }else{
                        $fullname= '<p class="font-weight-bolder">Sorry, Not Found!</p>';
                    }
                    
                    //$fullname = $client->primarycontact;
                    return $fullname;
                })
                ->addColumn('created_at', function ($client) {
                    $createdAt = date('d M Y h:i a', strtotime($client->created_at));
                    
                    return $createdAt;
                })
                ->addColumn('action', function ($client) {
                    if ($client->status == 1) {
                        $btn = '<div style="text-align: right;"><label class="switch"><input type="checkbox" checked onclick="changeStatus('.$client->id.')"><span class="slider"></span></label></div>';
                    }
                    if ($client->status == 0) {
                        $btn = '<div style="text-align: right;"><label class="switch"><input type="checkbox" onclick="changeStatus('.$client->id.')"><span class="slider"></span></label></div>';
                    }
                    
                    return $btn;
                })
                ->rawColumns(['name', 'primarycontact', 'created_at','action'])
                ->make(true);
        }
        return view('crm::.clients.list');
    }

    /**
    *
    *get client name and id only for select option
    *
    */
    public function getClientNameId()
    {
        return Client::orderBy('name')->get(['id', 'name']);
    }

    public function groups()
    {
        return ClientGroup::all();
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('crm::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            //'email' => 'nullable|unique:crm.clients,email',
            //'phone' => 'required|unique:crm.clients,phone',

        ];
        $messages = [
            'name.required' => 'The name field is required.',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();

        $client = new Client();
        $client->name = $request->name;
        $client->client_group_id = $request->clientgroup;
        $client->phone = $request->phone;
        $client->email = $request->email;
        $client->website = $request->website;
        $client->vat_number = $request->vatnumber;
        $client->address = $request->address;
        $client->city = $request->city;
        $client->state = $request->state;
        $client->zip = $request->zip;
        $client->country = $request->country;
        $client->save();

        $clientContact = new ClientContact();
        $clientContact->first_name = $request->firstname;
        $clientContact->last_name = $request->lastname;
        $clientContact->gender = $request->gender;
        $clientContact->job_title = $request->jobtitle;
        $clientContact->phone = $request->contact_person_phone;
        $clientContact->email = $request->contact_person_email;
        $clientContact->skype = $request->contact_person_skype;
        $clientContact->whatsapp = $request->contact_person_whats_app;
        if ($request->contact_person_password) {
            $password = $request->contact_person_password;
        }else{
            $password = '12345678';
        }
        $clientContact->password = Hash::make($password);
        $clientContact->is_primary_contact = 1;
        $clientContact->is_login_access = 1;
        $client->clientcontact()->save($clientContact);

        return $client;

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //$client = Client::with('clientcontact')->find($id);
        $client = Client::with('clientgroup')->find($id);
        $clientProjects = ClientProject::where('client_id', $id)->count();

        // $start = microtime(true);
        // $iValues = Invoice::where('client_id', $id)->sum('invoice_value');
        // $paymentsReceived = Invoice::where('client_id', $id)->sum('payment_received');
        // return $time = microtime(true) - $start;

        //$start = microtime(true);
        $invoicesPaymentRecord = Invoice::select([
            DB::raw("SUM(invoice_value) as invoicesValues"),
            DB::raw("SUM(payment_received) as paymentsReceived"),
        ])->where('client_id', $id)->first();
        //return $time = microtime(true) - $start;
        return view('crm::.clients.details', compact('client', 'clientProjects', 'invoicesPaymentRecord'));
        // return view('crm::.clients.details', compact('client', 'clientProjects', 'invoicesValues'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('crm::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name'     => 'required',
            'email'    => 'required|unique:crm.clients,email,'. $id .'',
            'phone'    => 'required|unique:crm.clients,phone,'. $id .'',
            'address'  => 'required',

        ];
        $messages = [
            'name.required' => 'The name field is required.',
        ];
        Validator::make($request->all(), $rules, $messages)->validate();

        $client = Client::find($id);
        if (!$request->status) {
            if ($client->status == 1) {
                $status = 0;
            }else{
                $status = 1;
            }
        }else{
            $status = $request->status;
        }
        $client->client_group_id = $request->clientgroup;
        $client->name            = $request->name;
        $client->phone           = $request->phone;
        $client->email           = $request->email;
        $client->address         = $request->address;
        $client->city            = $request->city;
        $client->state           = $request->state;
        $client->zip             = $request->zip;
        $client->country         = $request->country;
        $client->website         = $request->website;
        $client->vat_number      = $request->vatnumber;
        $client->status          = $status;
        $client->save();

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


    public function changeActivationStatus(Request $request){
        $clientid = $request->id;

        $client = Client::find($clientid);
        if ($client->status == 0) {
            Client::where('id', $clientid)->update([
                'status' => 1
            ]);
        }
        if ($client->status == 1) {
            Client::where('id', $clientid)->update([
                'status' => 0
            ]);
        }
        //return back();
    }
}
