<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>{{$settings['title'] ?? config('app.name')}} | Estimate-{{ $data->id }}</title>
	<link href="{{ public_path('resources/plugins/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<style>
		@font-face {
		    font-family: 'iskpota';
		    src: url('{{base_path()}}/fonts/kalpurush.ttf') format('truetype') 
		}
		body{
		    font-family: kalpurush, sans-serif; 
		}
		.header-brand-img{
			margin-top: 5px;
		}
		.text-center{
			text-align: center;
		}
		.float-left{
			float: left;
		}
		.float-right{
			/*text-align: right;*/
			float: right;
		}
		.text-muted{
			color: #6c757d;
		}
		.f-s-12{
			font-size: 18px;
		}
		.border-2{
			border: 2px solid #19B5FE;
		}
		#discountTable table tr td{
			border: 0px !important;
		}
	</style>
</head>
<body>

	<div class="invoice-header">
		<h3><span>
			@if($settings['logo'])
				<img src="{{url('storage',$settings['logo'])}}" class="header-brand-img" alt="" height="28" width="28">
			@else
				<img src="{{public_path('resources/Logo.png')}}" class="header-brand-img" alt="" height="28" width="28">
			@endif
			{{$settings['title'] ?? config('app.name')}}
			</span>
			<span class="float-right text-muted f-s-12 mt-2">Date: {{ date('d-M-Y h:i a', strtotime($data->updated_at)) }}</span></h3>
	</div>
	<table class="table text-center">
		<tr>
			<td>Estimate-{{ $data->id }}</td>
		</tr>
	</table>
	<table class="table">
		<tr>
			<td>
				<span>
					From,<br>
					<strong>{{$settings['title'] ?? config('app.name')}}</strong><br>
					{{$settings['address'] ?? config('app.address')}} <br>
					Phone: {{$settings['phone'] ?? '' }}<br>
					Email: {{$settings['email'] ?? '' }}
				</span>
			</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>
				<span>
					To,<br>
					<strong>{{ $data->client->name ?? '' }}</strong><br>{{ $data->client->address ?? '' }}, {{ $data->client->city ?? '' }}<br>{{ $data->client->state ?? '' }} {{ $data->client->zip ?? '' }} {{ $data->client->country ?? '' }}<br>
					Phone: {{ $data->client->phone ?? '' }}<br>
					Email: {{ $data->client->email ?? '' }}
				</span>
			</td>
		</tr>
	</table>
	<table class="table table-striped" id="itemsTable">
	    <thead>
	        <tr>
	            <th>#</th>
	            <th>Item</th>
	            <th class="text-center">Qty</th>
	            <th class="text-center">Rate</th>
	            <th class="text-center">Discount</th>
	            <th class="font-weight-bold text-center">Total</th>
	        </tr>
	    </thead>
	    <tbody>
	    	@foreach($data->estimateditems as $item)
	    	<tr>
	    		<td>{{$loop->index + 1}}</td>
	    		<td>{{ $item->item->name }}</td>
	    		<td class="text-center">{{ $item->quantity }}</td>
	    		<td class="text-center">{{ $item->rate }}</td>
	    		@if($item->discount_type == 1)
	    			<td class="text-center">{{ $item->discount }} <span>%</span></td>
	    		@else
					<td class="text-center">{{ $item->discount }}</td>
	    		@endif
	    		@if($item->discount_type == 1)
	    			<td class="text-center">{{ ($item->quantity * $item->rate) - (($item->rate * $item->discount)/100) }}</td>
	    		@else
					<td class="text-center class rowTotal">{{ ($item->quantity * $item->rate) - ($item->quantity * $item->discount) }}</td>
	    		@endif
	    	</tr>
	    	@endforeach
	    	{{-- @if(count($data->item) > 0)

	    	@endif --}}
	    </tbody>
	</table>
	<div>
		<p id="test"></p>
	    <table class="table text-right" id="discountTable">
	        <tbody>
	        	<tr>
	                <th colspan="4" class="text-right" style="border: none;">Subtotal:</th>
	                <td id="subTotal" class="font-weight-bold estSubTotal" style="border: none;"> {{ number_format($data->getRowTotalPercentage() + $data->getRowTotalFixedAmount(), 2) }}</td>
	            </tr>
	            <tr>
	                <th colspan="4" class="text-right" style="border: none;">Discount  <span>{{ $data->discount_type == 1 ? '%': '' }}</span> :</th>
	                <td class="font-weight-bold" style="border: none;" id="estDisc">{{ $data->discount }}</td>
	            </tr>
	            <tr>
	                <th colspan="4" class="text-right" style="border: none;">Total:</th>
	                <td class="font-weight-bold" id="estimateTotalValue">
	                	@if($data->discount_type == 1)
	                		{{ number_format((($data->getRowTotalPercentage() + $data->getRowTotalFixedAmount()) - ((($data->getRowTotalPercentage() + $data->getRowTotalFixedAmount()) * $data->discount)/100)), 2)  }}
						@else
							{{ number_format((($data->getRowTotalPercentage() + $data->getRowTotalFixedAmount()) - $data->discount), 2) }}
	                	@endif
	                </td>
	            </tr>
	        </tbody>
	    </table>
	</div>
</body>
</html>