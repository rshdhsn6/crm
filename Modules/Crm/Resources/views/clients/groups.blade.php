@extends('crm::layouts.master')
@section('title')
{{__('Home')}}
@endsection
@section('content')
  
<div class="container-fluid">
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-aperture bg-blue"></i>
                    <div class="d-inline">
                        <h5>{{__('Client Groups')}}</h5>
                        <span>{{__('Client groups define in which category they are(Ex: General, VIP, Low Budget etc.)')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('crm')}}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">{{__('Client')}}</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('Groups')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-block">
                    <h3 class="float-left"> {{__('Groups')}} </h3>
                    <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#addModal"> + {{__('Add Group')}}
                    </button>
                </div>
                <div class="card-body">
                    <div class="dt-responsive">
                        <table id="#" class="table nowrap">
                            <thead>
                            <tr>
                                <th>{{__('ID')}}</th>
                                <th>{{__('Group Name')}}</th>
                                <th>{{__('Description')}}</th>
                                <th class="text-right">{{__('Action')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Group Modal -->
<div class="modal animated zoomIn faster" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Group')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="Form" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('New Group')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" placeholder="{{__('name here')}}" name="name" id="name" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Description')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <textarea class="form-control" id="exampleTextarea1" rows="4" placeholder="{{__('description here')}}.." name="desc" id="desc"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                    <button type="submit" class="btn btn-primary save-btn"></button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('extraJS')
<script>
    $(document).ready(function () {
        var gTable = $('.table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('groups.index') }}",
            deferRender: true,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'name', name: 'name'},
                {data: 'desc', name: 'desc'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
        $('.modal').on('hidden.bs.modal', function (e) {
            $('form').trigger("reset");
        });

        /*--------------create and edit form start---------*/
        $('#addModal').on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            let purpose = button.data('whatever');
            let modal = $(this);
            if (purpose) {
                modal.find('.modal-title').text('{{__("Edit Group")}}');
                let id = button.data('id');
                $.ajax({
                    url: '{{ route('groups.index') }}/' + id,
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                       // modal.find('input[name=id]').val(response.id);
                       modal.find('input[name=name]').val(response.name);
                       modal.find('input[name=desc]').val(response.desc);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        alert(msg);
                    }
                });
                modal.find('button[type=submit]').removeClass('save-btn').addClass('update-btn').text('{{__("Update")}}').val(id);
            }else{
                modal.find('.modal-title').text('{{__("Add Group")}}');
                modal.find('button[type=submit]').removeClass('update-btn').addClass('save-btn').text('{{__("Save")}}');
            }
        });
        /*--------------create and edit form end-----------*/



        /*--------------save start---------*/
        $(document).on('click', '.save-btn', function () {
            let data = $('#Form').serialize();
            $.ajax({
                url: '{{route('groups.store')}}',
                type: 'POST',
                data: data,
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    $('#addModal').modal('toggle');
                    $('#Form').trigger("reset");
                    showSuccessToast('{{__("Added Successfully")}}');
                    gTable.draw(false);
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
        });
        /*--------------save end----------*/


        /*--------------update start---------*/
        $(document).on('click', '.update-btn', function () {
            let id = this.value;
            let data = $('#Form').serialize();
            $.ajax({
                url: "{{ route('groups.index')}}/" + id,
                type: 'PUT',
                data: data,
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    $('#addModal').modal('toggle');
                    $('#Form').trigger("reset");
                    showSuccessToast('{{__("Updated Successfully")}}');
                    gTable.draw(false);
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
           
        });
        /*--------------update end----------*/

    });
</script>
@endsection