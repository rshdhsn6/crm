@extends('crm::layouts.master')
@section('title')
{{__('Home')}}
@endsection
@section('content')
  
<div class="container-fluid">
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-aperture bg-blue"></i>
                    <div class="d-inline">
                        <h5>{{__('Clients List')}}</h5>
                        <span>{{__('This is the clients list with their primary contact. Click "company name" to see details about the client.')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('crm')}}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">{{__('Client')}}</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('List')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-block">
                    <h3 class="float-left"> {{__('Clients')}} </h3>
                    @can('hrm-department-add')
                        <button type="button" class="btn btn-success float-right" data-toggle="modal"
                                data-target="#addModal" data-whatever="0"> + {{__('Add Client')}}
                        </button>
                    @endcan
                </div>
                <div class="card-body">
                    <div class="dt-responsive">
                        <table id="#" class="table nowrap">
                            <thead>
                            <tr>
                                <th>{{__('ID')}}</th>
                                <th>{{__('Company Name')}}</th>
                                <th>{{__('Primary Contact')}}</th>
                                <th>{{__('Created At')}}</th>
                                <th class="text-right">{{__('Status')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Add Modal -->
<div class="modal animated zoomIn faster" id="addModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content app-content" id="validation">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">{{__('New Client')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Form wizard with step validation section start -->
                <form id="saveForm" action="javascript:void(0)" class="form form-wizard wizard-circle" method="POST">
                    {{ csrf_field() }}
                    <!-- Step 1 -->
                    <h6>General</h6>
                    <fieldset>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-form-label">{{__('Company name')}}<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="{{__('name here')}}" name="name" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-form-label" for="exampleTextarea1">{{__('Client Group')}}<span class="text-danger">*</span></label>
                                    <select name="clientgroup" id="clientgroup" class="form-control select2" required>

                                    </select>
                                    <p class="text-note fs-12">Clients Priority Level like(VIP, General etc.)</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-form-label">{{__('Phone')}}<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="{{__('phone here')}}" name="phone" oninput="this.value=(parseInt(this.value)||880)" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-form-label">{{__('Email')}}<span class="text-danger">*</span> </label>
                                    <input type="text" class="form-control" placeholder="{{__('email here')}}" name="email" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-form-label">{{__('Website')}}</label>
                                    <input type="text" class="form-control" placeholder="{{__('website here')}}" name="website">

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-form-label">{{__('VAT Number')}}</label>
                                    <input type="text" class="form-control" placeholder="{{__('vat number here')}}" name="vatnumber">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-form-label">{{__('Address')}}<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="{{__('address here')}}" name="address" required>

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-form-label">{{__('City')}}<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="{{__('city here')}}" name="city" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="col-form-label">{{__('State')}}</label>
                                    <input type="text" class="form-control" placeholder="{{__('state here')}}" name="state">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="col-form-label">{{__('Zip')}}<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="{{__('zip here')}}" name="zip" required>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <label class="col-form-label">{{__('Country')}}</label>
                                <input type="text" class="form-control" placeholder="{{__('country here')}}" name="country">
                            </div>
                        </div>
                    </fieldset>

                    <!-- Step 2 -->
                    <h6>Primary Contact</h6>
                    <fieldset>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-form-label">{{__('First Name')}}<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="{{__('Last Name')}}" name="firstname" required>

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-form-label">{{__('Last Name')}}<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="{{__('Last Name')}}" name="lastname" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="mb-2">
                                        <label class="col-form-label">{{__('Gender')}}</label>
                                    </div>
                                    <div class="form-radio">
                                        <div class="radio radio-inline">
                                            <label>
                                                <input type="radio" name="gender" value="1" checked="checked">
                                                <i class="helper"></i>Male
                                            </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <label>
                                                <input type="radio" name="gender" value="2">
                                                <i class="helper"></i>Female
                                            </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <label>
                                                <input type="radio" name="gender" value="3">
                                                <i class="helper"></i>Prefer not to disclose
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-form-label">{{__('Job Title')}}<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="{{__('Job Title')}}" name="jobtitle" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-form-label">{{__('Phone')}}<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="{{__('Phone')}}" name="contact_person_phone" oninput="this.value=(parseInt(this.value)||880)" required>

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-form-label">{{__('Email')}}<span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" placeholder="{{__('Email')}}" name="contact_person_email" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-form-label">{{__('Skype')}}</label>
                                    <input type="text" class="form-control" placeholder="{{__('Skype')}}" name="contact_person_skype">

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-form-label">{{__('WhatsApp')}}</label>
                                    <input type="text" class="form-control" placeholder="{{__('WhatsApp')}}" name="contact_person_whats_app">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-form-label">{{__('Password')}}</label>
                                    <input type="text" class="form-control" placeholder="{{__('Password')}}" name="contact_person_password">

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-form-label">{{__('Retype Password')}}</label>
                                    <input type="text" class="form-control" placeholder="{{__('Retype Password')}}" name="contact_person_password_confirm">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <p class="text-note fs-12 mt-m15"><strong>Note:</strong> You can ignore password field. Then, 12345678 will be used as default password for this contact person!</p>
                            </div>
                        </div>
                    </fieldset>
                </form>
                <!-- Form wizard with step validation section end -->
            </div>
        </div>
    </div>
</div>

@endsection
@section('extraCSS')
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/form/wizard.min.css">
    <link rel="stylesheet" href="{{asset('resources')}}/custom/switch.css">
@endsection
@section('extraJS')
    <script src="{{asset('resources')}}/plugins/form/jquery.steps.min.js"></script>
    <script src="{{asset('resources')}}/plugins/form/jquery.validate.min.js"></script>
    <script src="{{asset('resources')}}/plugins/form/wizard-steps.js"></script>
    <script src="{{asset('resources')}}/plugins/jquery.repeater/jquery.repeater.min.js"></script>

    <script>
        $(document).ready(function () {
            var oTable = $('.table').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('clients.index') }}",
                deferRender: true,
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                    {data: 'name', name: 'name'},
                    {data: 'primarycontact', name: 'primarycontact'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            loadClientGroups();
            function loadClientGroups(){
                $.ajax({
                    url: '{{route('client-groups')}}',
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#clientgroup').empty();
                        $.each(response, function (i, clientgroup) {
                            $('#clientgroup').append($('<option>', {
                                value: clientgroup.id,
                                text: clientgroup.name
                            }));
                        });
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }

            $('#saveForm').on('submit', function(e) {
                e.preventDefault();
                let data = $(this).serialize();

                $.ajax({
                    url: '{{route('clients.store')}}',
                    type: 'POST',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#saveForm').trigger("reset");
                        $('#addModal').modal('hide');
                        showSuccessToast('{{__("Added Successfully")}}');
                        oTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            });
        });

        function changeStatus(id){
            var c = confirm("You're going to change client's activation status! Are you sure?");
                if (c == true) {
                    $.ajax({
                        url: '{{ route('client-status-change') }}',
                        type: 'GET',
                        data:{id},
                        beforeSend: function (request) {
                            return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                        },
                        success: function (response) {
                            showSuccessToast('{{__("Updated Successfully")}}');
                        },
                        error: function (data) {
                            let msg = '';
                            if (data.responseJSON.errors) {
                                $.each(data.responseJSON.errors, function (i, error) {
                                    msg += '<p">' + error[0] + '</p>';
                                });
                            } else {
                                msg = data.responseJSON.message;
                            }
                            showDangerToast(msg);
                        }
                    });
                }
        }

    </script>
@endsection