@extends('crm::layouts.master')
@section('title')
{{__('Home')}}
@endsection
@section('content')
  
<div class="container-fluid">
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-aperture bg-blue"></i>
                    <div class="d-inline">
                        <h5>{{__('Client Details')}} : {{ ucwords($client->name) }} - <small class="font-italic">{{ $client->clientgroup->name ?? '' }}</small>
                            @if($client->status == 1)
                                <small class="text-success"> (<strong>{{__('Active')}}</strong>)</small>
                            @else
                                <small class="text-danger"> (<strong>{{__('Inactive')}}</strong>)</small>
                            @endif
                            <input type="hidden" id="clientid" value="{{ $client->id }}">
                            <input type="hidden" id="clientgroupid" value="{{ $client->clientgroup->id }}">
                        </h5>
                        <span>{{__('This section is client details such as company information, contacts list, projects list etc.')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('crm')}}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">{{__('Client')}}</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('Details')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-3">
                    <small class="text-muted d-block"><i class="fa fa-envelope" aria-hidden="true"></i> {{__('Email address')}} </small>
                    <h6>{{ $client->email }}</h6>
                </div>
                <div class="col-sm-3">
                    <small class="text-muted d-block"><i class="fa fa-mobile" aria-hidden="true"></i> {{__('Phone')}} </small>
                    <h6>{{ $client->phone }}</h6>
                </div>
                <div class="col-sm-3">
                    <small class="text-muted d-block"><i class="fas fa-map-marker"></i> {{__('Address')}}</small>
                    <h6>
                        @if($client->address)
                            {{ $client->address }}
                        @endif
                        @if($client->city)
                            , {{ $client->city }}
                        @endif
                        @if($client->state)
                            , {{ $client->state }}
                        @endif
                        @if($client->zip)
                            , {{ $client->zip }}
                        @endif
                        @if($client->country)
                            , {{ $client->country }}
                        @endif

                    </h6>
                </div>
                <div class="col-sm-3">
                    <small class="text-muted d-block"><i class="fas fa-globe"></i> {{__('Website')}} </small>
                    <h6><a class="text-blue" href="//{{ $client->website }}" target="_blank">{{ $client->website }}</a></h6>
                </div>
            </div>
            <hr>
            <!---top box start--->
            <div class="row">
                <div class="col-xl-3 col-md-6">
                    <div class="card prod-p-card card-yellow">
                        <div class="card-body">
                            <div class="row align-items-center mb-30">
                                <div class="col">
                                    <h6 class="mb-5 text-white">{{__('Projects')}}</h6>
                                    <h3 class="mb-0 fw-700 text-white">{{ $clientProjects }}</h3>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-tags text-warning f-18"></i>
                                </div>
                            </div>
                            <p class="mb-0 text-white"><span class="label label-warning mr-10">+52%</span>From Previous Month</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card prod-p-card card-blue">
                        <div class="card-body">
                            <div class="row align-items-center mb-30">
                                <div class="col">
                                    <h6 class="mb-5 text-white">{{__('Invoice Value')}}</h6>
                                    <h3 class="mb-0 fw-700 text-white">
                                        @if(isset($invoicesPaymentRecord))
                                            {{ $invoicesPaymentRecord->invoicesValues ?? 0 }}
                                        @endif
                                    </h3>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-database text-blue f-18"></i>
                                </div>
                            </div>
                            <p class="mb-0 text-white"><span class="label label-primary mr-10">+12%</span>From Previous Month</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card prod-p-card card-green">
                        <div class="card-body">
                            <div class="row align-items-center mb-30">
                                <div class="col">
                                    <h6 class="mb-5 text-white">{{__('Payments')}}</h6>
                                    <h3 class="mb-0 fw-700 text-white">
                                        @if(isset($invoicesPaymentRecord))
                                            {{ $invoicesPaymentRecord->paymentsReceived ?? 0 }}
                                        @endif
                                    </h3>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-dollar-sign text-green f-18"></i>
                                </div>
                            </div>
                            <p class="mb-0 text-white"><span class="label label-success mr-10">+52%</span>From Previous Month</p>
                        </div>
                    </div>
                </div>
                
                <div class="col-xl-3 col-md-6">
                    <div class="card prod-p-card card-red">
                        <div class="card-body">
                            <div class="row align-items-center mb-30">
                                <div class="col">
                                    <h6 class="mb-5 text-white">{{__('Due')}}</h6>
                                    <h3 class="mb-0 fw-700 text-white">
                                        @if(isset($invoicesPaymentRecord))
                                            {{ ($invoicesPaymentRecord->invoicesValues - $invoicesPaymentRecord->paymentsReceived) ?? 0 }}
                                        @endif
                                    </h3>
                                </div>
                                <div class="col-auto">
                                    <i class="fa fa-money-bill-alt text-red f-18"></i>
                                </div>
                            </div>
                            <p class="mb-0 text-white"><span class="label label-danger mr-10">+11%</span>From Previous Month</p>
                        </div>
                    </div>
                </div>
            </div>
            <!---top box end--->
        </div><!---card body end--->
    </div><!---card end--->

    <!-- tab start-->
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <!-- tablist start-->
                <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-contactarea-tab" data-toggle="pill" href="#contacts" role="tab" aria-controls="pills-timeline" aria-selected="true">{{__('Contacts')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#client-information" role="tab" aria-controls="pills-profile" aria-selected="false">{{__('Client Information')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-project-tab" data-toggle="pill" href="#projects" role="tab" aria-controls="pills-project" aria-selected="false">{{__('Projects')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-invoice-tab" data-toggle="pill" href="#invoices" role="tab" aria-controls="pills-invoice" aria-selected="false">{{__('Invoices')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#payments" role="tab" aria-controls="pills-profile" aria-selected="false">{{__('Payments')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#estimates" role="tab" aria-controls="pills-profile" aria-selected="false">{{__('Estimates')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#estimate-requests" role="tab" aria-controls="pills-profile" aria-selected="false">{{__('Estimate Requests')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#settings" role="tab" aria-controls="pills-setting" aria-selected="false">{{__('Setting')}}</a>
                    </li>
                </ul>
                <!-- tablist end-->

                <!-- tabContent start-->
                <div class="tab-content" id="pills-tabContent">

                    <!---------------contactarea start-------------------------->
                    <div class="tab-pane fade show active" id="contacts" role="tabpanel" aria-labelledby="pills-contactarea-tab">
                        <div class="card-header d-block">
                            <h3 class="float-left"> {{__('Contacts List')}} </h3>
                                <button type="button" class="btn btn-success float-right" data-toggle="modal"
                                        data-target="#contactModal" data-whatever="0"> + {{__('Add Contact')}}
                                </button>
                        </div>
                        <div class="card-body">
                            <div class="dt-responsive">
                                <table id="#" class="table nowrap contacts-table">
                                    <thead>
                                    <tr>
                                        <th>{{__('ID')}}</th>
                                        <th>{{__('Name')}}</th>
                                        <th>{{__('Phone')}}</th>
                                        <th>{{__('Email')}}</th>
                                        <th>{{__('Job Title')}}</th>
                                        <th class="text-center">{{__('Created At')}}</th>
                                        <th class="text-right">{{__('Action')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!---------------contactarea end---------------------------->

                    <!---------------client information update------------------>
                    <div class="tab-pane fade" id="client-information" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <form class="form-horizontal" id="clientUpdateForm" action="javascript:void(0)">
                                @csrf
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="exampleTextarea1">{{__('Client Group')}}<span class="text-danger">*</span></label>
                                            <select name="clientgroup" id="clientgroup" class="form-control select2">

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="company-name">{{__('Company Name')}} <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" placeholder="{{__('Company Name')}}" name="name" id="name" value="{{ $client->name }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="phone">{{__('Phone')}} <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" placeholder="{{__('Phone')}}" name="phone" id="phone" value="{{ $client->phone }}" oninput="this.value=(parseInt(this.value)||0)">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="phone">{{__('Email')}} <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" placeholder="{{__('Email')}}" name="email" id="email" value="{{ $client->email }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="address">{{__('Address')}} <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" placeholder="{{__('Address')}}" name="address" id="address" value="{{ $client->address }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="city">{{__('City')}}</label>
                                                    <input type="text" class="form-control" placeholder="{{__('City')}}" name="city" id="city" value="{{ $client->city }}">
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="state">{{__('State')}}</label>
                                                    <input type="text" class="form-control" placeholder="{{__('State')}}" name="state" id="state" value="{{ $client->state }}">
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label for="zip">{{__('Zip')}}</label>
                                                    <input type="text" class="form-control" placeholder="{{__('Zip')}}" name="zip" id="zip" value="{{ $client->zip }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="country">{{__('Country')}}</label>
                                            <input type="text" class="form-control" placeholder="{{__('Country')}}" name="country" id="country" value="{{ $client->country }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="website">{{__('Website')}}</label>
                                            <input type="text" class="form-control" placeholder="{{__('Website')}}" name="website" id="website" value="{{ $client->website }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="vatnumber">{{__('VAT Number')}}</label>
                                            <input type="text" class="form-control" placeholder="{{__('VAT Number')}}" name="vatnumber" id="vatnumber" value="{{ $client->vat_number }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <br><br>
                                        <div class="form-group row">
                                            <label for="status" class="col-sm-2">{{__('Current Status')}}</label>
                                            <div class="border-checkbox-section col-sm-10">
                                                @if($client->status == 1)
                                                <div class="border-checkbox-group border-checkbox-group-success">
                                                    <input class="border-checkbox" type="checkbox" name="status" id="status" value="1" checked>
                                                    <label class="border-checkbox-label" for="status">{{__('Active')}}</label>
                                                </div>
                                                @else
                                                    <div class="border-checkbox-group border-checkbox-group-warning">
                                                        <input class="border-checkbox" type="checkbox" name="status" id="status" value="0" checked>
                                                        <label class="border-checkbox-label" for="status">{{__('Inactive')}}</label>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-success client-update-btn">{{__('Update Information')}}</button>
                            </form>
                        </div>
                    </div>
                    <!---------------end of client information update----------->
                    
                    <!---------------client projects tab start------------------>
                    <div class="tab-pane fade" id="projects" role="tabpanel" aria-labelledby="pills-project-tab">
                        <div class="card-header d-block">
                            <h3 class="float-left"> {{__('Projects List')}} </h3>
                                <button type="button" class="btn btn-success float-right" data-toggle="modal"
                                        data-target="#projectModal" data-whatever="0"> + {{__('Add Project')}}
                                </button>
                        </div>
                        <div class="card-body">
                            <div class="dt-responsive">
                                <table id="#projectTable" class="table nowrap projectTable">
                                    <thead>
                                    <tr>
                                        <th>{{__('#')}}</th>
                                        <th>{{__('Title')}}</th>
                                        <th>{{__('Start Date')}}</th>
                                        <th>{{__('Deadline')}}</th>
                                        <th>{{__('Price')}}</th>
                                        <th>{{__('Status')}}</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!---------------client projects tab end-------------------->

                    
                    <!---------------client invoices tab start------------------>
                    <div class="tab-pane fade" id="invoices" role="tabpanel" aria-labelledby="pills-invoice-tab">
                        <div class="card-header d-block">
                            <h3 class="float-left"> {{__('Invoices List')}} </h3>
                                <button type="button" class="btn btn-success float-right" data-toggle="modal"
                                        data-target="#invoiceModal" data-id="{{ $client->id }}"> + {{__('Add Invoice')}}
                                </button>
                        </div>
                        <div class="card-body">
                            <div class="dt-responsive">
                                <table id="#invoiceTable" class="table nowrap invoiceTable">
                                    <thead>
                                    <tr>
                                        <th>{{__('ID')}}</th>
                                        <th>{{__('Project')}}</th>
                                        <th>{{__('Bill Date')}}</th>
                                        <th>{{__('Due Date')}}</th>
                                        <th>{{__('Invoice Value')}}</th>
                                        <th>{{__('Payment Received')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="4" class="text-right">Total</th>
                                            <th>0</th>
                                            <th>0</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!---------------client invoices tab start------------------>


                    <div class="tab-pane fade" id="payments" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <p>payments</p>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="estimates" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <p>estimates</p>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="estimate-request" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <p>estimate requests</p>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="pills-setting-tab">
                        <div class="card-body">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label for="example-name">Full Name</label>
                                    <input type="text" placeholder="Johnathan Doe" class="form-control" name="example-name" id="example-name">
                                </div>
                                <div class="form-group">
                                    <label for="example-email">Email</label>
                                    <input type="email" placeholder="johnathan@admin.com" class="form-control" name="example-email" id="example-email">
                                </div>
                                <div class="form-group">
                                    <label for="example-password">Password</label>
                                    <input type="password" value="password" class="form-control" name="example-password" id="example-password">
                                </div>
                                <div class="form-group">
                                    <label for="example-phone">Phone No</label>
                                    <input type="text" placeholder="123 456 7890" id="example-phone" name="example-phone" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="example-message">Message</label>
                                    <textarea name="example-message" name="example-message" rows="5" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="example-country">Select Country</label>
                                    <select name="example-message" id="example-message" class="form-control">
                                        <option>London</option>
                                        <option>India</option>
                                        <option>Usa</option>
                                        <option>Canada</option>
                                        <option>Thailand</option>
                                    </select>
                                </div>
                                <button class="btn btn-success" type="submit">Update Profile</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- tabContent end-->
            </div>
        </div>
    </div>
    <!-- tab end-->
</div>







<!-- Contact Create Modal start-->
<div class="modal animated zoomIn faster" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">{{__('New Contact')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="contactForm" action="javascript:void(0)">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-form-label">{{__('First Name')}}<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="{{__('Last Name')}}" name="firstname" required>
                                <input type="hidden" name="client_id" value="{{ $client->id }}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-form-label">{{__('Last Name')}}<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="{{__('Last Name')}}" name="lastname" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="mb-2">
                                    <label class="col-form-label">{{__('Gender')}}</label>
                                </div>
                                <div class="form-radio">
                                    <div class="radio radio-inline">
                                        <label>
                                            <input type="radio" name="gender" value="1" checked="checked">
                                            <i class="helper"></i>{{__('Male')}}
                                        </label>
                                    </div>
                                    <div class="radio radio-inline">
                                        <label>
                                            <input type="radio" name="gender" value="2">
                                            <i class="helper"></i>{{__('Female')}}
                                        </label>
                                    </div>
                                    <div class="radio radio-inline">
                                        <label>
                                            <input type="radio" name="gender" value="3">
                                            <i class="helper"></i>{{__('Prefer not to disclose')}}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-form-label">{{__('Job Title')}}<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="{{__('Job Title')}}" name="jobtitle" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-form-label">{{__('Phone')}}<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="{{__('Phone')}}" name="contact_person_phone" oninput="this.value=(parseInt(this.value)||0)" required>

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-form-label">{{__('Email')}}<span class="text-danger">*</span></label>
                                <input type="text" class="form-control" placeholder="{{__('Email')}}" name="contact_person_email" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-form-label">{{__('Skype')}}</label>
                                <input type="text" class="form-control" placeholder="{{__('Skype')}}" name="contact_person_skype">

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-form-label">{{__('WhatsApp')}}</label>
                                <input type="text" class="form-control" placeholder="{{__('WhatsApp')}}" name="contact_person_whats_app">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-form-label">{{__('Password')}}</label>
                                <input type="password" class="form-control" placeholder="{{__('Password')}}" id="contact_person_password" name="contact_person_password">

                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="col-form-label">{{__('Retype Password')}}</label>
                                <input type="password" class="form-control" placeholder="{{__('Retype Password')}}" id="contact_person_password_confirm" name="contact_person_password_confirm">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <p class="text-note fs-12 mt-m15"><strong>Note:</strong> You can ignore password field. Then, 12345678 will be used as default password for this contact person!</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="border-checkbox-section">
                                    <div class="border-checkbox-group border-checkbox-group-success">
                                        <input class="border-checkbox" type="checkbox" name="is_primary_contact" id="is_primary_contact" value="1">
                                        <label class="border-checkbox-label" for="is_primary_contact">{{__('Mark As Primary Contact')}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="border-checkbox-section">
                                    <div class="border-checkbox-group border-checkbox-group-warning">
                                        <input class="border-checkbox" type="checkbox" name="is_login_access" id="is_login_access" value="1">
                                        <label class="border-checkbox-label" for="is_login_access">{{__('Give Login Access')}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                    <button type="submit" class="btn btn-primary save-btn">{{__('Save')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Contact Create Modal end---->




<!-- Project Modal Start -->
<div class="modal animated zoomIn faster" id="projectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="projectForm" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Title')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" placeholder="{{__('title here')}}" name="title" id="title" required>
                            <input type="hidden" name="client" id="client" value="{{ $client->id }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Description')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <textarea class="form-control" id="exampleTextarea1" rows="4" placeholder="{{__('description here')}}.." name="description"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Start Date')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control date" placeholder="{{__('start date')}}" name="startdate" id="startdate" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Deadline')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control date" placeholder="{{__('end date')}}" name="enddate" id="enddate" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Price')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" placeholder="{{__('price here')}}" name="price" value="0" oninput="this.value=(parseInt(this.value)||0)" required>
                        </div>
                    </div>
                    <div id="project-status-box" class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Status')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <select name="status" id="projectstatus" class="form-control select2" required>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                    <button type="submit" class="btn btn-primary">{{__('Save')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Project Modal End   -->



<!--Invoice Modal Start-->
<div class="modal animated zoomIn faster" id="invoiceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Invoice')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="invoiceForm" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Bill date')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control date" placeholder="{{__('bill date here')}}" name="billdate" id="billdate" required>
                            <input type="hidden" name="client" value="{{ $client->id }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Due date')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control date" placeholder="{{__('due date here')}}" name="duedate" id="duedate" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Project')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <select name="project" id="project" class="form-control select2" required>

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Note')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <textarea class="form-control" id="exampleTextarea1" rows="4" placeholder="{{__('note here')}}.." name="note"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                    <button type="submit" class="btn btn-primary invoice-save-btn">{{__('Save')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--Invoice Modal End---->



@endsection
@section('extraCSS')

@endsection
@section('extraJS')
<script>
    let clientid = $('#clientid').val();
    let projectTable;
    let invoiceTable;
    $(document).ready(function () {
        let clientgroupid = $('#clientgroupid').val();
        var cTable = $('.contacts-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                    url: '{{ route('contacts.index') }}',
                    type: "GET",
                    data: function (d) {
                        d.clientid = clientid;
                        d._token = '{{csrf_token()}}';
                },
            },
            deferRender: true,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'fullname', name: 'fullname'},
                {data: 'phone', name: 'phone'},
                {data: 'email', name: 'email'},
                {data: 'job_title', name: 'job_title'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        loadClientGroups();
        function loadClientGroups(){
            $.ajax({
                url: '{{route('client-groups')}}',
                type: 'GET',
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    $('#clientgroup').empty();
                    $.each(response, function (i, clientgroup) {
                        $('#clientgroup').append($('<option>', {
                            value: clientgroup.id,
                            text: clientgroup.name,
                            selected: (clientgroup.id == clientgroupid) ? 'selected' : false
                        }));
                    });
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
        }




        $('.modal').on('hidden.bs.modal', function (e) {
            $('#contactForm').trigger("reset");
        });


        /*--------------create and edit form for contact person start--------------*/
        $('#contactModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var purpose = button.data('whatever'); // Extract info from data-* attributes
            var modal = $(this);
            
            if (purpose) {
                modal.find('.modal-title').text('{{__("Edit Contact")}}');
                var id = button.data('id');
                $.ajax({
                    url: '{{route('client-contact-edit')}}',
                    type: 'GET',
                    data: {id},
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        modal.find('input[name=firstname]').val(response.first_name);
                        modal.find('input[name=lastname]').val(response.last_name);
                        if(response.gender == 1){
                            modal.find('input[name=gender][value='+response.gender+']').prop('checked', true);
                        }
                        if(response.gender == 2){
                            modal.find('input[name=gender][value='+response.gender+']').prop('checked', true);
                        }
                        if(response.gender == 3){
                            modal.find('input[name=gender][value='+response.gender+']').prop('checked', true);
                        }
                        modal.find('input[name=jobtitle]').val(response.job_title);
                        modal.find('input[name=contact_person_phone]').val(response.phone);
                        modal.find('input[name=contact_person_email]').val(response.email);
                        modal.find('input[name=contact_person_skype]').val(response.skype);
                        modal.find('input[name=contact_person_whats_app]').val(response.whatsapp);
                        if (response.is_primary_contact == 1) {
                            modal.find('input[name=is_primary_contact]').attr('checked', true);
                        }else{
                            modal.find('input[name=is_primary_contact]').attr('checked', false);
                        }
                        if (response.is_login_access == 1) {
                            modal.find('input[name=is_login_access]').attr('checked', true);
                        }else{
                            modal.find('input[name=is_login_access]').attr('checked', false);
                        }
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
                modal.find('button[type=submit]').removeClass('save-btn').addClass('update-btn').text('{{__("Update")}}').val(id);
            } else {
                modal.find('.modal-title').text('{{__("Add Contact")}}');
                modal.find('button[type=submit]').removeClass('update-btn').addClass('save-btn').text('{{__("Save")}}');
            }
        });
        /*--------------create and edit form for contact person end---------------*/



        /*--------------saving contact person start-------------*/
        $(document).on('click', '.save-btn', function () {
            let pass     = $('#contact_person_password').val();
            let con_pass = $('#contact_person_password_confirm').val();
            if (pass) {
                if (pass.length < 8) {
                    showDangerToast('{{__("Password should be atleast 8 characters!")}}');
                    $("#contact_person_password").focus();
                    return false;
                }
                if (pass != con_pass) {
                    showDangerToast('{{__("Password did not matched!")}}');
                    $("#contact_person_password").focus();
                    return false;
                }
            }
            
            let data = $('#contactForm').serialize();

            let cm = confirm('{{__("You are going to create a new contact! Are you sure?")}}');
            if (cm == true) {
                $.ajax({
                    url: '{{route('contacts.store')}}',
                    type: 'POST',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        if (response.exists == true) {
                            showDangerToast('{{__("You can't make more than one primary contact!")}}');
                        }else{
                            $('#contactModal').modal('toggle');
                            $('#contactForm').trigger("reset");
                            showSuccessToast('{{__("Added Successfully")}}');
                            cTable.draw(false);
                        }
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                }); 
            }
            
        });
        /*--------------saving contact person end----------------*/
        /*--------------updating contact person start------------*/
        $(document).on('click', '.update-btn', function () {

            let pass     = $('#contact_person_password').val();
            let con_pass = $('#contact_person_password_confirm').val();
            if (pass) {
                if (pass.length < 8) {
                    showDangerToast('{{__("Password should be atleast 8 characters!")}}');
                    $("#contact_person_password").focus();
                    return false;
                }
                if (pass != con_pass) {
                    showDangerToast('{{__("Password did not matched!")}}');
                    $("#contact_person_password").focus();
                    return false;
                }
            }
            
            let data = $('#contactForm').serialize();
            let id = $(this).val();
            let cm = confirm('{{__("You are going to create a new contact! Are you sure?")}}');
            if (cm == true) {
                $.ajax({
                    url: "{{ route('contacts.index')}}/" + id,
                    type: 'PUT',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        if (response.exists == true) {
                            showDangerToast('{{__("You can't make more than one primary contact!")}}');
                        }else{
                            $('#contactModal').modal('toggle');
                            $('#contactForm').trigger("reset");
                            showSuccessToast('{{__("Updated Successfully")}}');
                            cTable.draw(false);
                        }
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }
            
        });
        /*--------------updating contact person end---------------*/
        /*----------contact person delete-------------------------*/
        $(document).on('click', '.delete-btn', function (e) {
            e.preventDefault(); // does not go through with the link.
            let confirmation = confirm("{{__('Are you sure to delete')}} ?");
            if (!confirmation) {
                return;
            }
            var id = $(this).data('id');
            $.ajax({
                url: '{{ route('contacts.index') }}/' + id,
                type: 'DELETE',
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    if (response.exists == true) {
                        showDangerToast('{{__("You can't delete primary contact!")}}');
                    }else{
                        showSuccessToast('{{__("Deleted Successfully")}}');
                        cTable.draw(false);
                    }
                    console.log(response);
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
        });
        /*----------end of contact person delete------------------*/
        /*----------start of client information update------------*/
        $(document).on('click', '.client-update-btn', function (e) {
            e.preventDefault();
            let confirmation = confirm("{{__('Are you sure to update these information!')}} ?");
            if (!confirmation) {
                return;
            }
            
            let data = $('#clientUpdateForm').serialize();
            $.ajax({
                url: "{{ route('clients.index')}}/" + clientid,
                type: 'PUT',
                data: data,
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    showSuccessToast('{{__("Updated Successfully")}}');
                    window.setTimeout(function(){location.reload()},3000);
                    //console.log(response);
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
            return false;
        });
        /*----------end of client information update--------------*/







        getAvailableProjectStatus();
        function getAvailableProjectStatus(){
            $.ajax({
                url: '{{route('porject-statues.index')}}',
                type: 'GET',
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                   $('#projectstatus').empty();
                   $.each(response.data, function (i, status) {
                       $('#projectstatus').append($('<option>', {
                           value: status.id,
                           text: status.title
                       }));
                   });
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                    console.log(data);
                }
            });        
        }

        /////////////   project tab start  ////////////////////////
        /*--------------create and edit form for project modal start--*/
        $('#projectModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var purpose = button.data('whatever'); // Extract info from data-* attributes
            var modal = $(this);
            
            if (purpose) {
                modal.find('.modal-title').text('{{__("Edit Project")}}');
                var id = button.data('id');
                $.ajax({
                    url: '{{route('client-projects.index')}}/' + id,
                    type: 'GET',
                    data: {id},
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        modal.find('input[name=title]').val(response.title);
                        modal.find('textarea[name=description]').val(response.description);
                        modal.find('input[name=startdate]').val(response.start_date);
                        modal.find('input[name=enddate]').val(response.end_date);
                        modal.find('input[name=price]').val(response.price);
                        modal.find('#projectstatus').val(response.status.id).trigger('change');
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
                modal.find('button[type=submit]').removeClass('project-save-btn').addClass('project-update-btn').text('{{__("Update")}}').val(id);
            } else {
                modal.find('.modal-title').text('{{__("Add Project")}}');
                modal.find('button[type=submit]').removeClass('project-update-btn').addClass('project-save-btn').text('{{__("Save")}}');
            }
        });
        /*--------------create and edit form for project modal end----*/

        /*--------------saving project start--------------------------*/
        $(document).on('click', '.project-save-btn', function () {
            if ($("#title").val() == '') {
                alert("Project title field required!");
                $("#title").focus();
                return false;
            }
            
            let data = $('#projectForm').serialize();

            $.ajax({
                url: '{{route('client-projects.store')}}',
                type: 'POST',
                data: data,
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    $('#projectModal').modal('toggle');
                    $('#projectForm').trigger("reset");
                    showSuccessToast('{{__("Added Successfully")}}');
                    projectTable.draw(false);
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
        });
        /*--------------saving project end----------------------------*/


        /*--------------updating project start------------------------*/
        $(document).on('click', '.project-update-btn', function () {
            if ($("#title").val() == '') {
                alert("Project title field required!");
                $("#title").focus();
                return false;
            }
            let data = $('#projectForm').serialize();
            let id = $(this).val();
            let cm = confirm('{{__("You are going to update! Are you sure?")}}');
            if (cm == true) {
                $.ajax({
                    url: "{{ route('client-projects.index')}}/" + id,
                    type: 'PUT',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#projectModal').modal('toggle');
                        $('#projectForm').trigger("reset");
                        showSuccessToast('{{__("Updated Successfully")}}');
                        projectTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                       showDangerToast(msg);
                    }
                });
            }
            
        });
        /*--------------updating project end--------------------*/
        /////////////   project tab end  ////////////////////////
        


        /////////////   invoice tab start   ///////////////////
        /*--------------create form for invoice modal start--*/
        $('#invoiceModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var modal = $(this);
            var clientid = button.data('id');
            $.ajax({
                url: '{{route('specific-client-project-name-id')}}',
                type: 'GET',
                data: {clientid},
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    $('#project').empty();
                    $('#project').append($('<option>', {
                        value: '',
                        text: '-'
                    }));
                    $.each(response, function (i, project) {
                        $('#project').append($('<option>', {
                            value: project.id,
                            text: project.title
                        }));
                    });
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
        });
        /*--------------create form for invoice modal end----*/
        /*--------------saving invoice start--------------------------*/
        $(document).on('click', '.invoice-save-btn', function () {
            if ($("#billdate").val() == '') {
                alert("Project bill date field required!");
                $("#billdate").focus();
                return false;
            }
            if ($("#duedate").val() == '') {
                alert("Project due date field required!");
                $("#duedate").focus();
                return false;
            }
            if ($("#project").val() == '') {
                alert("Please select a project!");
                return false;
            }
            
            let data = $('#invoiceForm').serialize();

            $.ajax({
                url: '{{route('invoices.store')}}',
                type: 'POST',
                data: data,
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    $('#invoiceModal').modal('toggle');
                    $('#invoiceForm').trigger("reset");
                    showSuccessToast('{{__("Added Successfully")}}');
                    invoiceTable.draw(false);
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
        });
        /*--------------saving invoice end----------------------------*/
        /////////////   invoice tab end   ////////////////////







    });
/*--------------document ready end---------------*/




/*---------get client projects start------*/
$('[href="#projects"]').click(function(e) {
    projectTable = $('.projectTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
               url: '{{ route('specific-client-projects') }}',
               type: "GET",
               data: function (d) {
                   d.clientid = clientid;
                   d._token = '{{csrf_token()}}';
               },
       },
       deferRender: true,
       columns: [
           {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
           {data: 'title', name: 'title'},
           {data: 'start_date', name: 'start_date'},
           {data: 'deadline', name: 'deadline'},
           {data: 'price', name: 'price'},
           {data: 'status.title', name: 'status.title'},
           {data: 'action', name: 'action'},
       ]
   });
    $(this).off(e);
});
/*---------get client projects end--------*/


/*---------get client invoices start------*/
$('[href="#invoices"]').click(function(e) {
    e.preventDefault();
    invoiceTable = $('.invoiceTable').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
               url: '{{ route('specific-client-invoices') }}',
               type: "GET",
               data: function (d) {
                   d.clientid = clientid;
                   d._token = '{{csrf_token()}}';
               },
       },
       deferRender: true,
       columns: [
           {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
           {data: 'project', name: 'project'},
           {data: 'billdate', name: 'billdate'},
           {data: 'duedate', name: 'duedate'},
           {data: 'invoice_value', name: 'invoice_value'},
           {data: 'payment_received', name: 'payment_received'},
       ],
       "footerCallback": function ( row, data, start, end, display ){
            var api = this.api(), data;
            total = api
                .column(4)
                .data()
                .reduce( function (a, b) {
                    return parseInt(a) + parseInt(b);
                }, 0);
                
            // Update footer
            $( api.column( 4 ).footer() ).html(total);
       }
   });
    $(this).off(e);
});
/*---------get client invoices end--------*/
</script>
@endsection