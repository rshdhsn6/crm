@extends('crm::layouts.master')
@section('title')
{{__('Home')}}
@endsection
@section('content')
  
<div class="container-fluid">
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-file-text bg-blue"></i>
                    <div class="d-inline">
                        <h5>{{__('Profile')}}</h5>
                        <span>{{__('Client contact person profile.')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('crm')}}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">{{__('Client')}}</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('Profile')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4 col-md-5">
            <div class="card">
                <div class="card-body">
                    <div class="text-center profile-photo">
                        <div class="image-upload">
                          <label for="file-input">
                            <i class="fas fa-camera"></i>
                          </label>
                          <input type="file" name="file-input" id="file-input" onchange="saveProfilePhoto(this)"/>
                          <input type="hidden" name="contactid" id="contactid" value="{{ $contactperson->id }}">
                        </div>
                    	@if($contactperson->photo == null && $contactperson->gender == 1)
                        	<img src="{{asset('storage/crm/male-avatar.png')}}" class="rounded-circle" width="150" />
                        @endif
                    	@if($contactperson->photo == null && $contactperson->gender == 2)
                        	<img src="{{asset('storage/crm/female-avatar.png')}}" class="rounded-circle" width="150" />
                        @endif
                    	@if($contactperson->photo == null && $contactperson->gender == 3)
                        	<img src="{{asset('storage/crm/other-avatar.png')}}" class="rounded-circle" width="150" />
                        @endif
                    	@if($contactperson->photo)
                            <img id="img1" src="{{asset('storage/crm/contact/'.$contactperson->photo)}}" class="rounded-circle" width="150" />
                        @endif
                        <h4 class="card-title mt-10">{{ ucfirst($contactperson->first_name) }} {{ ucfirst($contactperson->last_name) }}</h4>
                        <p class="card-subtitle">{{ ucwords($contactperson->job_title) }}</p>
                        <!--<div class="row text-center justify-content-md-center">
                            <div class="col-4"><a href="javascript:void(0)" class="link"><i class="ik ik-user"></i> <font class="font-medium">254</font></a></div>
                            <div class="col-4"><a href="javascript:void(0)" class="link"><i class="ik ik-image"></i> <font class="font-medium">54</font></a></div>
                        </div>-->
                    </div>
                </div>
                <hr class="mb-0"> 
                <div class="card-body"> 
                    <div class="row">
                    	<div class="col-sm-6">
                    		<small class="text-muted d-block"><i class="fa fa-envelope" aria-hidden="true"></i> Email address </small>
                    		<h6>{{ $contactperson->email }}</h6> 
                    		<small class="text-muted d-block pt-10"><i class="fa fa-mobile" aria-hidden="true"></i> Phone</small>
                    		<h6>{{ $contactperson->phone }}</h6>
                    		<small class="text-muted d-block pt-10"><i class="fa fa-transgender" aria-hidden="true"></i> Gender</small>
                    		<h6>
                                @if($contactperson->gender == 1)
                                    {{__('Male')}}
                                @elseif($contactperson->gender == 2)
                                    {{__('Female')}}
                                @else
                                    {{__('Prefer not to disclose')}}
                                @endif      
                            </h6>
                    	</div>
                    	<div class="col-sm-6">
                    		@if($contactperson->skype)
                    		<small class="text-muted d-block"><i class="fab fa-skype"></i> Skype</small>
                    		<h6>{{ $contactperson->skype }}</h6> 
                    		@endif
                    		@if($contactperson->whatsapp)
                    		<small class="text-muted d-block pt-10"><i class="fab fa-whatsapp"></i> WhatsApp</small>
                    		<h6>{{ $contactperson->whatsapp }}</h6>
                    		@endif

                    		<small class="text-muted d-block pt-10"><i class="fas fa-address-card"></i> Contact Type:</small>
							@if($contactperson->is_primary_contact == 1)
                    		<h6><i class="fa fa-check-circle text-success" aria-hidden="true"></i> Primay</h6>
                    		@endif
							@if($contactperson->is_primary_contact == 0)
                    		<h6><i class="fa fa-dot-circle text-warning" aria-hidden="true"></i> Normal</h6>
                    		@endif
                    	</div>
                    </div>
                    <hr class="mb-0">
                    <div class="map-box">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d248849.886539092!2d77.49085452149588!3d12.953959988118836!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C+Karnataka!5e0!3m2!1sen!2sin!4v1542005497600" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div> 
                    <small class="text-muted d-block pt-30">Other Profile</small>
                    <br/>
                    <button class="btn btn-icon btn-facebook"><i class="fab fa-facebook-f"></i></button>
                    <button class="btn btn-icon btn-twitter"><i class="fab fa-twitter"></i></button>
                    <button class="btn btn-icon btn-instagram"><i class="fab fa-instagram"></i></button>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-md-7">
            <div class="card">
                <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-timeline-tab" data-toggle="pill" href="#current-month" role="tab" aria-controls="pills-timeline" aria-selected="true">Client Info</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab" aria-controls="pills-profile" aria-selected="false">Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month" role="tab" aria-controls="pills-setting" aria-selected="false">Setting</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="current-month" role="tabpanel" aria-labelledby="pills-timeline-tab">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3 col-6"> <strong><i class='fas fa-building'></i> Company Name</strong>
                                    <br>
                                    <p class="text-muted">{{ ucwords($contactperson->company->name) }}</p>
                                </div>
                                <div class="col-md-3 col-6"> <strong><i class='fas fa-mobile'></i> Phone</strong>
                                    <br>
                                    <p class="text-muted">{{ $contactperson->company->phone }}</p>
                                </div>
                                <div class="col-md-3 col-6"> <strong><i class='fas fa-envelope'></i> Email</strong>
                                    <br>
                                    <p class="text-muted">{{ $contactperson->company->email }}</p>
                                </div>
                                <div class="col-md-3 col-6"> <strong><i class="fas fa-map-marker"></i> Location</strong>
                                    <br>
                                    <address class="text-muted">{{ $contactperson->company->address }}
										@if($contactperson->company->city)
											, {{ $contactperson->company->city }}
										@endif
										@if($contactperson->company->state)
											, {{ $contactperson->company->state }}
										@endif
										@if($contactperson->company->zip)
											, {{ $contactperson->company->zip }}
										@endif
										@if($contactperson->company->country)
											, {{ $contactperson->company->country }}
										@endif
                                    </address>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-6"> <strong><i class='fas fa-globe'></i> Website</strong>
                                    <br>
                                    <a class="text-success" href="//{{ $contactperson->company->website }}" target="_blank">{{ $contactperson->company->website }}</a>
                                </div>
                                <div class="col-md-3 col-6"> <strong><i class='fas fa-percent'></i> VAT Number</strong>
                                    <br>
                                    @if($contactperson->company->vat)
                                    <p class="text-muted">{{ $contactperson->company->vat }}</p>
                                    @else
									<p class="text-muted">Not Mentioned</p>
                                    @endif
                                </div>
                                <div class="col-md-3 col-6"> <strong><i class="fas fa-archive"></i> Current Status</strong>
                                    <br>
                                    @if($contactperson->company->status == 1)
                                    <p class="text-muted"><i class="fa fa-check-circle text-success" aria-hidden="true"></i> Active</p>
                                    @endif
                                    @if($contactperson->company->status == 0)
                                    <p class="text-muted"><i class="fas fa-window-close text-danger" aria-hidden="true"></i> Inactive</p>
                                    @endif
                                </div>
                                <div class="col-md-3 col-6"> <strong><i class="fas fa-clock"></i> Created Date</strong>
                                    <br>
                                    <p class="text-muted">{{ date('d M Y h:i a', strtotime($contactperson->company->created_at)) }}</p>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-3 col-6"> <strong>Full Name</strong>
                                    <br>
                                    <p class="text-muted">Johnathan Deo</p>
                                </div>
                                <div class="col-md-3 col-6"> <strong>Mobile</strong>
                                    <br>
                                    <p class="text-muted">(123) 456 7890</p>
                                </div>
                                <div class="col-md-3 col-6"> <strong>Email</strong>
                                    <br>
                                    <p class="text-muted">johnathan@admin.com</p>
                                </div>
                                <div class="col-md-3 col-6"> <strong>Location</strong>
                                    <br>
                                    <p class="text-muted">London</p>
                                </div>
                            </div>
                            <hr>
                            <p class="mt-30">Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries </p>
                            <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                            <h4 class="mt-30">Skill Set</h4>
                            <hr>
                            <h6 class="mt-30">Wordpress <span class="pull-right">80%</span></h6>
                            <div class="progress progress-sm">
                                <div class="progress-bar bg-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width:80%;"> <span class="sr-only">50% Complete</span> </div>
                            </div>
                            <h6 class="mt-30">HTML 5 <span class="pull-right">90%</span></h6>
                            <div class="progress  progress-sm">
                                <div class="progress-bar bg-info" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width:90%;"> <span class="sr-only">50% Complete</span> </div>
                            </div>
                            <h6 class="mt-30">jQuery <span class="pull-right">50%</span></h6>
                            <div class="progress  progress-sm">
                                <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:50%;"> <span class="sr-only">50% Complete</span> </div>
                            </div>
                            <h6 class="mt-30">Photoshop <span class="pull-right">70%</span></h6>
                            <div class="progress  progress-sm">
                                <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%;"> <span class="sr-only">50% Complete</span> </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="previous-month" role="tabpanel" aria-labelledby="pills-setting-tab">
                        <div class="card-body">
                            <form id="contactForm" action="javascript:void(0)">
                                {{ csrf_field() }}
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">{{__('First Name')}}<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" placeholder="{{__('Last Name')}}" name="firstname" value="{{ $contactperson->first_name }}" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">{{__('Last Name')}}<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" placeholder="{{__('Last Name')}}" name="lastname" value="{{ $contactperson->last_name }}" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="mb-2">
                                                    <label class="col-form-label">{{__('Gender')}}</label>
                                                </div>
                                                <div class="form-radio">
                                                    <div class="radio radio-inline">
                                                        <label>
                                                            <input type="radio" name="gender" value="1"  {{ $contactperson->gender == 1 ? 'checked' : '' }}>
                                                            <i class="helper"></i>Male
                                                        </label>
                                                    </div>
                                                    <div class="radio radio-inline">
                                                        <label>
                                                            <input type="radio" name="gender" value="2" {{ $contactperson->gender == 2 ? 'checked' : '' }}>
                                                            <i class="helper"></i>Female
                                                        </label>
                                                    </div>
                                                    <div class="radio radio-inline">
                                                        <label>
                                                            <input type="radio" name="gender" value="3" {{ $contactperson->gender == 3 ? 'checked' : '' }}>
                                                            <i class="helper"></i>Prefer not to disclose
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">{{__('Job Title')}}<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" placeholder="{{__('Job Title')}}" name="jobtitle" value="{{ $contactperson->job_title }}" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">{{__('Phone')}}<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" placeholder="{{__('Phone')}}" name="contact_person_phone" oninput="this.value=(parseInt(this.value)||0)" value="{{ $contactperson->phone }}" required>

                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">{{__('Email')}}<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" placeholder="{{__('Email')}}" name="contact_person_email" value="{{ $contactperson->email }}" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">{{__('Skype')}}</label>
                                                <input type="text" class="form-control" placeholder="{{__('Skype')}}" name="contact_person_skype" value="{{ $contactperson->skype }}">

                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">{{__('WhatsApp')}}</label>
                                                <input type="text" class="form-control" placeholder="{{__('WhatsApp')}}" name="contact_person_whats_app" value="{{ $contactperson->whatsapp }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">{{__('Password')}}</label>
                                                <input type="password" class="form-control" placeholder="{{__('Password')}}" id="contact_person_password" name="contact_person_password">

                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-form-label">{{__('Retype Password')}}</label>
                                                <input type="password" class="form-control" placeholder="{{__('Retype Password')}}" id="contact_person_password_confirm" name="contact_person_password_confirm">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <p class="text-note fs-12 mt-m15"><strong>Note:</strong> You can ignore password field. Then, 12345678 will be used as default password for this contact person!</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="border-checkbox-section">
                                                    <div class="border-checkbox-group border-checkbox-group-success">
                                                        <input class="border-checkbox" type="checkbox" name="is_primary_contact" id="is_primary_contact" value="1" {{ $contactperson->is_primary_contact == 1 ? 'checked' : '' }}>
                                                        <label class="border-checkbox-label" for="is_primary_contact">{{__('Mark As Primary Contact')}}</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="border-checkbox-section">
                                                    <div class="border-checkbox-group border-checkbox-group-warning">
                                                        <input class="border-checkbox" type="checkbox" name="is_login_access" id="is_login_access" value="1" {{ $contactperson->is_login_access == 1 ? 'checked' : '' }}>
                                                        <label class="border-checkbox-label" for="is_login_access">{{__('Give Login Access')}}</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <button class="btn btn-success update-btn" type="button">{{__('Update Profile')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




@endsection
@section('extraCSS')

@endsection
@section('extraJS')
<script>
    $(document).ready(function () {
        function saveProfilePhoto(pp){
            let id = $('#contactid').val();
            let photo = pp.files[0];
            var form_data = new FormData(); // Creating object of FormData class
                form_data.append("id", id) // Appending parameter named file with properties of file_field to form_data
                form_data.append("photo", photo) // Adding extra parameters to form_data
            $.ajax({
                url: '{{route('client-contact-photo-update')}}',
                type: 'POST',
                //async: false,
                processData: false,
                contentType: false,
                cache: false,
                data: form_data,
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    showSuccessToast('{{__("Updated Successfully")}}');
                    window.setTimeout(function(){location.reload()},3000);
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
        }
        $(document).on('click', '.update-btn', function (e) {
            e.preventDefault();

            let id = $('#contactid').val();
            let pass     = $('#contact_person_password').val();
            let con_pass = $('#contact_person_password_confirm').val();
            if (pass) {
                if (pass.length < 8) {
                    showDangerToast('{{__("Password should be atleast 8 characters!")}}');
                    $("#contact_person_password").focus();
                    return false;
                }
                if (pass != con_pass) {
                    showDangerToast('{{__("Password did not matched!")}}');
                    $("#contact_person_password").focus();
                    return false;
                }
            }
            
            let data = $('#contactForm').serialize();
            let cm = confirm('{{__("You are going to update profile! Are you sure?")}}');
            if (cm == true) {
                $.ajax({
                    url: "{{ route('contacts.index')}}/" + id,
                    type: 'PUT',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        if (response.exists == true) {
                            showDangerToast('{{__("You can't make more than one primary contact!")}}');
                        }else{
                            showSuccessToast('{{__("Updated Successfully")}}');
                        }
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }
        });
    });
</script>
@endsection