@extends('crm::layouts.master')
@section('title')
{{__('Home')}}
@endsection
@section('content')
  
<div class="container-fluid">
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-file-text bg-blue"></i>
                    <div class="d-inline">
                        <h5>{{__('Invoice Details')}}</h5>
                        <span>{{__('This is the details of specific invoice.')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('crm')}}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">{{__('Invoices')}}</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('Details')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header d-block">
            <h3 class="float-left text-border">{{__('Invoice')}} # {{ $invoice->id }}
                {{-- <input type="hidden" id="estimateId" value="{{ $invoice->id }}">
                <input type="hidden" id="estimateDiscount" value="{{ $invoice->discount }}">
                <input type="hidden" id="estimateDiscountType" value="{{ $invoice->discount_type }}"> --}}
            </h3>
            <div class="header-action-dropdown text-right">
                <div class="dropdown">
                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-cogs"></i> {{__('Actions')}}<span class="caret"></span>
                    </button>
                    <div class="dropdown-menu">
                        <button class="dropdown-item" type="button"><i class="ik ik-edit-2"></i> {{__('Edit Invoice')}}</button>
                    </div>
                    <button class="btn btn-primary ml-2" data-toggle="modal" data-target="#itemModal"><i class="ik ik-plus-circle"></i> {{__('Add Item')}}</button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <p><strong>{{__('Project')}}:</strong> {{ $invoice->project->title }} <strong>| {{__('Startdate')}}:</strong> {{ $invoice->project->start_date }} <strong>| {{__('End date')}}:</strong> {{ $invoice->project->end_date }}</p>
        </div>
    </div>
    <div class="card">
        <div class="card-header"><h3 class="d-block w-100">
			@if($settings['logo'])
			    <img src="{{url('storage',$settings['logo'])}}" class="header-brand-img"
			         alt="" height="28" width="28">
			@else
			    <img src="{{asset('resources')}}/Logo.png" class="header-brand-img"
			         alt="" height="28" width="28">
			@endif
        	{{$settings['title'] ?? config('app.name')}}<small class="float-right mt-1">Date: {{ date('d-M-Y h:i a', strtotime($invoice->updated_at)) }}</small></h3></div>
        <div class="card-body">
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    From
                    <address>
                        <strong>{{$settings['title'] ?? config('app.name')}},</strong><br>
                        {{$settings['address'] ?? config('app.address')}} <br>
                        Phone: {{$settings['phone']}}<br>
                        Email: {{$settings['email']}}
                    </address>
                </div>
                <div class="col-sm-4 invoice-col">
                    To
                    <address>
                        <strong>{{ $invoice->client->name ?? '' }}</strong><br>{{ $invoice->client->address ?? '' }}, {{ $invoice->client->city ?? '' }}<br>{{ $invoice->client->state ?? '' }} {{ $invoice->client->zip ?? '' }} {{ $invoice->client->country ?? '' }}<br>
                        Phone: {{ $invoice->client->phone ?? '' }}<br>
                        Email: {{ $invoice->client->email ?? '' }}
                    </address>
                </div>
                <div class="col-sm-4 invoice-col">
                    <b>Invoice #{{ $invoice->id }}</b><br>
                    <br>
                    <b>Billdate:</b> {{ $invoice->billdate ?? '' }}<br>
                    <b>Duedate:</b> {{ $invoice->duedate ?? '' }}<br>
                    <b>{{__('Invoice Initial Value')}}:</b> {{ $invoice->invoice_value ?? 0 }}<br>
                    <b>Payment Received:</b> {{ $invoice->payment_received ?? 0 }}
                </div>
            </div>

            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Qty</th>
                                <th>Product</th>
                                <th>Serial #</th>
                                <th>Description</th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Call of Duty</td>
                                <td>455-981-221</td>
                                <td>El snort testosterone trophy driving gloves handsome</td>
                                <td>$64.50</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Need for Speed IV</td>
                                <td>247-925-726</td>
                                <td>Wes Anderson umami biodiesel</td>
                                <td>$50.00</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Monsters DVD</td>
                                <td>735-845-642</td>
                                <td>Terry Richardson helvetica tousled street art master</td>
                                <td>$10.70</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Grown Ups Blue Ray</td>
                                <td>422-568-642</td>
                                <td>Tousled lomo letterpress</td>
                                <td>$25.99</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-2">
                    <p class="lead">Payment Methods:</p>
                    <div class="table-responsive">
                    	<table class="table table-bordered">
                    		<thead></thead>
	                    	<tbody>
	                    		@if(count($invoice->payments) > 0)
		                    		@foreach($invoice->payments as $payment)
		                    			<tr>
		                    				<td class="font-weight-bold">{{ $payment->paymentmethod->title ?? '' }}</td>
		                    				<td>{{ $payment->amount ?? 0 }}</td>
		                    			</tr>
		                    		@endforeach
		                    	@endif
	                    	</tbody>
                    	</table>
                    </div>
                </div>
                <div class="col-6 offset-4">
                    <p class="lead">Amount Due 10/11/2018</p>
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th style="width:50%">Subtotal:</th>
                                <td>$250.30</td>
                            </tr>
                            <tr>
                                <th>Tax (9.3%)</th>
                                <td>$10.34</td>
                            </tr>
                            <tr>
                                <th>Shipping:</th>
                                <td>$5.80</td>
                            </tr>
                            <tr>
                                <th>Total:</th>
                                <td>$265.24</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row no-print">
                <div class="col-12">
                    <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment</button>
                    <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Generate PDF</button>
                </div>
            </div>
        </div>
    </div>
</div>





<!--Add Item Modal Start-->
<div class="modal animated zoomIn faster" id="itemModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Item')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="itemForm" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="form-group row">
                        <input type="hidden" name="invoiceid" value="{{ $invoice->id }}">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Item')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <select name="item" id="item" class="form-control select2" required></select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Unit Type')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" placeholder="{{__('unit here')}}" name="unittype" id="unittype" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Rate')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" placeholder="{{__('rate here')}}" name="rate" id="rate" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Discount')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" name="discount" id="discount" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Discount Type')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <select name="itemdisctype" id="itemdisctype" class="form-control select2">
                                <option value="1">{{__('Percentage')}}(%)</option>
                                <option value="2">{{__('Fixed Amount')}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Quantity')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" name="quantity" id="quantity" oninput="this.value=(parseInt(this.value)||1)" value="1">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                    <button type="submit" class="btn btn-primary item-save-btn">{{__('Save')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--Add Item Modal End---->


@endsection

@section('extraJS')
<script>
$(document).ready(function(){
    $('.modal').on('hidden.bs.modal', function (e) {
        $('form').trigger("reset");
    });

    /*--------------create item form start---------*/
    $('#itemModal').on('show.bs.modal', function (event) {
        var modal = $(this);
        $.ajax({
            url: '{{route('items-id-name')}}',
            type: 'GET',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function (response) {
               $('#item').empty();
               $('#item').append($('<option>', {
                   value: '',
                   text: '-'
               }));
               $.each(response, function (i, item) {
                   $('#item').append($('<option>', {
                       value: item.id,
                       text: item.name
                   }));
               });
            },
            error: function (data) {
                let msg = '';
                if (data.responseJSON.errors) {
                    $.each(data.responseJSON.errors, function (i, error) {
                        msg += '<p">' + error[0] + '</p>';
                    });
                } else {
                    msg = data.responseJSON.message;
                }
                showDangerToast(msg);
            }
        });
    });
    /*--------------create item form end-----------*/
    $("#item").on("select2:select", function(event){
        event.preventDefault();
        let id = $("#item").val();
        var modal = $(this);
        if (id) {
            $.ajax({
                url: '{{route('items.index')}}/' + id,
                type: 'GET',
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    modal.find('input[name=unittype]').val(response.unittype.name);
                    modal.find('').val(response.discount_type);
                    $("#unittype").val(response.unittype.name);
                    $("#rate").val(response.rate);
                    $("#discount").val(response.discount);
                    $('#itemdisctype').empty();
                    if (response.discount_type == 1) {
                        $('#itemdisctype').append($('<option>', {
                            value: '1',
                            text: '{{__("Percentage(%)")}}'
                        }));
                    }else{
                        $('#itemdisctype').append($('<option>', {
                            value: '2',
                            text: '{{__("Fixed Amount")}}'
                        }));
                    }

                    console.log(response);
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
        }
        //$('#itemForm').trigger("reset");
    });


    /*--------------saving item start--------------*/
    $(document).on('click', '.item-save-btn', function (e) {
        e.preventDefault();

        if ($('#item').val() == '') {
            alert("Please select an item!");
            return false;
        }

        let data = $('#itemForm').serialize();
        $.ajax({
            url: '{{route('invoice-items.store')}}',
            type: 'POST',
            data: data,
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function (response) {
                if (response.message) {
                    showDangerToast(response.message);
                }else{
                     $('#itemModal').modal('toggle');
                     $('#itemForm').trigger("reset");
                     $('#item').empty();
                     $('#itemdisctype').empty();
                     showSuccessToast('{{__("Added Successfully")}}');
                     //iTable.draw(false);
                     //htmlReload();
                     location.reload();
                }
            },
            error: function (data) {
                let msg = '';
                if (data.responseJSON.errors) {
                    $.each(data.responseJSON.errors, function (i, error) {
                        msg += '<p">' + error[0] + '</p>';
                    });
                } else {
                    msg = data.responseJSON.message;
                }
                showDangerToast(msg);
            }
        });
    });
    /*--------------saving item end---------------*/
});
</script>
@endsection