@extends('crm::layouts.master')
@section('title')
{{__('Home')}}
@endsection
@section('content')
  
<div class="container-fluid">
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-file-text bg-blue"></i>
                    <div class="d-inline">
                        <h5>{{__('Invoices List')}}</h5>
                        <span>{{__('Invoices define the bill history of project completion.')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('crm')}}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">{{__('Invoices')}}</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('List')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-block">
                    <h3 class="float-left"> {{__('Invoices')}} </h3>
                    <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#invoiceModal" data-whatever="0"> + {{__('Add Invoice')}}
                    </button>
                </div>
                <div class="card-body">
                    <div class="dt-responsive">
                        <table id="invoiceTable" class="table nowrap">
                            <thead>
                            <tr>
                                <th>{{__('#')}}</th>
                                <th>{{__('Invoice ID')}}</th>
                                <th>{{__('Client')}}</th>
                                <th>{{__('Project')}}</th>
                                <th>{{__('Bill Date')}}</th>
                                <th>{{__('Due Date')}}</th>
                                <th>{{__('Inv. Value')}}</th>
                                <th>{{__('P. Received')}}</th>
                                <th>{{__('Due')}}</th>
                                <th>{{__('Discount')}}</th>
                                <th>{{__('Status')}}</th>
                                <th class="text-center"><i class="ik ik-align-justify"></i></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Invoice Modal -->
<div class="modal animated zoomIn faster" id="invoiceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Invoice')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="Form" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Bill date')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control date" placeholder="{{__('bill date here')}}" name="billdate" id="billdate" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Due date')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control date" placeholder="{{__('due date here')}}" name="duedate" id="duedate" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Client')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <select name="client" id="client" class="form-control select2" required>

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Project')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <select name="project" id="project" class="form-control select2" required>

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Note')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <textarea class="form-control" id="exampleTextarea1" rows="4" placeholder="{{__('note here')}}.." name="note"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                    <button type="submit" class="btn btn-primary"></button>
                </div>
            </form>
        </div>
    </div>
</div>



<!-- Payment Modal -->
<div class="modal animated zoomIn faster" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Payment')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="paymnetForm" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Payment date')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control date" placeholder="{{__('payment date here')}}" name="paymentdate" id="paymentdate" required>
                            <input type="hidden" name="invoiceid" id="invoiceid">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Amount')}}</label>
                        <div class="col-sm-3 col-lg-3">
                            <input type="text" class="form-control dueamount amnt" placeholder="{{__('amount here')}}" name="amount[]" required>
                        </div>
                        <div class="col-sm-3 col-lg-3">
                            <select name="paymentmethod[]" id="paymentmethod" class="form-control select2 paymentmethod" required>

                            </select>
                        </div>
                        <div class="col-sm-2 col-lg-2 payment-btn"><button type="button" id="addMore" class="form-control btn btn-info btn-sm"><i class="ik ik-plus-square"></i>Add</button></div>
                    </div>
                    <div id="appendArea">
                        
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Note')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <textarea class="form-control" id="exampleTextarea1" rows="4" placeholder="{{__('note here')}}.." name="note"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                    <button type="submit" class="btn btn-primary payment-save-btn">{{__('Save')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('extraJS')
<script>
    let todayDate;
    let iTable;
    $(document).ready(function () {
        todayDate = moment().format("DD-MMM-YYYY");
        $("#billdate").val(todayDate);
        $("#duedate").val(todayDate);
        $("#paymentdate").val(todayDate);
        
        iTable = $('#invoiceTable').DataTable({
            processing: true,
            serverSide: true,
            orderable: false,
            ajax: "{{ route('invoices.index') }}",
            deferRender: true,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'invoiceid', name: 'invoiceid'},
                {data: 'client', name: 'client.name'},
                {data: 'project', name: 'project'},
                {data: 'billdate', name: 'billdate'},
                {data: 'duedate', name: 'duedate'},
                {data: 'invoice_value', name: 'invoice_value'},
                {data: 'payment_received', name: 'payment_received'},
                {data: 'due', name: 'due'},
                {data: 'discount', name: 'discount'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
        getClientNameId();
    });

    function getClientNameId(){
        $.ajax({
            url: '{{route('client-name-id')}}',
            type: 'GET',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function (response) {
               $('#client').append($('<option>', {
                    value: '',
                    text: '-'
                }));
               $.each(response, function (i, client) {
                   $('#client').append($('<option>', {
                       value: client.id,
                       text: client.name
                   }));
               });
            },
            error: function (data) {
                let msg = '';
                if (data.responseJSON.errors) {
                    $.each(data.responseJSON.errors, function (i, error) {
                        msg += '<p">' + error[0] + '</p>';
                    });
                } else {
                    msg = data.responseJSON.message;
                }
                showDangerToast(msg);
                console.log(data);
            }
        });
    }

    $("#client").on("select2:select", function(event){
        event.preventDefault();
        let id = $("#client").val();
        $.ajax({
            url: '{{route('client-project-id-name')}}',
            type: 'GET',
            data:{
                clientid: id,
            },
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function (response) {
                $('#project').empty();
                $.each(response, function (i, project) {
                    $('#project').append($('<option>', {
                        value: project.id,
                        text: project.title
                    }));
                });
            },
            error: function (data) {
                let msg = '';
                if (data.responseJSON.errors) {
                    $.each(data.responseJSON.errors, function (i, error) {
                        msg += '<p">' + error[0] + '</p>';
                    });
                } else {
                    msg = data.responseJSON.message;
                }
                showDangerToast(msg);
                console.log(data);
            }
        });
    });



    $('.modal').on('hidden.bs.modal', function (e) {
        $('#Form').trigger("reset");
    });

    /*--------------create and edit form start---------*/
    $('#invoiceModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var purpose = button.data('whatever'); // Extract info from data-* attributes
        var modal = $(this);
        
        if (purpose) {
            modal.find('.modal-title').text('{{__("Edit Invoice")}}');
            var id = button.data('id');
            $.ajax({
                url: '{{route('invoices.index')}}/' + id + '/edit',
                type: 'GET',
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    modal.find('input[name=billdate]').val(response.billdate);
                    modal.find('input[name=duedate]').val(response.duedate);
                    modal.find('#client').val(response.client_id).trigger('change');
                    modal.find('textarea[name=note]').val(response.note);
                    //modal.find('input[name=project]').val(response.project_id).trigger('change');
                    $('#project').empty();
                    $('#project').append($('<option>', {
                            value: response.project.id,
                            text: response.project.title
                        }));
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                    console.log(data);
                }
            });
            modal.find('button[type=submit]').removeClass('save-btn').addClass('update-btn').text('{{__("Update")}}').val(id);
        } else {
            modal.find('.modal-title').text('{{__("Add Invoice")}}');
            modal.find('button[type=submit]').removeClass('update-btn').addClass('save-btn').text('{{__("Save")}}');
        }
    });
    /*--------------create and edit form end---------------*/

    /*--------------saving invoice start--------------*/
    $(document).on('click', '.save-btn', function () {

        if ($('#billdate').val() == '') {
            alert("Bill date field required!");
            $("#billdate").focus();
            return false;
        }
        if ($('#duedate').val() == '') {
            alert("Due date field required!");
            $("#duedate").focus();
            return false;
        }

        if ($('#client').val() == '') {
            alert("Please select a client!");
            return false;
        }
        if ($('#project').val() == '') {
            alert("Please select a project!");
            return false;
        }

        let data = $('#Form').serialize();
        $.ajax({
            url: '{{route('invoices.store')}}',
            type: 'POST',
            data: data,
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function (response) {
                $('#invoiceModal').modal('toggle');
                $('#Form').trigger("reset");
                $('#project').empty();
                showSuccessToast('{{__("Added Successfully")}}');
                iTable.draw(false);
            },
            error: function (data) {
                let msg = '';
                if (data.responseJSON.errors) {
                    $.each(data.responseJSON.errors, function (i, error) {
                        msg += '<p">' + error[0] + '</p>';
                    });
                } else {
                    msg = data.responseJSON.message;
                }
                showDangerToast(msg);
            }
        });
    });
    /*--------------saving invoice end---------------*/

    /*--------------updating Invoice start--------------*/
    $(document).on('click', '.update-btn', function () {
        if ($('#billdate').val() == '') {
            alert("Bill date field required!");
            $("#billdate").focus();
            return false;
        }
        if ($('#duedate').val() == '') {
            alert("Due date field required!");
            $("#duedate").focus();
            return false;
        }

        if ($('#client').val() == '') {
            alert("Please select a client!");
            return false;
        }
        if ($('#project').val() == '') {
            alert("Please select a project!");
            return false;
        }

        let data = $('#Form').serialize();

        let id = $(this).val();
        let cm = confirm('{{__("You are going to update invoice! Are you sure?")}}');
        if (cm == true) {
            $.ajax({
                url: "{{ route('invoices.index')}}/" + id,
                type: 'PUT',
                data: data,
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    $('#invoiceModal').modal('toggle');
                    $('#Form').trigger("reset");
                    showSuccessToast('{{__("Updated Successfully")}}');
                    iTable.draw(false);
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
        }
        
    });
    /*--------------updating invoice end---------------*/




    /////////////////////////////payment////////////////////
    /*--------------create and edit form start---------*/
    $('#paymentModal').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget); // Button that triggered the modal
        let invoiceid = button.data('id');

        $.ajax({
            url: '{{route('get-invoice-to-pay')}}',
            type: 'GET',
            data: {invoiceid},
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function (response) {
                let due = (parseInt(response.invoice_value) - parseInt(response.payment_received));
                $(".dueamount").val(due);
                $("#invoiceid").val(response.id);
                getAvailablePaymentMethods();
            },
            error: function (data) {
                let msg = '';
                if (data.responseJSON.errors) {
                    $.each(data.responseJSON.errors, function (i, error) {
                        msg += '<p">' + error[0] + '</p>';
                    });
                } else {
                    msg = data.responseJSON.message;
                }
                showDangerToast(msg);
                console.log(data);
            }
        });
    });
    /*--------------create and edit form end---------------*/


    $(document).on('click', '#addMore', function(){
        $("#appendArea").append('<div class="form-group row"><label class="col-sm-4 col-lg-4"></label>'
            +'<div class="col-sm-3 col-lg-3">'
                +'<input type="text" class="form-control" placeholder="{{__('amount here')}}" name="amount[]" id="amount" value="0">'
            +'</div>'
            +'<div class="col-sm-3 col-lg-3">'
                +'<select name="paymentmethod[]" id="paymentmethod" class="form-control select2 paymentmethod" required></select>'
            +'</div>'
            +'<div class="col-sm-2 col-lg-2 remove-btn">'
                +'<button type="button" class="form-control btn btn-danger btn-sm remove"><i class="ik ik-minus-square"></i>Remove</button>'
            +'</div></div>');
        getAvailablePaymentMethods();
    });

    $(document).on('click', '.remove', function(e){
        e.preventDefault();
        $(this).parents('div').eq(1).remove();
    });


    function getAvailablePaymentMethods(){
        $.ajax({
            url: '{{route('payment-methods.index')}}',
            type: 'GET',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function (response) {
                $('.paymentmethod').empty();
                $.each(response.data, function (i, paymentmethod) {
                    $('.paymentmethod').append($('<option>', {
                        value: paymentmethod.id,
                        text: paymentmethod.title
                    }));
                });
            }
        });
    }

    /*---------------saving payment start------------------*/
    $(document).on('click', '.payment-save-btn', function (e) {
        e.preventDefault();
        let data = $('#paymnetForm').serialize();
        let cm = confirm('{{__("You are going to make a payment transaction! Are you sure?")}}');
        if (cm == true) {
            $.ajax({
                url: '{{route('payments.store')}}',
                type: 'POST',
                data: data,
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    $('#paymentModal').modal('toggle');
                    $('#paymnetForm').trigger("reset");
                    showSuccessToast('{{__("Added Successfully")}}');
                    iTable.draw(false);
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
        }
    });
    /*---------------saving payment end--------------------*/
</script>
@endsection