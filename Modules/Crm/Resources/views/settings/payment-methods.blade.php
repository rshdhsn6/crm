@extends('crm::layouts.master')
@section('title')
{{__('Home')}}
@endsection
@section('content')
  
<div class="container-fluid">
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-credit-card bg-blue"></i>
                    <div class="d-inline">
                        <h5>{{__('Payment Methods')}}</h5>
                        <span>{{__('Here is available payment methods. Create new one if you have.')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('crm')}}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">{{__('Settings')}}</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('Other Setup')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-8">
            <div class="card">
                <div class="card-header">
                    <h3 class="float-left"> {{__('Payment Methods')}} </h3>
                </div>
                <div class="card-body">
                    <div class="dt-responsive dt-p-lf">
                        <table id="paymentMethodTable" class="table nowrap">
                            <thead>
                                <tr>
                                    <th>{{__('ID')}}</th>
                                    <th>{{__('Title')}}</th>
                                    <th class="text-right"></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="card">
                <div class="card-header d-block">
                    <h3 class="float-left" id="pmHeader"> {{__('New Payment Method')}} </h3>
                </div>
                <div class="card-body">
                    <div class="leftForm">
                        <div class="form-group row">
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control" placeholder="{{__('title here')}}" name="paymentmethod" id="paymentmethod" required>
                            </div>
                            <div class="col-sm-4 col-lg-4">
                                <button type="button" id="pmBtn" class="btn btn-primary save-payment-method">{{__('Save')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('extraCSS')

@endsection
@section('extraJS')

<script>
    let pMethodTbl;
    $(document).ready(function () {
        /*------load payment method start------*/
        pMethodTbl = $('#paymentMethodTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('payment-methods.index') }}",
            deferRender: true,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'title', name: 'title'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
        /*------load payment method end--------*/


        /*---------------saving payment method start-----------------*/
        $(document).on('click', '.save-payment-method', function(e){
            e.preventDefault();
            let paymentmethod = $("#paymentmethod").val();
            if (paymentmethod == '') {
                alert("Payment method title field required!");
                $("#paymentmethod").focus();
                return false;
            }
            $.ajax({
                url: '{{route('payment-methods.store')}}',
                type: 'POST',
                data: {
                    title: paymentmethod
                },
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    $("#paymentmethod").val('');
                    showSuccessToast('{{__("Added Successfully")}}');
                    pMethodTbl.draw(false);
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
        });
        /*---------------saving payment method end-------------------*/


        /*---------------edit payment method start-----------------*/
        $(document).on('click', '.edit-method', function(){
            let id = $(this).val();
            $.ajax({
                url: '{{route('payment-methods.index')}}/' + id,
                type: 'GET',
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                   $("#paymentmethod").val(response.title);
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                    console.log(data);
                }
            });
            $("#pmHeader").text('{{__("Edit Payment Method")}}');
            $("#pmBtn").removeClass('save-payment-method').addClass('update-payment-method').text('{{__("Update")}}').val(id);
        });
        /*---------------edit payment method start-----------------*/


        /*---------------updating payment method start-----------------*/
        $(document).on('click', '.update-payment-method', function(e){
            e.preventDefault();
            let paymentmethod = $("#paymentmethod").val();
            if (paymentmethod == '') {
                alert("Payment method title field required!");
                $("#paymentmethod").focus();
                return false;
            }

            let id = $(this).val();

            $.ajax({
                url: '{{route('payment-methods.index')}}/' + id,
                type: 'PUT',
                data: {
                    title: paymentmethod
                },
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    console.log(response);
                    $("#paymentmethod").val('');
                    showSuccessToast('{{__("Updated Successfully")}}');
                    pMethodTbl.draw(false);
                    $("#pmHeader").text('{{__("New Payment Method")}}');
                    $("#pmBtn").removeClass('update-payment-method').addClass('save-payment-method').text('{{__("Save")}}');
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
        });
        /*---------------updating payment method end-------------------*/
    });
</script>
@endsection