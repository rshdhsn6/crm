@extends('crm::layouts.master')
@section('title')
{{__('Home')}}
@endsection
@section('content')
  
<div class="container-fluid">
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-grid bg-blue"></i>
                    <div class="d-inline">
                        <h5>{{__('Other Setup')}}</h5>
                        <span>{{__('Please set here the task status, project status types, tax percentages and more.')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('crm')}}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">{{__('Settings')}}</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('Other Setup')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-taskstatus-tab" data-toggle="pill" href="#task-statustab" role="tab" aria-controls="pills-taskstatus" aria-selected="true">Task Status</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-projectstatus-tab" data-toggle="pill" href="#projectstatus-tab" role="tab" aria-controls="pills-projectstatus" aria-selected="false">Project Status</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-tax-tab" data-toggle="pill" href="#tax-tab" role="tab" aria-controls="pills-tax" aria-selected="false">Tax</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="task-statustab" role="tabpanel" aria-labelledby="pills-taskstatus-tab">
                        <div class="card-header d-block">
                            <h3 class="float-left"> {{__('Task Status List')}} </h3>
                            <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#taskStatusModal" data-whatever="0"> + {{__('Add Task Status')}}
                            </button>
                        </div>
                        <div class="card-body">
                            <div class="dt-responsive dt-p-lf">
                                <table id="taskstatus" class="table nowrap">
                                    <thead>
                                        <tr>
                                            <th>{{__('ID')}}</th>
                                            <th>{{__('Title')}}</th>
                                            <th>{{__('Color')}}</th>
                                            <th class="text-right"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="projectstatus-tab" role="tabpanel" aria-labelledby="pills-projectstatus-tab">
                        <div class="card-header d-block">
                            <h3 class="float-left"> {{__('Project Status List')}} </h3>
                            <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#" data-whatever="0"> + {{__('Add Project Status')}}
                            </button>
                        </div>
                        <div class="card-body">
                            <div class="dt-responsive dt-p-lf">
                                <table id="projectstatus" class="table nowrap projectstatus-table">
                                    <thead>
                                        <tr>
                                            <th>{{__('ID')}}</th>
                                            <th>{{__('Title')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tax-tab" role="tabpanel" aria-labelledby="pills-tax-tab">
                        <div class="card-header d-block">
                            <h3 class="float-left"> {{__('Tax List')}} </h3>
                            <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#taxModal" data-whatever="0"> + {{__('Add Tax')}}
                            </button>
                        </div>
                        <div class="card-body">
                            <div class="dt-responsive dt-p-lf">
                                <table id="taxTable" class="table nowrap projectstatus-table">
                                    <thead>
                                        <tr>
                                            <th>{{__('ID')}}</th>
                                            <th>{{__('Title')}}</th>
                                            <th>{{__('Percentage(%)')}}</th>
                                            <th class="text-right"><i class="ik ik-align-justify"></i></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Task Status Modal Start-->
<div class="modal animated zoomIn faster" id="taskStatusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Task Status')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="taskStatusForm" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">Title</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" placeholder="{{__('title here')}}" name="title" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Color')}}</label>
                        <div class="col-sm-8 col-lg-8">
                           <input type="text" class="form-control demo" placeholder="{{__('color code here')}}" name="colorcode" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                    <button type="submit" class="btn btn-primary save-btn"></button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Task Status Modal End-->




<!-- Tax Modal Start-->
<div class="modal animated zoomIn faster" id="taxModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="taxForm" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">Title</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" placeholder="{{__('title here')}}" name="taxtitle" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Percentage')}}</label>
                        <div class="col-sm-8 col-lg-8">
                           <input type="text" class="form-control" placeholder="{{__('0')}}" name="taxpercentage" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                    <button type="submit" class="btn btn-primary"></button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Tax Modal End-->
@endsection
@section('extraCSS')
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css">
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/jquery-minicolors/jquery.minicolors.css">
@endsection
@section('extraJS')
    <script src="{{asset('resources')}}/plugins/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js"></script>
    <script src="{{asset('resources')}}/plugins/jquery-minicolors/jquery.minicolors.min.js"></script>
    <script src="{{asset('resources')}}/js/form-picker.js"></script>
<script>
    $(document).ready(function () {
        var tsTable = $('#taskstatus').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('task-statuses.index') }}",
            deferRender: true,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'title', name: 'title'},
                {data: 'colorcode', name: 'colorcode'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        /*------load project statuses start------*/
        var psTable = $('#projectstatus').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('porject-statues.index') }}",
            deferRender: true,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'title', name: 'title'},
            ]
        });
        /*------load project statuses end--------*/



        /*------load tax start------*/
        var taxTable = $('#taxTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('taxes.index') }}",
            deferRender: true,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                {data: 'title', name: 'title'},
                {data: 'percentage', name: 'percentage'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
        /*------load tax end--------*/



        $('.modal').on('hidden.bs.modal', function (e) {
            $('#taskStatusForm').trigger("reset");
        });

        /*--------------create and edit form start---------*/
        $('#taskStatusModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var purpose = button.data('whatever'); // Extract info from data-* attributes
            var modal = $(this);
            
            if (purpose) {
                modal.find('.modal-title').text('{{__("Edit Task Status")}}');
                var id = button.data('id');
                $.ajax({
                    url: '{{route('task-statuses.index')}}/' + id,
                    type: 'GET',
                    data: {id},
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                       modal.find('input[name=title]').val(response.title);
                       modal.find('input[name=colorcode]').val(response.colorcode);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                        console.log(data);
                    }
                });
                modal.find('button[type=submit]').removeClass('save-btn').addClass('update-btn').text('{{__("Update")}}').val(id);
            } else {
                modal.find('.modal-title').text('{{__("Add Task Status")}}');
                modal.find('button[type=submit]').removeClass('update-btn').addClass('save-btn').text('{{__("Save")}}');
            }
        });
        /*--------------create and edit form end-----------*/

        /*--------------saving Task Status start--------------*/
        $(document).on('click', '.save-btn', function (e) {
            e.preventDefault();

            let data = $('#taskStatusForm').serialize();
            $.ajax({
                url: '{{route('task-statuses.store')}}',
                type: 'POST',
                data: data,
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    $('#taskStatusModal').modal('toggle');
                    $('#taskStatusForm').trigger("reset");
                    showSuccessToast('{{__("Added Successfully")}}');
                    tsTable.draw(false);
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
        });
        /*--------------saving Task Status end---------------*/

        /*--------------updating Task Status start--------------*/
        $(document).on('click', '.update-btn', function () {
            let data = $('#taskStatusForm').serialize();
            let id = $(this).val();
            let cm = confirm('{{__("You are going to update! Are you sure?")}}');
            if (cm == true) {
                $.ajax({
                    url: "{{ route('task-statuses.index')}}/" + id,
                    type: 'PUT',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#taskStatusModal').modal('toggle');
                        $('#taskStatusForm').trigger("reset");
                        showSuccessToast('{{__("Updated Successfully")}}');
                        //tsTable.draw(false);
                        location.reload();
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }
            
        });
        /*--------------updating Task Status end---------------*/









        /*--------------tax create and edit form start---------*/
        $('#taxModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var purpose = button.data('whatever'); // Extract info from data-* attributes
            var modal = $(this);
            
            if (purpose) {
                modal.find('.modal-title').text('{{__("Edit Tax Percentage")}}');
                var id = button.data('id');
                $.ajax({
                    url: '{{route('taxes.index')}}/' + id,
                    type: 'GET',
                    data: {id},
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                       modal.find('input[name=taxtitle]').val(response.title);
                       modal.find('input[name=taxpercentage]').val(response.percentage);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                        console.log(data);
                    }
                });
                modal.find('button[type=submit]').removeClass('tax-save-btn').addClass('tax-update-btn').text('{{__("Update")}}').val(id);
            } else {
                modal.find('.modal-title').text('{{__("New Tax Percentage")}}');
                modal.find('button[type=submit]').removeClass('tax-update-btn').addClass('tax-save-btn').text('{{__("Save")}}');
            }
        });
        /*--------------tax create and edit form end-----------*/

        /*--------------saving Tax start--------------*/
        $(document).on('click', '.tax-save-btn', function (e) {
            e.preventDefault();

            let data = $('#taxForm').serialize();
            $.ajax({
                url: '{{route('taxes.store')}}',
                type: 'POST',
                data: data,
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    $('#taxModal').modal('toggle');
                    $('#taxForm').trigger("reset");
                    showSuccessToast('{{__("Added Successfully")}}');
                    taxTable.draw(false);
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
        });
        /*--------------saving Tax end---------------*/

        /*--------------updating Task Status start--------------*/
        $(document).on('click', '.tax-update-btn', function () {
            let data = $('#taxForm').serialize();
            let id = $(this).val();
            let cm = confirm('{{__("You are going to update! Are you sure?")}}');
            if (cm == true) {
                $.ajax({
                    url: "{{ route('taxes.index')}}/" + id,
                    type: 'PUT',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#taxModal').modal('toggle');
                        $('#taxForm').trigger("reset");
                        showSuccessToast('{{__("Updated Successfully")}}');
                        taxTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }
            
        });
    });
</script>
@endsection