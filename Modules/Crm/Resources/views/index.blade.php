@extends('crm::layouts.master')
@section('title')
{{__('Home')}}
@endsection
@section('content')
    <div class="widget timeline text-white">
        <div class="widget-body color-overlay">
            <div class="d-flex justify-content-between align-items-center">
                <div class="state">
                    <div class="day-number mr-3 d-inline-block display-4">{{date('d')}}</div>
                    <div class="d-inline-block">
                        <div class="h4">{{date('l')}}</div>
                        <div class="">{{date('F Y')}}</div>
                    </div>
                </div>
                <div id="clock" class="col-6 text-right h1">
                    <span class="hours"></span> <span class="delemeter invisible">:</span>
                    <span class="minutes"></span> <span class="delemeter invisible">:</span>
                    <span class="seconds"></span>
                    {{--                        <span id="ampm"></span>--}}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <div class="widget">
                        <div class="widget-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="state">
                                    <h6>{{__('Users')}}</h6>
                                    <h2 id="c-users">0</h2>
                                </div>
                                <div class="icon">
                                    <i class="ik ik-users"></i>
                                </div>
                            </div>
                            <small class="text-small mt-10 d-block">{{__('All Users')}}</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="widget">
                        <div class="widget-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="state">
                                    <h6>{{__('Salary Paid')}}</h6>
                                    <h2 id="c-salary-paid">0</h2>
                                </div>
                                <div class="icon">
                                    <i class="ik ik-dollar-sign"></i>
                                </div>
                            </div>
                            <small class="text-small mt-10 d-block">{{date('1 F - t F (Y)')}}</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="widget">
                        <div class="widget-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="state">
                                    <h6>{{__('Application')}}</h6>
                                    <h2 id="c-leave-application">0</h2>
                                </div>
                                <div class="icon">
                                    <i class="ik ik-file-text"></i>
                                </div>
                            </div>
                            <small class="text-small mt-10 d-block">{{date('1 F - t F (Y)')}}</small>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="widget">
                        <div class="widget-body">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="state">
                                    <h6>{{__('Loan')}}</h6>
                                    <h2 id="c-loan">0</h2>
                                </div>
                                <div class="icon">
                                    <i class="ik ik-activity"></i>
                                </div>
                            </div>
                            <small class="text-small mt-10 d-block">{{date('1 F - t F (Y)')}}</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>


@endsection
@section('extraCSS')
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" href="{{asset('resources')}}/plugins/weather-icons/css/weather-icons.min.css">
    <style>
        .timeline {
            background: #ffffff url('{{asset('resources')}}/img/placeholder/placeimg_400_200_nature.jpg') no-repeat left top;
            background-size: cover;
        }

        .color-overlay {
            padding: 2em;
            box-sizing: border-box;
            background: rgba(123, 94, 155, 0.5);
            line-height: normal;
        }

        .day-number {
            font-weight: 700;
            line-height: 1;
        }
    </style>
@endsection
@section('extraJS')
    <script src="{{asset('resources')}}/js/chart.min.js"></script>
    <script src="{{asset('resources')}}/plugins/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="{{asset('resources')}}/js/calendar.js"></script>
    <script>
        $(document).ready(function () {
            // Timebar
            var $hOut = $('.hours'),
                $mOut = $('.minutes'),
                $sOut = $('.seconds');

            // $ampmOut = $('#ampm');

            function update() {
                var date = new Date();

                // var ampm = date.getHours() < 12
                //     ? 'AM'
                //     : 'PM';

                // var hours = date.getHours() == 0
                //     ? 12
                //     : date.getHours() > 12
                //         ? date.getHours() - 12
                //         : date.getHours();

                var hours = date.getHours() < 10
                    ? '0' + date.getHours()
                    : date.getHours();

                var minutes = date.getMinutes() < 10
                    ? '0' + date.getMinutes()
                    : date.getMinutes();

                var seconds = date.getSeconds() < 10
                    ? '0' + date.getSeconds()
                    : date.getSeconds();

                $hOut.text(hours);
                $mOut.text(minutes);
                $sOut.text(seconds);
                // $ampmOut.text(ampm);
                $('.delemeter').toggleClass('invisible');
            }

            update();
            window.setInterval(update, 1000);

        });
    </script>
@endsection