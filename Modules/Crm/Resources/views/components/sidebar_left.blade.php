<div class="app-sidebar colored">
    <div class="sidebar-header">
        <a class="header-brand" href="{{route('crm')}}">
            <div class="float-left">
                @if($settings['logo'])
                    <img src="{{url('storage',$settings['logo'])}}" class="header-brand-img"
                         alt="" height="40" width="40">
                @else
                    <img src="{{asset('resources')}}/Logo.png" class="header-brand-img"
                         alt="" height="40" width="40">
                @endif
            </div>
            <span class="text ml-2 mt-1 float-right"> {{$settings['title'] ?? config('app.name')}} <small>{{ Request::segment(1) }}</small></span>
        </a>
        <button type="button" class="nav-toggle"><i data-toggle="expanded"
                                                    class="ik ik-toggle-right toggle-icon"></i></button>
        <button id="sidebarClose" class="nav-close"><i class="ik ik-x"></i></button>
    </div>

    <div class="sidebar-content">
        <div class="nav-container">
            <nav id="main-menu-navigation" class="navigation-main">
                <div class="nav-item">
                    <a href="{{url('crm')}}"><i class="ik ik-bar-chart-2"></i><span>{{__('Dashboard')}}</span></a>
                </div>
                <div class="nav-item has-sub {{ Request::segment(1) == 'clients*' ? 'open active' : '' }}">
                    <a href="javascript:void(0)" ><i class="ik ik-aperture"></i><span>{{__('Clients')}}</span></a>
                    <div class="submenu-content">
                        <a href="{{route('groups.index')}}" class="menu-item">{{__('Groups')}}</a>
                        <a href="{{route('clients.index')}}" class="menu-item">{{__('Clients List')}}</a>
                    </div>
                </div>
                <div class="nav-item has-sub {{ Request::segment(1) == 'items*' ? 'open active' : '' }}">
                    <a href="javascript:void(0)"><i class="ik ik-layers"></i><span>{{__('Items')}}</span></a>
                    <div class="submenu-content">
                        <a href="{{route('unit-types.index')}}" class="menu-item">{{__('Unit Types')}}</a>
                        <a href="{{route('items.index')}}" class="menu-item">{{__('Items List')}}</a>
                    </div>
                </div>
                <div class="nav-item {{ Request::segment(1) == 'projects*' ? 'open active' : '' }}">
                    <a href="{{route('client-projects.index')}}"><i class="ik ik-package"></i><span>{{__('Projects')}}</span></a>
                </div>
                <div class="nav-item">
                    <a href="{{route('tasks.index')}}" class="menu-item"><i class="ik ik-server"></i><span>{{__('Tasks')}}</span></a>
                </div>
                <div class="nav-item">
                    <a href="{{route('invoices.index')}}" class="menu-item"><i class="ik ik-file-text"></i><span>{{__('Invoices')}}</span></a>
                </div>
                <div class="nav-item has-sub">
                    <a href="javascript:void(0)"><i
                            class="ik ik-layout"></i><span>{{__('Estimates')}}</span></a>
                    <div class="submenu-content">
                        <a href="{{route('estimate-requests')}}" class="menu-item">{{__('Estimate Request')}}</a>
                        <a href="{{route('estimates.index')}}" class="menu-item">{{__('Estimate List')}}</a>
                    </div>
                </div>
                <div class="nav-item has-sub">
                    <a href="javascript:void(0)"><i class="ik ik-settings"></i><span>{{__('Settings')}}</span></a>
                    <div class="submenu-content">
                        <a href="{{route('payment-methods.index')}}" class="menu-item">{{__('Paymnet Methods')}}</a>
                        <a href="{{route('other-setup')}}" class="menu-item">{{__('Other Setup')}}</a>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>
