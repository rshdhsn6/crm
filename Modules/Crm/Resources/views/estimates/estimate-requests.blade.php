@extends('crm::layouts.master')
@section('title')
{{__('Home')}}
@endsection
@section('content')
  
<div class="container-fluid">
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-file-text bg-blue"></i>
                    <div class="d-inline">
                        <h5>{{__('Estimate Requests')}}</h5>
                        <span>{{__('This is the list of the project request from client.Initally status is new, during processing it will fixed the price, time etc. and then if granted it will be marked as invoiced')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('crm')}}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">{{__('Estimate')}}</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('Requests')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-block">
                    <h3 class="float-left"> {{__('Estimate Requests')}} </h3>
                    <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#estimateModal" data-whatever="0"> + {{__('Add Estimate')}}
                    </button>
                </div>
                <div class="card-body">
                    <div class="dt-responsive">
                        <table id="estimateTable" class="table nowrap">
                            <thead>
                            <tr>
                                <th>{{__('#')}}</th>
                                <th>{{__('Request ID')}}</th>
                                <th>{{__('Client')}}</th>
                                <th>{{__('Budget')}}</th>
                                <th>{{__('Submitted At')}}</th>
                                <th>{{__('Updated At')}}</th>
                                <th>{{__('Status')}}</th>
                                <th class="text-center"><i class="ik ik-align-justify"></i></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Estimate Modal -->
<div class="modal animated zoomIn faster" id="estimateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Estimate Request')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="Form" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Item')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <select name="item" id="item" class="form-control select2" required>

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Client')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <select name="client" id="client" class="form-control select2" required>

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Description')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <textarea class="form-control" id="exampleTextarea1" rows="4" placeholder="{{__('Describe what you want done?')}}.." name="description"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label" for="budget">{{__('Budget')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" placeholder="{{__('budget here')}}" name="budget" id="budget" value="0" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label" for="examplelink">{{__('Example Link')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" placeholder="{{__('Work example link here')}}" name="examplelink" id="examplelink">
                        </div>
                    </div>
                    <div id="project-status-box">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Status')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <select name="status" id="status" class="form-control select2" required>
                                <option value="1">Draft</option>
                                <option value="2">Processing</option>
                                <option value="3">Estimated</option>
                                <option value="4">Canceled</option>
                                <option value="5">Re-Open</option>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                    <button type="submit" class="btn btn-primary"></button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('extraJS')
<script>
    let eTable;
    $(document).ready(function () {
        eTable = $('#estimateTable').DataTable({
            processing: true,
            serverSide: true,
            orderable: false,
            ajax: "{{ route('estimate-requests') }}",
            deferRender: true,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'estimateid', name: 'estimateid'},
                {data: 'client.name', name: 'client.name'},
                {data: 'budget', name: 'budget'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });




        getClientNameId();
        function getClientNameId(){
            $.ajax({
                url: '{{route('client-name-id')}}',
                type: 'GET',
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                   $('#client').empty();
                   $('#client').append($('<option>', {
                        value: '',
                        text: '-'
                    }));
                   $.each(response, function (i, client) {
                       $('#client').append($('<option>', {
                           value: client.id,
                           text: client.name
                       }));
                   });
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                    console.log(data);
                }
            });
        }

        getItemsNameId();
        function getItemsNameId(){
            $.ajax({
                url: '{{route('items-id-name')}}',
                type: 'GET',
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                   $('#item').empty();
                   $('#item').append($('<option>', {
                        value: '',
                        text: '-'
                    }));
                   $.each(response, function (i, item) {
                       $('#item').append($('<option>', {
                           value: item.id,
                           text: item.name
                       }));
                   });
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                    console.log(data);
                }
            });
        }


        $('.modal').on('hidden.bs.modal', function (e) {
            $('#Form').trigger("reset");
        });

        /*--------------create and edit form start---------*/
        $('.modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var purpose = button.data('whatever'); // Extract info from data-* attributes
            var modal = $(this);
            
            if (purpose) {
                modal.find('.modal-title').text('{{__("Edit Estimate Request")}}');
                var id = button.data('id');
                document.getElementById("project-status-box").setAttribute("class", "form-group row show-project-status-box");
                $.ajax({
                    url: '{{route('estimates.index')}}/' + id,
                    type: 'GET',
                    data: {id},
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        modal.find('#item').val(response.estimateditems[0].item_id).trigger('change');
                        modal.find('#client').val(response.client_id).trigger('change');
                        modal.find('textarea[name=description]').val(response.description);
                        modal.find('input[name=budget]').val(response.budget);
                        modal.find('input[name=examplelink]').val(response.example_link);
                        modal.find('#status').val(response.status).trigger('change');
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                        console.log(data);
                    }
                });
                modal.find('button[type=submit]').removeClass('save-btn').addClass('update-btn').text('{{__("Update")}}').val(id);
            } else {
                modal.find('.modal-title').text('{{__("Estimate Request")}}');
                document.getElementById("project-status-box").setAttribute("class", "form-group row hide-project-status-box");
                modal.find('button[type=submit]').removeClass('update-btn').addClass('save-btn').text('{{__("Request")}}');
            }
        });
        /*--------------create and edit form end----------*/

        /*--------------saving estimate start--------------*/
        $(document).on('click', '.save-btn', function (e) {
            e.preventDefault();
            if ($("#item").val() == '') {
                alert("Please select an item!");
                return false;
            }
            if ($("#client").val() == '') {
                alert("Please select a client!");
                return false;
            }
            if ($("#budget").val() == '') {
                alert("Budget field is required!");
                return false;
            }
            if ($("#budget").val() <= 0) {
                alert("Budget field should be greater than zero!");
                $("#budget").focus();
                return false;
            }

            let data = $('#Form').serialize();
            $.ajax({
                url: '{{route('estimates.store')}}',
                type: 'POST',
                data: data,
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    $('#estimateModal').modal('toggle');
                    $('#Form').trigger("reset");
                    showSuccessToast('{{__("Added Successfully")}}');
                    eTable.draw(false);
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
        });
        /*--------------saving estimate end---------------*/

        /*--------------updating estimate start--------------*/
        $(document).on('click', '.update-btn', function () {
            let data = $('#Form').serialize();
            let id = $(this).val();
            let cm = confirm('{{__("You are going to update! Are you sure?")}}');
            if (cm == true) {
               $.ajax({
                   url: "{{ route('estimates.index')}}/" + id,
                   type: 'PUT',
                   data: data,
                   beforeSend: function (request) {
                       return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                   },
                   success: function (response) {
                       $('#estimateModal').modal('toggle');
                       $('#Form').trigger("reset");
                       showSuccessToast('{{__("Updated Successfully")}}');
                       eTable.draw(false);
                   },
                   error: function (data) {
                       let msg = '';
                       if (data.responseJSON.errors) {
                           $.each(data.responseJSON.errors, function (i, error) {
                               msg += '<p">' + error[0] + '</p>';
                           });
                       } else {
                           msg = data.responseJSON.message;
                       }
                       showDangerToast(msg);
                   }
               });
            }
            
        });
        /*--------------updating estimate end---------------*/
    });
</script>
@endsection