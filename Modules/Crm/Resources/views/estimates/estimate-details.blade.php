@extends('crm::layouts.master')
@section('title')
{{__('Home')}}
@endsection
@section('content')
  
<div class="container-fluid">
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-file-text bg-blue"></i>
                    <div class="d-inline">
                        <h5>{{__('Estimate Details')}}</h5>
                        <span>{{__('This is the details of specific estimate.')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('crm')}}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">{{__('Estimates')}}</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('Details')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header d-block">
            <h3 class="float-left text-border">{{__('Estimate')}} # {{ $estimate->id }}
                <input type="hidden" id="estimateId" value="{{ $estimate->id }}">
                <input type="hidden" id="estimateDiscount" value="{{ $estimate->discount }}">
                <input type="hidden" id="estimateDiscountType" value="{{ $estimate->discount_type }}">
            </h3>
            <div class="header-action-dropdown text-right">
                <div class="dropdown">
                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-cogs"></i> {{__('Actions')}}<span class="caret"></span>
                    </button>
                    <div class="dropdown-menu">
                        @if($estimate->status == 3)
                            <button class="dropdown-item update-estimate-request-status" type="button" value="4" data-toggle="modal" data-target="#invoiceMarkModal"><i class="ik ik-check-circle"></i> {{__('Mark As Invoice')}}</button>
                        @endif
                        @if($estimate->status == 2)
                            <button class="dropdown-item update-estimate-request-status" type="button" value="3"><i class="ik ik-check-circle"></i> {{__('Mark As Estimated')}}</button>
                        @endif
                        @if($estimate->status == 5)
                            <button class="dropdown-item update-estimate-request-status" type="button" value="6"><i class="ik ik-refresh-ccw"></i> {{__('Re-Open')}}</button>
                        @endif
                        @if($estimate->status != 4)
                            <button class="dropdown-item update-estimate-request-status" type="button" value="5"><i class="ik ik-x-circle"></i> {{__('Mark As Canceled')}}</button>
                        @endif
                    </div>
                    <button class="btn btn-primary ml-2" data-toggle="modal" data-target="#itemModal"><i class="ik ik-plus-circle"></i> {{__('Add Item')}}</button>
                </div>
            </div>

        </div>
    </div>
    <div class="card">
        <div class="card-header"><h3 class="d-block w-100">
			@if($settings['logo'])
			    <img src="{{url('storage',$settings['logo'])}}" class="header-brand-img"
			         alt="" height="28" width="28">
			@else
			    <img src="{{asset('resources')}}/Logo.png" class="header-brand-img"
			         alt="" height="28" width="28">
			@endif
        	{{$settings['title'] ?? config('app.name')}}<small class="float-right mt-1">Date: {{ date('d-M-Y h:i a', strtotime($estimate->updated_at)) }}</small></h3></div>
        <div class="card-body">
            <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                    From
                    <address>
                        <strong>{{$settings['title'] ?? config('app.name')}}</strong><br>
                        {{$settings['address'] ?? config('app.address')}} <br>
                        Phone: {{$settings['phone']}}<br>
                        Email: {{$settings['email']}}
                    </address>
                </div>
                <div class="col-sm-4 invoice-col offset-sm-4">
                    To
                    <address>
                        <strong>{{ $estimate->client->name ?? '' }}</strong><br>{{ $estimate->client->address ?? '' }}, {{ $estimate->client->city ?? '' }}<br>{{ $estimate->client->state ?? '' }} {{ $estimate->client->zip ?? '' }} {{ $estimate->client->country ?? '' }}<br>
                        Phone: {{ $estimate->client->phone ?? '' }}<br>
                        Email: {{ $estimate->client->email ?? '' }}
                    </address>
                </div>
            </div>

            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-striped" id="itemsTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>Qty</th>
                                <th>Rate</th>
                                <th>Discount</th>
                                <th class="font-weight-bold text-center">Total</th>
                                <th class="text-right th-fixed-width"><i class="ik ik-align-justify"></i></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-4">
                    <p class="lead"></p>
                    <div class="table-responsive">
                    	<table class="table table-bordered">
                    		<thead></thead>
	                    	<tbody>
	                    		{{-- @if(count($invoice->payments) > 0)
		                    		@foreach($invoice->payments as $payment)
		                    			<tr>
		                    				<td>{{ $payment->paymentmethod->title ?? '' }}</td>
		                    				<td>{{ $payment->amount ?? 0 }}</td>
		                    			</tr>
		                    		@endforeach
		                    	@endif --}}
	                    	</tbody>
                    	</table>
                    </div>
                    @if($estimate->status == 4)
                        <p class="font-weight-bold text-uppercase"><i class="ik ik-check-square text-success"></i>{{__('Invoiced')}}</p>
                    @endif
                </div>
                <div class="col-3 offset-5">
                    <div id="total"></div>
                    <div class="table-responsive">
                        <table class="table" id="discountTable">
                            <tr>
                                <th>Subtotal:</th>
                                <td id="subTotal" class="font-weight-bold estSubTotal"></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Discount @if($estimate->discount_type == 1) <span>%</span> @else <span>(Fixed Amount)</span>@endif:</th>
                                <td class="font-weight-bold">{{ $estimate->discount }}</td>
                                <td class="text-right"><a class="edit-btn" type="button" data-toggle="modal" data-target="#discountModal" data-whatever="1" data-id="1"><i class="ik ik-edit-2"></i></a></td>
                            </tr>
                            <tr>
                                <th>Total:</th>
                                <td class="font-weight-bold" id="estimateTotalValue"></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row no-print">
                <div class="col-12">
                    <a href="javascript:void(0)" onclick="location.href='{{ route('esitmate-pdf-generate', ['id'=>$estimate->id]) }}'" class="btn btn-primary pull-right generate-pdf-btn" style="margin-right: 5px;"><i class="fa fa-download"></i> {{__('Generate PDF')}}</a>
                </div>
            </div>
        </div>
    </div>




    <!--Add Item Modal Start-->
    <div class="modal animated zoomIn faster" id="itemModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Item')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="itemForm" action="javascript:void(0)">
                    <div class="modal-body">
                        <div class="form-group row">
                            <input type="hidden" name="estimateid" value="{{ $estimate->id }}">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Item')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <select name="item" id="item" class="form-control select2" required></select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Unit Type')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control" placeholder="{{__('unit here')}}" name="unittype" id="unittype" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Rate')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control" placeholder="{{__('rate here')}}" name="rate" id="rate" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Discount')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control" name="discount" id="discount" readonly>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Discount Type')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <select name="itemdisctype" id="itemdisctype" class="form-control select2">
                                    <option value="1">Percentage(%)</option>
                                    <option value="2">Fixed Amount</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Quantity')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control" name="quantity" id="quantity" oninput="this.value=(parseInt(this.value)||1)" value="1">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary item-save-btn">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--Add Item Modal End---->

    <!--DiscountModal Start-->
    <div class="modal animated zoomIn faster" id="discountModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Discount')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="discountForm" action="javascript:void(0)">
                    <div class="modal-body">
                        <div class="form-group row">
                            <input type="hidden" name="estimateid" value="{{ $estimate->id }}">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Discount')}}</label>
                            <div class="col-sm-4 col-lg-4">
                                <input type="text" class="form-control" placeholder="{{__('discount here')}}" name="discount" id="discount" oninput="this.value=(parseInt(this.value)||0)" value="0">
                            </div>
                            <div class="col-sm-4 col-lg-4">
                                <select name="discount_type" id="discount_type" class="form-control select2">
                                    <option value="1">Percentage(%)</option>
                                    <option value="2">Fixed Amount</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary save-discount-btn"  value="{{ $estimate->id }}">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--DiscountModal End---->


    <!--markAsInvoiceModal Start-->
    <div class="modal animated zoomIn faster" id="invoiceMarkModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Mark As Invoice')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="markAsProjectForm" action="javascript:void(0)">
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Title')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control" placeholder="{{__('project title here')}}" name="title" id="title">
                            </div>
                        </div>
                        <div class="form-group row">
                           <label class="col-sm-4 col-lg-4 col-form-label">{{__('Start Date')}}</label>
                           <div class="col-sm-8 col-lg-8">
                               <input type="text" class="form-control date" placeholder="{{__('start date')}}" name="startdate" id="startdate" required>
                           </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-4 col-lg-4 col-form-label">{{__('Deadline')}}</label>
                            <div class="col-sm-8 col-lg-8">
                                <input type="text" class="form-control date" placeholder="{{__('end date')}}" name="enddate" id="enddate" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                        <button type="submit" class="btn btn-primary save-mark-as-invoice-btn" value="{{ $estimate->id }}">{{__('Save')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--markAsInvoiceModal End---->

</div>


@endsection

@section('extraCSS')
<link rel="stylesheet" href="{{asset('resources')}}/custom/remove-table-head-sort-arrow.css">
<script type="text/javascript" href="http://cdn.datatables.net/plug-ins/1.10.20/api/sum().js"></script>
@endsection
@section('extraJS')
<script>
    let iTable;
    let subTotal = 0;
	$(document).ready(function(){
        let estId = $("#estimateId").val();

        iTable = $('#itemsTable').DataTable({
            processing: true,
            serverSide: true,
            orderable: false,
            paging: false,
            bInfo: false,
            deferRender: true,
            searching: false,
            ajax: {
                    url: '{{ route('estimate-items.index') }}',
                    type: "GET",
                    data: function (d) {
                        d.estimateid = estId;
                        d._token = '{{csrf_token()}}';
                }
            },
            createdRow: function( row, data, dataIndex ) {
                    $( row ).find('td:eq(5)').attr('class', 'text-center');
            },
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'item.name', name: 'item.name'},
                {data: 'quantity', name: 'quantity'},
                {data: 'rate', name: 'rate'},
                {data: 'discount', name: 'discount'},
                {data: 'rowtotal', name: 'rowtotal'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            
            footerCallback: function (row, data, start, end, display) {                
                for (var i = 0; i < data.length; i++) {
                    subTotal += parseFloat(data[i]['rowtotal']);
                }
                $("#subTotal").text(subTotal);
                estimateTotal(subTotal);
            }

        });
        
        htmlReload();
        

        $('.modal').on('hidden.bs.modal', function (e) {
            $('form').trigger("reset");
        });

        /*--------------create item form start---------*/
        $('#itemModal').on('show.bs.modal', function (event) {
            var modal = $(this);
            $.ajax({
                url: '{{route('items-id-name')}}',
                type: 'GET',
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                   $('#item').empty();
                   $('#item').append($('<option>', {
                       value: '',
                       text: '-'
                   }));
                   $.each(response, function (i, item) {
                       $('#item').append($('<option>', {
                           value: item.id,
                           text: item.name
                       }));
                   });
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
        });
        /*--------------create item form end-----------*/

        $("#item").on("select2:select", function(event){
            event.preventDefault();
            let id = $("#item").val();
            var modal = $(this);
            if (id) {
                $.ajax({
                    url: '{{route('items.index')}}/' + id,
                    type: 'GET',
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        modal.find('input[name=unittype]').val(response.unittype.name);
                        modal.find('').val(response.discount_type);
                        $("#unittype").val(response.unittype.name);
                        $("#rate").val(response.rate);
                        $("#discount").val(response.discount);
                        $('#itemdisctype').empty();
                        if (response.discount_type == 1) {
                            $('#itemdisctype').append($('<option>', {
                                value: '1',
                                text: '{{__("Percentage(%)")}}'
                            }));
                        }else{
                            $('#itemdisctype').append($('<option>', {
                                value: '2',
                                text: '{{__("Fixed Amount")}}'
                            }));
                        }
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }
            //$('#itemForm').trigger("reset");
        });

        /*--------------saving item start--------------*/
        $(document).on('click', '.item-save-btn', function (e) {
            e.preventDefault();

            if ($('#item').val() == '') {
                alert("Please select an item!");
                return false;
            }

            let data = $('#itemForm').serialize();
            $.ajax({
                url: '{{route('estimate-items.store')}}',
                type: 'POST',
                data: data,
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    if (response.message) {
                        showDangerToast(response.message);
                    }else{
                         $('#itemModal').modal('toggle');
                         $('#itemForm').trigger("reset");
                         $('#item').empty();
                         $('#itemdisctype').empty();
                         showSuccessToast('{{__("Added Successfully")}}');
                         //iTable.draw(false);
                         //htmlReload();
                         location.reload();
                    }
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
        });
        /*--------------saving item end---------------*/




        /*--------------discount form start---------*/
        $('#discountModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var modal = $(this);
            var id = $("#estimateId").val();
            $.ajax({
                url: '{{route('estimate-discount')}}',
                type: 'GET',
                data: {id},
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    modal.find('input[name=discount]').val(response.discount);
                    modal.find('#discount_type').val(response.discount_type).trigger('change');
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                    console.log(data);
                }
            });
        });
        /*--------------discount form end-----------*/

        /*--------------saving discount start--------------*/
        $(document).on('click', '.save-discount-btn', function (e) {
            e.preventDefault();

            let data = $('#discountForm').serialize();
            let id = $(this).val();

            $.ajax({
                url: '{{route('estimate-discount')}}/' + id,
                type: 'PUT',
                data: data,
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    $('#discountModal').modal('toggle');
                    // $('#discountForm').trigger("reset");
                    // $('#discount_type').empty();
                    location.reload();
                    // iTable.draw(false);
                    // $("#discountTable").html(html);
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
        });
        /*--------------saving discount end---------------*/
        
    });


    /*----------delete item from cart start-------------------------*/
    $(document).on('click', '.delete-btn', function (e) {
        e.preventDefault(); // does not go through with the link.
        let confirmation = confirm("{{__('Are you sure to delete')}} ?");
        if (!confirmation) {
            return;
        }
        var id = $(this).data('id');
        $.ajax({
            url: '{{ route('estimate-items.index') }}/' + id,
            type: 'DELETE',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function (response) {
                showSuccessToast('{{__("Deleted Successfully")}}');
                //iTable.draw(false);
                //htmlReload();
                location.reload();
            },
            error: function (data) {
                let msg = '';
                if (data.responseJSON.errors) {
                    $.each(data.responseJSON.errors, function (i, error) {
                        msg += '<p">' + error[0] + '</p>';
                    });
                } else {
                    msg = data.responseJSON.message;
                }
                showDangerToast(msg);
            }
        });
    });
    /*----------delete item from cart end-------------------------*/

    /*----------mark as invoice modal start-----------*/
    $(document).on('click', '.save-mark-as-invoice-btn', function(e){
        e.preventDefault(); // does not go through with the link.
        let confirmation = confirm("{{__('You are going to mark as ready to production.Are you sure')}} ?");
        if (!confirmation) {
            return;
        }
        let estimateId = $(this).val();
        let title = $("#title").val();
        let startdate = $("#startdate").val();
        let enddate = $("#enddate").val();
        let price = $("#estimateTotalValue").text();
        $.ajax({
            url: '{{route('estimate-mark-as-project')}}',
            type: 'PUT',
            data: {
                estimateid: estimateId,
                title: title,
                startdate: startdate,
                enddate: enddate,
                price: price
            },
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function (response) {
                $('#invoiceMarkModal').modal('toggle');
                $('#markAsProjectForm').trigger("reset");
                showSuccessToast('{{__("Added As Project Successfully")}}');
                location.reload();
            },
            error: function (data) {
                let msg = '';
                if (data.responseJSON.errors) {
                    $.each(data.responseJSON.errors, function (i, error) {
                        msg += '<p">' + error[0] + '</p>';
                    });
                } else {
                    msg = data.responseJSON.message;
                }
                showDangerToast(msg);
            }
        });
    });
    /*----------mark as invoice modal end------------*/




    function htmlReload(){
        let html = "";
        html += '<tr>'+
                    '<th>Subtotal:</th>'+
                    '<td id="subTotal" class="font-weight-bold estSubTotal"></td>'+
                    '<td></td>'+
                '</tr>';
        html += '<tr>'+
                    '<th>Discount @if($estimate->discount_type == 1) <span>%</span> @else <span>(Fixed Amount)</span>@endif:</th>'+
                    '<td class="font-weight-bold">{{ $estimate->discount }}</td>'+
                    '<td class="text-right"><a class="edit-btn" type="button" data-toggle="modal" data-target="#discountModal" data-whatever="1" data-id="1"><i class="ik ik-edit-2"></i></a></td>'+
                '</tr>';
        html += '<tr>'+
                    '<th>Total:</th>'+
                    '<td class="font-weight-bold" id="estimateTotalValue"></td>'+
                    '<td></td>'+
                '</tr>';
        $("#discountTable").html(html);
    }

    function estimateTotal(subTotal){
        let estimateDiscount = $("#estimateDiscount").val();
        let estimateDiscountType = $("#estimateDiscountType").val();
        let estimateTotalValue = 0;
        if (estimateDiscountType == 2) {
            estimateTotalValue = (parseFloat(subTotal) - parseFloat(estimateDiscount));
        }else{
            estimateTotalValue = (parseFloat(subTotal) - parseFloat((subTotal * estimateDiscount)/100));
        }

        $("#estimateTotalValue").text(estimateTotalValue);
    }
</script>
@endsection