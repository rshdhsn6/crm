@extends('crm::layouts.master')
@section('title')
{{__('Home')}}
@endsection
@section('content')
  
<div class="container-fluid">
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-file-text bg-blue"></i>
                    <div class="d-inline">
                        <h5>{{__('Estimates List')}}</h5>
                        <span>{{__('This is the list of the project estimate from client those are estimated or caceled.')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('crm')}}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">{{__('Estimates')}}</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('List')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-block">
                    <h3 class="float-left"> {{__('Estimates List')}} </h3>
                </div>
                <div class="card-body">
                    <div class="dt-responsive">
                        <table id="estimateTable" class="table nowrap">
                            <thead>
                            <tr>
                                <th>{{__('#')}}</th>
                                <th>{{__('Estimate ID')}}</th>
                                <th>{{__('Client')}}</th>
                                <th>{{__('Budget')}}</th>
                                <th>{{__('Submitted At')}}</th>
                                <th>{{__('Updated At')}}</th>
                                <th>{{__('Status')}}</th>
                                <th class="text-center"><i class="ik ik-align-justify"></i></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Estimate Modal -->
<div class="modal animated zoomIn faster" id="estimateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Edit Estimate')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="Form" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="form-group row" id="client-box">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Client')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <select name="client" id="client" class="form-control select2" required>

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Description')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <textarea class="form-control" id="exampleTextarea1" rows="4" placeholder="{{__('Describe what you want done?')}}.." name="description"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label" for="budget">{{__('Budget')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" placeholder="{{__('budget here')}}" name="budget" id="budget" value="0" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label" for="examplelink">{{__('Example Link')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" placeholder="{{__('Work example link here')}}" name="examplelink" id="examplelink">
                        </div>
                    </div>
                    <input type="hidden" name="esitmateid" id="esitmateid">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                    <button type="submit" class="btn btn-primary update-btn">{{__('Update')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('extraJS')
<script>
    let eTable;
    $(document).ready(function () {
        eTable = $('#estimateTable').DataTable({
            processing: true,
            serverSide: true,
            orderable: false,
            ajax: "{{ route('estimates.index') }}",
            deferRender: true,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'estimateid', name: 'estimateid'},
                {data: 'client.name', name: 'client.name'},
                {data: 'budget', name: 'budget'},
                {data: 'created_at', name: 'created_at'},
                {data: 'updated_at', name: 'updated_at'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        /*--------------create form start---------*/
        $('.modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var modal = $(this);
            var id = button.data('id');

            $.ajax({
                url: '{{route('estimates.index')}}/' + id + '/edit',
                type: 'GET',
                data: {id},
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    if (response.status != 4) {
                        modal.find('#client').val(response.client_id).trigger('change');
                    }else{
                        document.getElementById("client").disabled = true;
                    }

                    modal.find('textarea[name=description]').val(response.description);
                    modal.find('input[name=budget]').val(response.budget);
                    modal.find('input[name=examplelink]').val(response.example_link);
                    modal.find('input[name=esitmateid]').val(response.id);

                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                    console.log(data);
                }
            });
            
        });
        /*--------------create form end----------*/

        loadClient();


        /*--------------updating estimate start--------------*/
        $(document).on('click', '.update-btn', function () {
            let data = $('#Form').serialize();
            $.ajax({
                url: "{{ route('update-estimate')}}/",
                type: 'PUT',
                data: data,
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    $('#estimateModal').modal('toggle');
                    $('#Form').trigger("reset");
                    showSuccessToast('{{__("Updated Successfully")}}');
                    eTable.draw(false);
                    console.log(response);
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
            
        });
        /*--------------updating estimate end---------------*/
    });
    function loadClient(){
        $.ajax({
            url: '{{route('client-name-id')}}',
            type: 'GET',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function (response) {
               $('#client').empty();
               $.each(response, function (i, client) {
                   $('#client').append($('<option>', {
                       value: client.id,
                       text: client.name
                   }));
               });
            },
            error: function (data) {
                let msg = '';
                if (data.responseJSON.errors) {
                    $.each(data.responseJSON.errors, function (i, error) {
                        msg += '<p">' + error[0] + '</p>';
                    });
                } else {
                    msg = data.responseJSON.message;
                }
                showDangerToast(msg);
                console.log(data);
            }
        });
    }
</script>
@endsection