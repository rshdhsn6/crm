@extends('crm::layouts.master')
@section('title')
{{__('Home')}}
@endsection
@section('content')
  
<div class="container-fluid">
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-clipboard bg-blue"></i>
                    <div class="d-inline">
                        <h5>{{__('Estimate Request Details')}}</h5>
                        <span>{{__('This is the estimate request details.')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('crm')}}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">{{__('Estimate Request')}}</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('Details')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-block">
                    <h3 class="float-left"> {{__('Estimate Request')}} # {{ $estimateRequest->id }}</h3>
                    <div class="header-action-dropdown text-right">
                        <div class="dropdown">
                            <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-cogs"></i> Actions<span class="caret"></span>
                            </button>
                            <div class="dropdown-menu">
                                @if($estimateRequest->status == 3 || $estimateRequest->status == 1)
                                <button class="dropdown-item update-estimate-request-status" type="button" value="2"><i class="ik ik-check-circle"></i> Mark As Processing</button>
                                @endif
                                @if($estimateRequest->status == 2 || $estimateRequest->status == 1)
                                <button class="dropdown-item update-estimate-request-status" type="button" value="3"><i class="ik ik-check-circle"></i> Mark As Estimated</button>
                                @endif
                                @if($estimateRequest->status == 4)
                                    <button class="dropdown-item update-estimate-request-status" type="button" value="1"><i class="ik ik-refresh-ccw"></i> Re-Open</button>
                                @else
                                    <button class="dropdown-item update-estimate-request-status" type="button" value="4"><i class="ik ik-x-circle"></i> Mark As Canceled</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h4 class="text-capitalize">{{ $estimateRequest->estimateditems[0]->item->name ?? '' }}</h4>
                    <input type="hidden" name="estimatereqid" id="estimatereqid" value="{{ $estimateRequest->id }}">
                    <p class="d-inline text-muted estimate-details-head"><strong>Status:</strong> 
                        @if($estimateRequest->status == 1)
                            <span class="badge badge-secondary">Draft</span>
                        @elseif($estimateRequest->status == 2)
                            <span class="badge badge-primary">Processing</span>
                        @elseif($estimateRequest->status == 3)
                            <span class="badge badge-success">Estimated</span>
                        @elseif($estimateRequest->status == 4)
                            <span class="badge badge-warning">Canceled</span>
                        @else
                            <span>Unknown</span>
                        @endif
                    </p>
                    <p class="d-inline text-muted pl-10"><strong>Create Date:</strong> {{ date('d-M-Y h:i A', strtotime($estimateRequest->created_at)) }}</p>
                    <p class="d-inline text-muted pl-10"><strong>Update Date:</strong> {{ date('d-M-Y h:i A', strtotime($estimateRequest->updated_at)) }}</p>
                    <p></p>
                    <ul class="list-group">
                      <li class="list-group-item"><i class="ik ik-check-square"></i> <span class="font-weight-bold">Describe what you want done?</span>
                        <p class="text-justify ml-20">{{ $estimateRequest->description}}</p>
                      </li>
                      <li class="list-group-item"><i class="ik ik-check-square"></i> <span class="font-weight-bold">What budget do you have in mind?</span>
                        <p class="text-justify ml-20">{{ $estimateRequest->budget}}</p>
                      </li>
                      <li class="list-group-item"><i class="ik ik-check-square"></i> <span class="font-weight-bold">Do you have any example link?</span>
                        <p class="text-justify ml-20">{{ $estimateRequest->example_link ?? 'No'}}</p>
                      </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Estimate Modal -->
<div class="modal animated zoomIn faster" id="estimateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Estimate Request')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="Form" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Item')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <select name="item" id="item" class="form-control select2" required>

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Client')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <select name="client" id="client" class="form-control select2" required>

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Description')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <textarea class="form-control" id="exampleTextarea1" rows="4" placeholder="{{__('Describe what you want done?')}}.." name="note"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label" for="budget">{{__('Budget')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" placeholder="{{__('budget here')}}" name="budget" id="budget" value="0" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label" for="examplelink">{{__('Example Link')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" placeholder="{{__('Work example link here')}}" name="examplelink" id="examplelink" required>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                    <button type="submit" class="btn btn-primary"></button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('extraJS')
<script>
    $(document).on('click', '.update-estimate-request-status', function(e){
        e.preventDefault();
        let status = $(this).val();
        let estimateid = $("#estimatereqid").val();

        let cm = confirm('{{__("You are going to update! Are you sure?")}}');
        if (cm == true) {
           $.ajax({
                url: "{{ route('estimate-request-status-update')}}",
                type: 'PUT',
                data: {
                    estimateid:estimateid,
                    status:status
                },
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    console.log(response);
                    showSuccessToast('{{__("Updated Successfully")}}');
                    location.reload();
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                   showDangerToast(msg);
                }
           });
        }
    });
</script>
@endsection