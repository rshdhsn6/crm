@extends('crm::layouts.master')
@section('title')
{{__('Home')}}
@endsection
@section('content')
  
<div class="container-fluid">
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-layers bg-blue"></i>
                    <div class="d-inline">
                        <h5>{{__('Unit Types')}}</h5>
                        <span>{{__('Unit Types define in which items they are(Ex: Hours, Pcs etc.)')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('crm')}}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">{{__('Items')}}</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('Unit Types')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-block">
                    <h3 class="float-left"> {{__('Unit Types')}} </h3>
                    @can('hrm-department-add')
                        <button type="button" class="btn btn-success float-right" data-toggle="modal"
                                data-target="#unitTypeModal" data-whatever="0"> + {{__('Add Unit Type')}}
                        </button>
                    @endcan
                </div>
                <div class="card-body">
                    <div class="dt-responsive">
                        <table id="unitTypes" class="table nowrap">
                            <thead>
                            <tr>
                                <th>{{__('ID')}}</th>
                                <th>{{__('Unit Type')}}</th>
                                <th>{{__('Description')}}</th>
                                <th>{{__('Created At')}}</th>
                                <th class="text-right"></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Unit Type Modal -->
<div class="modal animated zoomIn faster" id="unitTypeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Unit Type')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="Form" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('New Unit Type')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" placeholder="{{__('name here')}}" name="name" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Description')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <textarea class="form-control" id="exampleTextarea1" rows="4" placeholder="{{__('description here')}}.." name="description"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                    <button type="submit" class="btn btn-primary save-btn">{{__('Save')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('extraJS')
<script>
   $(document).ready(function () {
        var uTable = $('#unitTypes').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('unit-types.index') }}",
            deferRender: true,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'name', name: 'name'},
                {data: 'description', name: 'description'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        $('.modal').on('hidden.bs.modal', function (e) {
            $('form').trigger("reset");
        });

        /*--------------create and edit form start---------*/
        $('.modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var purpose = button.data('whatever'); // Extract info from data-* attributes
            var modal = $(this);
            
            if (purpose) {
                modal.find('.modal-title').text('{{__("Edit Unit Type")}}');
                var id = button.data('id');
                $.ajax({
                    url: '{{route('unit-types.index')}}/' + id,
                    type: 'GET',
                    data: {id},
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        modal.find('input[name=name]').val(response.name);
                        modal.find('textarea[name=description]').val(response.description);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
                modal.find('button[type=submit]').removeClass('save-btn').addClass('update-btn').text('{{__("Update")}}').val(id);
            } else {
                modal.find('.modal-title').text('{{__("Add Unit Type")}}');
                modal.find('button[type=submit]').removeClass('update-btn').addClass('save-btn').text('{{__("Save")}}');
            }
        });
        /*--------------create and edit form end---------------*/

        /*--------------saving unit type start--------------*/
        $(document).on('click', '.save-btn', function (e) {
            e.preventDefault();

            let data = $('#Form').serialize();
            $.ajax({
                url: '{{route('unit-types.store')}}',
                type: 'POST',
                data: data,
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    $('#unitTypeModal').modal('toggle');
                    $('#Form').trigger("reset");
                    showSuccessToast('{{__("Added Successfully")}}');
                    uTable.draw(false);
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
        });
        /*--------------saving unit type end---------------*/

        /*--------------updating unit type start--------------*/
        $(document).on('click', '.update-btn', function () {

            let data = $('#Form').serialize();
            let id = $(this).val();
            let cm = confirm('{{__("You are going to update! Are you sure?")}}');
            if (cm == true) {
                $.ajax({
                    url: "{{ route('unit-types.index')}}/" + id,
                    type: 'PUT',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#unitTypeModal').modal('toggle');
                        $('#Form').trigger("reset");
                        showSuccessToast('{{__("Updated Successfully")}}');
                        uTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }
            
        });
        /*--------------updating unit type end---------------*/
   });
</script>
@endsection