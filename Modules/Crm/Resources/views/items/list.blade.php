@extends('crm::layouts.master')
@section('title')
{{__('Home')}}
@endsection
@section('content')
  
<div class="container-fluid">
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-layers bg-blue"></i>
                    <div class="d-inline">
                        <h5>{{__('Items List')}}</h5>
                        <span>{{__('Items define these types of products or services you provide.')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('crm')}}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">{{__('Items')}}</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('List')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-block">
                    <h3 class="float-left"> {{__('Items')}} </h3>
                    @can('hrm-department-add')
                        <button type="button" class="btn btn-success float-right" data-toggle="modal"
                                data-target="#itemModal" data-whatever="0"> + {{__('Add Item')}}
                        </button>
                    @endcan
                </div>
                <div class="card-body">
                    <div class="dt-responsive">
                        <table id="items" class="table nowrap">
                            <thead>
                            <tr>
                                <th>{{__('ID')}}</th>
                                <th>{{__('Item')}}</th>
                                <th>{{__('Description')}}</th>
                                <th>{{__('Unit Type')}}</th>
                                <th>{{__('Rate')}}</th>
                                <th class="text-center">{{__('Discount')}}</th>
                                <th class="text-center">{{__('Discount Type')}}</th>
                                <th class="text-center">{{__('Last Update')}}</th>
                                <th class="text-right"></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Item Modal -->
<div class="modal animated zoomIn faster" id="itemModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">{{__('Item')}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="Form" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('New Item')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" placeholder="{{__('name here')}}" name="name" id="name" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Description')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <textarea class="form-control" id="exampleTextarea1" rows="4" placeholder="{{__('description here')}}.." name="description" id="description"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Unit Type')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <select name="unittype" id="unittype" class="form-control select2" required>

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Rate')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" placeholder="{{__('rate here')}}" name="rate" id="rate" value="0" oninput="this.value=(parseInt(this.value)||0)" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Discount')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" placeholder="{{__('discount here')}}" name="discount" id="discount" value="0" oninput="this.value=(parseInt(this.value)||0)" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="discountType" class="col-sm-4 col-lg-4 col-from-label">{{__('Discount Type')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <div class="form-radio">
                                <div class="radio radio-inline">
                                    <label>
                                        <input type="radio" name="discount_type" id="discount_type" value="1" checked="checked">
                                        <i class="helper"></i>{{__('Percentage')}} (%)
                                    </label>
                                </div>
                                <div class="radio radio-inline">
                                    <label>
                                        <input type="radio" name="discount_type" id="discount_type" value="2">
                                        <i class="helper"></i>{{__('Fixed Amount')}}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                    <button type="submit" class="btn btn-primary save-btn">{{__('Save')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('extraJS')
<script>
$(document).ready(function () {
     var iTable = $('#items').DataTable({
         processing: true,
         serverSide: true,
         ajax: "{{ route('items.index') }}",
         deferRender: true,
         columns: [
             {data: 'DT_RowIndex', name: 'DT_RowIndex'},
             {data: 'name', name: 'name'},
             {data: 'description', name: 'description'},
             {data: 'unittype.name', name: 'unittype.name'},
             {data: 'rate', name: 'rate'},
             {data: 'discount', name: 'discount'},
             {data: 'discount_type', name: 'discount_type'},
             {data: 'updated_at', name: 'updated_at'},
             {data: 'action', name: 'action', orderable: false, searchable: false},
         ]
     });

    loadUnitTypes();
    function loadUnitTypes(){
        $.ajax({
            url: '{{route('unit-types.index')}}',
            type: 'GET',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function (response) {
                $('#unittype').empty();
                $('#unittype').append($('<option>', {
                    value: '',
                    text: '-'
                }));
                $.each(response.data, function (i, unittype) {
                    $('#unittype').append($('<option>', {
                        value: unittype.id,
                        text: unittype.name
                    }));
                });
            },
            error: function (data) {
                let msg = '';
                if (data.responseJSON.errors) {
                    $.each(data.responseJSON.errors, function (i, error) {
                        msg += '<p">' + error[0] + '</p>';
                    });
                } else {
                    msg = data.responseJSON.message;
                }
                showDangerToast(msg);
            }
        });
    }

     $('.modal').on('hidden.bs.modal', function (e) {
         $('form').trigger("reset");
     });

     /*--------------create and edit form start---------*/
     $('.modal').on('show.bs.modal', function (event) {
         var button = $(event.relatedTarget); // Button that triggered the modal
         var purpose = button.data('whatever'); // Extract info from data-* attributes
         var modal = $(this);
         
         if (purpose) {
             modal.find('.modal-title').text('{{__("Edit Item")}}');
             var id = button.data('id');
             $.ajax({
                 url: '{{route('items.index')}}/' + id,
                 type: 'GET',
                 data: {id},
                 beforeSend: function (request) {
                     return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                 },
                 success: function (response) {
                    modal.find('input[name=name]').val(response.name);
                    modal.find('textarea[name=description]').val(response.description);
                    modal.find('#unittype').val(response.unittype.id).trigger('change');
                    modal.find('input[name=rate]').val(response.rate);
                    if(response.discount_type == 1){
                        modal.find(':radio[name=discount_type][value="1"]').prop('checked', true);
                    }else{
                        modal.find(':radio[name=discount_type][value="2"]').prop('checked', true);
                    }
                 },
                 error: function (data) {
                     let msg = '';
                     if (data.responseJSON.errors) {
                         $.each(data.responseJSON.errors, function (i, error) {
                             msg += '<p">' + error[0] + '</p>';
                         });
                     } else {
                         msg = data.responseJSON.message;
                     }
                     showDangerToast(msg);
                 }
             });
             modal.find('button[type=submit]').removeClass('save-btn').addClass('update-btn').text('{{__("Update")}}').val(id);
         } else {
             modal.find('.modal-title').text('{{__("Add Item")}}');
             modal.find('button[type=submit]').removeClass('update-btn').addClass('save-btn').text('{{__("Save")}}');
         }
     });
     /*--------------create and edit form end---------------*/

     /*--------------saving Item start--------------*/
     $(document).on('click', '.save-btn', function (e) {
         e.preventDefault();

         if ($("#name").val() == '') {
             alert("Item name is required!");
             return false;
         }
         if ($("#unittype").val() == '') {
             alert("Please select unit type!");
             return false;
         }
         if ($("#rate").val() == '') {
             alert("Rate field is required!");
             return false;
         }
         if ($("#rate").val() <= 0) {
             alert("Rate field should be greater than zero!");
             $("#rate").focus();
             return false;
         }

         let data = $('#Form').serialize();
         $.ajax({
             url: '{{route('items.store')}}',
             type: 'POST',
             data: data,
             beforeSend: function (request) {
                 return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
             },
             success: function (response) {
                 $('#itemModal').modal('toggle');
                 $('#Form').trigger("reset");
                 showSuccessToast('{{__("Added Successfully")}}');
                 iTable.draw(false);
             },
             error: function (data) {
                 let msg = '';
                 if (data.responseJSON.errors) {
                     $.each(data.responseJSON.errors, function (i, error) {
                         msg += '<p">' + error[0] + '</p>';
                     });
                 } else {
                     msg = data.responseJSON.message;
                 }
                 showDangerToast(msg);
             }
         });
     });
     /*--------------saving Item end---------------*/

     /*--------------updating Item start--------------*/
     $(document).on('click', '.update-btn', function () {
        if ($("#name").val() == '') {
            alert("Item name is required!");
            return false;
        }
        if ($("#unittype").val() == '') {
            alert("Please select unit type!");
            return false;
        }
        if ($("#rate").val() == '') {
            alert("Rate field is required!");
            return false;
        }
        if ($("#rate").val() <= 0) {
            alert("Rate field should be greater than zero!");
            $("#rate").focus();
            return false;
        }
        let data = $('#Form').serialize();
        let id = $(this).val();
        let cm = confirm('{{__("You are going to update! Are you sure?")}}');
        if (cm == true) {
            $.ajax({
                url: "{{ route('items.index')}}/" + id,
                type: 'PUT',
                data: data,
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    $('#itemModal').modal('toggle');
                    $('#Form').trigger("reset");
                    showSuccessToast('{{__("Updated Successfully")}}');
                    iTable.draw(false);
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
         }
         
     });
     /*--------------updating Item end---------------*/
});
</script>
@endsection