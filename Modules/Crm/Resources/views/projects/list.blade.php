@extends('crm::layouts.master')
@section('title')
{{__('Home')}}
@endsection
@section('content')
  
<div class="container-fluid">
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-package bg-blue"></i>
                    <div class="d-inline">
                        <h5>{{__('Projects List')}}</h5>
                        <span>{{__('Here is the list of client projects.')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('crm')}}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">{{__('Projects')}}</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('List')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-block">
                    <h3 class="float-left"> {{__('Projects')}} </h3>
                    <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#projectModal"> + {{__('Add Project')}}
                    </button>
                </div>
                <div class="card-body">
                    <div class="dt-responsive">
                        <table id="projects" class="table nowrap">
                            <thead>
                            <tr>
                                <th>{{__('ID')}}</th>
                                <th>{{__('Title')}}</th>
                                <th>{{__('Client')}}</th>
                                <th>{{__('Price')}}</th>
                                <th>{{__('Start Date')}}</th>
                                <th>{{__('Deadline')}}</th>
                                {{-- <th>{{__('Progress')}}</th> --}}
                                <th>{{__('Status')}}</th>
                                <th class="text-right"><i class="ik ik-align-justify"></i></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Project Modal Start -->
<div class="modal animated zoomIn faster" id="projectModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="Form" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Title')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" placeholder="{{__('title here')}}" name="title" id="title" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Client')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <select name="client" id="client" class="form-control select2" required>

                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Description')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <textarea class="form-control" id="exampleTextarea1" rows="4" placeholder="{{__('description here')}}.." name="description"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Start Date')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control date" placeholder="{{__('start date')}}" name="startdate" id="startdate" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Deadline')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control date" placeholder="{{__('end date')}}" name="enddate" id="enddate" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Price')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" placeholder="{{__('price here')}}" name="price" value="0" oninput="this.value=(parseInt(this.value)||0)" required>
                        </div>
                    </div>
                    <div id="project-status-box">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Status')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <select name="status" id="status" class="form-control select2" required>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                    <button type="submit" class="btn btn-primary save-btn">{{__('Save')}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Project Modal End   -->
@endsection

@section('extraJS')
<script>
$(document).ready(function () {
    var pTable = $('#projects').DataTable({
        processing: true,
        serverSide: true,
        orderable: false,
        ajax: "{{ route('client-projects.index') }}",
        deferRender: true,
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'title', name: 'title'},
            {data: 'client', name: 'client'},
            {data: 'price', name: 'price'},
            {data: 'start_date', name: 'start_date'},
            {data: 'deadline', name: 'deadline'},
            {data: 'status.title', name: 'status.title'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

    getClientNameId();
    function getClientNameId(){
        $.ajax({
            url: '{{route('client-name-id')}}',
            type: 'GET',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function (response) {
               $('#client').empty();
               $.each(response, function (i, client) {
                   $('#client').append($('<option>', {
                       value: client.id,
                       text: client.name
                   }));
               });
            },
            error: function (data) {
                let msg = '';
                if (data.responseJSON.errors) {
                    $.each(data.responseJSON.errors, function (i, error) {
                        msg += '<p">' + error[0] + '</p>';
                    });
                } else {
                    msg = data.responseJSON.message;
                }
                showDangerToast(msg);
                console.log(data);
            }
        });
    }

    getAvailableProjectStatus();
    function getAvailableProjectStatus(){
        $.ajax({
            url: '{{route('porject-statues.index')}}',
            type: 'GET',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function (response) {
               $('#status').empty();
               $.each(response.data, function (i, status) {
                   $('#status').append($('<option>', {
                       value: status.id,
                       text: status.title
                   }));
               });
            },
            error: function (data) {
                let msg = '';
                if (data.responseJSON.errors) {
                    $.each(data.responseJSON.errors, function (i, error) {
                        msg += '<p">' + error[0] + '</p>';
                    });
                } else {
                    msg = data.responseJSON.message;
                }
                showDangerToast(msg);
                console.log(data);
            }
        });        
    }

    $('.modal').on('hidden.bs.modal', function (e) {
        $('#Form').trigger("reset");
    });

    /*--------------create and edit form start---------*/
    $('.modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var purpose = button.data('whatever'); // Extract info from data-* attributes
        var modal = $(this);
        
        if (purpose) {
            modal.find('.modal-title').text('{{__("Edit Project")}}');
            var id = button.data('id');
            document.getElementById("project-status-box").setAttribute("class", "form-group row show-project-status-box");
            $.ajax({
                url: '{{route('client-projects.index')}}/' + id,
                type: 'GET',
                data: {id},
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                   modal.find('input[name=title]').val(response.title);
                   modal.find('#client').val(response.client.id).trigger('change');
                   modal.find('textarea[name=description]').val(response.description);
                   modal.find('input[name=startdate]').val(response.start_date);
                   modal.find('input[name=enddate]').val(response.end_date);
                   modal.find('input[name=price]').val(response.price);
                   modal.find('#status').val(response.status.id).trigger('change');
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                    console.log(data);
                }
            });
            modal.find('button[type=submit]').removeClass('save-btn').addClass('update-btn').text('{{__("Update")}}').val(id);
        } else {
            modal.find('.modal-title').text('{{__("Add Project")}}');
            document.getElementById("project-status-box").setAttribute("class", "form-group row hide-project-status-box");
            modal.find('button[type=submit]').removeClass('update-btn').addClass('save-btn').text('{{__("Save")}}');
        }
    });
    /*--------------create and edit form end----------*/

    /*--------------saving project start--------------*/
    $(document).on('click', '.save-btn', function () {

        let data = $('#Form').serialize();
        $.ajax({
            url: '{{route('client-projects.store')}}',
            type: 'POST',
            data: data,
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function (response) {
                $('#projectModal').modal('hide');
                $('#Form').trigger("reset");
                showSuccessToast('{{__("Added Successfully")}}');
                pTable.draw(false);
            },
            error: function (data) {
                let msg = '';
                if (data.responseJSON.errors) {
                    $.each(data.responseJSON.errors, function (i, error) {
                        msg += '<p">' + error[0] + '</p>';
                    });
                } else {
                    msg = data.responseJSON.message;
                }
                showDangerToast(msg);
            }
        });
    });
    /*--------------saving project end---------------*/

    /*--------------updating project start--------------*/
    $(document).on('click', '.update-btn', function () {
       let data = $('#Form').serialize();
       let id = $(this).val();
       let cm = confirm('{{__("You are going to update! Are you sure?")}}');
       if (cm == true) {
           $.ajax({
               url: "{{ route('client-projects.index')}}/" + id,
               type: 'PUT',
               data: data,
               beforeSend: function (request) {
                   return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
               },
               success: function (response) {
                   $('#projectModal').modal('hide');
                   $('#Form').trigger("reset");
                   showSuccessToast('{{__("Updated Successfully")}}');
                   pTable.draw(false);
               },
               error: function (data) {
                   let msg = '';
                   if (data.responseJSON.errors) {
                       $.each(data.responseJSON.errors, function (i, error) {
                           msg += '<p">' + error[0] + '</p>';
                       });
                   } else {
                       msg = data.responseJSON.message;
                   }
                   showDangerToast(msg);
               }
           });
        }
        
    });
    /*--------------updating project end---------------*/
});

</script>
@endsection