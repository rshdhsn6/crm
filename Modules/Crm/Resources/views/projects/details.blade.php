@extends('crm::layouts.master')
@section('title')
{{__('Home')}}
@endsection
@section('content')
  
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-timeline-tab" data-toggle="pill" href="#overview" role="tab" aria-controls="pills-timeline" aria-selected="true">Overview</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link loadTasks" id="pills-profile-tab" data-toggle="pill" href="#tasks-list" role="tab" aria-controls="pills-profile" aria-selected="false">Tasks List</a>
                        <input type="hidden" data-fetch-route="{{ route('project-tasks', ['projectid' => $project->id]) }}" id="fetchTasks">
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#projects" role="tab" aria-controls="pills-profile" aria-selected="false">Notes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#invoices" role="tab" aria-controls="pills-profile" aria-selected="false">Invoices</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#payments" role="tab" aria-controls="pills-profile" aria-selected="false">Payments</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#expenses" role="tab" aria-controls="pills-profile" aria-selected="false">Expenses</a>
                    </li>
                </ul>
            </div>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="overview" role="tabpanel" aria-labelledby="pills-timeline-tab">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="card proj-progress-card">
                                        <div class="card-block">
                                            <h5 class="mb-30 fw-700">
                                                <span class="text-green ml-10 float-right" id="a-present-percent">0%</span></h5>
                                            <div class="progress">
                                                <div id="a-present-bar" class="progress-bar bg-red" style="width: 0"></div>
                                            </div>
                                            <p><strong>Stardate:</strong> {{ $project->start_date }}</p>
                                            <hr>
                                            <p><strong>Deadline:</strong> {{ $project->end_date }}</p>
                                            <hr>
                                            <p><strong>Price:</strong> {{ $project->price }}</p>
                                            <hr>
                                            <p><strong>Client:</strong> {{ $project->client->name }}</p>
                                            <hr>
                                            <p><strong>Phone:</strong> {{ $project->client->phone }}</p>
                                            <hr>
                                            <p><strong>Address:</strong> {{ $project->client->address }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-block">
                                            <canvas id="doughnut-chart" width="1" height="1"></canvas>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header d-block">
                                            <h3 class="float-left"> {{__('Project Members')}} </h3>
                                        </div>
                                        <div class="card-body">
                                            <div class="dt-responsive">
                                                <table id="projects" class="table nowrap">
                                                    <thead>
                                                    <tr>
                                                        <th>{{__('#')}}</th>
                                                        <th>{{__('Name')}}</th>
                                                        <th>{{__('Phone')}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($projectMembers as $projectMember)
                                                            <tr>
                                                                <td>{{ $loop->index + 1 }}</td>
                                                                <td>
                                                                    @if($projectMember->employee->photo)
                                                                        <img class="img-responsive employee-img-small" src="{{ asset('storage/hrm/photo', $projectMember->employee->photo) }}" alt="Employee Photo">
                                                                    @elseif($projectMember->employee->photo == null && $projectMember->employee->gender == 0)
                                                                        <img class="img-responsive employee-img-small" src="{{ asset('storage/crm/male-avatar.png') }}" alt="Employee Photo">
                                                                    @else
                                                                        <img class="img-responsive employee-img-small" src="{{ asset('storage/crm/other-avatar.png') }}" alt="Employee Photo">
                                                                    @endif
                                                                    - {{ $projectMember->employee->name ?? '' }}
                                                                </td>
                                                                <td>{{ $projectMember->employee->phone ?? '' }}</td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-block">
                                            <h5>Description:</h5>
                                            <hr>
                                            <h6>{{ $project->title }}</h6>
                                            <p>{{ $project->description }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card latest-update-card">
                                <div class="card-header">
                                    <h3>Latest Activity</h3>
                                    <div class="card-header-right">
                                        <ul class="list-unstyled card-option">
                                            <li><i class="ik ik-chevron-left action-toggle"></i></li>
                                            <li><i class="ik ik-minus minimize-card"></i></li>
                                            <li><i class="ik ik-x close-card"></i></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="scroll-widget">
                                        <div class="latest-update-box">
                                            <br>
                                            @foreach($tasks as $task)
                                            <div class="row pb-30">
                                                <div class="col-auto text-right update-meta pr-0">
                                                    <i class="b-success update-icon ring" style="border: 4px solid {{ $task->status->colorcode }}"></i>
                                                </div>
                                                <div class="col pl-5">
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <img class="img-responsive employee-img-small" src="http://127.0.0.1:8000/storage/crm/male-avatar.png" alt="Employee Photo">
                                                                </div>
                                                                <div class="col-sm-8">
                                                                    <p><strong>{{ $task->employee->name ?? '' }}</strong></p>
                                                                    <p class="mt-n1"><small class="text-muted">{{ date('d-M-Y h:i A', strtotime($task->updated_at)) }}</small></p>
                                                                    <p class="mt-n1">Status: <span class="activity-status-btn" style="background-color: {{ $task->status->colorcode }}">{{ $task->status->title }}</span></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-8">
                                                            <h6>Task: #{{ $task->id }} - {{ $task->title }}</h6>
                                                            <p class="text-muted mb-0">{{ $task->description }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!---------------client project tasks list------------------>
                <div class="tab-pane fade" id="tasks-list" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="card">
                        <div class="card-body">
                            <div class="dt-responsive">
                                <table id="tasksTable" class="table nowrap">
                                    <thead>
                                    <tr>
                                        <th>{{__('ID')}}</th>
                                        <th>{{__('Title')}}</th>
                                        <th>{{__('Assigned To')}}</th>
                                        <th>{{__('Start Date')}}</th>
                                        <th>{{__('Deadline')}}</th>
                                        <th>{{__('Status')}}</th>
                                        <th class="text-right"><i class="ik ik-align-justify"></i></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!---------------end of client project tasks list----------->

                <div class="tab-pane fade" id="projects" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="card-body">
                        <p>Projects</p>
                    </div>
                </div>
                <div class="tab-pane fade" id="invoices" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="card-body">
                        <p>Invoices</p>
                    </div>
                </div>
                <div class="tab-pane fade" id="payments" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="card-body">
                        <p>payments</p>
                    </div>
                </div>
                <div class="tab-pane fade" id="expenses" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="card-body">
                        <p>expenses</p>
                    </div>
                </div>                    <div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="pills-setting-tab">
                    <div class="card-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="example-name">Full Name</label>
                                <input type="text" placeholder="Johnathan Doe" class="form-control" name="example-name" id="example-name">
                            </div>
                            <div class="form-group">
                                <label for="example-email">Email</label>
                                <input type="email" placeholder="johnathan@admin.com" class="form-control" name="example-email" id="example-email">
                            </div>
                            <div class="form-group">
                                <label for="example-password">Password</label>
                                <input type="password" value="password" class="form-control" name="example-password" id="example-password">
                            </div>
                            <div class="form-group">
                                <label for="example-phone">Phone No</label>
                                <input type="text" placeholder="123 456 7890" id="example-phone" name="example-phone" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="example-message">Message</label>
                                <textarea name="example-message" name="example-message" rows="5" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="example-country">Select Country</label>
                                <select name="example-message" id="example-message" class="form-control">
                                    <option>London</option>
                                    <option>India</option>
                                    <option>Usa</option>
                                    <option>Canada</option>
                                    <option>Thailand</option>
                                </select>
                            </div>
                            <button class="btn btn-success" type="submit">Update Profile</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@endsection
@section('extraCSS')

@endsection
@section('extraJS')
<script src="{{asset('resources')}}/js/chart.min.js"></script>
<script>
    let tTable;
    var deptChart = new Chart($("#doughnut-chart"), {
        type: 'doughnut',
        responsive: true,
        data: {
            labels: ['To Do', 'Inprogress', 'Done', 'Testing'],
            datasets: [
                {
                    label: "{{__('Employee')}}",
                    backgroundColor: ['Red', 'Blue', 'Green', '#5d6d0d'],
                    data: [10, 20, 30, 40]
                }
            ]
        },
        options: {
            responsive: true,
            title: {
                display: false,
                text: '{{__("Top 5 Department based on Total Employee")}}'
            },
            legend: {
                display: true,
                position: 'bottom',
                labels: {
                    boxWidth: 10,
                    // fontColor: 'rgb(255, 99, 132)'
                }
            }
        }
    });
    $(".loadTasks").on("click", function(event) {
        tTable = $('#tasksTable').DataTable({
                processing: true,
                serverSide: true,
                orderable: false,
                ajax: $("#fetchTasks").attr('data-fetch-route'),
                deferRender: true,
                columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'title', name: 'title'},
                {data: 'employee.name', name: 'employee.name'},
                {data: 'startdate', name: 'startdate'},
                {data: 'deadline', name: 'deadline'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
        $(this).off(event);
        loadTaskStatuses();
    });
    function loadTaskStatuses(){
        $.ajax({
            url: '{{route('task-statuses.index')}}',
            type: 'GET',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function (response) {
                $('.taskstatusupdate').append($('<option>', {
                     value: '',
                     text: '-'
                }));
               $.each(response.data, function (i, taskstatus) {
                   $('.taskstatusupdate').append($('<option>', {
                       value: taskstatus.id,
                       text: taskstatus.title
                   }));
               });
            },
            error: function (data) {
                let msg = '';
                if (data.responseJSON.errors) {
                    $.each(data.responseJSON.errors, function (i, error) {
                        msg += '<p">' + error[0] + '</p>';
                    });
                } else {
                    msg = data.responseJSON.message;
                }
                showDangerToast(msg);
                console.log(data);
            }
        });
    }

    function changeTaskStatus(id)
    {
        let data = {
            id: id,
            status: $("#taskstatusupdate-"+ id).val(),
        }
        $.ajax({
            url: '{{route('update-task-status')}}',
            type: 'PUT',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            data: data,
            success: function (response) {
               tTable.draw(false);
               loadTaskStatuses();
            },
            error: function (data) {
                let msg = '';
                if (data.responseJSON.errors) {
                    $.each(data.responseJSON.errors, function (i, error) {
                        msg += '<p">' + error[0] + '</p>';
                    });
                } else {
                    msg = data.responseJSON.message;
                }
                showDangerToast(msg);
                console.log(data);
            }
        });
    }
</script>
@endsection