@extends('crm::layouts.master')
@section('title')
{{__('Home')}}
@endsection
@section('content')
  
<div class="container-fluid">
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-repeat bg-blue"></i>
                    <div class="d-inline">
                        <h5>{{__('Project Tasks List')}}</h5>
                        <span>{{__('Here is the list of client projects.')}}</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('crm')}}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">{{__('Project Tasks')}}</a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{__('List')}}</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header d-block">
                    <h3 class="float-left"> {{__('Project Tasks')}} </h3>
                    <button type="button" class="btn btn-success float-right" data-toggle="modal" data-target="#projectTaskModal"> + {{__('Add Task')}}
                    </button>
                </div>
                <div class="card-body">
                    <div class="dt-responsive">
                        <table id="tasksTable" class="table nowrap">
                            <thead>
                            <tr>
                                <th>{{__('ID')}}</th>
                                <th>{{__('Title')}}</th>
                                <th>{{__('Project')}}</th>
                                <th>{{__('Assigned To')}}</th>
                                <th>{{__('Start Date')}}</th>
                                <th>{{__('Deadline')}}</th>
                                <th>{{__('Status')}}</th>
                                <th class="text-right"><i class="ik ik-align-justify"></i></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Project Task Modal -->
<div class="modal animated zoomIn faster" id="projectTaskModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="Form" action="javascript:void(0)">
                <div class="modal-body">
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Title')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control" placeholder="{{__('title here')}}" name="title" id="title" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label" for="exampleTextarea1">{{__('Description')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <textarea class="form-control" id="exampleTextarea1" rows="4" placeholder="{{__('description here')}}.." name="description"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Project')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <select name="project" id="project" class="form-control select2" required>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Assign To')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <select name="assignto" id="assignto" class="form-control select2" required>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Status')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <select name="taskstatus" id="taskstatus" class="form-control select2 taskstatus" required>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Start Date')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control date" placeholder="{{__('start date')}}" name="startdate" id="startdate" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4 col-lg-4 col-form-label">{{__('Deadline')}}</label>
                        <div class="col-sm-8 col-lg-8">
                            <input type="text" class="form-control date" placeholder="{{__('deadline here')}}" name="deadline" id="deadline" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                    <button type="submit" class="btn btn-primary save-btn"></button>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('extraJS')
<script>
    var tTable;
    $(document).ready(function () {

        tTable = $('#tasksTable').DataTable({
            processing: true,
            serverSide: true,
            orderable: false,
            ajax: "{{ route('tasks.index') }}",
            deferRender: true,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'title', name: 'title'},
                {data: 'project.title', name: 'project.title'},
                {data: 'employee.name', name: 'employee.name'},
                {data: 'startdate', name: 'startdate'},
                {data: 'deadline', name: 'deadline'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        getProjectNameId();
        function getProjectNameId(){
            $.ajax({
                url: '{{route('project-name-id')}}',
                type: 'GET',
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                   $('#project').append($('<option>', {
                        value: '',
                        text: '-'
                    }));
                   $.each(response, function (i, project) {
                       $('#project').append($('<option>', {
                           value: project.id,
                           text: project.title
                       }));
                   });
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                    console.log(data);
                }
            });
        }

        getEmployeeNameId();
        function getEmployeeNameId()
        {
            $.ajax({
                url: '{{route('get-employee-name-id')}}',
                type: 'GET',
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                   $('#assignto').append($('<option>', {
                        value: '',
                        text: '-'
                    }));
                   $.each(response, function (i, employee) {
                       $('#assignto').append($('<option>', {
                           value: employee.id,
                           text: employee.name
                       }));
                   });
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                    console.log(data);
                }
            });            
        }
        getAvailableTaskStatus();
        function getAvailableTaskStatus(){
            $.ajax({
                url: '{{route('task-statuses.index')}}',
                type: 'GET',
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    $('#taskstatus').append($('<option>', {
                         value: '',
                         text: '-'
                    }));
                    $.each(response.data, function (i, taskstatus) {
                        $('#taskstatus').append($('<option>', {
                           value: taskstatus.id,
                           text: taskstatus.title
                        }));
                    });
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                    console.log(data);
                }
            });        
        }

        $('.modal').on('hidden.bs.modal', function (e) {
            $('#Form').trigger("reset");
        });

        /*--------------create and edit form start---------*/
        $('.modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var purpose = button.data('whatever'); // Extract info from data-* attributes
            var modal = $(this);
            
            if (purpose) {
                modal.find('.modal-title').text('{{__("Edit Task")}}');
                var id = button.data('id');
                $.ajax({
                    url: '{{route('tasks.index')}}/' + id,
                    type: 'GET',
                    data: {id},
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        modal.find('input[name=title]').val(response.title);
                        modal.find('textarea[name=description]').val(response.description);
                        modal.find('#project').val(response.project_id).trigger('change');
                        modal.find('#assignto').val(response.employee_id).trigger('change');
                        modal.find('#taskstatus').val(response.task_status_id).trigger('change');
                        modal.find('input[name=startdate]').val(response.startdate);
                        modal.find('input[name=deadline]').val(response.deadline);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                        console.log(data);
                    }
                });
                modal.find('button[type=submit]').removeClass('save-btn').addClass('update-btn').text('{{__("Update")}}').val(id);
            } else {
                modal.find('.modal-title').text('{{__("Add Task")}}');
                modal.find('button[type=submit]').removeClass('update-btn').addClass('save-btn').text('{{__("Save")}}');
            }
        });
        /*--------------create and edit form end---------------*/

        /*--------------saving project start--------------*/
        $(document).on('click', '.save-btn', function (e) {
            e.preventDefault();

            if ($('#title').val() == '') {
                alert("Title field required!");
                $("#title").focus();
                return false;
            }

            if ($('#project').val() == '') {
                alert("Please select a project!");
                return false;
            }
            if ($('#assignto').val() == '') {
                alert("Please select an employee!");
                return false;
            }
            if ($('#taskstatus').val() == '') {
                alert("Please select status!");
                return false;
            }
            if ($('#startdate').val() == '') {
                alert("Please select start date!");
                $("#startdate").focus();
                return false;
            }
            if ($('#deadline').val() == '') {
                alert("Please select deadline!");
                $("#deadline").focus();
                return false;
            }

            let data = $('#Form').serialize();
            $.ajax({
                url: '{{route('tasks.store')}}',
                type: 'POST',
                data: data,
                beforeSend: function (request) {
                    return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                },
                success: function (response) {
                    //console.log(response);
                    $('#projectTaskModal').modal('toggle');
                    //$('#Form').trigger("reset");
                    //$('#Form').find('input[type="select"]').val('');
                    showSuccessToast('{{__("Added Successfully")}}');
                    tTable.draw(false);
                    reloadTaskStatus();
                },
                error: function (data) {
                    let msg = '';
                    if (data.responseJSON.errors) {
                        $.each(data.responseJSON.errors, function (i, error) {
                            msg += '<p">' + error[0] + '</p>';
                        });
                    } else {
                        msg = data.responseJSON.message;
                    }
                    showDangerToast(msg);
                }
            });
        });
        /*--------------saving project end---------------*/

        /*--------------updating Item start--------------*/
        $(document).on('click', '.update-btn', function () {
            if ($('#title').val() == '') {
                alert("Title field required!");
                $("#title").focus();
                return false;
            }

            if ($('#project').val() == '') {
                alert("Please select a project!");
                return false;
            }
            if ($('#assignto').val() == '') {
                alert("Please select an employee!");
                return false;
            }
            if ($('#taskstatus').val() == '') {
                alert("Please select status!");
                return false;
            }
            if ($('#startdate').val() == '') {
                alert("Please select start date!");
                $("#startdate").focus();
                return false;
            }
            if ($('#deadline').val() == '') {
                alert("Please select deadline!");
                $("#deadline").focus();
                return false;
            }

            let data = $('#Form').serialize();
            let id = $(this).val();
            let cm = confirm('{{__("You are going to update! Are you sure?")}}');
            if (cm == true) {
                $.ajax({
                    url: "{{ route('tasks.index')}}/" + id,
                    type: 'PUT',
                    data: data,
                    beforeSend: function (request) {
                        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                    },
                    success: function (response) {
                        $('#projectTaskModal').modal('toggle');
                        $('#Form').trigger("reset");
                        showSuccessToast('{{__("Updated Successfully")}}');
                        tTable.draw(false);
                    },
                    error: function (data) {
                        let msg = '';
                        if (data.responseJSON.errors) {
                            $.each(data.responseJSON.errors, function (i, error) {
                                msg += '<p">' + error[0] + '</p>';
                            });
                        } else {
                            msg = data.responseJSON.message;
                        }
                        showDangerToast(msg);
                    }
                });
            }
            
        });
        /*--------------updating Item end---------------*/

        reloadTaskStatus();
    });


    reloadTaskStatus();
    function reloadTaskStatus(){
        $.ajax({
            url: '{{route('task-statuses.index')}}',
            type: 'GET',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            success: function (response) {
                $('.taskstatusupdate').append($('<option>', {
                     value: '',
                     text: '-'
                }));
               $.each(response.data, function (i, taskstatus) {
                   $('.taskstatusupdate').append($('<option>', {
                       value: taskstatus.id,
                       text: taskstatus.title
                   }));
               });
            },
            error: function (data) {
                let msg = '';
                if (data.responseJSON.errors) {
                    $.each(data.responseJSON.errors, function (i, error) {
                        msg += '<p">' + error[0] + '</p>';
                    });
                } else {
                    msg = data.responseJSON.message;
                }
                showDangerToast(msg);
                console.log(data);
            }
        });
    }
    function changeTaskStatus(id)
    {
        let data = {
            id: id,
            status: $("#taskstatusupdate-"+ id).val(),
        }
        $.ajax({
            url: '{{route('update-task-status')}}',
            type: 'PUT',
            beforeSend: function (request) {
                return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
            },
            data: data,
            success: function (response) {
               tTable.draw(false);
               reloadTaskStatus();
            },
            error: function (data) {
                let msg = '';
                if (data.responseJSON.errors) {
                    $.each(data.responseJSON.errors, function (i, error) {
                        msg += '<p">' + error[0] + '</p>';
                    });
                } else {
                    msg = data.responseJSON.message;
                }
                showDangerToast(msg);
                console.log(data);
            }
        });
    }

</script>
@endsection