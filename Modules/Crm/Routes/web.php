<?php

Route::prefix('crm')->group(function() {
    Route::get('/', 'CrmController@index')->name('crm');

    Route::get('get-employee-name-id', 'CrmController@getEmployeeNameId')->name('get-employee-name-id');

    Route::prefix('clients')->group(function () {
        Route::apiResources([
            'groups'  => 'ClientGroupsController',
            'clients' => 'ClientsController',
            'contacts' => 'ClientContactsController',
        ]);
        Route::get('client-groups', 'ClientsController@groups')->name('client-groups');
        Route::get('client-status-change', 'ClientsController@changeActivationStatus')->name('client-status-change');
        Route::get('client-name-id', 'ClientsController@getClientNameId')->name('client-name-id');
        Route::get('client-contact-edit', 'ClientContactsController@edit')->name('client-contact-edit');
        Route::post('client-contact-photo-update', 'ClientContactsController@updateProfilePhoto')->name('client-contact-photo-update');
    });
    Route::prefix('items')->group(function () {
        Route::apiResources([
            'unit-types'  => 'UnitTypesController',
            'items'  => 'ItemsController',
        ]);
        Route::get('items-id-name', 'ItemsController@getItemsIdName')->name('items-id-name');
    });
    Route::prefix('projects')->group(function () {
        Route::apiResources([
            'client-projects'  => 'ClientProjectsController',
            'porject-statues'  => 'ProjectStatusesController',
            'tasks'  => 'ProjectTasksController',
        ]);
        Route::get('project-details/{id}', 'ClientProjectsController@projectDetails')->name('project-details');
        Route::get('project-tasks/{projectid}', 'ClientProjectsController@projectTasks')->name('project-tasks');
        Route::get('project-name-id', 'ClientProjectsController@getProjectNameId')->name('project-name-id');
        Route::get('specific-client-project-name-id', 'ClientProjectsController@getSpecificClientProjectNameId')->name('specific-client-project-name-id');
        Route::get('client-project-id-name', 'ClientProjectsController@getClientProjectIdName')->name('client-project-id-name');
        Route::put('update-task-status', 'ProjectTasksController@updateTaskStatus')->name('update-task-status');
        Route::get('specific-client-projects', 'ClientProjectsController@specificClientProjects')->name('specific-client-projects');
    });
    Route::resource('invoices', 'InvoicesController');
    Route::resource('invoice-items', 'InvoiceItemsController');
    Route::get('specific-client-invoices', 'InvoicesController@specificClientInvoices')->name('specific-client-invoices');
    Route::resource('payments', 'PaymentsController');
    Route::get('estimate-requests', 'EstimatesController@estimateRequests')->name('estimate-requests');
    Route::get('estimate-request-show/{id}', 'EstimatesController@estimateRequestShow')->name('estimate-request-show');
    Route::put('estimate-request-status-update', 'EstimatesController@estimateRequestStatusUpdate')->name('estimate-request-status-update');
    Route::resource('estimates', 'EstimatesController');
    Route::get('estimate-details/{id}', 'EstimatesController@estimateDetails')->name('estimate-details');
    Route::put('estimate-mark-as-project', 'EstimatesController@markAsProject')->name('estimate-mark-as-project');
    Route::put('update-estimate', 'EstimatesController@updateEstimate')->name('update-estimate');
    Route::resource('estimate-items', 'EstimatedItemsController');
    Route::get('estimate-discount', 'EstimatedItemsController@estimateDiscount')->name('estimate-discount');
    Route::put('estimate-discount/{id}', 'EstimatedItemsController@estimateDiscountUpdate');
    Route::get('esitmate-pdf-generate/{id}', 'EstimatedItemsController@estimatePdfGenerate')->name('esitmate-pdf-generate');

    Route::get('get-invoice-to-pay', 'InvoicesController@getInvoiceToPay')->name('get-invoice-to-pay');
    Route::prefix('settings')->group(function () {
        Route::get('other-setup', 'SettingsController@OtherSetup')->name('other-setup');
        Route::apiResources([
            'payment-methods' => 'PaymentMethodsController',
            'task-statuses'  => 'TaskStatusesController',
            'taxes'  => 'TaxesController',
        ]);
    });
});
