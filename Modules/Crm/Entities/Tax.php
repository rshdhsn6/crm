<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;

class Tax extends Model
{
	protected $connection = 'crm';
	protected $fillable = ['title', 'percentage'];
}
