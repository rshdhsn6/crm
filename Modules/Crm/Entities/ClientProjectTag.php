<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;

class ClientProjectTag extends Model
{
	protected $connection = 'crm';
	protected $fillable = ['client_id', 'tag_id'];
}
