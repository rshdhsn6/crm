<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Crm\Entities\Item;

class EstimatedItem extends Model
{
	protected $connection = 'crm';
	protected $fillable = ['estimate_id', 'item_id', 'quantity', 'rate', 'discount', 'discount_type'];

	public function item()
	{
		return $this->belongsTo(Item::class, 'item_id', 'id');
	}

}
