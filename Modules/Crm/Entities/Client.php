<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
	protected $connection = 'crm';
    protected $fillable = ['client_group_id', 'name', 'phone', 'email', 'address', 'city', 'state', 'zip', 'country', 'website', 'vat_number'];

    public function clientgroup()
    {
        return $this->belongsTo(ClientGroup::class, 'client_group_id', 'id');
    }
    
    public function clientcontact()
    {
    	return $this->hasMany(ClientContact::class, 'client_id', 'id');
    }

    public function primarycontact()
    {
    	return $this->hasOne(ClientContact::class, 'client_id', 'id');
    }

    public function project()
    {
        return $this->hasMany(ClientProject::class, 'client_id', 'id');
    }
}
