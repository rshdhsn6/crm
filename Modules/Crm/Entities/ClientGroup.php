<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;

class ClientGroup extends Model
{
	protected $connection = 'crm';

    protected $fillable = ['name','desc'];
}
