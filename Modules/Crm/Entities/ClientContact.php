<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;

class ClientContact extends Model
{
	protected $connection = 'crm';
    protected $fillable = ['client_id', 'first_name', 'last_name', 'gender', 'job_title', 'phone', 'email', 'skype', 'whatsapp', 'password', 'is_primary_contact', 'is_login_access'];

    public function company()
    {
    	return $this->belongsTo(Client::class, 'client_id', 'id');
    }

}
