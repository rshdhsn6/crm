<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Hrm\Entities\Employee;

class ProjectTask extends Model
{
	protected $connection = 'crm';
	protected $fillable = ['title', 'description', 'project_id', 'employee_id', 'task_status_id', 'startdate', 'dateline'];

	public function project()
	{
		return $this->belongsTo(ClientProject::class, 'project_id', 'id');
	}

	public function employee()
	{
		return $this->belongsTo(Employee::class, 'employee_id', 'id');
	}

	public function status()
	{
		return $this->belongsTo(TaskStatus::class, 'task_status_id', 'id');
	}
}