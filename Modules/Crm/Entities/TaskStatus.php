<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;

class TaskStatus extends Model
{
    	protected $connection = 'crm';
        protected $fillable = ['title', 'colorcode'];
}