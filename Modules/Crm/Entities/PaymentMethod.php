<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
		protected $connection = 'crm';
	    protected $fillable = ['title', 'status'];
}
