<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;

class UnitType extends Model
{
    protected $connection = 'crm';
    protected $fillable = ['name','description', 'status'];
}
