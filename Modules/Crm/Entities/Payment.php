<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
	protected $connection = 'crm';
	protected $fillable = ['invoice_id', 'payment_method_id', 'amount', 'paymentdate'];

	public function paymentmethod()
	{
		return $this->belongsTo(PaymentMethod::class, 'payment_method_id', 'id');
	}
}
