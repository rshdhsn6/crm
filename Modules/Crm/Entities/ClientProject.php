<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;

class ClientProject extends Model
{
	protected $connection = 'crm';
	protected $fillable = ['title', 'client_id', 'description', 'start_date', 'end_date'];

	public function client()
	{
		return $this->belongsTo(Client::class, 'client_id', 'id');
	}

	public function status()
	{
		return $this->hasOne(ProjectStatus::class, 'id', 'project_status_id');
	}
}
