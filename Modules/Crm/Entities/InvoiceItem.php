<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
	protected $connection = 'crm';
	protected $fillable = ['invoice_id', 'item_id', 'quantity', 'rate', 'discount', 'discount_type'];
}
