<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Modules\Crm\Entities\Client;
use Modules\Crm\Entities\ClientProject;
use Modules\Crm\Entities\EstimatedItem;
use Modules\Crm\Entities\Item;

class Estimate extends Model
{
	protected $connection = 'crm';
	protected $fillable = ['client_id', 'description', 'budget', 'example_link', 'status', 'discount', 'discount_type'];

	public function client()
	{
		return $this->belongsTo(Client::class, 'client_id', 'id');
	}

	public function estimateditems()
	{
		return $this->hasMany(EstimatedItem::class, 'estimate_id', 'id');
	}


	public function getRowTotalPercentage() {
	    return $this->estimateditems()->where('discount_type', 1)->sum(DB::raw('(quantity * rate) - ((rate * discount)/100)'));
	}

	public function getRowTotalFixedAmount() {
	    return $this->estimateditems()->where('discount_type', 2)->sum(DB::raw('(quantity * rate) - (quantity * discount)'));
	}

	public function project(){
		return $this->hasOne(ClientProject::class, 'estimate_id', 'id');
	}
}