<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
	protected $connection = 'crm';
	protected $fillable = ['client_id', 'project_id', 'billdate', 'duedate', 'invoice_value', 'payment_received', 'discount', 'note', 'status'];

	public function client()
	{
		return $this->belongsTo(Client::class, 'client_id', 'id');
	}

	public function project()
	{
		return $this->belongsTo(ClientProject::class, 'project_id', 'id');
	}

	public function payments()
	{
		return $this->hasMany(Payment::class, 'invoice_id', 'id');
	}
}