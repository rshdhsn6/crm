<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Hrm\Entities\Employee;

class ProjectMember extends Model
{
	protected $connection = 'crm';
	protected $fillable = ['project_id', 'client_id'];

	public function employee()
	{
		return $this->belongsTo(Employee::class);
	}
}
