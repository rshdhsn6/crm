<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;

class ProjectStatus extends Model
{
	protected $connection = 'crm';
	protected $fillable = ['title'];
}
