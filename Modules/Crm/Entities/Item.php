<?php

namespace Modules\Crm\Entities;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
	protected $connection = 'crm';
    protected $fillable = ['name', 'description', 'unit_type_id', 'rate', 'discount', 'discount_type'];

    public function unittype()
    {
    	return $this->belongsTo(UnitType::class, 'unit_type_id', 'id');
    }
}